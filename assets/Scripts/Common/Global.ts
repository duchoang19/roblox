interface Global {
    touchPos: cc.Vec2,
    boolEnableTouch: boolean,
    boolFirstTouchJoyStick: boolean,
    boolStartPlay: boolean,
    boolStartAttacking: boolean,
    boolCheckAttacking: boolean,
    boolCheckAttacked: boolean,
    boolendG: boolean,
    soundBG: cc.AudioClip,
    soundIntro: cc.AudioClip,
    soundAttack: cc.AudioClip,
    soundFootStep: cc.AudioClip,
    soundKatana: cc.AudioClip,
    soundScream: cc.AudioClip,
    soundSpin: cc.AudioClip,
    soundReward: cc.AudioClip,
    soundClickBtn: cc.AudioClip
}
let Global: Global = {
    touchPos: null,
    boolEnableTouch: false,
    boolFirstTouchJoyStick: false,
    boolStartPlay: false,
    boolStartAttacking: false,
    boolCheckAttacking: false,
    boolCheckAttacked: false,
    boolendG: false,
    soundBG: null,
    soundIntro: null,
    soundAttack: null,
    soundFootStep: null,
    soundKatana: null,
    soundScream: null,
    soundSpin: null,
    soundReward: null,
    soundClickBtn: null
};
export default Global;