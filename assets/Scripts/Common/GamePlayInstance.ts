
import StickMan1 from "../GamePlay/StickMan1";
import Singleton from "./Singleton";

const { ccclass, property } = cc._decorator;
export const instance = new cc.EventTarget();
@ccclass
export default class GamePlayInstance extends Singleton<GamePlayInstance> {
    gameplay: StickMan1 = StickMan1.Instance(StickMan1);
    constructor() {
        super();
        GamePlayInstance._instance = this;
    }
}
