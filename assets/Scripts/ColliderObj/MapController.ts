
import SquareCollider from "./SquareCollider";

const {ccclass, property} = cc._decorator;

@ccclass
export default class MapController extends cc.Component {

    // @property(ObjectCollider)
    // public ListCircleObjects: ObjectCollider[] = [];
    @property(SquareCollider)
    public ListSquareObjects: SquareCollider[] = [];
}
