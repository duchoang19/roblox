
import { instance } from "../Common/GamePlayInstance";
import Global from "../Common/Global";
import KeyEvent from "../Common/KeyEvent";
import Singleton from "../Common/Singleton";
import EnemyController from "../Enemy/EnemyController";
import StickMan1 from "./StickMan1";

const { ccclass, property } = cc._decorator;

declare const window: any;

@ccclass
export default class StickMan2 extends Singleton<StickMan1> {

    @property(cc.Node)
    joyStickFollow: cc.Node = null;

    @property(cc.Node)
    nEndGame: cc.Node = null;

    @property(cc.Node)
    Map: cc.Node = null;

    @property(cc.Integer)
    countEnemyEnd: number = 0;

    @property(cc.Node)
    MyCharacter: cc.Node = null;

    @property([cc.Material])
    characterMaterial: cc.Material[] = [];

    @property([cc.Node])
    katana: cc.Node[] = [];

    @property(cc.Node)
    Guide: cc.Node = null;

    @property(cc.Node)
    Effect: cc.Node = null;

    @property(cc.Node)
    txtSmasher: cc.Node = null;

    @property(cc.Node)
    enemyParent: cc.Node = null;

    @property(cc.Node)
    btnDownload: cc.Node = null;

    @property(cc.Node)
    nStart: cc.Node = null;

    countEnemy: number = 0;
    chooseDoneDeadpool: boolean = false;
    chooseDoneIronMan: boolean = false;

    boolcheckInteraction: boolean = false;
    ironsource: boolean = false;
    mindworks: boolean = false;
    vungle: boolean = false;

    constructor() {
        super();
        StickMan1._instance = this;
    }

    onEnable() {
        instance.on(KeyEvent.scale, this.scale, this);
        instance.on(KeyEvent.plusEnemy, this.plusEnemy, this);
        instance.on(KeyEvent.activeGuide, this.activeGuide, this);
    }

    onDisable() {
        instance.off(KeyEvent.scale, this.scale, this);
        instance.off(KeyEvent.plusEnemy, this.plusEnemy, this);
        instance.off(KeyEvent.activeGuide, this.activeGuide, this);
    }

    start() {
        this.Guide.active = false;
        this.enemyParent.active = false;
    }

    chooseDeadpool() {
        this.MyCharacter.getChildByName('stickman_new').getComponent(cc.MeshRenderer).setMaterial(0, this.characterMaterial[0]);
        this.katana[0].active = true;
        this.chooseDone();
    }

    chooseIronMan() {
        this.MyCharacter.getChildByName('stickman_new').getComponent(cc.MeshRenderer).setMaterial(0, this.characterMaterial[1]);
        this.katana[1].active = true;
        this.chooseDone();
    }

    chooseDone() {
        if (this.ironsource) {
            window.NUC.trigger.interaction();
        }
        this.MyCharacter.active = true;
        this.nStart.active = false;
        this.Guide.active = true;
        this.enemyParent.active = true;
        this.playGame();
    }

    update(dt) {
        if (this.countEnemy == this.countEnemyEnd) {
            this.endGame();
        }
    }

    playGame() {
        Global.boolStartPlay = true;
        for (let i = 0; i < this.enemyParent.childrenCount; i++) {
            this.enemyParent.children[i].getComponent(EnemyController).StartMove();
        }
    }

    endGame() {
        if (this.mindworks) {
            window.gameEnd && window.gameEnd();
        }
        if (this.ironsource) {
            window.NUC.trigger.endGame('win')
        }
        if (this.vungle) {
            parent.postMessage('complete', '*');
        }
        this.scheduleOnce(() => {
            this.MyCharacter.active = false;
            this.nEndGame.active = true;
        }, 1.3);
        this.joyStickFollow.active = false;
        this.Guide.active = false;
    }

    scale() {
        this.Effect.scale = 0;
        this.Effect.opacity = 255;
        this.Effect.runAction(cc.sequence(cc.scaleTo(0.2, 1).easing(cc.easeBounceOut()), cc.callFunc(() => {
            this.scheduleOnce(() => {
                this.Effect.runAction(cc.fadeOut(0.1));
            }, 0.2);
        })));
    }

    plusEnemy() {
        if (this.countEnemy < this.countEnemyEnd)
            this.countEnemy++;
    }

    activeGuide() {
        if (Global.boolStartPlay)
            this.Guide.active = true;
    }
}
