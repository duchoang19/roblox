import CharacterController from "../Character/CharacterController";
import { instance } from "../Common/GamePlayInstance";
import Global from "../Common/Global";
import KeyEvent from "../Common/KeyEvent";
import Singleton from "../Common/Singleton";
import EnemyController from "../Enemy/EnemyController";


const {ccclass, property} = cc._decorator;

declare const window: any;

@ccclass
export default class StickMan1 extends Singleton<StickMan1> {

    @property(cc.Node)
    joyStickFollow: cc.Node = null;

    @property(cc.Node)
    Map: cc.Node = null;

    @property(cc.Integer)
    countEnemyEnd: number = 0;

    @property(cc.Node)
    MyCharacter: cc.Node = null;

    @property(cc.Node)
    Guide: cc.Node = null;

    @property(cc.Node)
    Effect: cc.Node = null;

    @property(cc.Node)
    enemyParent: cc.Node = null;

    @property(cc.Node)
    btnDownload: cc.Node = null;

    @property(cc.Node)
    nStart: cc.Node = null;

    @property(cc.Node)
    endCard: cc.Node = null;

    @property(cc.Node)
    nightEnd: cc.Node = null;

    @property(cc.Node)
    circleEnd: cc.Node = null;

    @property(cc.Node)
    btnAll: cc.Node = null;

    countEnemy: number = 0;
    boolCheckEnd: boolean = false;

    boolcheckInteraction: boolean = false;
    ironsource: boolean = false;
    mindworks: boolean = false;
    vungle: boolean = false;

    constructor() {
        super();
        StickMan1._instance = this;
    }

    onEnable() {
        instance.on(KeyEvent.scale, this.scale, this);
        instance.on(KeyEvent.plusEnemy, this.plusEnemy, this);
        instance.on(KeyEvent.activeGuide, this.activeGuide, this);
    }

    onDisable() {
        instance.off(KeyEvent.scale, this.scale, this);
        instance.off(KeyEvent.plusEnemy, this.plusEnemy, this);
        instance.off(KeyEvent.activeGuide, this.activeGuide, this);
    }

    start() {
        this.Guide.active = false;
        this.MyCharacter.active = false;
        this.enemyParent.active = false;
    }

    onClickBtnPlay() {
        if (this.ironsource) {
            window.NUC.trigger.interaction();
        }
        this.nStart.active = false;
        this.Guide.active = true;
        this.MyCharacter.active = true;
        this.enemyParent.active = true;
        this.playGame();
    }

    update (dt) {
        if (this.countEnemy == this.countEnemyEnd && !this.boolCheckEnd) {
            Global.boolStartPlay = false;
            this.boolCheckEnd = true;
            this.scheduleOnce(() => {
                this.SupportEnd();
            }, 1);
        }
    }

    playGame() {
        Global.boolStartPlay = true;
        for(let i = 0; i < this.enemyParent.childrenCount; i++) {
            this.enemyParent.children[i].getComponent(EnemyController).StartMove();
        }
    }

    SupportEnd() {
        this.scheduleOnce(() => {
            this.nightEnd.active = true;
            this.scheduleOnce(() => {
                this.circleEnd.active = true;
                this.scheduleOnce(() => {
                    this.nightEnd.active = false;
                    this.circleEnd.active = false;
                    this.MyCharacter.opacity = 0;
                    this.EndGame();
                }, 0.6);
            }, 0.4);
            this.circleEnd.active = true;
        }, 0.2);
    }

    EndGame() {
        Global.boolendG = true;
        this.joyStickFollow.active = false;
        this.endCard.active = true;
        this.Map.active = true;
        this.endCard.children[2].active = true;
        this.endCard.children[3].active = true;
        this.endCard.children[4].active = true;
        this.MyCharacter.opacity = 255;
        this.MyCharacter.x = -3;
        this.MyCharacter.y = -60;
        this.MyCharacter.scale = 350;
        this.MyCharacter.eulerAngles = new cc.Vec3(90, 0, 180);
        this.MyCharacter.getComponent(CharacterController).ArrowDirection.active = false;
        this.Guide.active = true;
        for (let i = 0; i < this.enemyParent.childrenCount; i++) {
            this.enemyParent.children[i].getComponent(EnemyController).bodyDeath.active = false;
            this.enemyParent.children[i].active = false;;
        }
        this.btnAll.active = true;
        this.btnDownload.getComponent(cc.Button).interactable = false;
        if (this.mindworks) {
            window.gameEnd && window.gameEnd();
        }
        if (this.ironsource) {
            window.NUC.trigger.endGame('win')
        }
        if (this.vungle) {
            parent.postMessage('complete', '*');
        }
    }

    scale() {
        this.Effect.scale = 0;
        this.Effect.opacity = 255;
        this.Effect.runAction(cc.sequence(cc.scaleTo(0.2, 1).easing(cc.easeBounceOut()), cc.callFunc(() => {
            this.scheduleOnce(() => {
                this.Effect.runAction(cc.fadeOut(0.1));
            }, 0.2);
        })));
    }

    plusEnemy() {
        if (this.countEnemy < this.countEnemyEnd)
            this.countEnemy++;
    }

    activeGuide() {
        if(Global.boolStartPlay)
            this.Guide.active = true;
    }
}
