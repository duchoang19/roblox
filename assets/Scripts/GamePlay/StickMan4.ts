import CharacterController from "../Character/CharacterController";
import { instance } from "../Common/GamePlayInstance";
import Global from "../Common/Global";
import KeyEvent from "../Common/KeyEvent";
import Singleton from "../Common/Singleton";
import EnemyController from "../Enemy/EnemyController";
import StickMan1 from "./StickMan1";

const {ccclass, property} = cc._decorator;

declare const window: any;

@ccclass
export default class StickMan4 extends Singleton<StickMan1> {

    @property(cc.Node)
    joyStickFollow: cc.Node = null;

    @property(cc.Node)
    Map: cc.Node = null;

    @property(cc.Integer)
    countEnemyEnd: number = 0;

    @property(cc.Node)
    MyCharacter: cc.Node = null;

    @property(cc.Node)
    Guide: cc.Node = null;

    @property(cc.Node)
    Effect: cc.Node = null;

    @property(cc.Node)
    enemyParent: cc.Node = null;

    @property(cc.Node)
    btnDownload: cc.Node = null;

    @property(cc.Node)
    endCard: cc.Node = null;

    @property(cc.Node)
    txtSmasher: cc.Node = null;

    countEnemy: number = 0;
    boolCheckEnd: boolean = false;

    boolcheckInteraction: boolean = false;
    ironsource: boolean = false;
    mindworks: boolean = false;
    vungle: boolean = false;

    constructor() {
        super();
        StickMan1._instance = this;
    }

    onEnable() {
        instance.on(KeyEvent.scale, this.scale, this);
        instance.on(KeyEvent.plusEnemy, this.plusEnemy, this);
        instance.on(KeyEvent.activeGuide, this.activeGuide, this);
    }

    onDisable() {
        instance.off(KeyEvent.scale, this.scale, this);
        instance.off(KeyEvent.plusEnemy, this.plusEnemy, this);
        instance.off(KeyEvent.activeGuide, this.activeGuide, this);
    }

    start() {
        cc.audioEngine.play(Global.soundBG, true, 1);
        this.playGame();
    }

    update (dt) {
        if (Global.boolEnableTouch && !this.boolcheckInteraction) {
            this.txtSmasher.active = false;
            if (this.ironsource) {
                window.NUC.trigger.interaction();
            }
        }

        if (this.countEnemy == this.countEnemyEnd && !this.boolCheckEnd) {
            Global.boolStartPlay = false;
            this.boolCheckEnd = true;
            this.scheduleOnce(() => {
                this.EndGame();
            }, 1);
        }
    }

    playGame() {
        this.MyCharacter.active = true;
        Global.boolStartPlay = true;
        for(let i = 0; i < this.enemyParent.childrenCount; i++) {
            this.enemyParent.children[i].getComponent(EnemyController).StartMove();
        }
    }

    EndGame() {
        Global.boolendG = true;
        cc.audioEngine.stopAllEffects();
        this.joyStickFollow.active = false;
        this.Guide.active = false;
        this.endCard.active = true;
        if (this.mindworks) {
            window.gameEnd && window.gameEnd();
        }
        if (this.ironsource) {
            window.NUC.trigger.endGame('win')
        }
        if (this.vungle) {
            parent.postMessage('complete', '*');
        }
    }

    scale() {
        this.Effect.scale = 0;
        this.Effect.opacity = 255;
        this.Effect.runAction(cc.sequence(cc.scaleTo(0.2, 1).easing(cc.easeBounceOut()), cc.callFunc(() => {
            this.scheduleOnce(() => {
                this.Effect.runAction(cc.fadeOut(0.1));
            }, 0.2);
        })));
    }

    plusEnemy() {
        if (this.countEnemy < this.countEnemyEnd)
            this.countEnemy++;
    }

    activeGuide() {
        if(Global.boolStartPlay)
            this.Guide.active = true;
    }
}
