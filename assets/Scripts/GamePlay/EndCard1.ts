
const {ccclass, property} = cc._decorator;

declare const window: any;

@ccclass
export default class EndCard1 extends cc.Component {

    ironsource: boolean = false;
    mindworks: boolean = false;
    vungle: boolean = false;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        if (this.mindworks) {
            window.gameEnd && window.gameEnd();
        }
        if (this.ironsource) {
            window.NUC.trigger.endGame('win')
        }
        if (this.vungle) {
            parent.postMessage('complete', '*');
        }
    }

    // update (dt) {}
}
