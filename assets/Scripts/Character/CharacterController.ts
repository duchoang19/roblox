import MapController from "../ColliderObj/MapController";
import { ActionType } from "../Common/EnumDefine";
import GamePlayInstance, { instance } from "../Common/GamePlayInstance";
import Global from "../Common/Global";
import KeyEvent from "../Common/KeyEvent";

const { ccclass, property } = cc._decorator;

@ccclass
export default class CharacterController extends cc.Component {

    @property(cc.Node)
    ArrowDirection: cc.Node = null;

    @property(cc.Node)
    JoystickFollow: cc.Node = null;

    @property({ type: cc.Enum(ActionType) })
    actionType: ActionType = ActionType.IDLE;

    @property(cc.Integer)
    speed: number = 100;

    @property(cc.Node)
    Weapon: cc.Node = null;

    @property(cc.Integer)
    timeAnim: number = 0;

    @property(cc.Node)
    placeWeapon: cc.Node = null;

    @property(cc.Prefab)
    effectSmoke: cc.Prefab = null;

    @property(cc.Integer)
    clampLeft: number = 0;

    @property(cc.Integer)
    clampRight: number = 0;

    @property(cc.Integer)
    clampBottom: number = 0;

    @property(cc.Integer)
    clampTop: number = 0;

    @property(cc.Integer)
    level: number = 0;

    @property(cc.Integer)
    scale: number = 0;

    // @property(MapController)
    // mapController: MapController = null;

    originalSpeed: number = 0;

    gameplayInstance: GamePlayInstance = null;
    boolPlaySoundFoot: boolean = false;

    // onLoad () {}

    start() {
        this.gameplayInstance = GamePlayInstance.Instance(GamePlayInstance);
        Global.touchPos = cc.v2(0, 0);
        this.originalSpeed = this.speed;
    }

    update() {
        if (Global.boolEnableTouch && !Global.boolStartAttacking) {
            this.node.x = cc.misc.clampf(this.node.x, this.clampLeft, this.clampRight);
            this.node.y = cc.misc.clampf(this.node.y, this.clampBottom, this.clampTop);
            // let radius = Math.sqrt(this.clampRight * this.clampRight - this.node.x * this.node.x);
            // this.node.x = cc.misc.clampf(this.node.x, -radius, radius);
            // this.node.y = cc.misc.clampf(this.node.y, -radius, radius);  

            // this.mapController.ListSquareObjects.forEach(element => {
            //     if(element.OnTheLeft(this.node))
            //         {            
            //             this.node.x = cc.misc.clampf(this.node.x, this.clampLeft, element.positionLeft.x - 1);
            //             this.node.y = cc.misc.clampf(this.node.y, this.clampBottom, this.clampTop);
            //         }
            //     else if(element.OnTheRight(this.node))
            //         {
            //             this.node.x = cc.misc.clampf(this.node.x, element.positionRight.x + 1, this.clampRight);
            //             this.node.y = cc.misc.clampf(this.node.y, this.clampBottom, this.clampTop);
            //         }
            //     else if(element.OnTheTop(this.node))
            //         {
            //             this.node.x = cc.misc.clampf(this.node.x, this.clampLeft, this.clampRight);
            //             this.node.y = cc.misc.clampf(this.node.y, element.positionTop.y + 1, this.clampTop);
            //         }
            //     else if(element.OnTheBottom(this.node))
            //         {
            //             this.node.x = cc.misc.clampf(this.node.x, this.clampLeft, this.clampRight);
            //             this.node.y = cc.misc.clampf(this.node.y, this.clampBottom,  element.positionBottom.y - 1);
            //         }
            //         else
            //         {
            //             this.node.x = cc.misc.clampf(this.node.x, this.clampLeft, this.clampRight);
            //             this.node.y = cc.misc.clampf(this.node.y, this.clampBottom, this.clampTop);
            //         }
            // });

            let PosForX = this.node.getPosition();
            let PosForY = this.node.getPosition();
            PosForX.addSelf(Global.touchPos.mul(this.speed / 100));
            PosForY.addSelf(Global.touchPos.mul(this.speed / 100));
            this.node.x = PosForX.x;
            this.node.y = PosForY.y;
            if (!this.boolPlaySoundFoot) {
                this.boolPlaySoundFoot = true;
                cc.audioEngine.playEffect(Global.soundFootStep, false);
                this.scheduleOnce(() => {
                    this.boolPlaySoundFoot = false;
                }, 0.3);
            }
            var r = Math.atan2(Global.touchPos.y, Global.touchPos.x);
            var degree = r * 180 / (Math.PI);
            degree = 360 - degree + 90;
            this.node.is3DNode = true;
            this.node.eulerAngles = new cc.Vec3(-90, 180, degree);
        }
    }

    Attacking() {
        Global.boolStartAttacking = true;
        Global.boolCheckAttacking = false;

        if (this.node.name == "MyDeadpool") {
            this.node.getComponent(cc.SkeletonAnimation).play("Armature|sword_attack");
        }
        else {
            this.node.getComponent(cc.SkeletonAnimation).play("Armature|hammer_attack");
        }

        this.scheduleOnce(() => {
            Global.boolCheckAttacking = true;
            Global.boolCheckAttacked = true;
            this.spawnEffectSmoke(this.effectSmoke);
            // cc.audioEngine.playEffect(Global.soundAttack, false);
            cc.audioEngine.playEffect(Global.soundKatana, false);
        }, 0.5);

        this.scheduleOnce(() => {
            instance.emit(KeyEvent.activeGuide);
            Global.boolStartAttacking = false;
            if (this.node.name == "MyDeadpool") {
                this.node.getComponent(cc.SkeletonAnimation).play("Armature|sword_idle");
            }
            else {
                this.node.getComponent(cc.SkeletonAnimation).play("Armature|hammer_idle");
            }
        }, this.timeAnim);
    }

    LevelUpPlayer() {
        var tween = new cc.Tween().to(0.4, { scale: this.node.scale + this.scale });
        tween.target(this.node).start();
        if (this.level < 4)
            this.level++;
    }

    spawnEffectSmoke(smoke: cc.Prefab) {
        let smk = cc.instantiate(smoke);
        smk.parent = cc.Canvas.instance.node;
        let pos = this.node.convertToWorldSpaceAR(this.placeWeapon.getPosition());
        pos = cc.Canvas.instance.node.convertToNodeSpaceAR(pos);
        smk.x = pos.x;
        smk.y = pos.y;
        smk.z = 0;
    }
}
