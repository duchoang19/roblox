"use strict";
cc._RF.push(module, '8c229TU8nVEg7patjhCarLi', 'CharacterController');
// Scripts/Character/CharacterController.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var EnumDefine_1 = require("../Common/EnumDefine");
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var KeyEvent_1 = require("../Common/KeyEvent");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var CharacterController = /** @class */ (function (_super) {
    __extends(CharacterController, _super);
    function CharacterController() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.ArrowDirection = null;
        _this.JoystickFollow = null;
        _this.actionType = EnumDefine_1.ActionType.IDLE;
        _this.speed = 100;
        _this.Weapon = null;
        _this.timeAnim = 0;
        _this.placeWeapon = null;
        _this.effectSmoke = null;
        _this.clampLeft = 0;
        _this.clampRight = 0;
        _this.clampBottom = 0;
        _this.clampTop = 0;
        _this.level = 0;
        _this.scale = 0;
        // @property(MapController)
        // mapController: MapController = null;
        _this.originalSpeed = 0;
        _this.gameplayInstance = null;
        _this.boolPlaySoundFoot = false;
        return _this;
    }
    // onLoad () {}
    CharacterController.prototype.start = function () {
        this.gameplayInstance = GamePlayInstance_1.default.Instance(GamePlayInstance_1.default);
        Global_1.default.touchPos = cc.v2(0, 0);
        this.originalSpeed = this.speed;
    };
    CharacterController.prototype.update = function () {
        var _this = this;
        if (Global_1.default.boolEnableTouch && !Global_1.default.boolStartAttacking) {
            this.node.x = cc.misc.clampf(this.node.x, this.clampLeft, this.clampRight);
            this.node.y = cc.misc.clampf(this.node.y, this.clampBottom, this.clampTop);
            // let radius = Math.sqrt(this.clampRight * this.clampRight - this.node.x * this.node.x);
            // this.node.x = cc.misc.clampf(this.node.x, -radius, radius);
            // this.node.y = cc.misc.clampf(this.node.y, -radius, radius);  
            // this.mapController.ListSquareObjects.forEach(element => {
            //     if(element.OnTheLeft(this.node))
            //         {            
            //             this.node.x = cc.misc.clampf(this.node.x, this.clampLeft, element.positionLeft.x - 1);
            //             this.node.y = cc.misc.clampf(this.node.y, this.clampBottom, this.clampTop);
            //         }
            //     else if(element.OnTheRight(this.node))
            //         {
            //             this.node.x = cc.misc.clampf(this.node.x, element.positionRight.x + 1, this.clampRight);
            //             this.node.y = cc.misc.clampf(this.node.y, this.clampBottom, this.clampTop);
            //         }
            //     else if(element.OnTheTop(this.node))
            //         {
            //             this.node.x = cc.misc.clampf(this.node.x, this.clampLeft, this.clampRight);
            //             this.node.y = cc.misc.clampf(this.node.y, element.positionTop.y + 1, this.clampTop);
            //         }
            //     else if(element.OnTheBottom(this.node))
            //         {
            //             this.node.x = cc.misc.clampf(this.node.x, this.clampLeft, this.clampRight);
            //             this.node.y = cc.misc.clampf(this.node.y, this.clampBottom,  element.positionBottom.y - 1);
            //         }
            //         else
            //         {
            //             this.node.x = cc.misc.clampf(this.node.x, this.clampLeft, this.clampRight);
            //             this.node.y = cc.misc.clampf(this.node.y, this.clampBottom, this.clampTop);
            //         }
            // });
            var PosForX = this.node.getPosition();
            var PosForY = this.node.getPosition();
            PosForX.addSelf(Global_1.default.touchPos.mul(this.speed / 100));
            PosForY.addSelf(Global_1.default.touchPos.mul(this.speed / 100));
            this.node.x = PosForX.x;
            this.node.y = PosForY.y;
            if (!this.boolPlaySoundFoot) {
                this.boolPlaySoundFoot = true;
                cc.audioEngine.playEffect(Global_1.default.soundFootStep, false);
                this.scheduleOnce(function () {
                    _this.boolPlaySoundFoot = false;
                }, 0.3);
            }
            var r = Math.atan2(Global_1.default.touchPos.y, Global_1.default.touchPos.x);
            var degree = r * 180 / (Math.PI);
            degree = 360 - degree + 90;
            this.node.is3DNode = true;
            this.node.eulerAngles = new cc.Vec3(-90, 180, degree);
        }
    };
    CharacterController.prototype.Attacking = function () {
        var _this = this;
        Global_1.default.boolStartAttacking = true;
        Global_1.default.boolCheckAttacking = false;
        if (this.node.name == "MyDeadpool") {
            this.node.getComponent(cc.SkeletonAnimation).play("Armature|sword_attack");
        }
        else {
            this.node.getComponent(cc.SkeletonAnimation).play("Armature|hammer_attack");
        }
        this.scheduleOnce(function () {
            Global_1.default.boolCheckAttacking = true;
            Global_1.default.boolCheckAttacked = true;
            _this.spawnEffectSmoke(_this.effectSmoke);
            // cc.audioEngine.playEffect(Global.soundAttack, false);
            cc.audioEngine.playEffect(Global_1.default.soundKatana, false);
        }, 0.5);
        this.scheduleOnce(function () {
            GamePlayInstance_1.instance.emit(KeyEvent_1.default.activeGuide);
            Global_1.default.boolStartAttacking = false;
            if (_this.node.name == "MyDeadpool") {
                _this.node.getComponent(cc.SkeletonAnimation).play("Armature|sword_idle");
            }
            else {
                _this.node.getComponent(cc.SkeletonAnimation).play("Armature|hammer_idle");
            }
        }, this.timeAnim);
    };
    CharacterController.prototype.LevelUpPlayer = function () {
        var tween = new cc.Tween().to(0.4, { scale: this.node.scale + this.scale });
        tween.target(this.node).start();
        if (this.level < 4)
            this.level++;
    };
    CharacterController.prototype.spawnEffectSmoke = function (smoke) {
        var smk = cc.instantiate(smoke);
        smk.parent = cc.Canvas.instance.node;
        var pos = this.node.convertToWorldSpaceAR(this.placeWeapon.getPosition());
        pos = cc.Canvas.instance.node.convertToNodeSpaceAR(pos);
        smk.x = pos.x;
        smk.y = pos.y;
        smk.z = 0;
    };
    __decorate([
        property(cc.Node)
    ], CharacterController.prototype, "ArrowDirection", void 0);
    __decorate([
        property(cc.Node)
    ], CharacterController.prototype, "JoystickFollow", void 0);
    __decorate([
        property({ type: cc.Enum(EnumDefine_1.ActionType) })
    ], CharacterController.prototype, "actionType", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "speed", void 0);
    __decorate([
        property(cc.Node)
    ], CharacterController.prototype, "Weapon", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "timeAnim", void 0);
    __decorate([
        property(cc.Node)
    ], CharacterController.prototype, "placeWeapon", void 0);
    __decorate([
        property(cc.Prefab)
    ], CharacterController.prototype, "effectSmoke", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "clampLeft", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "clampRight", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "clampBottom", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "clampTop", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "level", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "scale", void 0);
    CharacterController = __decorate([
        ccclass
    ], CharacterController);
    return CharacterController;
}(cc.Component));
exports.default = CharacterController;

cc._RF.pop();