"use strict";
cc._RF.push(module, '0d8b754C3JKK6LSITAlYBhq', 'StickMan2');
// Scripts/GamePlay/StickMan2.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var KeyEvent_1 = require("../Common/KeyEvent");
var Singleton_1 = require("../Common/Singleton");
var EnemyController_1 = require("../Enemy/EnemyController");
var StickMan1_1 = require("./StickMan1");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var StickMan2 = /** @class */ (function (_super) {
    __extends(StickMan2, _super);
    function StickMan2() {
        var _this = _super.call(this) || this;
        _this.joyStickFollow = null;
        _this.nEndGame = null;
        _this.Map = null;
        _this.countEnemyEnd = 0;
        _this.MyCharacter = null;
        _this.characterMaterial = [];
        _this.katana = [];
        _this.Guide = null;
        _this.Effect = null;
        _this.txtSmasher = null;
        _this.enemyParent = null;
        _this.btnDownload = null;
        _this.nStart = null;
        _this.countEnemy = 0;
        _this.chooseDoneDeadpool = false;
        _this.chooseDoneIronMan = false;
        _this.boolcheckInteraction = false;
        _this.ironsource = false;
        _this.mindworks = false;
        _this.vungle = false;
        StickMan1_1.default._instance = _this;
        return _this;
    }
    StickMan2.prototype.onEnable = function () {
        GamePlayInstance_1.instance.on(KeyEvent_1.default.scale, this.scale, this);
        GamePlayInstance_1.instance.on(KeyEvent_1.default.plusEnemy, this.plusEnemy, this);
        GamePlayInstance_1.instance.on(KeyEvent_1.default.activeGuide, this.activeGuide, this);
    };
    StickMan2.prototype.onDisable = function () {
        GamePlayInstance_1.instance.off(KeyEvent_1.default.scale, this.scale, this);
        GamePlayInstance_1.instance.off(KeyEvent_1.default.plusEnemy, this.plusEnemy, this);
        GamePlayInstance_1.instance.off(KeyEvent_1.default.activeGuide, this.activeGuide, this);
    };
    StickMan2.prototype.start = function () {
        this.Guide.active = false;
        this.enemyParent.active = false;
    };
    StickMan2.prototype.chooseDeadpool = function () {
        this.MyCharacter.getChildByName('stickman_new').getComponent(cc.MeshRenderer).setMaterial(0, this.characterMaterial[0]);
        this.katana[0].active = true;
        this.chooseDone();
    };
    StickMan2.prototype.chooseIronMan = function () {
        this.MyCharacter.getChildByName('stickman_new').getComponent(cc.MeshRenderer).setMaterial(0, this.characterMaterial[1]);
        this.katana[1].active = true;
        this.chooseDone();
    };
    StickMan2.prototype.chooseDone = function () {
        if (this.ironsource) {
            window.NUC.trigger.interaction();
        }
        this.MyCharacter.active = true;
        this.nStart.active = false;
        this.Guide.active = true;
        this.enemyParent.active = true;
        this.playGame();
    };
    StickMan2.prototype.update = function (dt) {
        if (this.countEnemy == this.countEnemyEnd) {
            this.endGame();
        }
    };
    StickMan2.prototype.playGame = function () {
        Global_1.default.boolStartPlay = true;
        for (var i = 0; i < this.enemyParent.childrenCount; i++) {
            this.enemyParent.children[i].getComponent(EnemyController_1.default).StartMove();
        }
    };
    StickMan2.prototype.endGame = function () {
        var _this = this;
        if (this.mindworks) {
            window.gameEnd && window.gameEnd();
        }
        if (this.ironsource) {
            window.NUC.trigger.endGame('win');
        }
        if (this.vungle) {
            parent.postMessage('complete', '*');
        }
        this.scheduleOnce(function () {
            _this.MyCharacter.active = false;
            _this.nEndGame.active = true;
        }, 1.3);
        this.joyStickFollow.active = false;
        this.Guide.active = false;
    };
    StickMan2.prototype.scale = function () {
        var _this = this;
        this.Effect.scale = 0;
        this.Effect.opacity = 255;
        this.Effect.runAction(cc.sequence(cc.scaleTo(0.2, 1).easing(cc.easeBounceOut()), cc.callFunc(function () {
            _this.scheduleOnce(function () {
                _this.Effect.runAction(cc.fadeOut(0.1));
            }, 0.2);
        })));
    };
    StickMan2.prototype.plusEnemy = function () {
        if (this.countEnemy < this.countEnemyEnd)
            this.countEnemy++;
    };
    StickMan2.prototype.activeGuide = function () {
        if (Global_1.default.boolStartPlay)
            this.Guide.active = true;
    };
    __decorate([
        property(cc.Node)
    ], StickMan2.prototype, "joyStickFollow", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan2.prototype, "nEndGame", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan2.prototype, "Map", void 0);
    __decorate([
        property(cc.Integer)
    ], StickMan2.prototype, "countEnemyEnd", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan2.prototype, "MyCharacter", void 0);
    __decorate([
        property([cc.Material])
    ], StickMan2.prototype, "characterMaterial", void 0);
    __decorate([
        property([cc.Node])
    ], StickMan2.prototype, "katana", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan2.prototype, "Guide", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan2.prototype, "Effect", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan2.prototype, "txtSmasher", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan2.prototype, "enemyParent", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan2.prototype, "btnDownload", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan2.prototype, "nStart", void 0);
    StickMan2 = __decorate([
        ccclass
    ], StickMan2);
    return StickMan2;
}(Singleton_1.default));
exports.default = StickMan2;

cc._RF.pop();