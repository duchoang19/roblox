
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/Global.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '3c677+i/i1KO7mX8mMrc5uJ', 'Global');
// Scripts/Common/Global.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Global = {
    touchPos: null,
    boolEnableTouch: false,
    boolFirstTouchJoyStick: false,
    boolStartPlay: false,
    boolStartAttacking: false,
    boolCheckAttacking: false,
    boolCheckAttacked: false,
    boolendG: false,
    soundBG: null,
    soundIntro: null,
    soundAttack: null,
    soundFootStep: null,
    soundKatana: null,
    soundScream: null,
    soundSpin: null,
    soundReward: null,
    soundClickBtn: null
};
exports.default = Global;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxHbG9iYWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsSUFBSSxNQUFNLEdBQVc7SUFDakIsUUFBUSxFQUFFLElBQUk7SUFDZCxlQUFlLEVBQUUsS0FBSztJQUN0QixzQkFBc0IsRUFBRSxLQUFLO0lBQzdCLGFBQWEsRUFBRSxLQUFLO0lBQ3BCLGtCQUFrQixFQUFFLEtBQUs7SUFDekIsa0JBQWtCLEVBQUUsS0FBSztJQUN6QixpQkFBaUIsRUFBRSxLQUFLO0lBQ3hCLFFBQVEsRUFBRSxLQUFLO0lBQ2YsT0FBTyxFQUFFLElBQUk7SUFDYixVQUFVLEVBQUUsSUFBSTtJQUNoQixXQUFXLEVBQUUsSUFBSTtJQUNqQixhQUFhLEVBQUUsSUFBSTtJQUNuQixXQUFXLEVBQUUsSUFBSTtJQUNqQixXQUFXLEVBQUUsSUFBSTtJQUNqQixTQUFTLEVBQUUsSUFBSTtJQUNmLFdBQVcsRUFBRSxJQUFJO0lBQ2pCLGFBQWEsRUFBRSxJQUFJO0NBQ3RCLENBQUM7QUFDRixrQkFBZSxNQUFNLENBQUMiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbnRlcmZhY2UgR2xvYmFsIHtcclxuICAgIHRvdWNoUG9zOiBjYy5WZWMyLFxyXG4gICAgYm9vbEVuYWJsZVRvdWNoOiBib29sZWFuLFxyXG4gICAgYm9vbEZpcnN0VG91Y2hKb3lTdGljazogYm9vbGVhbixcclxuICAgIGJvb2xTdGFydFBsYXk6IGJvb2xlYW4sXHJcbiAgICBib29sU3RhcnRBdHRhY2tpbmc6IGJvb2xlYW4sXHJcbiAgICBib29sQ2hlY2tBdHRhY2tpbmc6IGJvb2xlYW4sXHJcbiAgICBib29sQ2hlY2tBdHRhY2tlZDogYm9vbGVhbixcclxuICAgIGJvb2xlbmRHOiBib29sZWFuLFxyXG4gICAgc291bmRCRzogY2MuQXVkaW9DbGlwLFxyXG4gICAgc291bmRJbnRybzogY2MuQXVkaW9DbGlwLFxyXG4gICAgc291bmRBdHRhY2s6IGNjLkF1ZGlvQ2xpcCxcclxuICAgIHNvdW5kRm9vdFN0ZXA6IGNjLkF1ZGlvQ2xpcCxcclxuICAgIHNvdW5kS2F0YW5hOiBjYy5BdWRpb0NsaXAsXHJcbiAgICBzb3VuZFNjcmVhbTogY2MuQXVkaW9DbGlwLFxyXG4gICAgc291bmRTcGluOiBjYy5BdWRpb0NsaXAsXHJcbiAgICBzb3VuZFJld2FyZDogY2MuQXVkaW9DbGlwLFxyXG4gICAgc291bmRDbGlja0J0bjogY2MuQXVkaW9DbGlwXHJcbn1cclxubGV0IEdsb2JhbDogR2xvYmFsID0ge1xyXG4gICAgdG91Y2hQb3M6IG51bGwsXHJcbiAgICBib29sRW5hYmxlVG91Y2g6IGZhbHNlLFxyXG4gICAgYm9vbEZpcnN0VG91Y2hKb3lTdGljazogZmFsc2UsXHJcbiAgICBib29sU3RhcnRQbGF5OiBmYWxzZSxcclxuICAgIGJvb2xTdGFydEF0dGFja2luZzogZmFsc2UsXHJcbiAgICBib29sQ2hlY2tBdHRhY2tpbmc6IGZhbHNlLFxyXG4gICAgYm9vbENoZWNrQXR0YWNrZWQ6IGZhbHNlLFxyXG4gICAgYm9vbGVuZEc6IGZhbHNlLFxyXG4gICAgc291bmRCRzogbnVsbCxcclxuICAgIHNvdW5kSW50cm86IG51bGwsXHJcbiAgICBzb3VuZEF0dGFjazogbnVsbCxcclxuICAgIHNvdW5kRm9vdFN0ZXA6IG51bGwsXHJcbiAgICBzb3VuZEthdGFuYTogbnVsbCxcclxuICAgIHNvdW5kU2NyZWFtOiBudWxsLFxyXG4gICAgc291bmRTcGluOiBudWxsLFxyXG4gICAgc291bmRSZXdhcmQ6IG51bGwsXHJcbiAgICBzb3VuZENsaWNrQnRuOiBudWxsXHJcbn07XHJcbmV4cG9ydCBkZWZhdWx0IEdsb2JhbDsiXX0=