
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/GamePlayInstance.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '3079c9yA9NNG7yWTNHi7OqT', 'GamePlayInstance');
// Scripts/Common/GamePlayInstance.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var StickMan1_1 = require("../GamePlay/StickMan1");
var Singleton_1 = require("./Singleton");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
exports.instance = new cc.EventTarget();
var GamePlayInstance = /** @class */ (function (_super) {
    __extends(GamePlayInstance, _super);
    function GamePlayInstance() {
        var _this = _super.call(this) || this;
        _this.gameplay = StickMan1_1.default.Instance(StickMan1_1.default);
        GamePlayInstance_1._instance = _this;
        return _this;
    }
    GamePlayInstance_1 = GamePlayInstance;
    var GamePlayInstance_1;
    GamePlayInstance = GamePlayInstance_1 = __decorate([
        ccclass
    ], GamePlayInstance);
    return GamePlayInstance;
}(Singleton_1.default));
exports.default = GamePlayInstance;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxHYW1lUGxheUluc3RhbmNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNBLG1EQUE4QztBQUM5Qyx5Q0FBb0M7QUFFOUIsSUFBQSxrQkFBcUMsRUFBbkMsb0JBQU8sRUFBRSxzQkFBMEIsQ0FBQztBQUMvQixRQUFBLFFBQVEsR0FBRyxJQUFJLEVBQUUsQ0FBQyxXQUFXLEVBQUUsQ0FBQztBQUU3QztJQUE4QyxvQ0FBMkI7SUFFckU7UUFBQSxZQUNJLGlCQUFPLFNBRVY7UUFKRCxjQUFRLEdBQWMsbUJBQVMsQ0FBQyxRQUFRLENBQUMsbUJBQVMsQ0FBQyxDQUFDO1FBR2hELGtCQUFnQixDQUFDLFNBQVMsR0FBRyxLQUFJLENBQUM7O0lBQ3RDLENBQUM7eUJBTGdCLGdCQUFnQjs7SUFBaEIsZ0JBQWdCO1FBRHBDLE9BQU87T0FDYSxnQkFBZ0IsQ0FNcEM7SUFBRCx1QkFBQztDQU5ELEFBTUMsQ0FONkMsbUJBQVMsR0FNdEQ7a0JBTm9CLGdCQUFnQiIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG5pbXBvcnQgU3RpY2tNYW4xIGZyb20gXCIuLi9HYW1lUGxheS9TdGlja01hbjFcIjtcclxuaW1wb3J0IFNpbmdsZXRvbiBmcm9tIFwiLi9TaW5nbGV0b25cIjtcclxuXHJcbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XHJcbmV4cG9ydCBjb25zdCBpbnN0YW5jZSA9IG5ldyBjYy5FdmVudFRhcmdldCgpO1xyXG5AY2NjbGFzc1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBHYW1lUGxheUluc3RhbmNlIGV4dGVuZHMgU2luZ2xldG9uPEdhbWVQbGF5SW5zdGFuY2U+IHtcclxuICAgIGdhbWVwbGF5OiBTdGlja01hbjEgPSBTdGlja01hbjEuSW5zdGFuY2UoU3RpY2tNYW4xKTtcclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIHN1cGVyKCk7XHJcbiAgICAgICAgR2FtZVBsYXlJbnN0YW5jZS5faW5zdGFuY2UgPSB0aGlzO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==