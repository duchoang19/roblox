
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/GamePlay/StickMan2.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '0d8b754C3JKK6LSITAlYBhq', 'StickMan2');
// Scripts/GamePlay/StickMan2.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var KeyEvent_1 = require("../Common/KeyEvent");
var Singleton_1 = require("../Common/Singleton");
var EnemyController_1 = require("../Enemy/EnemyController");
var StickMan1_1 = require("./StickMan1");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var StickMan2 = /** @class */ (function (_super) {
    __extends(StickMan2, _super);
    function StickMan2() {
        var _this = _super.call(this) || this;
        _this.joyStickFollow = null;
        _this.nEndGame = null;
        _this.Map = null;
        _this.countEnemyEnd = 0;
        _this.MyCharacter = null;
        _this.characterMaterial = [];
        _this.katana = [];
        _this.Guide = null;
        _this.Effect = null;
        _this.txtSmasher = null;
        _this.enemyParent = null;
        _this.btnDownload = null;
        _this.nStart = null;
        _this.countEnemy = 0;
        _this.chooseDoneDeadpool = false;
        _this.chooseDoneIronMan = false;
        _this.boolcheckInteraction = false;
        _this.ironsource = false;
        _this.mindworks = false;
        _this.vungle = false;
        StickMan1_1.default._instance = _this;
        return _this;
    }
    StickMan2.prototype.onEnable = function () {
        GamePlayInstance_1.instance.on(KeyEvent_1.default.scale, this.scale, this);
        GamePlayInstance_1.instance.on(KeyEvent_1.default.plusEnemy, this.plusEnemy, this);
        GamePlayInstance_1.instance.on(KeyEvent_1.default.activeGuide, this.activeGuide, this);
    };
    StickMan2.prototype.onDisable = function () {
        GamePlayInstance_1.instance.off(KeyEvent_1.default.scale, this.scale, this);
        GamePlayInstance_1.instance.off(KeyEvent_1.default.plusEnemy, this.plusEnemy, this);
        GamePlayInstance_1.instance.off(KeyEvent_1.default.activeGuide, this.activeGuide, this);
    };
    StickMan2.prototype.start = function () {
        this.Guide.active = false;
        this.enemyParent.active = false;
    };
    StickMan2.prototype.chooseDeadpool = function () {
        this.MyCharacter.getChildByName('stickman_new').getComponent(cc.MeshRenderer).setMaterial(0, this.characterMaterial[0]);
        this.katana[0].active = true;
        this.chooseDone();
    };
    StickMan2.prototype.chooseIronMan = function () {
        this.MyCharacter.getChildByName('stickman_new').getComponent(cc.MeshRenderer).setMaterial(0, this.characterMaterial[1]);
        this.katana[1].active = true;
        this.chooseDone();
    };
    StickMan2.prototype.chooseDone = function () {
        if (this.ironsource) {
            window.NUC.trigger.interaction();
        }
        this.MyCharacter.active = true;
        this.nStart.active = false;
        this.Guide.active = true;
        this.enemyParent.active = true;
        this.playGame();
    };
    StickMan2.prototype.update = function (dt) {
        if (this.countEnemy == this.countEnemyEnd) {
            this.endGame();
        }
    };
    StickMan2.prototype.playGame = function () {
        Global_1.default.boolStartPlay = true;
        for (var i = 0; i < this.enemyParent.childrenCount; i++) {
            this.enemyParent.children[i].getComponent(EnemyController_1.default).StartMove();
        }
    };
    StickMan2.prototype.endGame = function () {
        var _this = this;
        if (this.mindworks) {
            window.gameEnd && window.gameEnd();
        }
        if (this.ironsource) {
            window.NUC.trigger.endGame('win');
        }
        if (this.vungle) {
            parent.postMessage('complete', '*');
        }
        this.scheduleOnce(function () {
            _this.MyCharacter.active = false;
            _this.nEndGame.active = true;
        }, 1.3);
        this.joyStickFollow.active = false;
        this.Guide.active = false;
    };
    StickMan2.prototype.scale = function () {
        var _this = this;
        this.Effect.scale = 0;
        this.Effect.opacity = 255;
        this.Effect.runAction(cc.sequence(cc.scaleTo(0.2, 1).easing(cc.easeBounceOut()), cc.callFunc(function () {
            _this.scheduleOnce(function () {
                _this.Effect.runAction(cc.fadeOut(0.1));
            }, 0.2);
        })));
    };
    StickMan2.prototype.plusEnemy = function () {
        if (this.countEnemy < this.countEnemyEnd)
            this.countEnemy++;
    };
    StickMan2.prototype.activeGuide = function () {
        if (Global_1.default.boolStartPlay)
            this.Guide.active = true;
    };
    __decorate([
        property(cc.Node)
    ], StickMan2.prototype, "joyStickFollow", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan2.prototype, "nEndGame", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan2.prototype, "Map", void 0);
    __decorate([
        property(cc.Integer)
    ], StickMan2.prototype, "countEnemyEnd", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan2.prototype, "MyCharacter", void 0);
    __decorate([
        property([cc.Material])
    ], StickMan2.prototype, "characterMaterial", void 0);
    __decorate([
        property([cc.Node])
    ], StickMan2.prototype, "katana", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan2.prototype, "Guide", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan2.prototype, "Effect", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan2.prototype, "txtSmasher", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan2.prototype, "enemyParent", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan2.prototype, "btnDownload", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan2.prototype, "nStart", void 0);
    StickMan2 = __decorate([
        ccclass
    ], StickMan2);
    return StickMan2;
}(Singleton_1.default));
exports.default = StickMan2;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcR2FtZVBsYXlcXFN0aWNrTWFuMi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQSwrREFBc0Q7QUFDdEQsMkNBQXNDO0FBQ3RDLCtDQUEwQztBQUMxQyxpREFBNEM7QUFDNUMsNERBQXVEO0FBQ3ZELHlDQUFvQztBQUU5QixJQUFBLGtCQUFxQyxFQUFuQyxvQkFBTyxFQUFFLHNCQUEwQixDQUFDO0FBSzVDO0lBQXVDLDZCQUFvQjtJQWtEdkQ7UUFBQSxZQUNJLGlCQUFPLFNBRVY7UUFsREQsb0JBQWMsR0FBWSxJQUFJLENBQUM7UUFHL0IsY0FBUSxHQUFZLElBQUksQ0FBQztRQUd6QixTQUFHLEdBQVksSUFBSSxDQUFDO1FBR3BCLG1CQUFhLEdBQVcsQ0FBQyxDQUFDO1FBRzFCLGlCQUFXLEdBQVksSUFBSSxDQUFDO1FBRzVCLHVCQUFpQixHQUFrQixFQUFFLENBQUM7UUFHdEMsWUFBTSxHQUFjLEVBQUUsQ0FBQztRQUd2QixXQUFLLEdBQVksSUFBSSxDQUFDO1FBR3RCLFlBQU0sR0FBWSxJQUFJLENBQUM7UUFHdkIsZ0JBQVUsR0FBWSxJQUFJLENBQUM7UUFHM0IsaUJBQVcsR0FBWSxJQUFJLENBQUM7UUFHNUIsaUJBQVcsR0FBWSxJQUFJLENBQUM7UUFHNUIsWUFBTSxHQUFZLElBQUksQ0FBQztRQUV2QixnQkFBVSxHQUFXLENBQUMsQ0FBQztRQUN2Qix3QkFBa0IsR0FBWSxLQUFLLENBQUM7UUFDcEMsdUJBQWlCLEdBQVksS0FBSyxDQUFDO1FBRW5DLDBCQUFvQixHQUFZLEtBQUssQ0FBQztRQUN0QyxnQkFBVSxHQUFZLEtBQUssQ0FBQztRQUM1QixlQUFTLEdBQVksS0FBSyxDQUFDO1FBQzNCLFlBQU0sR0FBWSxLQUFLLENBQUM7UUFJcEIsbUJBQVMsQ0FBQyxTQUFTLEdBQUcsS0FBSSxDQUFDOztJQUMvQixDQUFDO0lBRUQsNEJBQVEsR0FBUjtRQUNJLDJCQUFRLENBQUMsRUFBRSxDQUFDLGtCQUFRLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDOUMsMkJBQVEsQ0FBQyxFQUFFLENBQUMsa0JBQVEsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUN0RCwyQkFBUSxDQUFDLEVBQUUsQ0FBQyxrQkFBUSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQzlELENBQUM7SUFFRCw2QkFBUyxHQUFUO1FBQ0ksMkJBQVEsQ0FBQyxHQUFHLENBQUMsa0JBQVEsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztRQUMvQywyQkFBUSxDQUFDLEdBQUcsQ0FBQyxrQkFBUSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3ZELDJCQUFRLENBQUMsR0FBRyxDQUFDLGtCQUFRLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDL0QsQ0FBQztJQUVELHlCQUFLLEdBQUw7UUFDSSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDMUIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO0lBQ3BDLENBQUM7SUFFRCxrQ0FBYyxHQUFkO1FBQ0ksSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsY0FBYyxDQUFDLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxZQUFZLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3hILElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUM3QixJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDdEIsQ0FBQztJQUVELGlDQUFhLEdBQWI7UUFDSSxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLFlBQVksQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDeEgsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQzdCLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztJQUN0QixDQUFDO0lBRUQsOEJBQVUsR0FBVjtRQUNJLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNqQixNQUFNLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUNwQztRQUNELElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUMvQixJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDM0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUMvQixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDcEIsQ0FBQztJQUVELDBCQUFNLEdBQU4sVUFBTyxFQUFFO1FBQ0wsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7WUFDdkMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO1NBQ2xCO0lBQ0wsQ0FBQztJQUVELDRCQUFRLEdBQVI7UUFDSSxnQkFBTSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7UUFDNUIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3JELElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyx5QkFBZSxDQUFDLENBQUMsU0FBUyxFQUFFLENBQUM7U0FDMUU7SUFDTCxDQUFDO0lBRUQsMkJBQU8sR0FBUDtRQUFBLGlCQWdCQztRQWZHLElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNoQixNQUFNLENBQUMsT0FBTyxJQUFJLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztTQUN0QztRQUNELElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNqQixNQUFNLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUE7U0FDcEM7UUFDRCxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDYixNQUFNLENBQUMsV0FBVyxDQUFDLFVBQVUsRUFBRSxHQUFHLENBQUMsQ0FBQztTQUN2QztRQUNELElBQUksQ0FBQyxZQUFZLENBQUM7WUFDZCxLQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFDaEMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ2hDLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUNSLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUNuQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7SUFDOUIsQ0FBQztJQUVELHlCQUFLLEdBQUw7UUFBQSxpQkFRQztRQVBHLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztRQUN0QixJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUM7UUFDMUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLGFBQWEsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLFFBQVEsQ0FBQztZQUN6RixLQUFJLENBQUMsWUFBWSxDQUFDO2dCQUNkLEtBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUMzQyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDWixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDVCxDQUFDO0lBRUQsNkJBQVMsR0FBVDtRQUNJLElBQUksSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsYUFBYTtZQUNwQyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDMUIsQ0FBQztJQUVELCtCQUFXLEdBQVg7UUFDSSxJQUFJLGdCQUFNLENBQUMsYUFBYTtZQUNwQixJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7SUFDakMsQ0FBQztJQTdJRDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO3FEQUNhO0lBRy9CO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7K0NBQ087SUFHekI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzswQ0FDRTtJQUdwQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDO29EQUNLO0lBRzFCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7a0RBQ1U7SUFHNUI7UUFEQyxRQUFRLENBQUMsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUM7d0RBQ2M7SUFHdEM7UUFEQyxRQUFRLENBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUM7NkNBQ0c7SUFHdkI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzs0Q0FDSTtJQUd0QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOzZDQUNLO0lBR3ZCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7aURBQ1M7SUFHM0I7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztrREFDVTtJQUc1QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO2tEQUNVO0lBRzVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7NkNBQ0s7SUF2Q04sU0FBUztRQUQ3QixPQUFPO09BQ2EsU0FBUyxDQWlKN0I7SUFBRCxnQkFBQztDQWpKRCxBQWlKQyxDQWpKc0MsbUJBQVMsR0FpSi9DO2tCQWpKb0IsU0FBUyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG5pbXBvcnQgeyBpbnN0YW5jZSB9IGZyb20gXCIuLi9Db21tb24vR2FtZVBsYXlJbnN0YW5jZVwiO1xyXG5pbXBvcnQgR2xvYmFsIGZyb20gXCIuLi9Db21tb24vR2xvYmFsXCI7XHJcbmltcG9ydCBLZXlFdmVudCBmcm9tIFwiLi4vQ29tbW9uL0tleUV2ZW50XCI7XHJcbmltcG9ydCBTaW5nbGV0b24gZnJvbSBcIi4uL0NvbW1vbi9TaW5nbGV0b25cIjtcclxuaW1wb3J0IEVuZW15Q29udHJvbGxlciBmcm9tIFwiLi4vRW5lbXkvRW5lbXlDb250cm9sbGVyXCI7XHJcbmltcG9ydCBTdGlja01hbjEgZnJvbSBcIi4vU3RpY2tNYW4xXCI7XHJcblxyXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuZGVjbGFyZSBjb25zdCB3aW5kb3c6IGFueTtcclxuXHJcbkBjY2NsYXNzXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFN0aWNrTWFuMiBleHRlbmRzIFNpbmdsZXRvbjxTdGlja01hbjE+IHtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGpveVN0aWNrRm9sbG93OiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIG5FbmRHYW1lOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIE1hcDogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLkludGVnZXIpXHJcbiAgICBjb3VudEVuZW15RW5kOiBudW1iZXIgPSAwO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgTXlDaGFyYWN0ZXI6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShbY2MuTWF0ZXJpYWxdKVxyXG4gICAgY2hhcmFjdGVyTWF0ZXJpYWw6IGNjLk1hdGVyaWFsW10gPSBbXTtcclxuXHJcbiAgICBAcHJvcGVydHkoW2NjLk5vZGVdKVxyXG4gICAga2F0YW5hOiBjYy5Ob2RlW10gPSBbXTtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIEd1aWRlOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIEVmZmVjdDogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICB0eHRTbWFzaGVyOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGVuZW15UGFyZW50OiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGJ0bkRvd25sb2FkOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIG5TdGFydDogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgY291bnRFbmVteTogbnVtYmVyID0gMDtcclxuICAgIGNob29zZURvbmVEZWFkcG9vbDogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgY2hvb3NlRG9uZUlyb25NYW46IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgICBib29sY2hlY2tJbnRlcmFjdGlvbjogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgaXJvbnNvdXJjZTogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgbWluZHdvcmtzOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICB2dW5nbGU6IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICBzdXBlcigpO1xyXG4gICAgICAgIFN0aWNrTWFuMS5faW5zdGFuY2UgPSB0aGlzO1xyXG4gICAgfVxyXG5cclxuICAgIG9uRW5hYmxlKCkge1xyXG4gICAgICAgIGluc3RhbmNlLm9uKEtleUV2ZW50LnNjYWxlLCB0aGlzLnNjYWxlLCB0aGlzKTtcclxuICAgICAgICBpbnN0YW5jZS5vbihLZXlFdmVudC5wbHVzRW5lbXksIHRoaXMucGx1c0VuZW15LCB0aGlzKTtcclxuICAgICAgICBpbnN0YW5jZS5vbihLZXlFdmVudC5hY3RpdmVHdWlkZSwgdGhpcy5hY3RpdmVHdWlkZSwgdGhpcyk7XHJcbiAgICB9XHJcblxyXG4gICAgb25EaXNhYmxlKCkge1xyXG4gICAgICAgIGluc3RhbmNlLm9mZihLZXlFdmVudC5zY2FsZSwgdGhpcy5zY2FsZSwgdGhpcyk7XHJcbiAgICAgICAgaW5zdGFuY2Uub2ZmKEtleUV2ZW50LnBsdXNFbmVteSwgdGhpcy5wbHVzRW5lbXksIHRoaXMpO1xyXG4gICAgICAgIGluc3RhbmNlLm9mZihLZXlFdmVudC5hY3RpdmVHdWlkZSwgdGhpcy5hY3RpdmVHdWlkZSwgdGhpcyk7XHJcbiAgICB9XHJcblxyXG4gICAgc3RhcnQoKSB7XHJcbiAgICAgICAgdGhpcy5HdWlkZS5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLmVuZW15UGFyZW50LmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIGNob29zZURlYWRwb29sKCkge1xyXG4gICAgICAgIHRoaXMuTXlDaGFyYWN0ZXIuZ2V0Q2hpbGRCeU5hbWUoJ3N0aWNrbWFuX25ldycpLmdldENvbXBvbmVudChjYy5NZXNoUmVuZGVyZXIpLnNldE1hdGVyaWFsKDAsIHRoaXMuY2hhcmFjdGVyTWF0ZXJpYWxbMF0pO1xyXG4gICAgICAgIHRoaXMua2F0YW5hWzBdLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5jaG9vc2VEb25lKCk7XHJcbiAgICB9XHJcblxyXG4gICAgY2hvb3NlSXJvbk1hbigpIHtcclxuICAgICAgICB0aGlzLk15Q2hhcmFjdGVyLmdldENoaWxkQnlOYW1lKCdzdGlja21hbl9uZXcnKS5nZXRDb21wb25lbnQoY2MuTWVzaFJlbmRlcmVyKS5zZXRNYXRlcmlhbCgwLCB0aGlzLmNoYXJhY3Rlck1hdGVyaWFsWzFdKTtcclxuICAgICAgICB0aGlzLmthdGFuYVsxXS5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuY2hvb3NlRG9uZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIGNob29zZURvbmUoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuaXJvbnNvdXJjZSkge1xyXG4gICAgICAgICAgICB3aW5kb3cuTlVDLnRyaWdnZXIuaW50ZXJhY3Rpb24oKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5NeUNoYXJhY3Rlci5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMublN0YXJ0LmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuR3VpZGUuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLmVuZW15UGFyZW50LmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5wbGF5R2FtZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZShkdCkge1xyXG4gICAgICAgIGlmICh0aGlzLmNvdW50RW5lbXkgPT0gdGhpcy5jb3VudEVuZW15RW5kKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZW5kR2FtZSgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwbGF5R2FtZSgpIHtcclxuICAgICAgICBHbG9iYWwuYm9vbFN0YXJ0UGxheSA9IHRydWU7XHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLmVuZW15UGFyZW50LmNoaWxkcmVuQ291bnQ7IGkrKykge1xyXG4gICAgICAgICAgICB0aGlzLmVuZW15UGFyZW50LmNoaWxkcmVuW2ldLmdldENvbXBvbmVudChFbmVteUNvbnRyb2xsZXIpLlN0YXJ0TW92ZSgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBlbmRHYW1lKCkge1xyXG4gICAgICAgIGlmICh0aGlzLm1pbmR3b3Jrcykge1xyXG4gICAgICAgICAgICB3aW5kb3cuZ2FtZUVuZCAmJiB3aW5kb3cuZ2FtZUVuZCgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5pcm9uc291cmNlKSB7XHJcbiAgICAgICAgICAgIHdpbmRvdy5OVUMudHJpZ2dlci5lbmRHYW1lKCd3aW4nKVxyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy52dW5nbGUpIHtcclxuICAgICAgICAgICAgcGFyZW50LnBvc3RNZXNzYWdlKCdjb21wbGV0ZScsICcqJyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuc2NoZWR1bGVPbmNlKCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5NeUNoYXJhY3Rlci5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5uRW5kR2FtZS5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgIH0sIDEuMyk7XHJcbiAgICAgICAgdGhpcy5qb3lTdGlja0ZvbGxvdy5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLkd1aWRlLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIHNjYWxlKCkge1xyXG4gICAgICAgIHRoaXMuRWZmZWN0LnNjYWxlID0gMDtcclxuICAgICAgICB0aGlzLkVmZmVjdC5vcGFjaXR5ID0gMjU1O1xyXG4gICAgICAgIHRoaXMuRWZmZWN0LnJ1bkFjdGlvbihjYy5zZXF1ZW5jZShjYy5zY2FsZVRvKDAuMiwgMSkuZWFzaW5nKGNjLmVhc2VCb3VuY2VPdXQoKSksIGNjLmNhbGxGdW5jKCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5FZmZlY3QucnVuQWN0aW9uKGNjLmZhZGVPdXQoMC4xKSk7XHJcbiAgICAgICAgICAgIH0sIDAuMik7XHJcbiAgICAgICAgfSkpKTtcclxuICAgIH1cclxuXHJcbiAgICBwbHVzRW5lbXkoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuY291bnRFbmVteSA8IHRoaXMuY291bnRFbmVteUVuZClcclxuICAgICAgICAgICAgdGhpcy5jb3VudEVuZW15Kys7XHJcbiAgICB9XHJcblxyXG4gICAgYWN0aXZlR3VpZGUoKSB7XHJcbiAgICAgICAgaWYgKEdsb2JhbC5ib29sU3RhcnRQbGF5KVxyXG4gICAgICAgICAgICB0aGlzLkd1aWRlLmFjdGl2ZSA9IHRydWU7XHJcbiAgICB9XHJcbn1cclxuIl19