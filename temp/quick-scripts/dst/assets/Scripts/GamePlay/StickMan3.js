
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/GamePlay/StickMan3.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '74f44KHFKxG/6SyuFNJl0Fm', 'StickMan3');
// Scripts/GamePlay/StickMan3.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var CharacterController_1 = require("../Character/CharacterController");
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var KeyEvent_1 = require("../Common/KeyEvent");
var Singleton_1 = require("../Common/Singleton");
var EnemyController_1 = require("../Enemy/EnemyController");
var StickMan1_1 = require("./StickMan1");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var StickMan3 = /** @class */ (function (_super) {
    __extends(StickMan3, _super);
    function StickMan3() {
        var _this = _super.call(this) || this;
        _this.joyStickFollow = null;
        _this.Map = null;
        _this.countEnemyEnd = 0;
        _this.MyCharacter = null;
        _this.Guide = null;
        _this.Effect = null;
        _this.txtStart = null;
        _this.enemyParent = null;
        _this.btnDownload = null;
        _this.endCard = null;
        _this.nightEnd = null;
        _this.circleEnd = null;
        _this.btnAll = null;
        _this.nEnemyEnd = null;
        _this.countEnemy = 0;
        _this.boolCheckEnd = false;
        _this.boolcheckInteraction = false;
        _this.ironsource = false;
        _this.mindworks = false;
        _this.vungle = false;
        StickMan1_1.default._instance = _this;
        return _this;
    }
    StickMan3.prototype.onEnable = function () {
        GamePlayInstance_1.instance.on(KeyEvent_1.default.scale, this.scale, this);
        GamePlayInstance_1.instance.on(KeyEvent_1.default.plusEnemy, this.plusEnemy, this);
        GamePlayInstance_1.instance.on(KeyEvent_1.default.activeGuide, this.activeGuide, this);
    };
    StickMan3.prototype.onDisable = function () {
        GamePlayInstance_1.instance.off(KeyEvent_1.default.scale, this.scale, this);
        GamePlayInstance_1.instance.off(KeyEvent_1.default.plusEnemy, this.plusEnemy, this);
        GamePlayInstance_1.instance.off(KeyEvent_1.default.activeGuide, this.activeGuide, this);
    };
    StickMan3.prototype.start = function () {
        this.playGame();
    };
    StickMan3.prototype.update = function (dt) {
        var _this = this;
        if (Global_1.default.boolEnableTouch && !this.boolcheckInteraction) {
            this.txtStart.active = false;
            if (this.ironsource) {
                window.NUC.trigger.interaction();
            }
        }
        if (this.countEnemy == this.countEnemyEnd && !this.boolCheckEnd) {
            Global_1.default.boolStartPlay = false;
            this.boolCheckEnd = true;
            this.scheduleOnce(function () {
                _this.SupportEnd();
            }, 1);
        }
    };
    StickMan3.prototype.playGame = function () {
        this.MyCharacter.active = true;
        Global_1.default.boolStartPlay = true;
        for (var i = 0; i < this.enemyParent.childrenCount; i++) {
            this.enemyParent.children[i].getComponent(EnemyController_1.default).StartMove();
        }
    };
    StickMan3.prototype.SupportEnd = function () {
        var _this = this;
        this.scheduleOnce(function () {
            _this.nightEnd.active = true;
            _this.scheduleOnce(function () {
                _this.circleEnd.active = true;
                _this.scheduleOnce(function () {
                    _this.nightEnd.active = false;
                    _this.circleEnd.active = false;
                    _this.MyCharacter.opacity = 0;
                    _this.EndGame();
                }, 0.6);
            }, 0.4);
            _this.circleEnd.active = true;
        }, 0.2);
    };
    StickMan3.prototype.EndGame = function () {
        Global_1.default.boolendG = true;
        this.joyStickFollow.active = false;
        this.endCard.active = true;
        this.playGame();
        this.Map.active = true;
        this.endCard.children[2].active = true;
        this.endCard.children[3].active = true;
        this.endCard.children[4].active = true;
        for (var i = 0; i < this.nEnemyEnd.childrenCount; i++) {
            this.nEnemyEnd.children[i].getComponent(EnemyController_1.default).StartMove();
        }
        this.MyCharacter.opacity = 255;
        this.MyCharacter.x = -3;
        this.MyCharacter.y = -60;
        this.MyCharacter.scale = 350;
        this.MyCharacter.eulerAngles = new cc.Vec3(90, 0, 180);
        this.MyCharacter.getComponent(CharacterController_1.default).ArrowDirection.active = false;
        this.Guide.active = true;
        for (var i = 0; i < this.enemyParent.childrenCount; i++) {
            this.enemyParent.children[i].getComponent(EnemyController_1.default).bodyDeath.active = false;
            this.enemyParent.children[i].active = false;
            ;
        }
        this.btnAll.active = true;
        this.btnDownload.getComponent(cc.Button).interactable = false;
        if (this.mindworks) {
            window.gameEnd && window.gameEnd();
        }
        if (this.ironsource) {
            window.NUC.trigger.endGame('win');
        }
        if (this.vungle) {
            parent.postMessage('complete', '*');
        }
    };
    StickMan3.prototype.scale = function () {
        var _this = this;
        this.Effect.scale = 0;
        this.Effect.opacity = 255;
        this.Effect.runAction(cc.sequence(cc.scaleTo(0.2, 1).easing(cc.easeBounceOut()), cc.callFunc(function () {
            _this.scheduleOnce(function () {
                _this.Effect.runAction(cc.fadeOut(0.1));
            }, 0.2);
        })));
    };
    StickMan3.prototype.plusEnemy = function () {
        if (this.countEnemy < this.countEnemyEnd)
            this.countEnemy++;
    };
    StickMan3.prototype.activeGuide = function () {
        if (Global_1.default.boolStartPlay)
            this.Guide.active = true;
    };
    __decorate([
        property(cc.Node)
    ], StickMan3.prototype, "joyStickFollow", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan3.prototype, "Map", void 0);
    __decorate([
        property(cc.Integer)
    ], StickMan3.prototype, "countEnemyEnd", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan3.prototype, "MyCharacter", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan3.prototype, "Guide", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan3.prototype, "Effect", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan3.prototype, "txtStart", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan3.prototype, "enemyParent", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan3.prototype, "btnDownload", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan3.prototype, "endCard", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan3.prototype, "nightEnd", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan3.prototype, "circleEnd", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan3.prototype, "btnAll", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan3.prototype, "nEnemyEnd", void 0);
    StickMan3 = __decorate([
        ccclass
    ], StickMan3);
    return StickMan3;
}(Singleton_1.default));
exports.default = StickMan3;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcR2FtZVBsYXlcXFN0aWNrTWFuMy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSx3RUFBbUU7QUFDbkUsK0RBQXNEO0FBQ3RELDJDQUFzQztBQUN0QywrQ0FBMEM7QUFDMUMsaURBQTRDO0FBQzVDLDREQUF1RDtBQUN2RCx5Q0FBb0M7QUFFOUIsSUFBQSxrQkFBbUMsRUFBbEMsb0JBQU8sRUFBRSxzQkFBeUIsQ0FBQztBQUsxQztJQUF1Qyw2QkFBb0I7SUFvRHZEO1FBQUEsWUFDSSxpQkFBTyxTQUVWO1FBcERELG9CQUFjLEdBQVksSUFBSSxDQUFDO1FBRy9CLFNBQUcsR0FBWSxJQUFJLENBQUM7UUFHcEIsbUJBQWEsR0FBVyxDQUFDLENBQUM7UUFHMUIsaUJBQVcsR0FBWSxJQUFJLENBQUM7UUFHNUIsV0FBSyxHQUFZLElBQUksQ0FBQztRQUd0QixZQUFNLEdBQVksSUFBSSxDQUFDO1FBR3ZCLGNBQVEsR0FBWSxJQUFJLENBQUM7UUFHekIsaUJBQVcsR0FBWSxJQUFJLENBQUM7UUFHNUIsaUJBQVcsR0FBWSxJQUFJLENBQUM7UUFHNUIsYUFBTyxHQUFZLElBQUksQ0FBQztRQUd4QixjQUFRLEdBQVksSUFBSSxDQUFDO1FBR3pCLGVBQVMsR0FBWSxJQUFJLENBQUM7UUFHMUIsWUFBTSxHQUFZLElBQUksQ0FBQztRQUd2QixlQUFTLEdBQVksSUFBSSxDQUFDO1FBRTFCLGdCQUFVLEdBQVcsQ0FBQyxDQUFDO1FBQ3ZCLGtCQUFZLEdBQVksS0FBSyxDQUFDO1FBRTlCLDBCQUFvQixHQUFZLEtBQUssQ0FBQztRQUN0QyxnQkFBVSxHQUFZLEtBQUssQ0FBQztRQUM1QixlQUFTLEdBQVksS0FBSyxDQUFDO1FBQzNCLFlBQU0sR0FBWSxLQUFLLENBQUM7UUFJcEIsbUJBQVMsQ0FBQyxTQUFTLEdBQUcsS0FBSSxDQUFDOztJQUMvQixDQUFDO0lBRUQsNEJBQVEsR0FBUjtRQUNJLDJCQUFRLENBQUMsRUFBRSxDQUFDLGtCQUFRLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDOUMsMkJBQVEsQ0FBQyxFQUFFLENBQUMsa0JBQVEsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUN0RCwyQkFBUSxDQUFDLEVBQUUsQ0FBQyxrQkFBUSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQzlELENBQUM7SUFFRCw2QkFBUyxHQUFUO1FBQ0ksMkJBQVEsQ0FBQyxHQUFHLENBQUMsa0JBQVEsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztRQUMvQywyQkFBUSxDQUFDLEdBQUcsQ0FBQyxrQkFBUSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3ZELDJCQUFRLENBQUMsR0FBRyxDQUFDLGtCQUFRLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDL0QsQ0FBQztJQUVELHlCQUFLLEdBQUw7UUFDSSxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDcEIsQ0FBQztJQUVELDBCQUFNLEdBQU4sVUFBUSxFQUFFO1FBQVYsaUJBZUM7UUFkRyxJQUFJLGdCQUFNLENBQUMsZUFBZSxJQUFJLENBQUMsSUFBSSxDQUFDLG9CQUFvQixFQUFFO1lBQ3RELElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztZQUM3QixJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7Z0JBQ2pCLE1BQU0sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxDQUFDO2FBQ3BDO1NBQ0o7UUFFRCxJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLGFBQWEsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDN0QsZ0JBQU0sQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO1lBQzdCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1lBQ3pCLElBQUksQ0FBQyxZQUFZLENBQUM7Z0JBQ2QsS0FBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO1lBQ3RCLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUNUO0lBQ0wsQ0FBQztJQUVELDRCQUFRLEdBQVI7UUFDSSxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDL0IsZ0JBQU0sQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1FBQzVCLEtBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUNwRCxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMseUJBQWUsQ0FBQyxDQUFDLFNBQVMsRUFBRSxDQUFDO1NBQzFFO0lBQ0wsQ0FBQztJQUVELDhCQUFVLEdBQVY7UUFBQSxpQkFjQztRQWJHLElBQUksQ0FBQyxZQUFZLENBQUM7WUFDZCxLQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7WUFDNUIsS0FBSSxDQUFDLFlBQVksQ0FBQztnQkFDZCxLQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7Z0JBQzdCLEtBQUksQ0FBQyxZQUFZLENBQUM7b0JBQ2QsS0FBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO29CQUM3QixLQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7b0JBQzlCLEtBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQztvQkFDN0IsS0FBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO2dCQUNuQixDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7WUFDWixDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7WUFDUixLQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDakMsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQ1osQ0FBQztJQUVELDJCQUFPLEdBQVA7UUFDSSxnQkFBTSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFDdkIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ25DLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUMzQixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDaEIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDdkMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUN2QyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ3ZDLEtBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUNsRCxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMseUJBQWUsQ0FBQyxDQUFDLFNBQVMsRUFBRSxDQUFDO1NBQ3hFO1FBQ0QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDO1FBQy9CLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQ3hCLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQztRQUM3QixJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsR0FBRyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUN2RCxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyw2QkFBbUIsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ2pGLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUN6QixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDckQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLHlCQUFlLENBQUMsQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztZQUNwRixJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1lBQUEsQ0FBQztTQUNoRDtRQUNELElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUMxQixJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztRQUM5RCxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDaEIsTUFBTSxDQUFDLE9BQU8sSUFBSSxNQUFNLENBQUMsT0FBTyxFQUFFLENBQUM7U0FDdEM7UUFDRCxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDakIsTUFBTSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFBO1NBQ3BDO1FBQ0QsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2IsTUFBTSxDQUFDLFdBQVcsQ0FBQyxVQUFVLEVBQUUsR0FBRyxDQUFDLENBQUM7U0FDdkM7SUFDTCxDQUFDO0lBRUQseUJBQUssR0FBTDtRQUFBLGlCQVFDO1FBUEcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQztRQUMxQixJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsYUFBYSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsUUFBUSxDQUFDO1lBQ3pGLEtBQUksQ0FBQyxZQUFZLENBQUM7Z0JBQ2QsS0FBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQzNDLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUNaLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNULENBQUM7SUFFRCw2QkFBUyxHQUFUO1FBQ0ksSUFBSSxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxhQUFhO1lBQ3BDLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztJQUMxQixDQUFDO0lBRUQsK0JBQVcsR0FBWDtRQUNJLElBQUcsZ0JBQU0sQ0FBQyxhQUFhO1lBQ25CLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztJQUNqQyxDQUFDO0lBcktEO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7cURBQ2E7SUFHL0I7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzswQ0FDRTtJQUdwQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDO29EQUNLO0lBRzFCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7a0RBQ1U7SUFHNUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzs0Q0FDSTtJQUd0QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOzZDQUNLO0lBR3ZCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7K0NBQ087SUFHekI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztrREFDVTtJQUc1QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO2tEQUNVO0lBRzVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7OENBQ007SUFHeEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzsrQ0FDTztJQUd6QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO2dEQUNRO0lBRzFCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7NkNBQ0s7SUFHdkI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztnREFDUTtJQTFDVCxTQUFTO1FBRDdCLE9BQU87T0FDYSxTQUFTLENBeUs3QjtJQUFELGdCQUFDO0NBektELEFBeUtDLENBektzQyxtQkFBUyxHQXlLL0M7a0JBektvQixTQUFTIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IENoYXJhY3RlckNvbnRyb2xsZXIgZnJvbSBcIi4uL0NoYXJhY3Rlci9DaGFyYWN0ZXJDb250cm9sbGVyXCI7XHJcbmltcG9ydCB7IGluc3RhbmNlIH0gZnJvbSBcIi4uL0NvbW1vbi9HYW1lUGxheUluc3RhbmNlXCI7XHJcbmltcG9ydCBHbG9iYWwgZnJvbSBcIi4uL0NvbW1vbi9HbG9iYWxcIjtcclxuaW1wb3J0IEtleUV2ZW50IGZyb20gXCIuLi9Db21tb24vS2V5RXZlbnRcIjtcclxuaW1wb3J0IFNpbmdsZXRvbiBmcm9tIFwiLi4vQ29tbW9uL1NpbmdsZXRvblwiO1xyXG5pbXBvcnQgRW5lbXlDb250cm9sbGVyIGZyb20gXCIuLi9FbmVteS9FbmVteUNvbnRyb2xsZXJcIjtcclxuaW1wb3J0IFN0aWNrTWFuMSBmcm9tIFwiLi9TdGlja01hbjFcIjtcclxuXHJcbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eX0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuZGVjbGFyZSBjb25zdCB3aW5kb3c6IGFueTtcclxuXHJcbkBjY2NsYXNzXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFN0aWNrTWFuMyBleHRlbmRzIFNpbmdsZXRvbjxTdGlja01hbjE+IHtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGpveVN0aWNrRm9sbG93OiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIE1hcDogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLkludGVnZXIpXHJcbiAgICBjb3VudEVuZW15RW5kOiBudW1iZXIgPSAwO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgTXlDaGFyYWN0ZXI6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgR3VpZGU6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgRWZmZWN0OiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIHR4dFN0YXJ0OiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGVuZW15UGFyZW50OiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGJ0bkRvd25sb2FkOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGVuZENhcmQ6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgbmlnaHRFbmQ6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgY2lyY2xlRW5kOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGJ0bkFsbDogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBuRW5lbXlFbmQ6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIGNvdW50RW5lbXk6IG51bWJlciA9IDA7XHJcbiAgICBib29sQ2hlY2tFbmQ6IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgICBib29sY2hlY2tJbnRlcmFjdGlvbjogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgaXJvbnNvdXJjZTogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgbWluZHdvcmtzOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICB2dW5nbGU6IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICBzdXBlcigpO1xyXG4gICAgICAgIFN0aWNrTWFuMS5faW5zdGFuY2UgPSB0aGlzO1xyXG4gICAgfVxyXG5cclxuICAgIG9uRW5hYmxlKCkge1xyXG4gICAgICAgIGluc3RhbmNlLm9uKEtleUV2ZW50LnNjYWxlLCB0aGlzLnNjYWxlLCB0aGlzKTtcclxuICAgICAgICBpbnN0YW5jZS5vbihLZXlFdmVudC5wbHVzRW5lbXksIHRoaXMucGx1c0VuZW15LCB0aGlzKTtcclxuICAgICAgICBpbnN0YW5jZS5vbihLZXlFdmVudC5hY3RpdmVHdWlkZSwgdGhpcy5hY3RpdmVHdWlkZSwgdGhpcyk7XHJcbiAgICB9XHJcblxyXG4gICAgb25EaXNhYmxlKCkge1xyXG4gICAgICAgIGluc3RhbmNlLm9mZihLZXlFdmVudC5zY2FsZSwgdGhpcy5zY2FsZSwgdGhpcyk7XHJcbiAgICAgICAgaW5zdGFuY2Uub2ZmKEtleUV2ZW50LnBsdXNFbmVteSwgdGhpcy5wbHVzRW5lbXksIHRoaXMpO1xyXG4gICAgICAgIGluc3RhbmNlLm9mZihLZXlFdmVudC5hY3RpdmVHdWlkZSwgdGhpcy5hY3RpdmVHdWlkZSwgdGhpcyk7XHJcbiAgICB9XHJcblxyXG4gICAgc3RhcnQoKSB7XHJcbiAgICAgICAgdGhpcy5wbGF5R2FtZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZSAoZHQpIHtcclxuICAgICAgICBpZiAoR2xvYmFsLmJvb2xFbmFibGVUb3VjaCAmJiAhdGhpcy5ib29sY2hlY2tJbnRlcmFjdGlvbikge1xyXG4gICAgICAgICAgICB0aGlzLnR4dFN0YXJ0LmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICBpZiAodGhpcy5pcm9uc291cmNlKSB7XHJcbiAgICAgICAgICAgICAgICB3aW5kb3cuTlVDLnRyaWdnZXIuaW50ZXJhY3Rpb24oKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRoaXMuY291bnRFbmVteSA9PSB0aGlzLmNvdW50RW5lbXlFbmQgJiYgIXRoaXMuYm9vbENoZWNrRW5kKSB7XHJcbiAgICAgICAgICAgIEdsb2JhbC5ib29sU3RhcnRQbGF5ID0gZmFsc2U7XHJcbiAgICAgICAgICAgIHRoaXMuYm9vbENoZWNrRW5kID0gdHJ1ZTtcclxuICAgICAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5TdXBwb3J0RW5kKCk7XHJcbiAgICAgICAgICAgIH0sIDEpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwbGF5R2FtZSgpIHtcclxuICAgICAgICB0aGlzLk15Q2hhcmFjdGVyLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgR2xvYmFsLmJvb2xTdGFydFBsYXkgPSB0cnVlO1xyXG4gICAgICAgIGZvcihsZXQgaSA9IDA7IGkgPCB0aGlzLmVuZW15UGFyZW50LmNoaWxkcmVuQ291bnQ7IGkrKykge1xyXG4gICAgICAgICAgICB0aGlzLmVuZW15UGFyZW50LmNoaWxkcmVuW2ldLmdldENvbXBvbmVudChFbmVteUNvbnRyb2xsZXIpLlN0YXJ0TW92ZSgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBTdXBwb3J0RW5kKCkge1xyXG4gICAgICAgIHRoaXMuc2NoZWR1bGVPbmNlKCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5uaWdodEVuZC5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNpcmNsZUVuZC5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubmlnaHRFbmQuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jaXJjbGVFbmQuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5NeUNoYXJhY3Rlci5vcGFjaXR5ID0gMDtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLkVuZEdhbWUoKTtcclxuICAgICAgICAgICAgICAgIH0sIDAuNik7XHJcbiAgICAgICAgICAgIH0sIDAuNCk7XHJcbiAgICAgICAgICAgIHRoaXMuY2lyY2xlRW5kLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgfSwgMC4yKTtcclxuICAgIH1cclxuXHJcbiAgICBFbmRHYW1lKCkge1xyXG4gICAgICAgIEdsb2JhbC5ib29sZW5kRyA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5qb3lTdGlja0ZvbGxvdy5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLmVuZENhcmQuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLnBsYXlHYW1lKCk7XHJcbiAgICAgICAgdGhpcy5NYXAuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLmVuZENhcmQuY2hpbGRyZW5bMl0uYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLmVuZENhcmQuY2hpbGRyZW5bM10uYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLmVuZENhcmQuY2hpbGRyZW5bNF0uYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICBmb3IobGV0IGkgPSAwOyBpIDwgdGhpcy5uRW5lbXlFbmQuY2hpbGRyZW5Db3VudDsgaSsrKSB7XHJcbiAgICAgICAgICAgIHRoaXMubkVuZW15RW5kLmNoaWxkcmVuW2ldLmdldENvbXBvbmVudChFbmVteUNvbnRyb2xsZXIpLlN0YXJ0TW92ZSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLk15Q2hhcmFjdGVyLm9wYWNpdHkgPSAyNTU7XHJcbiAgICAgICAgdGhpcy5NeUNoYXJhY3Rlci54ID0gLTM7XHJcbiAgICAgICAgdGhpcy5NeUNoYXJhY3Rlci55ID0gLTYwO1xyXG4gICAgICAgIHRoaXMuTXlDaGFyYWN0ZXIuc2NhbGUgPSAzNTA7XHJcbiAgICAgICAgdGhpcy5NeUNoYXJhY3Rlci5ldWxlckFuZ2xlcyA9IG5ldyBjYy5WZWMzKDkwLCAwLCAxODApO1xyXG4gICAgICAgIHRoaXMuTXlDaGFyYWN0ZXIuZ2V0Q29tcG9uZW50KENoYXJhY3RlckNvbnRyb2xsZXIpLkFycm93RGlyZWN0aW9uLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuR3VpZGUuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMuZW5lbXlQYXJlbnQuY2hpbGRyZW5Db3VudDsgaSsrKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZW5lbXlQYXJlbnQuY2hpbGRyZW5baV0uZ2V0Q29tcG9uZW50KEVuZW15Q29udHJvbGxlcikuYm9keURlYXRoLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLmVuZW15UGFyZW50LmNoaWxkcmVuW2ldLmFjdGl2ZSA9IGZhbHNlOztcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5idG5BbGwuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLmJ0bkRvd25sb2FkLmdldENvbXBvbmVudChjYy5CdXR0b24pLmludGVyYWN0YWJsZSA9IGZhbHNlO1xyXG4gICAgICAgIGlmICh0aGlzLm1pbmR3b3Jrcykge1xyXG4gICAgICAgICAgICB3aW5kb3cuZ2FtZUVuZCAmJiB3aW5kb3cuZ2FtZUVuZCgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5pcm9uc291cmNlKSB7XHJcbiAgICAgICAgICAgIHdpbmRvdy5OVUMudHJpZ2dlci5lbmRHYW1lKCd3aW4nKVxyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy52dW5nbGUpIHtcclxuICAgICAgICAgICAgcGFyZW50LnBvc3RNZXNzYWdlKCdjb21wbGV0ZScsICcqJyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHNjYWxlKCkge1xyXG4gICAgICAgIHRoaXMuRWZmZWN0LnNjYWxlID0gMDtcclxuICAgICAgICB0aGlzLkVmZmVjdC5vcGFjaXR5ID0gMjU1O1xyXG4gICAgICAgIHRoaXMuRWZmZWN0LnJ1bkFjdGlvbihjYy5zZXF1ZW5jZShjYy5zY2FsZVRvKDAuMiwgMSkuZWFzaW5nKGNjLmVhc2VCb3VuY2VPdXQoKSksIGNjLmNhbGxGdW5jKCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5FZmZlY3QucnVuQWN0aW9uKGNjLmZhZGVPdXQoMC4xKSk7XHJcbiAgICAgICAgICAgIH0sIDAuMik7XHJcbiAgICAgICAgfSkpKTtcclxuICAgIH1cclxuXHJcbiAgICBwbHVzRW5lbXkoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuY291bnRFbmVteSA8IHRoaXMuY291bnRFbmVteUVuZClcclxuICAgICAgICAgICAgdGhpcy5jb3VudEVuZW15Kys7XHJcbiAgICB9XHJcblxyXG4gICAgYWN0aXZlR3VpZGUoKSB7XHJcbiAgICAgICAgaWYoR2xvYmFsLmJvb2xTdGFydFBsYXkpXHJcbiAgICAgICAgICAgIHRoaXMuR3VpZGUuYWN0aXZlID0gdHJ1ZTtcclxuICAgIH1cclxufVxyXG4iXX0=