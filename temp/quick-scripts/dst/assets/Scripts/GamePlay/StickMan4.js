
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/GamePlay/StickMan4.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'fa7375rkj5NwoiatN7H+fwx', 'StickMan4');
// Scripts/GamePlay/StickMan4.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var KeyEvent_1 = require("../Common/KeyEvent");
var Singleton_1 = require("../Common/Singleton");
var EnemyController_1 = require("../Enemy/EnemyController");
var StickMan1_1 = require("./StickMan1");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var StickMan4 = /** @class */ (function (_super) {
    __extends(StickMan4, _super);
    function StickMan4() {
        var _this = _super.call(this) || this;
        _this.joyStickFollow = null;
        _this.Map = null;
        _this.countEnemyEnd = 0;
        _this.MyCharacter = null;
        _this.Guide = null;
        _this.Effect = null;
        _this.enemyParent = null;
        _this.btnDownload = null;
        _this.endCard = null;
        _this.txtSmasher = null;
        _this.countEnemy = 0;
        _this.boolCheckEnd = false;
        _this.boolcheckInteraction = false;
        _this.ironsource = false;
        _this.mindworks = false;
        _this.vungle = false;
        StickMan1_1.default._instance = _this;
        return _this;
    }
    StickMan4.prototype.onEnable = function () {
        GamePlayInstance_1.instance.on(KeyEvent_1.default.scale, this.scale, this);
        GamePlayInstance_1.instance.on(KeyEvent_1.default.plusEnemy, this.plusEnemy, this);
        GamePlayInstance_1.instance.on(KeyEvent_1.default.activeGuide, this.activeGuide, this);
    };
    StickMan4.prototype.onDisable = function () {
        GamePlayInstance_1.instance.off(KeyEvent_1.default.scale, this.scale, this);
        GamePlayInstance_1.instance.off(KeyEvent_1.default.plusEnemy, this.plusEnemy, this);
        GamePlayInstance_1.instance.off(KeyEvent_1.default.activeGuide, this.activeGuide, this);
    };
    StickMan4.prototype.start = function () {
        cc.audioEngine.play(Global_1.default.soundBG, true, 1);
        this.playGame();
    };
    StickMan4.prototype.update = function (dt) {
        var _this = this;
        if (Global_1.default.boolEnableTouch && !this.boolcheckInteraction) {
            this.txtSmasher.active = false;
            if (this.ironsource) {
                window.NUC.trigger.interaction();
            }
        }
        if (this.countEnemy == this.countEnemyEnd && !this.boolCheckEnd) {
            Global_1.default.boolStartPlay = false;
            this.boolCheckEnd = true;
            this.scheduleOnce(function () {
                _this.EndGame();
            }, 1);
        }
    };
    StickMan4.prototype.playGame = function () {
        this.MyCharacter.active = true;
        Global_1.default.boolStartPlay = true;
        for (var i = 0; i < this.enemyParent.childrenCount; i++) {
            this.enemyParent.children[i].getComponent(EnemyController_1.default).StartMove();
        }
    };
    StickMan4.prototype.EndGame = function () {
        Global_1.default.boolendG = true;
        cc.audioEngine.stopAllEffects();
        this.joyStickFollow.active = false;
        this.Guide.active = false;
        this.endCard.active = true;
        if (this.mindworks) {
            window.gameEnd && window.gameEnd();
        }
        if (this.ironsource) {
            window.NUC.trigger.endGame('win');
        }
        if (this.vungle) {
            parent.postMessage('complete', '*');
        }
    };
    StickMan4.prototype.scale = function () {
        var _this = this;
        this.Effect.scale = 0;
        this.Effect.opacity = 255;
        this.Effect.runAction(cc.sequence(cc.scaleTo(0.2, 1).easing(cc.easeBounceOut()), cc.callFunc(function () {
            _this.scheduleOnce(function () {
                _this.Effect.runAction(cc.fadeOut(0.1));
            }, 0.2);
        })));
    };
    StickMan4.prototype.plusEnemy = function () {
        if (this.countEnemy < this.countEnemyEnd)
            this.countEnemy++;
    };
    StickMan4.prototype.activeGuide = function () {
        if (Global_1.default.boolStartPlay)
            this.Guide.active = true;
    };
    __decorate([
        property(cc.Node)
    ], StickMan4.prototype, "joyStickFollow", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan4.prototype, "Map", void 0);
    __decorate([
        property(cc.Integer)
    ], StickMan4.prototype, "countEnemyEnd", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan4.prototype, "MyCharacter", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan4.prototype, "Guide", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan4.prototype, "Effect", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan4.prototype, "enemyParent", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan4.prototype, "btnDownload", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan4.prototype, "endCard", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan4.prototype, "txtSmasher", void 0);
    StickMan4 = __decorate([
        ccclass
    ], StickMan4);
    return StickMan4;
}(Singleton_1.default));
exports.default = StickMan4;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcR2FtZVBsYXlcXFN0aWNrTWFuNC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQSwrREFBc0Q7QUFDdEQsMkNBQXNDO0FBQ3RDLCtDQUEwQztBQUMxQyxpREFBNEM7QUFDNUMsNERBQXVEO0FBQ3ZELHlDQUFvQztBQUU5QixJQUFBLGtCQUFtQyxFQUFsQyxvQkFBTyxFQUFFLHNCQUF5QixDQUFDO0FBSzFDO0lBQXVDLDZCQUFvQjtJQXdDdkQ7UUFBQSxZQUNJLGlCQUFPLFNBRVY7UUF4Q0Qsb0JBQWMsR0FBWSxJQUFJLENBQUM7UUFHL0IsU0FBRyxHQUFZLElBQUksQ0FBQztRQUdwQixtQkFBYSxHQUFXLENBQUMsQ0FBQztRQUcxQixpQkFBVyxHQUFZLElBQUksQ0FBQztRQUc1QixXQUFLLEdBQVksSUFBSSxDQUFDO1FBR3RCLFlBQU0sR0FBWSxJQUFJLENBQUM7UUFHdkIsaUJBQVcsR0FBWSxJQUFJLENBQUM7UUFHNUIsaUJBQVcsR0FBWSxJQUFJLENBQUM7UUFHNUIsYUFBTyxHQUFZLElBQUksQ0FBQztRQUd4QixnQkFBVSxHQUFZLElBQUksQ0FBQztRQUUzQixnQkFBVSxHQUFXLENBQUMsQ0FBQztRQUN2QixrQkFBWSxHQUFZLEtBQUssQ0FBQztRQUU5QiwwQkFBb0IsR0FBWSxLQUFLLENBQUM7UUFDdEMsZ0JBQVUsR0FBWSxLQUFLLENBQUM7UUFDNUIsZUFBUyxHQUFZLEtBQUssQ0FBQztRQUMzQixZQUFNLEdBQVksS0FBSyxDQUFDO1FBSXBCLG1CQUFTLENBQUMsU0FBUyxHQUFHLEtBQUksQ0FBQzs7SUFDL0IsQ0FBQztJQUVELDRCQUFRLEdBQVI7UUFDSSwyQkFBUSxDQUFDLEVBQUUsQ0FBQyxrQkFBUSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzlDLDJCQUFRLENBQUMsRUFBRSxDQUFDLGtCQUFRLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDdEQsMkJBQVEsQ0FBQyxFQUFFLENBQUMsa0JBQVEsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUM5RCxDQUFDO0lBRUQsNkJBQVMsR0FBVDtRQUNJLDJCQUFRLENBQUMsR0FBRyxDQUFDLGtCQUFRLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDL0MsMkJBQVEsQ0FBQyxHQUFHLENBQUMsa0JBQVEsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUN2RCwyQkFBUSxDQUFDLEdBQUcsQ0FBQyxrQkFBUSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQy9ELENBQUM7SUFFRCx5QkFBSyxHQUFMO1FBQ0ksRUFBRSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsZ0JBQU0sQ0FBQyxPQUFPLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQzdDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNwQixDQUFDO0lBRUQsMEJBQU0sR0FBTixVQUFRLEVBQUU7UUFBVixpQkFlQztRQWRHLElBQUksZ0JBQU0sQ0FBQyxlQUFlLElBQUksQ0FBQyxJQUFJLENBQUMsb0JBQW9CLEVBQUU7WUFDdEQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1lBQy9CLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtnQkFDakIsTUFBTSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLENBQUM7YUFDcEM7U0FDSjtRQUVELElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsYUFBYSxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRTtZQUM3RCxnQkFBTSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7WUFDN0IsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7WUFDekIsSUFBSSxDQUFDLFlBQVksQ0FBQztnQkFDZCxLQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDbkIsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ1Q7SUFDTCxDQUFDO0lBRUQsNEJBQVEsR0FBUjtRQUNJLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUMvQixnQkFBTSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7UUFDNUIsS0FBSSxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3BELElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyx5QkFBZSxDQUFDLENBQUMsU0FBUyxFQUFFLENBQUM7U0FDMUU7SUFDTCxDQUFDO0lBRUQsMkJBQU8sR0FBUDtRQUNJLGdCQUFNLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztRQUN2QixFQUFFLENBQUMsV0FBVyxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ2hDLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUNuQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDMUIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQzNCLElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNoQixNQUFNLENBQUMsT0FBTyxJQUFJLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztTQUN0QztRQUNELElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNqQixNQUFNLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUE7U0FDcEM7UUFDRCxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDYixNQUFNLENBQUMsV0FBVyxDQUFDLFVBQVUsRUFBRSxHQUFHLENBQUMsQ0FBQztTQUN2QztJQUNMLENBQUM7SUFFRCx5QkFBSyxHQUFMO1FBQUEsaUJBUUM7UUFQRyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7UUFDdEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDO1FBQzFCLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxhQUFhLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxRQUFRLENBQUM7WUFDekYsS0FBSSxDQUFDLFlBQVksQ0FBQztnQkFDZCxLQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDM0MsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ1osQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ1QsQ0FBQztJQUVELDZCQUFTLEdBQVQ7UUFDSSxJQUFJLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLGFBQWE7WUFDcEMsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO0lBQzFCLENBQUM7SUFFRCwrQkFBVyxHQUFYO1FBQ0ksSUFBRyxnQkFBTSxDQUFDLGFBQWE7WUFDbkIsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO0lBQ2pDLENBQUM7SUF2SEQ7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztxREFDYTtJQUcvQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOzBDQUNFO0lBR3BCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUM7b0RBQ0s7SUFHMUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztrREFDVTtJQUc1QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOzRDQUNJO0lBR3RCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7NkNBQ0s7SUFHdkI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztrREFDVTtJQUc1QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO2tEQUNVO0lBRzVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7OENBQ007SUFHeEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztpREFDUztJQTlCVixTQUFTO1FBRDdCLE9BQU87T0FDYSxTQUFTLENBMkg3QjtJQUFELGdCQUFDO0NBM0hELEFBMkhDLENBM0hzQyxtQkFBUyxHQTJIL0M7a0JBM0hvQixTQUFTIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IENoYXJhY3RlckNvbnRyb2xsZXIgZnJvbSBcIi4uL0NoYXJhY3Rlci9DaGFyYWN0ZXJDb250cm9sbGVyXCI7XHJcbmltcG9ydCB7IGluc3RhbmNlIH0gZnJvbSBcIi4uL0NvbW1vbi9HYW1lUGxheUluc3RhbmNlXCI7XHJcbmltcG9ydCBHbG9iYWwgZnJvbSBcIi4uL0NvbW1vbi9HbG9iYWxcIjtcclxuaW1wb3J0IEtleUV2ZW50IGZyb20gXCIuLi9Db21tb24vS2V5RXZlbnRcIjtcclxuaW1wb3J0IFNpbmdsZXRvbiBmcm9tIFwiLi4vQ29tbW9uL1NpbmdsZXRvblwiO1xyXG5pbXBvcnQgRW5lbXlDb250cm9sbGVyIGZyb20gXCIuLi9FbmVteS9FbmVteUNvbnRyb2xsZXJcIjtcclxuaW1wb3J0IFN0aWNrTWFuMSBmcm9tIFwiLi9TdGlja01hbjFcIjtcclxuXHJcbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eX0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuZGVjbGFyZSBjb25zdCB3aW5kb3c6IGFueTtcclxuXHJcbkBjY2NsYXNzXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFN0aWNrTWFuNCBleHRlbmRzIFNpbmdsZXRvbjxTdGlja01hbjE+IHtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGpveVN0aWNrRm9sbG93OiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIE1hcDogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLkludGVnZXIpXHJcbiAgICBjb3VudEVuZW15RW5kOiBudW1iZXIgPSAwO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgTXlDaGFyYWN0ZXI6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgR3VpZGU6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgRWZmZWN0OiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGVuZW15UGFyZW50OiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGJ0bkRvd25sb2FkOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGVuZENhcmQ6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgdHh0U21hc2hlcjogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgY291bnRFbmVteTogbnVtYmVyID0gMDtcclxuICAgIGJvb2xDaGVja0VuZDogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuICAgIGJvb2xjaGVja0ludGVyYWN0aW9uOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBpcm9uc291cmNlOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBtaW5kd29ya3M6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIHZ1bmdsZTogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIHN1cGVyKCk7XHJcbiAgICAgICAgU3RpY2tNYW4xLl9pbnN0YW5jZSA9IHRoaXM7XHJcbiAgICB9XHJcblxyXG4gICAgb25FbmFibGUoKSB7XHJcbiAgICAgICAgaW5zdGFuY2Uub24oS2V5RXZlbnQuc2NhbGUsIHRoaXMuc2NhbGUsIHRoaXMpO1xyXG4gICAgICAgIGluc3RhbmNlLm9uKEtleUV2ZW50LnBsdXNFbmVteSwgdGhpcy5wbHVzRW5lbXksIHRoaXMpO1xyXG4gICAgICAgIGluc3RhbmNlLm9uKEtleUV2ZW50LmFjdGl2ZUd1aWRlLCB0aGlzLmFjdGl2ZUd1aWRlLCB0aGlzKTtcclxuICAgIH1cclxuXHJcbiAgICBvbkRpc2FibGUoKSB7XHJcbiAgICAgICAgaW5zdGFuY2Uub2ZmKEtleUV2ZW50LnNjYWxlLCB0aGlzLnNjYWxlLCB0aGlzKTtcclxuICAgICAgICBpbnN0YW5jZS5vZmYoS2V5RXZlbnQucGx1c0VuZW15LCB0aGlzLnBsdXNFbmVteSwgdGhpcyk7XHJcbiAgICAgICAgaW5zdGFuY2Uub2ZmKEtleUV2ZW50LmFjdGl2ZUd1aWRlLCB0aGlzLmFjdGl2ZUd1aWRlLCB0aGlzKTtcclxuICAgIH1cclxuXHJcbiAgICBzdGFydCgpIHtcclxuICAgICAgICBjYy5hdWRpb0VuZ2luZS5wbGF5KEdsb2JhbC5zb3VuZEJHLCB0cnVlLCAxKTtcclxuICAgICAgICB0aGlzLnBsYXlHYW1lKCk7XHJcbiAgICB9XHJcblxyXG4gICAgdXBkYXRlIChkdCkge1xyXG4gICAgICAgIGlmIChHbG9iYWwuYm9vbEVuYWJsZVRvdWNoICYmICF0aGlzLmJvb2xjaGVja0ludGVyYWN0aW9uKSB7XHJcbiAgICAgICAgICAgIHRoaXMudHh0U21hc2hlci5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgaWYgKHRoaXMuaXJvbnNvdXJjZSkge1xyXG4gICAgICAgICAgICAgICAgd2luZG93Lk5VQy50cmlnZ2VyLmludGVyYWN0aW9uKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICh0aGlzLmNvdW50RW5lbXkgPT0gdGhpcy5jb3VudEVuZW15RW5kICYmICF0aGlzLmJvb2xDaGVja0VuZCkge1xyXG4gICAgICAgICAgICBHbG9iYWwuYm9vbFN0YXJ0UGxheSA9IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLmJvb2xDaGVja0VuZCA9IHRydWU7XHJcbiAgICAgICAgICAgIHRoaXMuc2NoZWR1bGVPbmNlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuRW5kR2FtZSgpO1xyXG4gICAgICAgICAgICB9LCAxKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcGxheUdhbWUoKSB7XHJcbiAgICAgICAgdGhpcy5NeUNoYXJhY3Rlci5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgIEdsb2JhbC5ib29sU3RhcnRQbGF5ID0gdHJ1ZTtcclxuICAgICAgICBmb3IobGV0IGkgPSAwOyBpIDwgdGhpcy5lbmVteVBhcmVudC5jaGlsZHJlbkNvdW50OyBpKyspIHtcclxuICAgICAgICAgICAgdGhpcy5lbmVteVBhcmVudC5jaGlsZHJlbltpXS5nZXRDb21wb25lbnQoRW5lbXlDb250cm9sbGVyKS5TdGFydE1vdmUoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgRW5kR2FtZSgpIHtcclxuICAgICAgICBHbG9iYWwuYm9vbGVuZEcgPSB0cnVlO1xyXG4gICAgICAgIGNjLmF1ZGlvRW5naW5lLnN0b3BBbGxFZmZlY3RzKCk7XHJcbiAgICAgICAgdGhpcy5qb3lTdGlja0ZvbGxvdy5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLkd1aWRlLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuZW5kQ2FyZC5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgIGlmICh0aGlzLm1pbmR3b3Jrcykge1xyXG4gICAgICAgICAgICB3aW5kb3cuZ2FtZUVuZCAmJiB3aW5kb3cuZ2FtZUVuZCgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5pcm9uc291cmNlKSB7XHJcbiAgICAgICAgICAgIHdpbmRvdy5OVUMudHJpZ2dlci5lbmRHYW1lKCd3aW4nKVxyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy52dW5nbGUpIHtcclxuICAgICAgICAgICAgcGFyZW50LnBvc3RNZXNzYWdlKCdjb21wbGV0ZScsICcqJyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHNjYWxlKCkge1xyXG4gICAgICAgIHRoaXMuRWZmZWN0LnNjYWxlID0gMDtcclxuICAgICAgICB0aGlzLkVmZmVjdC5vcGFjaXR5ID0gMjU1O1xyXG4gICAgICAgIHRoaXMuRWZmZWN0LnJ1bkFjdGlvbihjYy5zZXF1ZW5jZShjYy5zY2FsZVRvKDAuMiwgMSkuZWFzaW5nKGNjLmVhc2VCb3VuY2VPdXQoKSksIGNjLmNhbGxGdW5jKCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5FZmZlY3QucnVuQWN0aW9uKGNjLmZhZGVPdXQoMC4xKSk7XHJcbiAgICAgICAgICAgIH0sIDAuMik7XHJcbiAgICAgICAgfSkpKTtcclxuICAgIH1cclxuXHJcbiAgICBwbHVzRW5lbXkoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuY291bnRFbmVteSA8IHRoaXMuY291bnRFbmVteUVuZClcclxuICAgICAgICAgICAgdGhpcy5jb3VudEVuZW15Kys7XHJcbiAgICB9XHJcblxyXG4gICAgYWN0aXZlR3VpZGUoKSB7XHJcbiAgICAgICAgaWYoR2xvYmFsLmJvb2xTdGFydFBsYXkpXHJcbiAgICAgICAgICAgIHRoaXMuR3VpZGUuYWN0aXZlID0gdHJ1ZTtcclxuICAgIH1cclxufVxyXG4iXX0=