
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/GamePlay/Roblox2.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '8a4f5AjrJxHWL4OQhM6YREH', 'Roblox2');
// Scripts/GamePlay/Roblox2.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var CharacterController_1 = require("../Character/CharacterController");
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var KeyEvent_1 = require("../Common/KeyEvent");
var Singleton_1 = require("../Common/Singleton");
var EnemyController_1 = require("../Enemy/EnemyController");
var StickMan1_1 = require("./StickMan1");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Roblox2 = /** @class */ (function (_super) {
    __extends(Roblox2, _super);
    function Roblox2() {
        var _this = _super.call(this) || this;
        _this.joyStickFollow = null;
        _this.Map = null;
        _this.countEnemyEnd = 0;
        _this.MyCharacter = null;
        _this.Guide = null;
        _this.Effect = null;
        _this.enemyParent = null;
        _this.btnDownload = null;
        _this.endCard = null;
        _this.nightEnd = null;
        _this.circleEnd = null;
        _this.btnAll = null;
        _this.txtSmasher = null;
        _this.countEnemy = 0;
        _this.boolCheckEnd = false;
        _this.boolcheckInteraction = false;
        _this.ironsource = false;
        _this.mindworks = false;
        _this.vungle = false;
        StickMan1_1.default._instance = _this;
        return _this;
    }
    Roblox2.prototype.onEnable = function () {
        GamePlayInstance_1.instance.on(KeyEvent_1.default.scale, this.scale, this);
        GamePlayInstance_1.instance.on(KeyEvent_1.default.plusEnemy, this.plusEnemy, this);
        GamePlayInstance_1.instance.on(KeyEvent_1.default.activeGuide, this.activeGuide, this);
    };
    Roblox2.prototype.onDisable = function () {
        GamePlayInstance_1.instance.off(KeyEvent_1.default.scale, this.scale, this);
        GamePlayInstance_1.instance.off(KeyEvent_1.default.plusEnemy, this.plusEnemy, this);
        GamePlayInstance_1.instance.off(KeyEvent_1.default.activeGuide, this.activeGuide, this);
    };
    Roblox2.prototype.start = function () {
        cc.audioEngine.play(Global_1.default.soundBG, true, 1);
        this.playGame();
    };
    Roblox2.prototype.update = function (dt) {
        var _this = this;
        if (Global_1.default.boolEnableTouch && !this.boolcheckInteraction) {
            this.txtSmasher.active = false;
            if (this.ironsource) {
                window.NUC.trigger.interaction();
            }
        }
        if (this.countEnemy == this.countEnemyEnd && !this.boolCheckEnd) {
            Global_1.default.boolStartPlay = false;
            this.boolCheckEnd = true;
            this.scheduleOnce(function () {
                _this.SupportEnd();
            }, 1);
        }
    };
    Roblox2.prototype.playGame = function () {
        this.MyCharacter.active = true;
        Global_1.default.boolStartPlay = true;
        for (var i = 0; i < this.enemyParent.childrenCount; i++) {
            this.enemyParent.children[i].getComponent(EnemyController_1.default).StartMove();
        }
    };
    Roblox2.prototype.SupportEnd = function () {
        var _this = this;
        this.scheduleOnce(function () {
            _this.nightEnd.active = true;
            _this.scheduleOnce(function () {
                _this.circleEnd.active = true;
                _this.scheduleOnce(function () {
                    _this.nightEnd.active = false;
                    _this.circleEnd.active = false;
                    _this.MyCharacter.opacity = 0;
                    _this.EndGame();
                }, 0.6);
            }, 0.4);
            _this.circleEnd.active = true;
        }, 0.2);
    };
    Roblox2.prototype.EndGame = function () {
        Global_1.default.boolendG = true;
        cc.audioEngine.stopAllEffects();
        this.joyStickFollow.active = false;
        this.endCard.active = true;
        this.Map.active = true;
        this.endCard.children[2].active = true;
        this.endCard.children[3].active = true;
        this.endCard.children[4].active = true;
        this.MyCharacter.opacity = 255;
        this.MyCharacter.x = 0;
        this.MyCharacter.y = -60;
        this.MyCharacter.scale = 15;
        this.MyCharacter.eulerAngles = new cc.Vec3(90, 0, 180);
        this.MyCharacter.getComponent(CharacterController_1.default).ArrowDirection.active = false;
        this.Guide.active = true;
        for (var i = 0; i < this.enemyParent.childrenCount; i++) {
            this.enemyParent.children[i].getComponent(EnemyController_1.default).bodyDeath.active = false;
            this.enemyParent.children[i].active = false;
            ;
        }
        this.btnAll.active = true;
        this.btnDownload.getComponent(cc.Button).interactable = false;
        if (this.mindworks) {
            window.gameEnd && window.gameEnd();
        }
        if (this.ironsource) {
            window.NUC.trigger.endGame('win');
        }
        if (this.vungle) {
            parent.postMessage('complete', '*');
        }
    };
    Roblox2.prototype.scale = function () {
        var _this = this;
        this.Effect.scale = 0;
        this.Effect.opacity = 255;
        this.Effect.runAction(cc.sequence(cc.scaleTo(0.2, 1).easing(cc.easeBounceOut()), cc.callFunc(function () {
            _this.scheduleOnce(function () {
                _this.Effect.runAction(cc.fadeOut(0.1));
            }, 0.2);
        })));
    };
    Roblox2.prototype.plusEnemy = function () {
        if (this.countEnemy < this.countEnemyEnd)
            this.countEnemy++;
    };
    Roblox2.prototype.activeGuide = function () {
        if (Global_1.default.boolStartPlay)
            this.Guide.active = true;
    };
    __decorate([
        property(cc.Node)
    ], Roblox2.prototype, "joyStickFollow", void 0);
    __decorate([
        property(cc.Node)
    ], Roblox2.prototype, "Map", void 0);
    __decorate([
        property(cc.Integer)
    ], Roblox2.prototype, "countEnemyEnd", void 0);
    __decorate([
        property(cc.Node)
    ], Roblox2.prototype, "MyCharacter", void 0);
    __decorate([
        property(cc.Node)
    ], Roblox2.prototype, "Guide", void 0);
    __decorate([
        property(cc.Node)
    ], Roblox2.prototype, "Effect", void 0);
    __decorate([
        property(cc.Node)
    ], Roblox2.prototype, "enemyParent", void 0);
    __decorate([
        property(cc.Node)
    ], Roblox2.prototype, "btnDownload", void 0);
    __decorate([
        property(cc.Node)
    ], Roblox2.prototype, "endCard", void 0);
    __decorate([
        property(cc.Node)
    ], Roblox2.prototype, "nightEnd", void 0);
    __decorate([
        property(cc.Node)
    ], Roblox2.prototype, "circleEnd", void 0);
    __decorate([
        property(cc.Node)
    ], Roblox2.prototype, "btnAll", void 0);
    __decorate([
        property(cc.Node)
    ], Roblox2.prototype, "txtSmasher", void 0);
    Roblox2 = __decorate([
        ccclass
    ], Roblox2);
    return Roblox2;
}(Singleton_1.default));
exports.default = Roblox2;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcR2FtZVBsYXlcXFJvYmxveDIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsd0VBQW1FO0FBQ25FLCtEQUFzRDtBQUN0RCwyQ0FBc0M7QUFDdEMsK0NBQTBDO0FBQzFDLGlEQUE0QztBQUM1Qyw0REFBdUQ7QUFDdkQseUNBQW9DO0FBRTlCLElBQUEsa0JBQW1DLEVBQWxDLG9CQUFPLEVBQUUsc0JBQXlCLENBQUM7QUFLMUM7SUFBcUMsMkJBQW9CO0lBaURyRDtRQUFBLFlBQ0ksaUJBQU8sU0FFVjtRQWpERCxvQkFBYyxHQUFZLElBQUksQ0FBQztRQUcvQixTQUFHLEdBQVksSUFBSSxDQUFDO1FBR3BCLG1CQUFhLEdBQVcsQ0FBQyxDQUFDO1FBRzFCLGlCQUFXLEdBQVksSUFBSSxDQUFDO1FBRzVCLFdBQUssR0FBWSxJQUFJLENBQUM7UUFHdEIsWUFBTSxHQUFZLElBQUksQ0FBQztRQUd2QixpQkFBVyxHQUFZLElBQUksQ0FBQztRQUc1QixpQkFBVyxHQUFZLElBQUksQ0FBQztRQUc1QixhQUFPLEdBQVksSUFBSSxDQUFDO1FBR3hCLGNBQVEsR0FBWSxJQUFJLENBQUM7UUFHekIsZUFBUyxHQUFZLElBQUksQ0FBQztRQUcxQixZQUFNLEdBQVksSUFBSSxDQUFDO1FBR3ZCLGdCQUFVLEdBQVksSUFBSSxDQUFDO1FBRTNCLGdCQUFVLEdBQVcsQ0FBQyxDQUFDO1FBQ3ZCLGtCQUFZLEdBQVksS0FBSyxDQUFDO1FBRTlCLDBCQUFvQixHQUFZLEtBQUssQ0FBQztRQUN0QyxnQkFBVSxHQUFZLEtBQUssQ0FBQztRQUM1QixlQUFTLEdBQVksS0FBSyxDQUFDO1FBQzNCLFlBQU0sR0FBWSxLQUFLLENBQUM7UUFJcEIsbUJBQVMsQ0FBQyxTQUFTLEdBQUcsS0FBSSxDQUFDOztJQUMvQixDQUFDO0lBRUQsMEJBQVEsR0FBUjtRQUNJLDJCQUFRLENBQUMsRUFBRSxDQUFDLGtCQUFRLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDOUMsMkJBQVEsQ0FBQyxFQUFFLENBQUMsa0JBQVEsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUN0RCwyQkFBUSxDQUFDLEVBQUUsQ0FBQyxrQkFBUSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQzlELENBQUM7SUFFRCwyQkFBUyxHQUFUO1FBQ0ksMkJBQVEsQ0FBQyxHQUFHLENBQUMsa0JBQVEsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztRQUMvQywyQkFBUSxDQUFDLEdBQUcsQ0FBQyxrQkFBUSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3ZELDJCQUFRLENBQUMsR0FBRyxDQUFDLGtCQUFRLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDL0QsQ0FBQztJQUVELHVCQUFLLEdBQUw7UUFDSSxFQUFFLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxnQkFBTSxDQUFDLE9BQU8sRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDN0MsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQ3BCLENBQUM7SUFFRCx3QkFBTSxHQUFOLFVBQVEsRUFBRTtRQUFWLGlCQWVDO1FBZEcsSUFBSSxnQkFBTSxDQUFDLGVBQWUsSUFBSSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsRUFBRTtZQUN0RCxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFDL0IsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO2dCQUNqQixNQUFNLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsQ0FBQzthQUNwQztTQUNKO1FBRUQsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxhQUFhLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFO1lBQzdELGdCQUFNLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztZQUM3QixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztZQUN6QixJQUFJLENBQUMsWUFBWSxDQUFDO2dCQUNkLEtBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztZQUN0QixDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDVDtJQUNMLENBQUM7SUFFRCwwQkFBUSxHQUFSO1FBQ0ksSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQy9CLGdCQUFNLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztRQUM1QixLQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDcEQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLHlCQUFlLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQztTQUMxRTtJQUNMLENBQUM7SUFFRCw0QkFBVSxHQUFWO1FBQUEsaUJBY0M7UUFiRyxJQUFJLENBQUMsWUFBWSxDQUFDO1lBQ2QsS0FBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1lBQzVCLEtBQUksQ0FBQyxZQUFZLENBQUM7Z0JBQ2QsS0FBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO2dCQUM3QixLQUFJLENBQUMsWUFBWSxDQUFDO29CQUNkLEtBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztvQkFDN0IsS0FBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO29CQUM5QixLQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUM7b0JBQzdCLEtBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztnQkFDbkIsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQ1osQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQ1IsS0FBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ2pDLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztJQUNaLENBQUM7SUFFRCx5QkFBTyxHQUFQO1FBQ0ksZ0JBQU0sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLEVBQUUsQ0FBQyxXQUFXLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDaEMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ25DLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUMzQixJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDdkIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUN2QyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDdkMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDO1FBQy9CLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN2QixJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQztRQUN6QixJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7UUFDNUIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEdBQUcsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDdkQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsNkJBQW1CLENBQUMsQ0FBQyxjQUFjLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUNqRixJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDekIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3JELElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyx5QkFBZSxDQUFDLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFDcEYsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztZQUFBLENBQUM7U0FDaEQ7UUFDRCxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDMUIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7UUFDOUQsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2hCLE1BQU0sQ0FBQyxPQUFPLElBQUksTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO1NBQ3RDO1FBQ0QsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ2pCLE1BQU0sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQTtTQUNwQztRQUNELElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNiLE1BQU0sQ0FBQyxXQUFXLENBQUMsVUFBVSxFQUFFLEdBQUcsQ0FBQyxDQUFDO1NBQ3ZDO0lBQ0wsQ0FBQztJQUVELHVCQUFLLEdBQUw7UUFBQSxpQkFRQztRQVBHLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztRQUN0QixJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUM7UUFDMUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLGFBQWEsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLFFBQVEsQ0FBQztZQUN6RixLQUFJLENBQUMsWUFBWSxDQUFDO2dCQUNkLEtBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUMzQyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDWixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDVCxDQUFDO0lBRUQsMkJBQVMsR0FBVDtRQUNJLElBQUksSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsYUFBYTtZQUNwQyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDMUIsQ0FBQztJQUVELDZCQUFXLEdBQVg7UUFDSSxJQUFHLGdCQUFNLENBQUMsYUFBYTtZQUNuQixJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7SUFDakMsQ0FBQztJQWhLRDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO21EQUNhO0lBRy9CO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7d0NBQ0U7SUFHcEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQztrREFDSztJQUcxQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO2dEQUNVO0lBRzVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7MENBQ0k7SUFHdEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzsyQ0FDSztJQUd2QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO2dEQUNVO0lBRzVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7Z0RBQ1U7SUFHNUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzs0Q0FDTTtJQUd4QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOzZDQUNPO0lBR3pCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7OENBQ1E7SUFHMUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzsyQ0FDSztJQUd2QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOytDQUNTO0lBdkNWLE9BQU87UUFEM0IsT0FBTztPQUNhLE9BQU8sQ0FvSzNCO0lBQUQsY0FBQztDQXBLRCxBQW9LQyxDQXBLb0MsbUJBQVMsR0FvSzdDO2tCQXBLb0IsT0FBTyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBDaGFyYWN0ZXJDb250cm9sbGVyIGZyb20gXCIuLi9DaGFyYWN0ZXIvQ2hhcmFjdGVyQ29udHJvbGxlclwiO1xyXG5pbXBvcnQgeyBpbnN0YW5jZSB9IGZyb20gXCIuLi9Db21tb24vR2FtZVBsYXlJbnN0YW5jZVwiO1xyXG5pbXBvcnQgR2xvYmFsIGZyb20gXCIuLi9Db21tb24vR2xvYmFsXCI7XHJcbmltcG9ydCBLZXlFdmVudCBmcm9tIFwiLi4vQ29tbW9uL0tleUV2ZW50XCI7XHJcbmltcG9ydCBTaW5nbGV0b24gZnJvbSBcIi4uL0NvbW1vbi9TaW5nbGV0b25cIjtcclxuaW1wb3J0IEVuZW15Q29udHJvbGxlciBmcm9tIFwiLi4vRW5lbXkvRW5lbXlDb250cm9sbGVyXCI7XHJcbmltcG9ydCBTdGlja01hbjEgZnJvbSBcIi4vU3RpY2tNYW4xXCI7XHJcblxyXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcclxuXHJcbmRlY2xhcmUgY29uc3Qgd2luZG93OiBhbnk7XHJcblxyXG5AY2NjbGFzc1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBSb2Jsb3gyIGV4dGVuZHMgU2luZ2xldG9uPFN0aWNrTWFuMT4ge1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgam95U3RpY2tGb2xsb3c6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgTWFwOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuSW50ZWdlcilcclxuICAgIGNvdW50RW5lbXlFbmQ6IG51bWJlciA9IDA7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBNeUNoYXJhY3RlcjogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBHdWlkZTogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBFZmZlY3Q6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgZW5lbXlQYXJlbnQ6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgYnRuRG93bmxvYWQ6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgZW5kQ2FyZDogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBuaWdodEVuZDogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBjaXJjbGVFbmQ6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgYnRuQWxsOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIHR4dFNtYXNoZXI6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIGNvdW50RW5lbXk6IG51bWJlciA9IDA7XHJcbiAgICBib29sQ2hlY2tFbmQ6IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgICBib29sY2hlY2tJbnRlcmFjdGlvbjogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgaXJvbnNvdXJjZTogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgbWluZHdvcmtzOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICB2dW5nbGU6IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICBzdXBlcigpO1xyXG4gICAgICAgIFN0aWNrTWFuMS5faW5zdGFuY2UgPSB0aGlzO1xyXG4gICAgfVxyXG5cclxuICAgIG9uRW5hYmxlKCkge1xyXG4gICAgICAgIGluc3RhbmNlLm9uKEtleUV2ZW50LnNjYWxlLCB0aGlzLnNjYWxlLCB0aGlzKTtcclxuICAgICAgICBpbnN0YW5jZS5vbihLZXlFdmVudC5wbHVzRW5lbXksIHRoaXMucGx1c0VuZW15LCB0aGlzKTtcclxuICAgICAgICBpbnN0YW5jZS5vbihLZXlFdmVudC5hY3RpdmVHdWlkZSwgdGhpcy5hY3RpdmVHdWlkZSwgdGhpcyk7XHJcbiAgICB9XHJcblxyXG4gICAgb25EaXNhYmxlKCkge1xyXG4gICAgICAgIGluc3RhbmNlLm9mZihLZXlFdmVudC5zY2FsZSwgdGhpcy5zY2FsZSwgdGhpcyk7XHJcbiAgICAgICAgaW5zdGFuY2Uub2ZmKEtleUV2ZW50LnBsdXNFbmVteSwgdGhpcy5wbHVzRW5lbXksIHRoaXMpO1xyXG4gICAgICAgIGluc3RhbmNlLm9mZihLZXlFdmVudC5hY3RpdmVHdWlkZSwgdGhpcy5hY3RpdmVHdWlkZSwgdGhpcyk7XHJcbiAgICB9XHJcblxyXG4gICAgc3RhcnQoKSB7XHJcbiAgICAgICAgY2MuYXVkaW9FbmdpbmUucGxheShHbG9iYWwuc291bmRCRywgdHJ1ZSwgMSk7XHJcbiAgICAgICAgdGhpcy5wbGF5R2FtZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZSAoZHQpIHtcclxuICAgICAgICBpZiAoR2xvYmFsLmJvb2xFbmFibGVUb3VjaCAmJiAhdGhpcy5ib29sY2hlY2tJbnRlcmFjdGlvbikge1xyXG4gICAgICAgICAgICB0aGlzLnR4dFNtYXNoZXIuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmlyb25zb3VyY2UpIHtcclxuICAgICAgICAgICAgICAgIHdpbmRvdy5OVUMudHJpZ2dlci5pbnRlcmFjdGlvbigpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAodGhpcy5jb3VudEVuZW15ID09IHRoaXMuY291bnRFbmVteUVuZCAmJiAhdGhpcy5ib29sQ2hlY2tFbmQpIHtcclxuICAgICAgICAgICAgR2xvYmFsLmJvb2xTdGFydFBsYXkgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5ib29sQ2hlY2tFbmQgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLlN1cHBvcnRFbmQoKTtcclxuICAgICAgICAgICAgfSwgMSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHBsYXlHYW1lKCkge1xyXG4gICAgICAgIHRoaXMuTXlDaGFyYWN0ZXIuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICBHbG9iYWwuYm9vbFN0YXJ0UGxheSA9IHRydWU7XHJcbiAgICAgICAgZm9yKGxldCBpID0gMDsgaSA8IHRoaXMuZW5lbXlQYXJlbnQuY2hpbGRyZW5Db3VudDsgaSsrKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZW5lbXlQYXJlbnQuY2hpbGRyZW5baV0uZ2V0Q29tcG9uZW50KEVuZW15Q29udHJvbGxlcikuU3RhcnRNb3ZlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIFN1cHBvcnRFbmQoKSB7XHJcbiAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLm5pZ2h0RW5kLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgICAgIHRoaXMuc2NoZWR1bGVPbmNlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuY2lyY2xlRW5kLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5uaWdodEVuZC5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNpcmNsZUVuZC5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLk15Q2hhcmFjdGVyLm9wYWNpdHkgPSAwO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuRW5kR2FtZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgMC42KTtcclxuICAgICAgICAgICAgfSwgMC40KTtcclxuICAgICAgICAgICAgdGhpcy5jaXJjbGVFbmQuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB9LCAwLjIpO1xyXG4gICAgfVxyXG5cclxuICAgIEVuZEdhbWUoKSB7XHJcbiAgICAgICAgR2xvYmFsLmJvb2xlbmRHID0gdHJ1ZTtcclxuICAgICAgICBjYy5hdWRpb0VuZ2luZS5zdG9wQWxsRWZmZWN0cygpO1xyXG4gICAgICAgIHRoaXMuam95U3RpY2tGb2xsb3cuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5lbmRDYXJkLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5NYXAuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLmVuZENhcmQuY2hpbGRyZW5bMl0uYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLmVuZENhcmQuY2hpbGRyZW5bM10uYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLmVuZENhcmQuY2hpbGRyZW5bNF0uYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLk15Q2hhcmFjdGVyLm9wYWNpdHkgPSAyNTU7XHJcbiAgICAgICAgdGhpcy5NeUNoYXJhY3Rlci54ID0gMDtcclxuICAgICAgICB0aGlzLk15Q2hhcmFjdGVyLnkgPSAtNjA7XHJcbiAgICAgICAgdGhpcy5NeUNoYXJhY3Rlci5zY2FsZSA9IDE1O1xyXG4gICAgICAgIHRoaXMuTXlDaGFyYWN0ZXIuZXVsZXJBbmdsZXMgPSBuZXcgY2MuVmVjMyg5MCwgMCwgMTgwKTtcclxuICAgICAgICB0aGlzLk15Q2hhcmFjdGVyLmdldENvbXBvbmVudChDaGFyYWN0ZXJDb250cm9sbGVyKS5BcnJvd0RpcmVjdGlvbi5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLkd1aWRlLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLmVuZW15UGFyZW50LmNoaWxkcmVuQ291bnQ7IGkrKykge1xyXG4gICAgICAgICAgICB0aGlzLmVuZW15UGFyZW50LmNoaWxkcmVuW2ldLmdldENvbXBvbmVudChFbmVteUNvbnRyb2xsZXIpLmJvZHlEZWF0aC5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5lbmVteVBhcmVudC5jaGlsZHJlbltpXS5hY3RpdmUgPSBmYWxzZTs7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuYnRuQWxsLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5idG5Eb3dubG9hZC5nZXRDb21wb25lbnQoY2MuQnV0dG9uKS5pbnRlcmFjdGFibGUgPSBmYWxzZTtcclxuICAgICAgICBpZiAodGhpcy5taW5kd29ya3MpIHtcclxuICAgICAgICAgICAgd2luZG93LmdhbWVFbmQgJiYgd2luZG93LmdhbWVFbmQoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMuaXJvbnNvdXJjZSkge1xyXG4gICAgICAgICAgICB3aW5kb3cuTlVDLnRyaWdnZXIuZW5kR2FtZSgnd2luJylcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMudnVuZ2xlKSB7XHJcbiAgICAgICAgICAgIHBhcmVudC5wb3N0TWVzc2FnZSgnY29tcGxldGUnLCAnKicpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBzY2FsZSgpIHtcclxuICAgICAgICB0aGlzLkVmZmVjdC5zY2FsZSA9IDA7XHJcbiAgICAgICAgdGhpcy5FZmZlY3Qub3BhY2l0eSA9IDI1NTtcclxuICAgICAgICB0aGlzLkVmZmVjdC5ydW5BY3Rpb24oY2Muc2VxdWVuY2UoY2Muc2NhbGVUbygwLjIsIDEpLmVhc2luZyhjYy5lYXNlQm91bmNlT3V0KCkpLCBjYy5jYWxsRnVuYygoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuc2NoZWR1bGVPbmNlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuRWZmZWN0LnJ1bkFjdGlvbihjYy5mYWRlT3V0KDAuMSkpO1xyXG4gICAgICAgICAgICB9LCAwLjIpO1xyXG4gICAgICAgIH0pKSk7XHJcbiAgICB9XHJcblxyXG4gICAgcGx1c0VuZW15KCkge1xyXG4gICAgICAgIGlmICh0aGlzLmNvdW50RW5lbXkgPCB0aGlzLmNvdW50RW5lbXlFbmQpXHJcbiAgICAgICAgICAgIHRoaXMuY291bnRFbmVteSsrO1xyXG4gICAgfVxyXG5cclxuICAgIGFjdGl2ZUd1aWRlKCkge1xyXG4gICAgICAgIGlmKEdsb2JhbC5ib29sU3RhcnRQbGF5KVxyXG4gICAgICAgICAgICB0aGlzLkd1aWRlLmFjdGl2ZSA9IHRydWU7XHJcbiAgICB9XHJcbn1cclxuIl19