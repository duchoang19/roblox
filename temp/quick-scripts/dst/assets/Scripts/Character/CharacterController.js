
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Character/CharacterController.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '8c229TU8nVEg7patjhCarLi', 'CharacterController');
// Scripts/Character/CharacterController.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var EnumDefine_1 = require("../Common/EnumDefine");
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var KeyEvent_1 = require("../Common/KeyEvent");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var CharacterController = /** @class */ (function (_super) {
    __extends(CharacterController, _super);
    function CharacterController() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.ArrowDirection = null;
        _this.JoystickFollow = null;
        _this.actionType = EnumDefine_1.ActionType.IDLE;
        _this.speed = 100;
        _this.Weapon = null;
        _this.timeAnim = 0;
        _this.placeWeapon = null;
        _this.effectSmoke = null;
        _this.clampLeft = 0;
        _this.clampRight = 0;
        _this.clampBottom = 0;
        _this.clampTop = 0;
        _this.level = 0;
        _this.scale = 0;
        // @property(MapController)
        // mapController: MapController = null;
        _this.originalSpeed = 0;
        _this.gameplayInstance = null;
        _this.boolPlaySoundFoot = false;
        return _this;
    }
    // onLoad () {}
    CharacterController.prototype.start = function () {
        this.gameplayInstance = GamePlayInstance_1.default.Instance(GamePlayInstance_1.default);
        Global_1.default.touchPos = cc.v2(0, 0);
        this.originalSpeed = this.speed;
    };
    CharacterController.prototype.update = function () {
        var _this = this;
        if (Global_1.default.boolEnableTouch && !Global_1.default.boolStartAttacking) {
            this.node.x = cc.misc.clampf(this.node.x, this.clampLeft, this.clampRight);
            this.node.y = cc.misc.clampf(this.node.y, this.clampBottom, this.clampTop);
            // let radius = Math.sqrt(this.clampRight * this.clampRight - this.node.x * this.node.x);
            // this.node.x = cc.misc.clampf(this.node.x, -radius, radius);
            // this.node.y = cc.misc.clampf(this.node.y, -radius, radius);  
            // this.mapController.ListSquareObjects.forEach(element => {
            //     if(element.OnTheLeft(this.node))
            //         {            
            //             this.node.x = cc.misc.clampf(this.node.x, this.clampLeft, element.positionLeft.x - 1);
            //             this.node.y = cc.misc.clampf(this.node.y, this.clampBottom, this.clampTop);
            //         }
            //     else if(element.OnTheRight(this.node))
            //         {
            //             this.node.x = cc.misc.clampf(this.node.x, element.positionRight.x + 1, this.clampRight);
            //             this.node.y = cc.misc.clampf(this.node.y, this.clampBottom, this.clampTop);
            //         }
            //     else if(element.OnTheTop(this.node))
            //         {
            //             this.node.x = cc.misc.clampf(this.node.x, this.clampLeft, this.clampRight);
            //             this.node.y = cc.misc.clampf(this.node.y, element.positionTop.y + 1, this.clampTop);
            //         }
            //     else if(element.OnTheBottom(this.node))
            //         {
            //             this.node.x = cc.misc.clampf(this.node.x, this.clampLeft, this.clampRight);
            //             this.node.y = cc.misc.clampf(this.node.y, this.clampBottom,  element.positionBottom.y - 1);
            //         }
            //         else
            //         {
            //             this.node.x = cc.misc.clampf(this.node.x, this.clampLeft, this.clampRight);
            //             this.node.y = cc.misc.clampf(this.node.y, this.clampBottom, this.clampTop);
            //         }
            // });
            var PosForX = this.node.getPosition();
            var PosForY = this.node.getPosition();
            PosForX.addSelf(Global_1.default.touchPos.mul(this.speed / 100));
            PosForY.addSelf(Global_1.default.touchPos.mul(this.speed / 100));
            this.node.x = PosForX.x;
            this.node.y = PosForY.y;
            if (!this.boolPlaySoundFoot) {
                this.boolPlaySoundFoot = true;
                cc.audioEngine.playEffect(Global_1.default.soundFootStep, false);
                this.scheduleOnce(function () {
                    _this.boolPlaySoundFoot = false;
                }, 0.3);
            }
            var r = Math.atan2(Global_1.default.touchPos.y, Global_1.default.touchPos.x);
            var degree = r * 180 / (Math.PI);
            degree = 360 - degree + 90;
            this.node.is3DNode = true;
            this.node.eulerAngles = new cc.Vec3(-90, 180, degree);
        }
    };
    CharacterController.prototype.Attacking = function () {
        var _this = this;
        Global_1.default.boolStartAttacking = true;
        Global_1.default.boolCheckAttacking = false;
        if (this.node.name == "MyDeadpool") {
            this.node.getComponent(cc.SkeletonAnimation).play("Armature|sword_attack");
        }
        else {
            this.node.getComponent(cc.SkeletonAnimation).play("Armature|hammer_attack");
        }
        this.scheduleOnce(function () {
            Global_1.default.boolCheckAttacking = true;
            Global_1.default.boolCheckAttacked = true;
            _this.spawnEffectSmoke(_this.effectSmoke);
            // cc.audioEngine.playEffect(Global.soundAttack, false);
            cc.audioEngine.playEffect(Global_1.default.soundKatana, false);
        }, 0.5);
        this.scheduleOnce(function () {
            GamePlayInstance_1.instance.emit(KeyEvent_1.default.activeGuide);
            Global_1.default.boolStartAttacking = false;
            if (_this.node.name == "MyDeadpool") {
                _this.node.getComponent(cc.SkeletonAnimation).play("Armature|sword_idle");
            }
            else {
                _this.node.getComponent(cc.SkeletonAnimation).play("Armature|hammer_idle");
            }
        }, this.timeAnim);
    };
    CharacterController.prototype.LevelUpPlayer = function () {
        var tween = new cc.Tween().to(0.4, { scale: this.node.scale + this.scale });
        tween.target(this.node).start();
        if (this.level < 4)
            this.level++;
    };
    CharacterController.prototype.spawnEffectSmoke = function (smoke) {
        var smk = cc.instantiate(smoke);
        smk.parent = cc.Canvas.instance.node;
        var pos = this.node.convertToWorldSpaceAR(this.placeWeapon.getPosition());
        pos = cc.Canvas.instance.node.convertToNodeSpaceAR(pos);
        smk.x = pos.x;
        smk.y = pos.y;
        smk.z = 0;
    };
    __decorate([
        property(cc.Node)
    ], CharacterController.prototype, "ArrowDirection", void 0);
    __decorate([
        property(cc.Node)
    ], CharacterController.prototype, "JoystickFollow", void 0);
    __decorate([
        property({ type: cc.Enum(EnumDefine_1.ActionType) })
    ], CharacterController.prototype, "actionType", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "speed", void 0);
    __decorate([
        property(cc.Node)
    ], CharacterController.prototype, "Weapon", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "timeAnim", void 0);
    __decorate([
        property(cc.Node)
    ], CharacterController.prototype, "placeWeapon", void 0);
    __decorate([
        property(cc.Prefab)
    ], CharacterController.prototype, "effectSmoke", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "clampLeft", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "clampRight", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "clampBottom", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "clampTop", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "level", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "scale", void 0);
    CharacterController = __decorate([
        ccclass
    ], CharacterController);
    return CharacterController;
}(cc.Component));
exports.default = CharacterController;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ2hhcmFjdGVyXFxDaGFyYWN0ZXJDb250cm9sbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNBLG1EQUFrRDtBQUNsRCwrREFBd0U7QUFDeEUsMkNBQXNDO0FBQ3RDLCtDQUEwQztBQUVwQyxJQUFBLGtCQUFxQyxFQUFuQyxvQkFBTyxFQUFFLHNCQUEwQixDQUFDO0FBRzVDO0lBQWlELHVDQUFZO0lBRDdEO1FBQUEscUVBcUtDO1FBaktHLG9CQUFjLEdBQVksSUFBSSxDQUFDO1FBRy9CLG9CQUFjLEdBQVksSUFBSSxDQUFDO1FBRy9CLGdCQUFVLEdBQWUsdUJBQVUsQ0FBQyxJQUFJLENBQUM7UUFHekMsV0FBSyxHQUFXLEdBQUcsQ0FBQztRQUdwQixZQUFNLEdBQVksSUFBSSxDQUFDO1FBR3ZCLGNBQVEsR0FBVyxDQUFDLENBQUM7UUFHckIsaUJBQVcsR0FBWSxJQUFJLENBQUM7UUFHNUIsaUJBQVcsR0FBYyxJQUFJLENBQUM7UUFHOUIsZUFBUyxHQUFXLENBQUMsQ0FBQztRQUd0QixnQkFBVSxHQUFXLENBQUMsQ0FBQztRQUd2QixpQkFBVyxHQUFXLENBQUMsQ0FBQztRQUd4QixjQUFRLEdBQVcsQ0FBQyxDQUFDO1FBR3JCLFdBQUssR0FBVyxDQUFDLENBQUM7UUFHbEIsV0FBSyxHQUFXLENBQUMsQ0FBQztRQUVsQiwyQkFBMkI7UUFDM0IsdUNBQXVDO1FBRXZDLG1CQUFhLEdBQVcsQ0FBQyxDQUFDO1FBRTFCLHNCQUFnQixHQUFxQixJQUFJLENBQUM7UUFDMUMsdUJBQWlCLEdBQVksS0FBSyxDQUFDOztJQWtIdkMsQ0FBQztJQWhIRyxlQUFlO0lBRWYsbUNBQUssR0FBTDtRQUNJLElBQUksQ0FBQyxnQkFBZ0IsR0FBRywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQztRQUNwRSxnQkFBTSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUM5QixJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7SUFDcEMsQ0FBQztJQUVELG9DQUFNLEdBQU47UUFBQSxpQkF1REM7UUF0REcsSUFBSSxnQkFBTSxDQUFDLGVBQWUsSUFBSSxDQUFDLGdCQUFNLENBQUMsa0JBQWtCLEVBQUU7WUFDdEQsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDM0UsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDM0UseUZBQXlGO1lBQ3pGLDhEQUE4RDtZQUM5RCxnRUFBZ0U7WUFFaEUsNERBQTREO1lBQzVELHVDQUF1QztZQUN2Qyx3QkFBd0I7WUFDeEIscUdBQXFHO1lBQ3JHLDBGQUEwRjtZQUMxRixZQUFZO1lBQ1osNkNBQTZDO1lBQzdDLFlBQVk7WUFDWix1R0FBdUc7WUFDdkcsMEZBQTBGO1lBQzFGLFlBQVk7WUFDWiwyQ0FBMkM7WUFDM0MsWUFBWTtZQUNaLDBGQUEwRjtZQUMxRixtR0FBbUc7WUFDbkcsWUFBWTtZQUNaLDhDQUE4QztZQUM5QyxZQUFZO1lBQ1osMEZBQTBGO1lBQzFGLDBHQUEwRztZQUMxRyxZQUFZO1lBQ1osZUFBZTtZQUNmLFlBQVk7WUFDWiwwRkFBMEY7WUFDMUYsMEZBQTBGO1lBQzFGLFlBQVk7WUFDWixNQUFNO1lBRU4sSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUN0QyxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ3RDLE9BQU8sQ0FBQyxPQUFPLENBQUMsZ0JBQU0sQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUN2RCxPQUFPLENBQUMsT0FBTyxDQUFDLGdCQUFNLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDdkQsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUN4QixJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQ3hCLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUU7Z0JBQ3pCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUM7Z0JBQzlCLEVBQUUsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLGdCQUFNLENBQUMsYUFBYSxFQUFFLEtBQUssQ0FBQyxDQUFDO2dCQUN2RCxJQUFJLENBQUMsWUFBWSxDQUFDO29CQUNkLEtBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUM7Z0JBQ25DLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQzthQUNYO1lBQ0QsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxnQkFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUUsZ0JBQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDekQsSUFBSSxNQUFNLEdBQUcsQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUNqQyxNQUFNLEdBQUcsR0FBRyxHQUFHLE1BQU0sR0FBRyxFQUFFLENBQUM7WUFDM0IsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1lBQzFCLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBTSxDQUFDLENBQUM7U0FDekQ7SUFDTCxDQUFDO0lBRUQsdUNBQVMsR0FBVDtRQUFBLGlCQTZCQztRQTVCRyxnQkFBTSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQztRQUNqQyxnQkFBTSxDQUFDLGtCQUFrQixHQUFHLEtBQUssQ0FBQztRQUVsQyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLFlBQVksRUFBRTtZQUNoQyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsQ0FBQztTQUM5RTthQUNJO1lBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLENBQUM7U0FDL0U7UUFFRCxJQUFJLENBQUMsWUFBWSxDQUFDO1lBQ2QsZ0JBQU0sQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUM7WUFDakMsZ0JBQU0sQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUM7WUFDaEMsS0FBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUN4Qyx3REFBd0Q7WUFDeEQsRUFBRSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsZ0JBQU0sQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDekQsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBRVIsSUFBSSxDQUFDLFlBQVksQ0FBQztZQUNkLDJCQUFRLENBQUMsSUFBSSxDQUFDLGtCQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDcEMsZ0JBQU0sQ0FBQyxrQkFBa0IsR0FBRyxLQUFLLENBQUM7WUFDbEMsSUFBSSxLQUFJLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxZQUFZLEVBQUU7Z0JBQ2hDLEtBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO2FBQzVFO2lCQUNJO2dCQUNELEtBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO2FBQzdFO1FBQ0wsQ0FBQyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUN0QixDQUFDO0lBRUQsMkNBQWEsR0FBYjtRQUNJLElBQUksS0FBSyxHQUFHLElBQUksRUFBRSxDQUFDLEtBQUssRUFBRSxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUM7UUFDNUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDaEMsSUFBSSxJQUFJLENBQUMsS0FBSyxHQUFHLENBQUM7WUFDZCxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDckIsQ0FBQztJQUVELDhDQUFnQixHQUFoQixVQUFpQixLQUFnQjtRQUM3QixJQUFJLEdBQUcsR0FBRyxFQUFFLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ2hDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDO1FBQ3JDLElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO1FBQzFFLEdBQUcsR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDeEQsR0FBRyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQ2QsR0FBRyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQ2QsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDZCxDQUFDO0lBaEtEO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7K0RBQ2E7SUFHL0I7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzsrREFDYTtJQUcvQjtRQURDLFFBQVEsQ0FBQyxFQUFFLElBQUksRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLHVCQUFVLENBQUMsRUFBRSxDQUFDOzJEQUNDO0lBR3pDO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUM7c0RBQ0Q7SUFHcEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzt1REFDSztJQUd2QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDO3lEQUNBO0lBR3JCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7NERBQ1U7SUFHNUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQzs0REFDVTtJQUc5QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDOzBEQUNDO0lBR3RCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUM7MkRBQ0U7SUFHdkI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQzs0REFDRztJQUd4QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDO3lEQUNBO0lBR3JCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUM7c0RBQ0g7SUFHbEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQztzREFDSDtJQTFDRCxtQkFBbUI7UUFEdkMsT0FBTztPQUNhLG1CQUFtQixDQW9LdkM7SUFBRCwwQkFBQztDQXBLRCxBQW9LQyxDQXBLZ0QsRUFBRSxDQUFDLFNBQVMsR0FvSzVEO2tCQXBLb0IsbUJBQW1CIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IE1hcENvbnRyb2xsZXIgZnJvbSBcIi4uL0NvbGxpZGVyT2JqL01hcENvbnRyb2xsZXJcIjtcclxuaW1wb3J0IHsgQWN0aW9uVHlwZSB9IGZyb20gXCIuLi9Db21tb24vRW51bURlZmluZVwiO1xyXG5pbXBvcnQgR2FtZVBsYXlJbnN0YW5jZSwgeyBpbnN0YW5jZSB9IGZyb20gXCIuLi9Db21tb24vR2FtZVBsYXlJbnN0YW5jZVwiO1xyXG5pbXBvcnQgR2xvYmFsIGZyb20gXCIuLi9Db21tb24vR2xvYmFsXCI7XHJcbmltcG9ydCBLZXlFdmVudCBmcm9tIFwiLi4vQ29tbW9uL0tleUV2ZW50XCI7XHJcblxyXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQ2hhcmFjdGVyQ29udHJvbGxlciBleHRlbmRzIGNjLkNvbXBvbmVudCB7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBBcnJvd0RpcmVjdGlvbjogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBKb3lzdGlja0ZvbGxvdzogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KHsgdHlwZTogY2MuRW51bShBY3Rpb25UeXBlKSB9KVxyXG4gICAgYWN0aW9uVHlwZTogQWN0aW9uVHlwZSA9IEFjdGlvblR5cGUuSURMRTtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuSW50ZWdlcilcclxuICAgIHNwZWVkOiBudW1iZXIgPSAxMDA7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBXZWFwb246IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5JbnRlZ2VyKVxyXG4gICAgdGltZUFuaW06IG51bWJlciA9IDA7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBwbGFjZVdlYXBvbjogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLlByZWZhYilcclxuICAgIGVmZmVjdFNtb2tlOiBjYy5QcmVmYWIgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5JbnRlZ2VyKVxyXG4gICAgY2xhbXBMZWZ0OiBudW1iZXIgPSAwO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5JbnRlZ2VyKVxyXG4gICAgY2xhbXBSaWdodDogbnVtYmVyID0gMDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuSW50ZWdlcilcclxuICAgIGNsYW1wQm90dG9tOiBudW1iZXIgPSAwO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5JbnRlZ2VyKVxyXG4gICAgY2xhbXBUb3A6IG51bWJlciA9IDA7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLkludGVnZXIpXHJcbiAgICBsZXZlbDogbnVtYmVyID0gMDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuSW50ZWdlcilcclxuICAgIHNjYWxlOiBudW1iZXIgPSAwO1xyXG5cclxuICAgIC8vIEBwcm9wZXJ0eShNYXBDb250cm9sbGVyKVxyXG4gICAgLy8gbWFwQ29udHJvbGxlcjogTWFwQ29udHJvbGxlciA9IG51bGw7XHJcblxyXG4gICAgb3JpZ2luYWxTcGVlZDogbnVtYmVyID0gMDtcclxuXHJcbiAgICBnYW1lcGxheUluc3RhbmNlOiBHYW1lUGxheUluc3RhbmNlID0gbnVsbDtcclxuICAgIGJvb2xQbGF5U291bmRGb290OiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgLy8gb25Mb2FkICgpIHt9XHJcblxyXG4gICAgc3RhcnQoKSB7XHJcbiAgICAgICAgdGhpcy5nYW1lcGxheUluc3RhbmNlID0gR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKTtcclxuICAgICAgICBHbG9iYWwudG91Y2hQb3MgPSBjYy52MigwLCAwKTtcclxuICAgICAgICB0aGlzLm9yaWdpbmFsU3BlZWQgPSB0aGlzLnNwZWVkO1xyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZSgpIHtcclxuICAgICAgICBpZiAoR2xvYmFsLmJvb2xFbmFibGVUb3VjaCAmJiAhR2xvYmFsLmJvb2xTdGFydEF0dGFja2luZykge1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUueCA9IGNjLm1pc2MuY2xhbXBmKHRoaXMubm9kZS54LCB0aGlzLmNsYW1wTGVmdCwgdGhpcy5jbGFtcFJpZ2h0KTtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLnkgPSBjYy5taXNjLmNsYW1wZih0aGlzLm5vZGUueSwgdGhpcy5jbGFtcEJvdHRvbSwgdGhpcy5jbGFtcFRvcCk7XHJcbiAgICAgICAgICAgIC8vIGxldCByYWRpdXMgPSBNYXRoLnNxcnQodGhpcy5jbGFtcFJpZ2h0ICogdGhpcy5jbGFtcFJpZ2h0IC0gdGhpcy5ub2RlLnggKiB0aGlzLm5vZGUueCk7XHJcbiAgICAgICAgICAgIC8vIHRoaXMubm9kZS54ID0gY2MubWlzYy5jbGFtcGYodGhpcy5ub2RlLngsIC1yYWRpdXMsIHJhZGl1cyk7XHJcbiAgICAgICAgICAgIC8vIHRoaXMubm9kZS55ID0gY2MubWlzYy5jbGFtcGYodGhpcy5ub2RlLnksIC1yYWRpdXMsIHJhZGl1cyk7ICBcclxuXHJcbiAgICAgICAgICAgIC8vIHRoaXMubWFwQ29udHJvbGxlci5MaXN0U3F1YXJlT2JqZWN0cy5mb3JFYWNoKGVsZW1lbnQgPT4ge1xyXG4gICAgICAgICAgICAvLyAgICAgaWYoZWxlbWVudC5PblRoZUxlZnQodGhpcy5ub2RlKSlcclxuICAgICAgICAgICAgLy8gICAgICAgICB7ICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIC8vICAgICAgICAgICAgIHRoaXMubm9kZS54ID0gY2MubWlzYy5jbGFtcGYodGhpcy5ub2RlLngsIHRoaXMuY2xhbXBMZWZ0LCBlbGVtZW50LnBvc2l0aW9uTGVmdC54IC0gMSk7XHJcbiAgICAgICAgICAgIC8vICAgICAgICAgICAgIHRoaXMubm9kZS55ID0gY2MubWlzYy5jbGFtcGYodGhpcy5ub2RlLnksIHRoaXMuY2xhbXBCb3R0b20sIHRoaXMuY2xhbXBUb3ApO1xyXG4gICAgICAgICAgICAvLyAgICAgICAgIH1cclxuICAgICAgICAgICAgLy8gICAgIGVsc2UgaWYoZWxlbWVudC5PblRoZVJpZ2h0KHRoaXMubm9kZSkpXHJcbiAgICAgICAgICAgIC8vICAgICAgICAge1xyXG4gICAgICAgICAgICAvLyAgICAgICAgICAgICB0aGlzLm5vZGUueCA9IGNjLm1pc2MuY2xhbXBmKHRoaXMubm9kZS54LCBlbGVtZW50LnBvc2l0aW9uUmlnaHQueCArIDEsIHRoaXMuY2xhbXBSaWdodCk7XHJcbiAgICAgICAgICAgIC8vICAgICAgICAgICAgIHRoaXMubm9kZS55ID0gY2MubWlzYy5jbGFtcGYodGhpcy5ub2RlLnksIHRoaXMuY2xhbXBCb3R0b20sIHRoaXMuY2xhbXBUb3ApO1xyXG4gICAgICAgICAgICAvLyAgICAgICAgIH1cclxuICAgICAgICAgICAgLy8gICAgIGVsc2UgaWYoZWxlbWVudC5PblRoZVRvcCh0aGlzLm5vZGUpKVxyXG4gICAgICAgICAgICAvLyAgICAgICAgIHtcclxuICAgICAgICAgICAgLy8gICAgICAgICAgICAgdGhpcy5ub2RlLnggPSBjYy5taXNjLmNsYW1wZih0aGlzLm5vZGUueCwgdGhpcy5jbGFtcExlZnQsIHRoaXMuY2xhbXBSaWdodCk7XHJcbiAgICAgICAgICAgIC8vICAgICAgICAgICAgIHRoaXMubm9kZS55ID0gY2MubWlzYy5jbGFtcGYodGhpcy5ub2RlLnksIGVsZW1lbnQucG9zaXRpb25Ub3AueSArIDEsIHRoaXMuY2xhbXBUb3ApO1xyXG4gICAgICAgICAgICAvLyAgICAgICAgIH1cclxuICAgICAgICAgICAgLy8gICAgIGVsc2UgaWYoZWxlbWVudC5PblRoZUJvdHRvbSh0aGlzLm5vZGUpKVxyXG4gICAgICAgICAgICAvLyAgICAgICAgIHtcclxuICAgICAgICAgICAgLy8gICAgICAgICAgICAgdGhpcy5ub2RlLnggPSBjYy5taXNjLmNsYW1wZih0aGlzLm5vZGUueCwgdGhpcy5jbGFtcExlZnQsIHRoaXMuY2xhbXBSaWdodCk7XHJcbiAgICAgICAgICAgIC8vICAgICAgICAgICAgIHRoaXMubm9kZS55ID0gY2MubWlzYy5jbGFtcGYodGhpcy5ub2RlLnksIHRoaXMuY2xhbXBCb3R0b20sICBlbGVtZW50LnBvc2l0aW9uQm90dG9tLnkgLSAxKTtcclxuICAgICAgICAgICAgLy8gICAgICAgICB9XHJcbiAgICAgICAgICAgIC8vICAgICAgICAgZWxzZVxyXG4gICAgICAgICAgICAvLyAgICAgICAgIHtcclxuICAgICAgICAgICAgLy8gICAgICAgICAgICAgdGhpcy5ub2RlLnggPSBjYy5taXNjLmNsYW1wZih0aGlzLm5vZGUueCwgdGhpcy5jbGFtcExlZnQsIHRoaXMuY2xhbXBSaWdodCk7XHJcbiAgICAgICAgICAgIC8vICAgICAgICAgICAgIHRoaXMubm9kZS55ID0gY2MubWlzYy5jbGFtcGYodGhpcy5ub2RlLnksIHRoaXMuY2xhbXBCb3R0b20sIHRoaXMuY2xhbXBUb3ApO1xyXG4gICAgICAgICAgICAvLyAgICAgICAgIH1cclxuICAgICAgICAgICAgLy8gfSk7XHJcblxyXG4gICAgICAgICAgICBsZXQgUG9zRm9yWCA9IHRoaXMubm9kZS5nZXRQb3NpdGlvbigpO1xyXG4gICAgICAgICAgICBsZXQgUG9zRm9yWSA9IHRoaXMubm9kZS5nZXRQb3NpdGlvbigpO1xyXG4gICAgICAgICAgICBQb3NGb3JYLmFkZFNlbGYoR2xvYmFsLnRvdWNoUG9zLm11bCh0aGlzLnNwZWVkIC8gMTAwKSk7XHJcbiAgICAgICAgICAgIFBvc0ZvclkuYWRkU2VsZihHbG9iYWwudG91Y2hQb3MubXVsKHRoaXMuc3BlZWQgLyAxMDApKTtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLnggPSBQb3NGb3JYLng7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS55ID0gUG9zRm9yWS55O1xyXG4gICAgICAgICAgICBpZiAoIXRoaXMuYm9vbFBsYXlTb3VuZEZvb3QpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuYm9vbFBsYXlTb3VuZEZvb3QgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgY2MuYXVkaW9FbmdpbmUucGxheUVmZmVjdChHbG9iYWwuc291bmRGb290U3RlcCwgZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuYm9vbFBsYXlTb3VuZEZvb3QgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIH0sIDAuMyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdmFyIHIgPSBNYXRoLmF0YW4yKEdsb2JhbC50b3VjaFBvcy55LCBHbG9iYWwudG91Y2hQb3MueCk7XHJcbiAgICAgICAgICAgIHZhciBkZWdyZWUgPSByICogMTgwIC8gKE1hdGguUEkpO1xyXG4gICAgICAgICAgICBkZWdyZWUgPSAzNjAgLSBkZWdyZWUgKyA5MDtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLmlzM0ROb2RlID0gdHJ1ZTtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLmV1bGVyQW5nbGVzID0gbmV3IGNjLlZlYzMoLTkwLCAxODAsIGRlZ3JlZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIEF0dGFja2luZygpIHtcclxuICAgICAgICBHbG9iYWwuYm9vbFN0YXJ0QXR0YWNraW5nID0gdHJ1ZTtcclxuICAgICAgICBHbG9iYWwuYm9vbENoZWNrQXR0YWNraW5nID0gZmFsc2U7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLm5vZGUubmFtZSA9PSBcIk15RGVhZHBvb2xcIikge1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlNrZWxldG9uQW5pbWF0aW9uKS5wbGF5KFwiQXJtYXR1cmV8c3dvcmRfYXR0YWNrXCIpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLmdldENvbXBvbmVudChjYy5Ta2VsZXRvbkFuaW1hdGlvbikucGxheShcIkFybWF0dXJlfGhhbW1lcl9hdHRhY2tcIik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgIEdsb2JhbC5ib29sQ2hlY2tBdHRhY2tpbmcgPSB0cnVlO1xyXG4gICAgICAgICAgICBHbG9iYWwuYm9vbENoZWNrQXR0YWNrZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLnNwYXduRWZmZWN0U21va2UodGhpcy5lZmZlY3RTbW9rZSk7XHJcbiAgICAgICAgICAgIC8vIGNjLmF1ZGlvRW5naW5lLnBsYXlFZmZlY3QoR2xvYmFsLnNvdW5kQXR0YWNrLCBmYWxzZSk7XHJcbiAgICAgICAgICAgIGNjLmF1ZGlvRW5naW5lLnBsYXlFZmZlY3QoR2xvYmFsLnNvdW5kS2F0YW5hLCBmYWxzZSk7XHJcbiAgICAgICAgfSwgMC41KTtcclxuXHJcbiAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICBpbnN0YW5jZS5lbWl0KEtleUV2ZW50LmFjdGl2ZUd1aWRlKTtcclxuICAgICAgICAgICAgR2xvYmFsLmJvb2xTdGFydEF0dGFja2luZyA9IGZhbHNlO1xyXG4gICAgICAgICAgICBpZiAodGhpcy5ub2RlLm5hbWUgPT0gXCJNeURlYWRwb29sXCIpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubm9kZS5nZXRDb21wb25lbnQoY2MuU2tlbGV0b25BbmltYXRpb24pLnBsYXkoXCJBcm1hdHVyZXxzd29yZF9pZGxlXCIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLmdldENvbXBvbmVudChjYy5Ta2VsZXRvbkFuaW1hdGlvbikucGxheShcIkFybWF0dXJlfGhhbW1lcl9pZGxlXCIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSwgdGhpcy50aW1lQW5pbSk7XHJcbiAgICB9XHJcblxyXG4gICAgTGV2ZWxVcFBsYXllcigpIHtcclxuICAgICAgICB2YXIgdHdlZW4gPSBuZXcgY2MuVHdlZW4oKS50bygwLjQsIHsgc2NhbGU6IHRoaXMubm9kZS5zY2FsZSArIHRoaXMuc2NhbGUgfSk7XHJcbiAgICAgICAgdHdlZW4udGFyZ2V0KHRoaXMubm9kZSkuc3RhcnQoKTtcclxuICAgICAgICBpZiAodGhpcy5sZXZlbCA8IDQpXHJcbiAgICAgICAgICAgIHRoaXMubGV2ZWwrKztcclxuICAgIH1cclxuXHJcbiAgICBzcGF3bkVmZmVjdFNtb2tlKHNtb2tlOiBjYy5QcmVmYWIpIHtcclxuICAgICAgICBsZXQgc21rID0gY2MuaW5zdGFudGlhdGUoc21va2UpO1xyXG4gICAgICAgIHNtay5wYXJlbnQgPSBjYy5DYW52YXMuaW5zdGFuY2Uubm9kZTtcclxuICAgICAgICBsZXQgcG9zID0gdGhpcy5ub2RlLmNvbnZlcnRUb1dvcmxkU3BhY2VBUih0aGlzLnBsYWNlV2VhcG9uLmdldFBvc2l0aW9uKCkpO1xyXG4gICAgICAgIHBvcyA9IGNjLkNhbnZhcy5pbnN0YW5jZS5ub2RlLmNvbnZlcnRUb05vZGVTcGFjZUFSKHBvcyk7XHJcbiAgICAgICAgc21rLnggPSBwb3MueDtcclxuICAgICAgICBzbWsueSA9IHBvcy55O1xyXG4gICAgICAgIHNtay56ID0gMDtcclxuICAgIH1cclxufVxyXG4iXX0=