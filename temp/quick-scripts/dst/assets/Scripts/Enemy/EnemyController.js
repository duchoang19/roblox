
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Enemy/EnemyController.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '6fb44ar/vdHCqZo6WpsF2GK', 'EnemyController');
// Scripts/Enemy/EnemyController.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var CharacterController_1 = require("../Character/CharacterController");
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var KeyEvent_1 = require("../Common/KeyEvent");
var Random_1 = require("../Common/Random");
var Utility_1 = require("../Common/Utility");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var EnemyController = /** @class */ (function (_super) {
    __extends(EnemyController, _super);
    function EnemyController() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.clampLeft = 0;
        _this.clampRight = 0;
        _this.clampTop = 0;
        _this.clampBottom = 0;
        _this.PointShoot = null;
        _this.bodyDeath = null;
        _this.effectBlood = null;
        _this.posBlood = null;
        _this.weapon = null;
        _this.moveX = 0;
        _this.moveY = 0;
        _this.moveZ = 0;
        _this.degree = 0;
        _this.boolCheckAttacking = false;
        _this.boolCheckAttacked = false;
        _this.boolEnemyDeath = false;
        _this.checkFollowPlayer = false;
        _this.checkDeath = false;
        return _this;
    }
    EnemyController.prototype.onLoad = function () {
        // this.node.getComponent(cc.BoxCollider3D).enabled = true;
    };
    EnemyController.prototype.update = function () {
        if (!this.checkDeath) {
            if (Global_1.default.boolStartAttacking && Global_1.default.boolCheckAttacking) {
                var pos1 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.convertToWorldSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.children[0].getPosition()));
                var pos2 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.convertToWorldSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.children[1].getPosition()));
                var pos3 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.convertToWorldSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.children[2].getPosition()));
                var direction1 = cc.v2(pos1.x - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.x, pos1.y - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.y);
                var direction2 = cc.v2(pos2.x - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.x, pos2.y - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.y);
                var degree = direction1.signAngle(direction2);
                degree = cc.misc.radiansToDegrees(degree);
                var posEnemy = cc.v2(this.node.x - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.x, this.node.y - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.y);
                var degreeWithPos1 = posEnemy.signAngle(direction1);
                degreeWithPos1 = cc.misc.radiansToDegrees(degreeWithPos1);
                var degreeWithPos2 = posEnemy.signAngle(direction2);
                degreeWithPos2 = cc.misc.radiansToDegrees(degreeWithPos2);
                var realNeed = 0;
                degreeWithPos1 = Math.abs(degreeWithPos1);
                degreeWithPos2 = Math.abs(degreeWithPos2);
                if (degreeWithPos1 > degreeWithPos2) {
                    realNeed = degreeWithPos1;
                }
                else {
                    realNeed = degreeWithPos2;
                }
                var distance = Utility_1.default.Instance(Utility_1.default).Distance(cc.v2(this.node.x, this.node.y), cc.v2(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.x, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.y));
                var maxDistance = Utility_1.default.Instance(Utility_1.default).Distance(cc.v2(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.x, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.y), cc.v2(pos3.x, pos3.y));
                if (Math.abs(realNeed) < degree) {
                    if (distance < maxDistance) {
                        if (Global_1.default.boolCheckAttacked && !this.boolEnemyDeath) {
                            GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.getComponent(CharacterController_1.default).LevelUpPlayer();
                            // this.DestroyByKatana();
                            this.DestroyByHammer();
                            this.checkDeath = true;
                            Global_1.default.boolCheckAttacked = false;
                            Global_1.default.boolCheckAttacking = false;
                        }
                    }
                }
            }
        }
    };
    EnemyController.prototype.StartMove = function () {
        var _this = this;
        this.schedule(function () {
            _this.moveEnemy();
        }, 6.5, cc.macro.REPEAT_FOREVER, 0.1);
    };
    EnemyController.prototype.DestroyByHammer = function () {
        this.boolEnemyDeath = true;
        cc.audioEngine.playEffect(Global_1.default.soundScream, false);
        GamePlayInstance_1.instance.emit(KeyEvent_1.default.plusEnemy);
        GamePlayInstance_1.instance.emit(KeyEvent_1.default.scale);
        this.node.stopAllActions();
        this.unscheduleAllCallbacks();
        this.node.getComponent(cc.SkeletonAnimation).stop();
        this.node.eulerAngles = new cc.Vec3(0, 0, -this.node.eulerAngles.z);
        this.weapon.active = false;
        this.node.scaleZ = 12;
        this.node.z = this.moveZ + 1;
        this.checkDeath = true;
        this.SpawnerBlood();
        this.SpawnerEffectBlood(this.node.x, this.node.y, this.node.z + 2);
    };
    EnemyController.prototype.DestroyByKatana = function () {
        this.boolEnemyDeath = true;
        cc.audioEngine.playEffect(Global_1.default.soundScream, false);
        GamePlayInstance_1.instance.emit(KeyEvent_1.default.plusEnemy);
        GamePlayInstance_1.instance.emit(KeyEvent_1.default.scale);
        this.checkDeath = true;
        this.SpawnerBodyDeath(0.7);
        this.SpawnerEffectBlood(this.node.x, this.node.y, this.node.z);
    };
    EnemyController.prototype.SpawnerBlood = function () {
        this.bodyDeath.active = true;
        var pos = this.node.convertToWorldSpaceAR(this.posBlood.getPosition());
        pos = cc.Canvas.instance.node.convertToNodeSpaceAR(pos);
        this.bodyDeath.setPosition(pos.x, pos.y, 0);
        this.bodyDeath.eulerAngles = cc.v3(90, 0, this.node.eulerAngles.z);
    };
    EnemyController.prototype.SpawnerEffectBlood = function (x, y, z) {
        var EffectBlood = cc.instantiate(this.effectBlood);
        EffectBlood.parent = cc.Canvas.instance.node;
        EffectBlood.x = x;
        EffectBlood.y = y;
        EffectBlood.z = z + 1;
    };
    EnemyController.prototype.SpawnerBodyDeath = function (timing) {
        var _this = this;
        this.bodyDeath.active = true;
        this.bodyDeath.setPosition(this.node.x, this.node.y, this.node.z);
        this.bodyDeath.eulerAngles = cc.v3(-90, 180, this.node.eulerAngles.z);
        this.node.opacity = 0;
        this.scheduleOnce(function () {
            _this.bodyDeath.getComponent(cc.SkeletonAnimation).stop();
            _this.node.destroy();
        }, timing);
    };
    EnemyController.prototype.moveEnemy = function () {
        var _this = this;
        this.node.getComponent(cc.SkeletonAnimation).play("Armature|sword_run");
        if (this.checkFollowPlayer) {
            var Distance = Utility_1.default.Instance(Utility_1.default).Distance(cc.v2(this.node.x, this.node.y), cc.v2(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.x, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.y));
            var duration = Distance / 21;
            this.moveX = GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.x;
            this.moveY = GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.y;
            this.degree = this.betweenDegree(cc.v2(this.node.x, this.node.y), cc.v2(this.moveX, this.moveY)) - 90;
            var tween = new cc.Tween().to(duration, { position: cc.v3(this.moveX, this.moveY, this.moveZ) }).call(function () {
                _this.EnemyAttack();
            });
            tween.target(this.node).start();
        }
        else {
            this.moveX = Random_1.default.Instance(Random_1.default).RandomRange(this.clampLeft, this.clampRight);
            this.moveY = Random_1.default.Instance(Random_1.default).RandomRange(this.clampBottom, this.clampTop);
            while (Utility_1.default.Instance(Utility_1.default).Distance(cc.v2(this.node.x, this.node.y), cc.v2(this.moveX, this.moveY)) < 100) {
                this.moveX = Random_1.default.Instance(Random_1.default).RandomRange(this.clampLeft, this.clampRight);
                this.moveY = Random_1.default.Instance(Random_1.default).RandomRange(this.clampBottom, this.clampTop);
            }
            this.degree = this.betweenDegree(cc.v2(this.node.x, this.node.y), cc.v2(this.moveX, this.moveY)) + 90;
            var tween = new cc.Tween().to(6, { position: cc.v3(this.moveX, this.moveY, this.moveZ) }).call(function () {
                _this.node.getComponent(cc.SkeletonAnimation).play("Armature|sword_idle");
            });
            tween.target(this.node).start();
        }
        this.node.runAction(cc.rotate3DTo(0.2, cc.v3(-90, -180, -this.degree)));
    };
    EnemyController.prototype.betweenDegree = function (dirVec, comVec) {
        var angleDeg = Math.atan2(dirVec.y - comVec.y, dirVec.x - comVec.x) * 180 / Math.PI;
        return angleDeg;
    };
    EnemyController.prototype.EnemyAttack = function () {
        var _this = this;
        this.boolCheckAttacking = false;
        if (this.node.name == "MyVenom") {
            this.node.getComponent(cc.SkeletonAnimation).play("AmongUs_Attack");
        }
        else {
            this.node.getComponent(cc.SkeletonAnimation).play("Hammer Attack");
        }
        this.scheduleOnce(function () {
            _this.boolCheckAttacking = true;
            _this.boolCheckAttacked = true;
            cc.audioEngine.playEffect(Global_1.default.soundAttack, false);
        }, 0.5);
        this.scheduleOnce(function () {
            Global_1.default.boolStartAttacking = false;
            _this.node.getComponent(cc.SkeletonAnimation).play("Among_US_idle");
        }, 1);
    };
    __decorate([
        property(cc.Integer)
    ], EnemyController.prototype, "clampLeft", void 0);
    __decorate([
        property(cc.Integer)
    ], EnemyController.prototype, "clampRight", void 0);
    __decorate([
        property(cc.Integer)
    ], EnemyController.prototype, "clampTop", void 0);
    __decorate([
        property(cc.Integer)
    ], EnemyController.prototype, "clampBottom", void 0);
    __decorate([
        property(cc.Node)
    ], EnemyController.prototype, "PointShoot", void 0);
    __decorate([
        property(cc.Node)
    ], EnemyController.prototype, "bodyDeath", void 0);
    __decorate([
        property(cc.Prefab)
    ], EnemyController.prototype, "effectBlood", void 0);
    __decorate([
        property(cc.Node)
    ], EnemyController.prototype, "posBlood", void 0);
    __decorate([
        property(cc.Node)
    ], EnemyController.prototype, "weapon", void 0);
    EnemyController = __decorate([
        ccclass
    ], EnemyController);
    return EnemyController;
}(cc.Component));
exports.default = EnemyController;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcRW5lbXlcXEVuZW15Q29udHJvbGxlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSx3RUFBbUU7QUFFbkUsK0RBQXdFO0FBQ3hFLDJDQUFzQztBQUN0QywrQ0FBMEM7QUFDMUMsMkNBQXNDO0FBQ3RDLDZDQUF3QztBQUVsQyxJQUFBLGtCQUFxQyxFQUFuQyxvQkFBTyxFQUFFLHNCQUEwQixDQUFDO0FBRTVDO0lBQTZDLG1DQUFZO0lBRHpEO1FBQUEscUVBdU1DO1FBbk1HLGVBQVMsR0FBVyxDQUFDLENBQUM7UUFHdEIsZ0JBQVUsR0FBVyxDQUFDLENBQUM7UUFHdkIsY0FBUSxHQUFXLENBQUMsQ0FBQztRQUdyQixpQkFBVyxHQUFXLENBQUMsQ0FBQztRQUd4QixnQkFBVSxHQUFZLElBQUksQ0FBQztRQUczQixlQUFTLEdBQVksSUFBSSxDQUFDO1FBRzFCLGlCQUFXLEdBQWMsSUFBSSxDQUFDO1FBRzlCLGNBQVEsR0FBWSxJQUFJLENBQUM7UUFHekIsWUFBTSxHQUFZLElBQUksQ0FBQztRQUV2QixXQUFLLEdBQVcsQ0FBQyxDQUFDO1FBQ2xCLFdBQUssR0FBVyxDQUFDLENBQUM7UUFDbEIsV0FBSyxHQUFXLENBQUMsQ0FBQztRQUNsQixZQUFNLEdBQVcsQ0FBQyxDQUFDO1FBQ25CLHdCQUFrQixHQUFZLEtBQUssQ0FBQztRQUNwQyx1QkFBaUIsR0FBWSxLQUFLLENBQUM7UUFDbkMsb0JBQWMsR0FBWSxLQUFLLENBQUM7UUFDaEMsdUJBQWlCLEdBQVksS0FBSyxDQUFDO1FBQ25DLGdCQUFVLEdBQVksS0FBSyxDQUFDOztJQWlLaEMsQ0FBQztJQS9KRyxnQ0FBTSxHQUFOO1FBQ0ksMkRBQTJEO0lBQy9ELENBQUM7SUFFRCxnQ0FBTSxHQUFOO1FBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDbEIsSUFBSSxnQkFBTSxDQUFDLGtCQUFrQixJQUFJLGdCQUFNLENBQUMsa0JBQWtCLEVBQUU7Z0JBQ3hELElBQUksSUFBSSxHQUFHLEVBQUUsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLHFCQUFxQixDQUFDLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDNU8sSUFBSSxJQUFJLEdBQUcsRUFBRSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUMsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUM1TyxJQUFJLElBQUksR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxxQkFBcUIsQ0FBQywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQzVPLElBQUksVUFBVSxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQyxHQUFHLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2pMLElBQUksVUFBVSxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQyxHQUFHLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2pMLElBQUksTUFBTSxHQUFHLFVBQVUsQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQzlDLE1BQU0sR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUMxQyxJQUFJLFFBQVEsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3pMLElBQUksY0FBYyxHQUFHLFFBQVEsQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQ3BELGNBQWMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGNBQWMsQ0FBQyxDQUFDO2dCQUMxRCxJQUFJLGNBQWMsR0FBRyxRQUFRLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUNwRCxjQUFjLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFDMUQsSUFBSSxRQUFRLEdBQUcsQ0FBQyxDQUFDO2dCQUNqQixjQUFjLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFDMUMsY0FBYyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUM7Z0JBQzFDLElBQUksY0FBYyxHQUFHLGNBQWMsRUFBRTtvQkFDakMsUUFBUSxHQUFHLGNBQWMsQ0FBQztpQkFDN0I7cUJBQ0k7b0JBQ0QsUUFBUSxHQUFHLGNBQWMsQ0FBQztpQkFDN0I7Z0JBQ0QsSUFBSSxRQUFRLEdBQUcsaUJBQU8sQ0FBQyxRQUFRLENBQUMsaUJBQU8sQ0FBQyxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsRUFBRSwwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2xPLElBQUksV0FBVyxHQUFHLGlCQUFPLENBQUMsUUFBUSxDQUFDLGlCQUFPLENBQUMsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsRUFBRSwwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDM04sSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxHQUFHLE1BQU0sRUFBRTtvQkFDN0IsSUFBSSxRQUFRLEdBQUcsV0FBVyxFQUFFO3dCQUN4QixJQUFJLGdCQUFNLENBQUMsaUJBQWlCLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFOzRCQUNsRCwwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyw2QkFBbUIsQ0FBQyxDQUFDLGFBQWEsRUFBRSxDQUFDOzRCQUNuSCwwQkFBMEI7NEJBQzFCLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQzs0QkFDdkIsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7NEJBQ3ZCLGdCQUFNLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDOzRCQUNqQyxnQkFBTSxDQUFDLGtCQUFrQixHQUFHLEtBQUssQ0FBQzt5QkFDckM7cUJBQ0o7aUJBQ0o7YUFDSjtTQUNKO0lBQ0wsQ0FBQztJQUVELG1DQUFTLEdBQVQ7UUFBQSxpQkFJQztRQUhHLElBQUksQ0FBQyxRQUFRLENBQUM7WUFDVixLQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDckIsQ0FBQyxFQUFFLEdBQUcsRUFBRSxFQUFFLENBQUMsS0FBSyxDQUFDLGNBQWMsRUFBRSxHQUFHLENBQUMsQ0FBQztJQUMxQyxDQUFDO0lBRUQseUNBQWUsR0FBZjtRQUNJLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO1FBQzNCLEVBQUUsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLGdCQUFNLENBQUMsV0FBVyxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQ3JELDJCQUFRLENBQUMsSUFBSSxDQUFDLGtCQUFRLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDbEMsMkJBQVEsQ0FBQyxJQUFJLENBQUMsa0JBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM5QixJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQzNCLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1FBQzlCLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ3BELElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDcEUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQzNCLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQztRQUN0QixJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztRQUM3QixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztRQUN2QixJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDcEIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO0lBQ3ZFLENBQUM7SUFFRCx5Q0FBZSxHQUFmO1FBQ0ksSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7UUFDM0IsRUFBRSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsZ0JBQU0sQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDckQsMkJBQVEsQ0FBQyxJQUFJLENBQUMsa0JBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNsQywyQkFBUSxDQUFDLElBQUksQ0FBQyxrQkFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzlCLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUMzQixJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNuRSxDQUFDO0lBRUQsc0NBQVksR0FBWjtRQUNJLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUM3QixJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztRQUN2RSxHQUFHLEdBQUcsRUFBRSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3hELElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUM1QyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDdkUsQ0FBQztJQUVELDRDQUFrQixHQUFsQixVQUFtQixDQUFTLEVBQUUsQ0FBUyxFQUFFLENBQVM7UUFDOUMsSUFBSSxXQUFXLEdBQUcsRUFBRSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDbkQsV0FBVyxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUM7UUFDN0MsV0FBVyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDbEIsV0FBVyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDbEIsV0FBVyxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQzFCLENBQUM7SUFFRCwwQ0FBZ0IsR0FBaEIsVUFBaUIsTUFBYztRQUEvQixpQkFTQztRQVJHLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUM3QixJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ2xFLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUUsR0FBRyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3RFLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQztRQUN0QixJQUFJLENBQUMsWUFBWSxDQUFDO1lBQ2QsS0FBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDekQsS0FBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUN4QixDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDZixDQUFDO0lBRUQsbUNBQVMsR0FBVDtRQUFBLGlCQTJCQztRQTFCRyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQztRQUN4RSxJQUFJLElBQUksQ0FBQyxpQkFBaUIsRUFBRTtZQUN4QixJQUFJLFFBQVEsR0FBRyxpQkFBTyxDQUFDLFFBQVEsQ0FBQyxpQkFBTyxDQUFDLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxFQUFFLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNsTyxJQUFJLFFBQVEsR0FBRyxRQUFRLEdBQUcsRUFBRSxDQUFDO1lBQzdCLElBQUksQ0FBQyxLQUFLLEdBQUcsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7WUFDaEYsSUFBSSxDQUFDLEtBQUssR0FBRywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztZQUNoRixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDdEcsSUFBSSxLQUFLLEdBQUcsSUFBSSxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUMsRUFBRSxDQUFDLFFBQVEsRUFBRSxFQUFFLFFBQVEsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQztnQkFDbEcsS0FBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ3ZCLENBQUMsQ0FBQyxDQUFDO1lBQ0gsS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7U0FDbkM7YUFDSTtZQUNELElBQUksQ0FBQyxLQUFLLEdBQUcsZ0JBQU0sQ0FBQyxRQUFRLENBQUMsZ0JBQU0sQ0FBQyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUNsRixJQUFJLENBQUMsS0FBSyxHQUFHLGdCQUFNLENBQUMsUUFBUSxDQUFDLGdCQUFNLENBQUMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDbEYsT0FBTyxpQkFBTyxDQUFDLFFBQVEsQ0FBQyxpQkFBTyxDQUFDLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsR0FBRyxFQUFFO2dCQUM3RyxJQUFJLENBQUMsS0FBSyxHQUFHLGdCQUFNLENBQUMsUUFBUSxDQUFDLGdCQUFNLENBQUMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQ2xGLElBQUksQ0FBQyxLQUFLLEdBQUcsZ0JBQU0sQ0FBQyxRQUFRLENBQUMsZ0JBQU0sQ0FBQyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQzthQUNyRjtZQUNELElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUN0RyxJQUFJLEtBQUssR0FBRyxJQUFJLEVBQUUsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUUsUUFBUSxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUMzRixLQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsQ0FBQztZQUM3RSxDQUFDLENBQUMsQ0FBQztZQUNILEtBQUssQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO1NBQ25DO1FBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDNUUsQ0FBQztJQUVELHVDQUFhLEdBQWIsVUFBYyxNQUFNLEVBQUUsTUFBTTtRQUN4QixJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQztRQUNwRixPQUFPLFFBQVEsQ0FBQztJQUNwQixDQUFDO0lBRUQscUNBQVcsR0FBWDtRQUFBLGlCQWlCQztRQWhCRyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO1FBQ2hDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksU0FBUyxFQUFFO1lBQzdCLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1NBQ3ZFO2FBQ0k7WUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7U0FDdEU7UUFDRCxJQUFJLENBQUMsWUFBWSxDQUFDO1lBQ2QsS0FBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQztZQUMvQixLQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO1lBQzlCLEVBQUUsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLGdCQUFNLENBQUMsV0FBVyxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQ3pELENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUNSLElBQUksQ0FBQyxZQUFZLENBQUM7WUFDZCxnQkFBTSxDQUFDLGtCQUFrQixHQUFHLEtBQUssQ0FBQztZQUNsQyxLQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7UUFDdkUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQ1YsQ0FBQztJQWpNRDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDO3NEQUNDO0lBR3RCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUM7dURBQ0U7SUFHdkI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQztxREFDQTtJQUdyQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDO3dEQUNHO0lBR3hCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7dURBQ1M7SUFHM0I7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztzREFDUTtJQUcxQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDO3dEQUNVO0lBRzlCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7cURBQ087SUFHekI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzttREFDSztJQTNCTixlQUFlO1FBRG5DLE9BQU87T0FDYSxlQUFlLENBc01uQztJQUFELHNCQUFDO0NBdE1ELEFBc01DLENBdE00QyxFQUFFLENBQUMsU0FBUyxHQXNNeEQ7a0JBdE1vQixlQUFlIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IENoYXJhY3RlckNvbnRyb2xsZXIgZnJvbSBcIi4uL0NoYXJhY3Rlci9DaGFyYWN0ZXJDb250cm9sbGVyXCI7XHJcbmltcG9ydCBNYXBDb250cm9sbGVyIGZyb20gXCIuLi9Db2xsaWRlck9iai9NYXBDb250cm9sbGVyXCI7XHJcbmltcG9ydCBHYW1lUGxheUluc3RhbmNlLCB7IGluc3RhbmNlIH0gZnJvbSBcIi4uL0NvbW1vbi9HYW1lUGxheUluc3RhbmNlXCI7XHJcbmltcG9ydCBHbG9iYWwgZnJvbSBcIi4uL0NvbW1vbi9HbG9iYWxcIjtcclxuaW1wb3J0IEtleUV2ZW50IGZyb20gXCIuLi9Db21tb24vS2V5RXZlbnRcIjtcclxuaW1wb3J0IFJhbmRvbSBmcm9tIFwiLi4vQ29tbW9uL1JhbmRvbVwiO1xyXG5pbXBvcnQgVXRpbGl0eSBmcm9tIFwiLi4vQ29tbW9uL1V0aWxpdHlcIjtcclxuXHJcbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XHJcbkBjY2NsYXNzXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEVuZW15Q29udHJvbGxlciBleHRlbmRzIGNjLkNvbXBvbmVudCB7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLkludGVnZXIpXHJcbiAgICBjbGFtcExlZnQ6IG51bWJlciA9IDA7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLkludGVnZXIpXHJcbiAgICBjbGFtcFJpZ2h0OiBudW1iZXIgPSAwO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5JbnRlZ2VyKVxyXG4gICAgY2xhbXBUb3A6IG51bWJlciA9IDA7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLkludGVnZXIpXHJcbiAgICBjbGFtcEJvdHRvbTogbnVtYmVyID0gMDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIFBvaW50U2hvb3Q6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgYm9keURlYXRoOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuUHJlZmFiKVxyXG4gICAgZWZmZWN0Qmxvb2Q6IGNjLlByZWZhYiA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBwb3NCbG9vZDogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICB3ZWFwb246IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIG1vdmVYOiBudW1iZXIgPSAwO1xyXG4gICAgbW92ZVk6IG51bWJlciA9IDA7XHJcbiAgICBtb3ZlWjogbnVtYmVyID0gMDtcclxuICAgIGRlZ3JlZTogbnVtYmVyID0gMDtcclxuICAgIGJvb2xDaGVja0F0dGFja2luZzogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgYm9vbENoZWNrQXR0YWNrZWQ6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIGJvb2xFbmVteURlYXRoOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBjaGVja0ZvbGxvd1BsYXllcjogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgY2hlY2tEZWF0aDogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuICAgIG9uTG9hZCgpIHtcclxuICAgICAgICAvLyB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLkJveENvbGxpZGVyM0QpLmVuYWJsZWQgPSB0cnVlO1xyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZSgpIHtcclxuICAgICAgICBpZiAoIXRoaXMuY2hlY2tEZWF0aCkge1xyXG4gICAgICAgICAgICBpZiAoR2xvYmFsLmJvb2xTdGFydEF0dGFja2luZyAmJiBHbG9iYWwuYm9vbENoZWNrQXR0YWNraW5nKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgcG9zMSA9IGNjLkNhbnZhcy5pbnN0YW5jZS5ub2RlLmNvbnZlcnRUb05vZGVTcGFjZUFSKEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIuY29udmVydFRvV29ybGRTcGFjZUFSKEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIuY2hpbGRyZW5bMF0uZ2V0UG9zaXRpb24oKSkpO1xyXG4gICAgICAgICAgICAgICAgbGV0IHBvczIgPSBjYy5DYW52YXMuaW5zdGFuY2Uubm9kZS5jb252ZXJ0VG9Ob2RlU3BhY2VBUihHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLmNvbnZlcnRUb1dvcmxkU3BhY2VBUihHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLmNoaWxkcmVuWzFdLmdldFBvc2l0aW9uKCkpKTtcclxuICAgICAgICAgICAgICAgIGxldCBwb3MzID0gY2MuQ2FudmFzLmluc3RhbmNlLm5vZGUuY29udmVydFRvTm9kZVNwYWNlQVIoR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5NeUNoYXJhY3Rlci5jb252ZXJ0VG9Xb3JsZFNwYWNlQVIoR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5NeUNoYXJhY3Rlci5jaGlsZHJlblsyXS5nZXRQb3NpdGlvbigpKSk7XHJcbiAgICAgICAgICAgICAgICBsZXQgZGlyZWN0aW9uMSA9IGNjLnYyKHBvczEueCAtIEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueCwgcG9zMS55IC0gR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5NeUNoYXJhY3Rlci55KTtcclxuICAgICAgICAgICAgICAgIGxldCBkaXJlY3Rpb24yID0gY2MudjIocG9zMi54IC0gR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5NeUNoYXJhY3Rlci54LCBwb3MyLnkgLSBHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLnkpO1xyXG4gICAgICAgICAgICAgICAgbGV0IGRlZ3JlZSA9IGRpcmVjdGlvbjEuc2lnbkFuZ2xlKGRpcmVjdGlvbjIpO1xyXG4gICAgICAgICAgICAgICAgZGVncmVlID0gY2MubWlzYy5yYWRpYW5zVG9EZWdyZWVzKGRlZ3JlZSk7XHJcbiAgICAgICAgICAgICAgICBsZXQgcG9zRW5lbXkgPSBjYy52Mih0aGlzLm5vZGUueCAtIEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueCwgdGhpcy5ub2RlLnkgLSBHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLnkpO1xyXG4gICAgICAgICAgICAgICAgbGV0IGRlZ3JlZVdpdGhQb3MxID0gcG9zRW5lbXkuc2lnbkFuZ2xlKGRpcmVjdGlvbjEpO1xyXG4gICAgICAgICAgICAgICAgZGVncmVlV2l0aFBvczEgPSBjYy5taXNjLnJhZGlhbnNUb0RlZ3JlZXMoZGVncmVlV2l0aFBvczEpO1xyXG4gICAgICAgICAgICAgICAgbGV0IGRlZ3JlZVdpdGhQb3MyID0gcG9zRW5lbXkuc2lnbkFuZ2xlKGRpcmVjdGlvbjIpO1xyXG4gICAgICAgICAgICAgICAgZGVncmVlV2l0aFBvczIgPSBjYy5taXNjLnJhZGlhbnNUb0RlZ3JlZXMoZGVncmVlV2l0aFBvczIpO1xyXG4gICAgICAgICAgICAgICAgbGV0IHJlYWxOZWVkID0gMDtcclxuICAgICAgICAgICAgICAgIGRlZ3JlZVdpdGhQb3MxID0gTWF0aC5hYnMoZGVncmVlV2l0aFBvczEpO1xyXG4gICAgICAgICAgICAgICAgZGVncmVlV2l0aFBvczIgPSBNYXRoLmFicyhkZWdyZWVXaXRoUG9zMik7XHJcbiAgICAgICAgICAgICAgICBpZiAoZGVncmVlV2l0aFBvczEgPiBkZWdyZWVXaXRoUG9zMikge1xyXG4gICAgICAgICAgICAgICAgICAgIHJlYWxOZWVkID0gZGVncmVlV2l0aFBvczE7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICByZWFsTmVlZCA9IGRlZ3JlZVdpdGhQb3MyO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgbGV0IGRpc3RhbmNlID0gVXRpbGl0eS5JbnN0YW5jZShVdGlsaXR5KS5EaXN0YW5jZShjYy52Mih0aGlzLm5vZGUueCwgdGhpcy5ub2RlLnkpLCBjYy52MihHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLngsIEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueSkpO1xyXG4gICAgICAgICAgICAgICAgbGV0IG1heERpc3RhbmNlID0gVXRpbGl0eS5JbnN0YW5jZShVdGlsaXR5KS5EaXN0YW5jZShjYy52MihHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLngsIEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueSksIGNjLnYyKHBvczMueCwgcG9zMy55KSk7XHJcbiAgICAgICAgICAgICAgICBpZiAoTWF0aC5hYnMocmVhbE5lZWQpIDwgZGVncmVlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGRpc3RhbmNlIDwgbWF4RGlzdGFuY2UpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKEdsb2JhbC5ib29sQ2hlY2tBdHRhY2tlZCAmJiAhdGhpcy5ib29sRW5lbXlEZWF0aCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5NeUNoYXJhY3Rlci5nZXRDb21wb25lbnQoQ2hhcmFjdGVyQ29udHJvbGxlcikuTGV2ZWxVcFBsYXllcigpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gdGhpcy5EZXN0cm95QnlLYXRhbmEoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuRGVzdHJveUJ5SGFtbWVyKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNoZWNrRGVhdGggPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgR2xvYmFsLmJvb2xDaGVja0F0dGFja2VkID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBHbG9iYWwuYm9vbENoZWNrQXR0YWNraW5nID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgU3RhcnRNb3ZlKCkge1xyXG4gICAgICAgIHRoaXMuc2NoZWR1bGUoKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLm1vdmVFbmVteSgpO1xyXG4gICAgICAgIH0sIDYuNSwgY2MubWFjcm8uUkVQRUFUX0ZPUkVWRVIsIDAuMSk7XHJcbiAgICB9XHJcblxyXG4gICAgRGVzdHJveUJ5SGFtbWVyKCkge1xyXG4gICAgICAgIHRoaXMuYm9vbEVuZW15RGVhdGggPSB0cnVlO1xyXG4gICAgICAgIGNjLmF1ZGlvRW5naW5lLnBsYXlFZmZlY3QoR2xvYmFsLnNvdW5kU2NyZWFtLCBmYWxzZSk7XHJcbiAgICAgICAgaW5zdGFuY2UuZW1pdChLZXlFdmVudC5wbHVzRW5lbXkpO1xyXG4gICAgICAgIGluc3RhbmNlLmVtaXQoS2V5RXZlbnQuc2NhbGUpO1xyXG4gICAgICAgIHRoaXMubm9kZS5zdG9wQWxsQWN0aW9ucygpO1xyXG4gICAgICAgIHRoaXMudW5zY2hlZHVsZUFsbENhbGxiYWNrcygpO1xyXG4gICAgICAgIHRoaXMubm9kZS5nZXRDb21wb25lbnQoY2MuU2tlbGV0b25BbmltYXRpb24pLnN0b3AoKTtcclxuICAgICAgICB0aGlzLm5vZGUuZXVsZXJBbmdsZXMgPSBuZXcgY2MuVmVjMygwLCAwLCAtdGhpcy5ub2RlLmV1bGVyQW5nbGVzLnopO1xyXG4gICAgICAgIHRoaXMud2VhcG9uLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMubm9kZS5zY2FsZVogPSAxMjtcclxuICAgICAgICB0aGlzLm5vZGUueiA9IHRoaXMubW92ZVogKyAxO1xyXG4gICAgICAgIHRoaXMuY2hlY2tEZWF0aCA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5TcGF3bmVyQmxvb2QoKTtcclxuICAgICAgICB0aGlzLlNwYXduZXJFZmZlY3RCbG9vZCh0aGlzLm5vZGUueCwgdGhpcy5ub2RlLnksIHRoaXMubm9kZS56ICsgMik7XHJcbiAgICB9XHJcblxyXG4gICAgRGVzdHJveUJ5S2F0YW5hKCkge1xyXG4gICAgICAgIHRoaXMuYm9vbEVuZW15RGVhdGggPSB0cnVlO1xyXG4gICAgICAgIGNjLmF1ZGlvRW5naW5lLnBsYXlFZmZlY3QoR2xvYmFsLnNvdW5kU2NyZWFtLCBmYWxzZSk7XHJcbiAgICAgICAgaW5zdGFuY2UuZW1pdChLZXlFdmVudC5wbHVzRW5lbXkpO1xyXG4gICAgICAgIGluc3RhbmNlLmVtaXQoS2V5RXZlbnQuc2NhbGUpO1xyXG4gICAgICAgIHRoaXMuY2hlY2tEZWF0aCA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5TcGF3bmVyQm9keURlYXRoKDAuNyk7XHJcbiAgICAgICAgdGhpcy5TcGF3bmVyRWZmZWN0Qmxvb2QodGhpcy5ub2RlLngsIHRoaXMubm9kZS55LCB0aGlzLm5vZGUueik7XHJcbiAgICB9XHJcblxyXG4gICAgU3Bhd25lckJsb29kKCkge1xyXG4gICAgICAgIHRoaXMuYm9keURlYXRoLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgbGV0IHBvcyA9IHRoaXMubm9kZS5jb252ZXJ0VG9Xb3JsZFNwYWNlQVIodGhpcy5wb3NCbG9vZC5nZXRQb3NpdGlvbigpKTtcclxuICAgICAgICBwb3MgPSBjYy5DYW52YXMuaW5zdGFuY2Uubm9kZS5jb252ZXJ0VG9Ob2RlU3BhY2VBUihwb3MpO1xyXG4gICAgICAgIHRoaXMuYm9keURlYXRoLnNldFBvc2l0aW9uKHBvcy54LCBwb3MueSwgMCk7XHJcbiAgICAgICAgdGhpcy5ib2R5RGVhdGguZXVsZXJBbmdsZXMgPSBjYy52Myg5MCwgMCwgdGhpcy5ub2RlLmV1bGVyQW5nbGVzLnopO1xyXG4gICAgfVxyXG5cclxuICAgIFNwYXduZXJFZmZlY3RCbG9vZCh4OiBudW1iZXIsIHk6IG51bWJlciwgejogbnVtYmVyKSB7XHJcbiAgICAgICAgbGV0IEVmZmVjdEJsb29kID0gY2MuaW5zdGFudGlhdGUodGhpcy5lZmZlY3RCbG9vZCk7XHJcbiAgICAgICAgRWZmZWN0Qmxvb2QucGFyZW50ID0gY2MuQ2FudmFzLmluc3RhbmNlLm5vZGU7XHJcbiAgICAgICAgRWZmZWN0Qmxvb2QueCA9IHg7XHJcbiAgICAgICAgRWZmZWN0Qmxvb2QueSA9IHk7XHJcbiAgICAgICAgRWZmZWN0Qmxvb2QueiA9IHogKyAxO1xyXG4gICAgfVxyXG5cclxuICAgIFNwYXduZXJCb2R5RGVhdGgodGltaW5nOiBudW1iZXIpIHtcclxuICAgICAgICB0aGlzLmJvZHlEZWF0aC5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuYm9keURlYXRoLnNldFBvc2l0aW9uKHRoaXMubm9kZS54LCB0aGlzLm5vZGUueSwgdGhpcy5ub2RlLnopO1xyXG4gICAgICAgIHRoaXMuYm9keURlYXRoLmV1bGVyQW5nbGVzID0gY2MudjMoLTkwLCAxODAsIHRoaXMubm9kZS5ldWxlckFuZ2xlcy56KTtcclxuICAgICAgICB0aGlzLm5vZGUub3BhY2l0eSA9IDA7XHJcbiAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmJvZHlEZWF0aC5nZXRDb21wb25lbnQoY2MuU2tlbGV0b25BbmltYXRpb24pLnN0b3AoKTtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLmRlc3Ryb3koKTtcclxuICAgICAgICB9LCB0aW1pbmcpO1xyXG4gICAgfVxyXG5cclxuICAgIG1vdmVFbmVteSgpIHtcclxuICAgICAgICB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlNrZWxldG9uQW5pbWF0aW9uKS5wbGF5KFwiQXJtYXR1cmV8c3dvcmRfcnVuXCIpO1xyXG4gICAgICAgIGlmICh0aGlzLmNoZWNrRm9sbG93UGxheWVyKSB7XHJcbiAgICAgICAgICAgIGxldCBEaXN0YW5jZSA9IFV0aWxpdHkuSW5zdGFuY2UoVXRpbGl0eSkuRGlzdGFuY2UoY2MudjIodGhpcy5ub2RlLngsIHRoaXMubm9kZS55KSwgY2MudjIoR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5NeUNoYXJhY3Rlci54LCBHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLnkpKTtcclxuICAgICAgICAgICAgbGV0IGR1cmF0aW9uID0gRGlzdGFuY2UgLyAyMTtcclxuICAgICAgICAgICAgdGhpcy5tb3ZlWCA9IEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueDtcclxuICAgICAgICAgICAgdGhpcy5tb3ZlWSA9IEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueTtcclxuICAgICAgICAgICAgdGhpcy5kZWdyZWUgPSB0aGlzLmJldHdlZW5EZWdyZWUoY2MudjIodGhpcy5ub2RlLngsIHRoaXMubm9kZS55KSwgY2MudjIodGhpcy5tb3ZlWCwgdGhpcy5tb3ZlWSkpIC0gOTA7XHJcbiAgICAgICAgICAgIHZhciB0d2VlbiA9IG5ldyBjYy5Ud2VlbigpLnRvKGR1cmF0aW9uLCB7IHBvc2l0aW9uOiBjYy52Myh0aGlzLm1vdmVYLCB0aGlzLm1vdmVZLCB0aGlzLm1vdmVaKSB9KS5jYWxsKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuRW5lbXlBdHRhY2soKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIHR3ZWVuLnRhcmdldCh0aGlzLm5vZGUpLnN0YXJ0KCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLm1vdmVYID0gUmFuZG9tLkluc3RhbmNlKFJhbmRvbSkuUmFuZG9tUmFuZ2UodGhpcy5jbGFtcExlZnQsIHRoaXMuY2xhbXBSaWdodCk7XHJcbiAgICAgICAgICAgIHRoaXMubW92ZVkgPSBSYW5kb20uSW5zdGFuY2UoUmFuZG9tKS5SYW5kb21SYW5nZSh0aGlzLmNsYW1wQm90dG9tLCB0aGlzLmNsYW1wVG9wKTtcclxuICAgICAgICAgICAgd2hpbGUgKFV0aWxpdHkuSW5zdGFuY2UoVXRpbGl0eSkuRGlzdGFuY2UoY2MudjIodGhpcy5ub2RlLngsIHRoaXMubm9kZS55KSwgY2MudjIodGhpcy5tb3ZlWCwgdGhpcy5tb3ZlWSkpIDwgMTAwKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm1vdmVYID0gUmFuZG9tLkluc3RhbmNlKFJhbmRvbSkuUmFuZG9tUmFuZ2UodGhpcy5jbGFtcExlZnQsIHRoaXMuY2xhbXBSaWdodCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm1vdmVZID0gUmFuZG9tLkluc3RhbmNlKFJhbmRvbSkuUmFuZG9tUmFuZ2UodGhpcy5jbGFtcEJvdHRvbSwgdGhpcy5jbGFtcFRvcCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5kZWdyZWUgPSB0aGlzLmJldHdlZW5EZWdyZWUoY2MudjIodGhpcy5ub2RlLngsIHRoaXMubm9kZS55KSwgY2MudjIodGhpcy5tb3ZlWCwgdGhpcy5tb3ZlWSkpICsgOTA7XHJcbiAgICAgICAgICAgIHZhciB0d2VlbiA9IG5ldyBjYy5Ud2VlbigpLnRvKDYsIHsgcG9zaXRpb246IGNjLnYzKHRoaXMubW92ZVgsIHRoaXMubW92ZVksIHRoaXMubW92ZVopIH0pLmNhbGwoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLmdldENvbXBvbmVudChjYy5Ta2VsZXRvbkFuaW1hdGlvbikucGxheShcIkFybWF0dXJlfHN3b3JkX2lkbGVcIik7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB0d2Vlbi50YXJnZXQodGhpcy5ub2RlKS5zdGFydCgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLm5vZGUucnVuQWN0aW9uKGNjLnJvdGF0ZTNEVG8oMC4yLCBjYy52MygtOTAsIC0xODAsIC10aGlzLmRlZ3JlZSkpKTtcclxuICAgIH1cclxuXHJcbiAgICBiZXR3ZWVuRGVncmVlKGRpclZlYywgY29tVmVjKSB7XHJcbiAgICAgICAgbGV0IGFuZ2xlRGVnID0gTWF0aC5hdGFuMihkaXJWZWMueSAtIGNvbVZlYy55LCBkaXJWZWMueCAtIGNvbVZlYy54KSAqIDE4MCAvIE1hdGguUEk7XHJcbiAgICAgICAgcmV0dXJuIGFuZ2xlRGVnO1xyXG4gICAgfVxyXG5cclxuICAgIEVuZW15QXR0YWNrKCkge1xyXG4gICAgICAgIHRoaXMuYm9vbENoZWNrQXR0YWNraW5nID0gZmFsc2U7XHJcbiAgICAgICAgaWYgKHRoaXMubm9kZS5uYW1lID09IFwiTXlWZW5vbVwiKSB7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS5nZXRDb21wb25lbnQoY2MuU2tlbGV0b25BbmltYXRpb24pLnBsYXkoXCJBbW9uZ1VzX0F0dGFja1wiKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS5nZXRDb21wb25lbnQoY2MuU2tlbGV0b25BbmltYXRpb24pLnBsYXkoXCJIYW1tZXIgQXR0YWNrXCIpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuYm9vbENoZWNrQXR0YWNraW5nID0gdHJ1ZTtcclxuICAgICAgICAgICAgdGhpcy5ib29sQ2hlY2tBdHRhY2tlZCA9IHRydWU7XHJcbiAgICAgICAgICAgIGNjLmF1ZGlvRW5naW5lLnBsYXlFZmZlY3QoR2xvYmFsLnNvdW5kQXR0YWNrLCBmYWxzZSk7XHJcbiAgICAgICAgfSwgMC41KTtcclxuICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgIEdsb2JhbC5ib29sU3RhcnRBdHRhY2tpbmcgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLmdldENvbXBvbmVudChjYy5Ta2VsZXRvbkFuaW1hdGlvbikucGxheShcIkFtb25nX1VTX2lkbGVcIik7XHJcbiAgICAgICAgfSwgMSk7XHJcbiAgICB9XHJcblxyXG59Il19