
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/ColliderObj/MapController.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'bc404dQ86xGZIbTFHeg8YaK', 'MapController');
// Scripts/ColliderObj/MapController.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SquareCollider_1 = require("./SquareCollider");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var MapController = /** @class */ (function (_super) {
    __extends(MapController, _super);
    function MapController() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        // @property(ObjectCollider)
        // public ListCircleObjects: ObjectCollider[] = [];
        _this.ListSquareObjects = [];
        return _this;
    }
    __decorate([
        property(SquareCollider_1.default)
    ], MapController.prototype, "ListSquareObjects", void 0);
    MapController = __decorate([
        ccclass
    ], MapController);
    return MapController;
}(cc.Component));
exports.default = MapController;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29sbGlkZXJPYmpcXE1hcENvbnRyb2xsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0EsbURBQThDO0FBRXhDLElBQUEsa0JBQW1DLEVBQWxDLG9CQUFPLEVBQUUsc0JBQXlCLENBQUM7QUFHMUM7SUFBMkMsaUNBQVk7SUFEdkQ7UUFBQSxxRUFPQztRQUpHLDRCQUE0QjtRQUM1QixtREFBbUQ7UUFFNUMsdUJBQWlCLEdBQXFCLEVBQUUsQ0FBQzs7SUFDcEQsQ0FBQztJQURHO1FBREMsUUFBUSxDQUFDLHdCQUFjLENBQUM7NERBQ3VCO0lBTC9CLGFBQWE7UUFEakMsT0FBTztPQUNhLGFBQWEsQ0FNakM7SUFBRCxvQkFBQztDQU5ELEFBTUMsQ0FOMEMsRUFBRSxDQUFDLFNBQVMsR0FNdEQ7a0JBTm9CLGFBQWEiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJcclxuaW1wb3J0IFNxdWFyZUNvbGxpZGVyIGZyb20gXCIuL1NxdWFyZUNvbGxpZGVyXCI7XHJcblxyXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcclxuXHJcbkBjY2NsYXNzXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIE1hcENvbnRyb2xsZXIgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xyXG5cclxuICAgIC8vIEBwcm9wZXJ0eShPYmplY3RDb2xsaWRlcilcclxuICAgIC8vIHB1YmxpYyBMaXN0Q2lyY2xlT2JqZWN0czogT2JqZWN0Q29sbGlkZXJbXSA9IFtdO1xyXG4gICAgQHByb3BlcnR5KFNxdWFyZUNvbGxpZGVyKVxyXG4gICAgcHVibGljIExpc3RTcXVhcmVPYmplY3RzOiBTcXVhcmVDb2xsaWRlcltdID0gW107XHJcbn1cclxuIl19