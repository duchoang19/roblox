
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/ColliderObj/SquareCollider.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '2b4c7MyE2JG67FTDw4URo8E', 'SquareCollider');
// Scripts/ColliderObj/SquareCollider.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SquareCollider = /** @class */ (function (_super) {
    __extends(SquareCollider, _super);
    function SquareCollider() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.high = 0;
        _this.positionLeft = null;
        _this.positionRight = null;
        _this.positionTop = null;
        _this.positionBottom = null;
        return _this;
    }
    SquareCollider.prototype.start = function () {
        this.positionLeft = new cc.Vec2(this.node.x - this.high, this.node.y);
        this.positionRight = new cc.Vec2(this.node.x + this.high, this.node.y);
        this.positionTop = new cc.Vec2(this.node.x, this.node.y + this.high);
        this.positionBottom = new cc.Vec2(this.node.x, this.node.y - this.high);
    };
    SquareCollider.prototype.OnTheLeft = function (player) {
        if (player.x < this.positionLeft.x && player.y < this.positionTop.y && player.y > this.positionBottom.y)
            return true;
        else
            return false;
    };
    SquareCollider.prototype.OnTheRight = function (player) {
        if (player.x > this.positionRight.x && player.y < this.positionTop.y && player.y > this.positionBottom.y)
            return true;
        else
            return false;
    };
    SquareCollider.prototype.OnTheTop = function (player) {
        if (player.y > this.positionTop.y && player.x < this.positionRight.x && player.x > this.positionLeft.x)
            return true;
        else
            return false;
    };
    SquareCollider.prototype.OnTheBottom = function (player) {
        if (player.y < this.positionBottom.y && player.x < this.positionRight.x && player.x > this.positionLeft.x)
            return true;
        else
            return false;
    };
    __decorate([
        property(cc.Float)
    ], SquareCollider.prototype, "high", void 0);
    SquareCollider = __decorate([
        ccclass
    ], SquareCollider);
    return SquareCollider;
}(cc.Component));
exports.default = SquareCollider;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29sbGlkZXJPYmpcXFNxdWFyZUNvbGxpZGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNNLElBQUEsa0JBQXFDLEVBQW5DLG9CQUFPLEVBQUUsc0JBQTBCLENBQUM7QUFHNUM7SUFBNEMsa0NBQVk7SUFEeEQ7UUFBQSxxRUE2Q0M7UUF6Q0csVUFBSSxHQUFXLENBQUMsQ0FBQztRQUVWLGtCQUFZLEdBQVksSUFBSSxDQUFDO1FBQzdCLG1CQUFhLEdBQVksSUFBSSxDQUFDO1FBQzlCLGlCQUFXLEdBQVksSUFBSSxDQUFDO1FBQzVCLG9CQUFjLEdBQVksSUFBSSxDQUFDOztJQW9DMUMsQ0FBQztJQWxDRyw4QkFBSyxHQUFMO1FBQ0ksSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3RFLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN2RSxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDckUsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzVFLENBQUM7SUFFTSxrQ0FBUyxHQUFoQixVQUFpQixNQUFlO1FBQzVCLElBQUksTUFBTSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsSUFBSSxNQUFNLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBQ25HLE9BQU8sSUFBSSxDQUFDOztZQUVaLE9BQU8sS0FBSyxDQUFDO0lBQ3JCLENBQUM7SUFFTSxtQ0FBVSxHQUFqQixVQUFrQixNQUFlO1FBQzdCLElBQUksTUFBTSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsSUFBSSxNQUFNLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBQ3BHLE9BQU8sSUFBSSxDQUFDOztZQUVaLE9BQU8sS0FBSyxDQUFDO0lBQ3JCLENBQUM7SUFFTSxpQ0FBUSxHQUFmLFVBQWdCLE1BQWU7UUFDM0IsSUFBSSxNQUFNLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLElBQUksTUFBTSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDbEcsT0FBTyxJQUFJLENBQUM7O1lBRVosT0FBTyxLQUFLLENBQUM7SUFDckIsQ0FBQztJQUVNLG9DQUFXLEdBQWxCLFVBQW1CLE1BQWU7UUFDOUIsSUFBSSxNQUFNLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLElBQUksTUFBTSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDckcsT0FBTyxJQUFJLENBQUM7O1lBRVosT0FBTyxLQUFLLENBQUM7SUFDckIsQ0FBQztJQXhDRDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO2dEQUNGO0lBSEEsY0FBYztRQURsQyxPQUFPO09BQ2EsY0FBYyxDQTRDbEM7SUFBRCxxQkFBQztDQTVDRCxBQTRDQyxDQTVDMkMsRUFBRSxDQUFDLFNBQVMsR0E0Q3ZEO2tCQTVDb0IsY0FBYyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgU3F1YXJlQ29sbGlkZXIgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5GbG9hdClcclxuICAgIGhpZ2g6IG51bWJlciA9IDA7XHJcblxyXG4gICAgcHVibGljIHBvc2l0aW9uTGVmdDogY2MuVmVjMiA9IG51bGw7XHJcbiAgICBwdWJsaWMgcG9zaXRpb25SaWdodDogY2MuVmVjMiA9IG51bGw7XHJcbiAgICBwdWJsaWMgcG9zaXRpb25Ub3A6IGNjLlZlYzIgPSBudWxsO1xyXG4gICAgcHVibGljIHBvc2l0aW9uQm90dG9tOiBjYy5WZWMyID0gbnVsbDtcclxuXHJcbiAgICBzdGFydCgpIHtcclxuICAgICAgICB0aGlzLnBvc2l0aW9uTGVmdCA9IG5ldyBjYy5WZWMyKHRoaXMubm9kZS54IC0gdGhpcy5oaWdoLCB0aGlzLm5vZGUueSk7XHJcbiAgICAgICAgdGhpcy5wb3NpdGlvblJpZ2h0ID0gbmV3IGNjLlZlYzIodGhpcy5ub2RlLnggKyB0aGlzLmhpZ2gsIHRoaXMubm9kZS55KTtcclxuICAgICAgICB0aGlzLnBvc2l0aW9uVG9wID0gbmV3IGNjLlZlYzIodGhpcy5ub2RlLngsIHRoaXMubm9kZS55ICsgdGhpcy5oaWdoKTtcclxuICAgICAgICB0aGlzLnBvc2l0aW9uQm90dG9tID0gbmV3IGNjLlZlYzIodGhpcy5ub2RlLngsIHRoaXMubm9kZS55IC0gdGhpcy5oaWdoKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgT25UaGVMZWZ0KHBsYXllcjogY2MuTm9kZSk6IEJvb2xlYW4ge1xyXG4gICAgICAgIGlmIChwbGF5ZXIueCA8IHRoaXMucG9zaXRpb25MZWZ0LnggJiYgcGxheWVyLnkgPCB0aGlzLnBvc2l0aW9uVG9wLnkgJiYgcGxheWVyLnkgPiB0aGlzLnBvc2l0aW9uQm90dG9tLnkpXHJcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIGVsc2VcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBPblRoZVJpZ2h0KHBsYXllcjogY2MuTm9kZSk6IEJvb2xlYW4ge1xyXG4gICAgICAgIGlmIChwbGF5ZXIueCA+IHRoaXMucG9zaXRpb25SaWdodC54ICYmIHBsYXllci55IDwgdGhpcy5wb3NpdGlvblRvcC55ICYmIHBsYXllci55ID4gdGhpcy5wb3NpdGlvbkJvdHRvbS55KVxyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICBlbHNlXHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgT25UaGVUb3AocGxheWVyOiBjYy5Ob2RlKTogQm9vbGVhbiB7XHJcbiAgICAgICAgaWYgKHBsYXllci55ID4gdGhpcy5wb3NpdGlvblRvcC55ICYmIHBsYXllci54IDwgdGhpcy5wb3NpdGlvblJpZ2h0LnggJiYgcGxheWVyLnggPiB0aGlzLnBvc2l0aW9uTGVmdC54KVxyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICBlbHNlXHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgT25UaGVCb3R0b20ocGxheWVyOiBjYy5Ob2RlKTogQm9vbGVhbiB7XHJcbiAgICAgICAgaWYgKHBsYXllci55IDwgdGhpcy5wb3NpdGlvbkJvdHRvbS55ICYmIHBsYXllci54IDwgdGhpcy5wb3NpdGlvblJpZ2h0LnggJiYgcGxheWVyLnggPiB0aGlzLnBvc2l0aW9uTGVmdC54KVxyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICBlbHNlXHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxufVxyXG4iXX0=