
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Joystick/JoystickFollow.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '73034Tph19N/64CSzvxwsiQ', 'JoystickFollow');
// Scripts/Joystick/JoystickFollow.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var CharacterController_1 = require("../Character/CharacterController");
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var JoystickFollow = /** @class */ (function (_super) {
    __extends(JoystickFollow, _super);
    function JoystickFollow() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.joyRing = null;
        _this.joyDot = null;
        _this.stickPos = null;
        _this.touchLocation = null;
        _this.radius = 0;
        return _this;
        // update (dt) {}
    }
    JoystickFollow.prototype.onLoad = function () {
        this.radius = this.joyRing.width / 2;
    };
    JoystickFollow.prototype.start = function () {
        this.gameplayInstance = GamePlayInstance_1.default.Instance(GamePlayInstance_1.default);
        this.node.on(cc.Node.EventType.TOUCH_START, this.touchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.touchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.touchCancel, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.touchCancel, this);
    };
    JoystickFollow.prototype.touchStart = function (event) {
        if (!Global_1.default.boolendG && Global_1.default.boolStartPlay && !Global_1.default.boolStartAttacking) {
            var mousePos = event.getLocation();
            var localMousePos = this.node.convertToNodeSpaceAR(mousePos);
            this.node.opacity = 255;
            this.stickPos = localMousePos;
            this.touchLocation = event.getLocation();
            this.joyRing.setPosition(localMousePos);
            this.joyDot.setPosition(localMousePos);
            this.gameplayInstance.gameplay.Guide.active = false;
            // this.gameplayInstance.gameplay.txtSmasher.active = false;
            this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController_1.default).ArrowDirection.active = true;
        }
    };
    JoystickFollow.prototype.touchMove = function (event) {
        if (!Global_1.default.boolendG && Global_1.default.boolStartPlay && !Global_1.default.boolStartAttacking) {
            this.node.opacity = 255;
            Global_1.default.boolEnableTouch = true;
            if (!Global_1.default.boolFirstTouchJoyStick) {
                Global_1.default.boolFirstTouchJoyStick = true;
                if (this.gameplayInstance.gameplay.MyCharacter.name == 'MyDeadpool') {
                    this.gameplayInstance.gameplay.MyCharacter.getComponent(cc.SkeletonAnimation).play("Armature|sword_run");
                }
                else {
                    this.gameplayInstance.gameplay.MyCharacter.getComponent(cc.SkeletonAnimation).play("Armature|hammer_run");
                }
            }
            if (this.touchLocation === event.getLocation()) {
                return false;
            }
            this.gameplayInstance.gameplay.Guide.active = false;
            var touchPos = this.joyRing.convertToNodeSpaceAR(event.getLocation());
            var distance = touchPos.mag();
            var posX = this.stickPos.x + touchPos.x;
            var posY = this.stickPos.y + touchPos.y;
            var p = cc.v2(posX, posY).sub(this.joyRing.getPosition()).normalize();
            Global_1.default.touchPos = p;
            if (this.radius > distance) {
                this.joyDot.setPosition(cc.v2(posX, posY));
            }
            else {
                var x = this.stickPos.x + p.x * this.radius;
                var y = this.stickPos.y + p.y * this.radius;
                this.joyDot.setPosition(cc.v2(x, y));
            }
        }
        this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController_1.default).ArrowDirection.active = true;
    };
    JoystickFollow.prototype.touchCancel = function (event) {
        if (!Global_1.default.boolendG && Global_1.default.boolStartPlay && !Global_1.default.boolStartAttacking) {
            Global_1.default.boolEnableTouch = false;
            this.joyDot.setPosition(this.joyRing.getPosition());
            this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController_1.default).Attacking();
            this.node.opacity = 0;
            Global_1.default.boolFirstTouchJoyStick = false;
        }
    };
    __decorate([
        property(cc.Node)
    ], JoystickFollow.prototype, "joyRing", void 0);
    __decorate([
        property(cc.Node)
    ], JoystickFollow.prototype, "joyDot", void 0);
    JoystickFollow = __decorate([
        ccclass
    ], JoystickFollow);
    return JoystickFollow;
}(cc.Component));
exports.default = JoystickFollow;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcSm95c3RpY2tcXEpveXN0aWNrRm9sbG93LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLHdFQUFtRTtBQUNuRSwrREFBMEQ7QUFDMUQsMkNBQXNDO0FBRWhDLElBQUEsa0JBQW1DLEVBQWxDLG9CQUFPLEVBQUUsc0JBQXlCLENBQUM7QUFHMUM7SUFBNEMsa0NBQVk7SUFEeEQ7UUFBQSxxRUFzRkM7UUFsRkcsYUFBTyxHQUFZLElBQUksQ0FBQztRQUd4QixZQUFNLEdBQVksSUFBSSxDQUFDO1FBRXZCLGNBQVEsR0FBWSxJQUFJLENBQUM7UUFDekIsbUJBQWEsR0FBWSxJQUFJLENBQUM7UUFDOUIsWUFBTSxHQUFXLENBQUMsQ0FBQzs7UUEwRW5CLGlCQUFpQjtJQUNyQixDQUFDO0lBeEVHLCtCQUFNLEdBQU47UUFDSSxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztJQUN6QyxDQUFDO0lBRUQsOEJBQUssR0FBTDtRQUNJLElBQUksQ0FBQyxnQkFBZ0IsR0FBRywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQztRQUNwRSxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNuRSxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNqRSxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNsRSxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUN6RSxDQUFDO0lBRUQsbUNBQVUsR0FBVixVQUFXLEtBQUs7UUFDWixJQUFHLENBQUMsZ0JBQU0sQ0FBQyxRQUFRLElBQUksZ0JBQU0sQ0FBQyxhQUFhLElBQUksQ0FBQyxnQkFBTSxDQUFDLGtCQUFrQixFQUFFO1lBQ3ZFLElBQUksUUFBUSxHQUFHLEtBQUssQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUNuQyxJQUFJLGFBQWEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzdELElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQztZQUN4QixJQUFJLENBQUMsUUFBUSxHQUFHLGFBQWEsQ0FBQztZQUM5QixJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUN6QyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUN4QyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUN2QyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1lBQ3BELDREQUE0RDtZQUM1RCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsNkJBQW1CLENBQUMsQ0FBQyxjQUFjLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztTQUM3RztJQUNMLENBQUM7SUFFRCxrQ0FBUyxHQUFULFVBQVUsS0FBSztRQUNYLElBQUcsQ0FBQyxnQkFBTSxDQUFDLFFBQVEsSUFBSSxnQkFBTSxDQUFDLGFBQWEsSUFBSSxDQUFDLGdCQUFNLENBQUMsa0JBQWtCLEVBQUU7WUFDdkUsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDO1lBQ3hCLGdCQUFNLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztZQUM5QixJQUFHLENBQUMsZ0JBQU0sQ0FBQyxzQkFBc0IsRUFBRTtnQkFDL0IsZ0JBQU0sQ0FBQyxzQkFBc0IsR0FBRyxJQUFJLENBQUM7Z0JBQ3JDLElBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxJQUFJLFlBQVksRUFBRTtvQkFDaEUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO2lCQUM1RztxQkFBTTtvQkFDSCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUM7aUJBQzdHO2FBQ0o7WUFDRCxJQUFHLElBQUksQ0FBQyxhQUFhLEtBQUssS0FBSyxDQUFDLFdBQVcsRUFBRSxFQUFDO2dCQUMxQyxPQUFPLEtBQUssQ0FBQzthQUNoQjtZQUVELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFDcEQsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztZQUN0RSxJQUFJLFFBQVEsR0FBRyxRQUFRLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDOUIsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUN4QyxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQ3hDLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUMsU0FBUyxFQUFFLENBQUM7WUFDdEUsZ0JBQU0sQ0FBQyxRQUFRLEdBQUcsQ0FBQyxDQUFDO1lBQ3BCLElBQUcsSUFBSSxDQUFDLE1BQU0sR0FBRyxRQUFRLEVBQUU7Z0JBQ3ZCLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUM7YUFDOUM7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO2dCQUM1QyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7Z0JBQzVDLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDeEM7U0FDSjtRQUNELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyw2QkFBbUIsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO0lBQzlHLENBQUM7SUFFRCxvQ0FBVyxHQUFYLFVBQVksS0FBSztRQUNiLElBQUksQ0FBQyxnQkFBTSxDQUFDLFFBQVEsSUFBSSxnQkFBTSxDQUFDLGFBQWEsSUFBSSxDQUFDLGdCQUFNLENBQUMsa0JBQWtCLEVBQUU7WUFDeEUsZ0JBQU0sQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDO1lBQy9CLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztZQUNwRCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsNkJBQW1CLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQztZQUN6RixJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUM7WUFDdEIsZ0JBQU0sQ0FBQyxzQkFBc0IsR0FBRyxLQUFLLENBQUM7U0FDekM7SUFDTCxDQUFDO0lBL0VEO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7bURBQ007SUFHeEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztrREFDSztJQU5OLGNBQWM7UUFEbEMsT0FBTztPQUNhLGNBQWMsQ0FxRmxDO0lBQUQscUJBQUM7Q0FyRkQsQUFxRkMsQ0FyRjJDLEVBQUUsQ0FBQyxTQUFTLEdBcUZ2RDtrQkFyRm9CLGNBQWMiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgQ2hhcmFjdGVyQ29udHJvbGxlciBmcm9tIFwiLi4vQ2hhcmFjdGVyL0NoYXJhY3RlckNvbnRyb2xsZXJcIjtcclxuaW1wb3J0IEdhbWVQbGF5SW5zdGFuY2UgZnJvbSBcIi4uL0NvbW1vbi9HYW1lUGxheUluc3RhbmNlXCI7XHJcbmltcG9ydCBHbG9iYWwgZnJvbSBcIi4uL0NvbW1vbi9HbG9iYWxcIjtcclxuXHJcbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eX0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgSm95c3RpY2tGb2xsb3cgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgam95UmluZzogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBqb3lEb3Q6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIHN0aWNrUG9zOiBjYy5WZWMyID0gbnVsbDtcclxuICAgIHRvdWNoTG9jYXRpb246IGNjLlZlYzIgPSBudWxsO1xyXG4gICAgcmFkaXVzOiBudW1iZXIgPSAwO1xyXG4gICAgZ2FtZXBsYXlJbnN0YW5jZTogR2FtZVBsYXlJbnN0YW5jZTtcclxuXHJcbiAgICBvbkxvYWQgKCkge1xyXG4gICAgICAgIHRoaXMucmFkaXVzID0gdGhpcy5qb3lSaW5nLndpZHRoIC8gMjtcclxuICAgIH1cclxuXHJcbiAgICBzdGFydCAoKSB7XHJcbiAgICAgICAgdGhpcy5nYW1lcGxheUluc3RhbmNlID0gR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKTtcclxuICAgICAgICB0aGlzLm5vZGUub24oY2MuTm9kZS5FdmVudFR5cGUuVE9VQ0hfU1RBUlQsIHRoaXMudG91Y2hTdGFydCwgdGhpcyk7XHJcbiAgICAgICAgdGhpcy5ub2RlLm9uKGNjLk5vZGUuRXZlbnRUeXBlLlRPVUNIX01PVkUsIHRoaXMudG91Y2hNb3ZlLCB0aGlzKTtcclxuICAgICAgICB0aGlzLm5vZGUub24oY2MuTm9kZS5FdmVudFR5cGUuVE9VQ0hfRU5ELCB0aGlzLnRvdWNoQ2FuY2VsLCB0aGlzKTtcclxuICAgICAgICB0aGlzLm5vZGUub24oY2MuTm9kZS5FdmVudFR5cGUuVE9VQ0hfQ0FOQ0VMLCB0aGlzLnRvdWNoQ2FuY2VsLCB0aGlzKTtcclxuICAgIH1cclxuXHJcbiAgICB0b3VjaFN0YXJ0KGV2ZW50KSB7XHJcbiAgICAgICAgaWYoIUdsb2JhbC5ib29sZW5kRyAmJiBHbG9iYWwuYm9vbFN0YXJ0UGxheSAmJiAhR2xvYmFsLmJvb2xTdGFydEF0dGFja2luZykge1xyXG4gICAgICAgICAgICB2YXIgbW91c2VQb3MgPSBldmVudC5nZXRMb2NhdGlvbigpO1xyXG4gICAgICAgICAgICBsZXQgbG9jYWxNb3VzZVBvcyA9IHRoaXMubm9kZS5jb252ZXJ0VG9Ob2RlU3BhY2VBUihtb3VzZVBvcyk7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS5vcGFjaXR5ID0gMjU1O1xyXG4gICAgICAgICAgICB0aGlzLnN0aWNrUG9zID0gbG9jYWxNb3VzZVBvcztcclxuICAgICAgICAgICAgdGhpcy50b3VjaExvY2F0aW9uID0gZXZlbnQuZ2V0TG9jYXRpb24oKTtcclxuICAgICAgICAgICAgdGhpcy5qb3lSaW5nLnNldFBvc2l0aW9uKGxvY2FsTW91c2VQb3MpO1xyXG4gICAgICAgICAgICB0aGlzLmpveURvdC5zZXRQb3NpdGlvbihsb2NhbE1vdXNlUG9zKTtcclxuICAgICAgICAgICAgdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lkd1aWRlLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICAvLyB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkudHh0U21hc2hlci5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLmdldENvbXBvbmVudChDaGFyYWN0ZXJDb250cm9sbGVyKS5BcnJvd0RpcmVjdGlvbi5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICB0b3VjaE1vdmUoZXZlbnQpIHtcclxuICAgICAgICBpZighR2xvYmFsLmJvb2xlbmRHICYmIEdsb2JhbC5ib29sU3RhcnRQbGF5ICYmICFHbG9iYWwuYm9vbFN0YXJ0QXR0YWNraW5nKSB7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS5vcGFjaXR5ID0gMjU1O1xyXG4gICAgICAgICAgICBHbG9iYWwuYm9vbEVuYWJsZVRvdWNoID0gdHJ1ZTtcclxuICAgICAgICAgICAgaWYoIUdsb2JhbC5ib29sRmlyc3RUb3VjaEpveVN0aWNrKSB7XHJcbiAgICAgICAgICAgICAgICBHbG9iYWwuYm9vbEZpcnN0VG91Y2hKb3lTdGljayA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICBpZih0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIubmFtZSA9PSAnTXlEZWFkcG9vbCcpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIuZ2V0Q29tcG9uZW50KGNjLlNrZWxldG9uQW5pbWF0aW9uKS5wbGF5KFwiQXJtYXR1cmV8c3dvcmRfcnVuXCIpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIuZ2V0Q29tcG9uZW50KGNjLlNrZWxldG9uQW5pbWF0aW9uKS5wbGF5KFwiQXJtYXR1cmV8aGFtbWVyX3J1blwiKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZih0aGlzLnRvdWNoTG9jYXRpb24gPT09IGV2ZW50LmdldExvY2F0aW9uKCkpe1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuR3VpZGUuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgICAgIGxldCB0b3VjaFBvcyA9IHRoaXMuam95UmluZy5jb252ZXJ0VG9Ob2RlU3BhY2VBUihldmVudC5nZXRMb2NhdGlvbigpKTtcclxuICAgICAgICAgICAgbGV0IGRpc3RhbmNlID0gdG91Y2hQb3MubWFnKCk7XHJcbiAgICAgICAgICAgIGxldCBwb3NYID0gdGhpcy5zdGlja1Bvcy54ICsgdG91Y2hQb3MueDtcclxuICAgICAgICAgICAgbGV0IHBvc1kgPSB0aGlzLnN0aWNrUG9zLnkgKyB0b3VjaFBvcy55O1xyXG4gICAgICAgICAgICBsZXQgcCA9IGNjLnYyKHBvc1gsIHBvc1kpLnN1Yih0aGlzLmpveVJpbmcuZ2V0UG9zaXRpb24oKSkubm9ybWFsaXplKCk7XHJcbiAgICAgICAgICAgIEdsb2JhbC50b3VjaFBvcyA9IHA7XHJcbiAgICAgICAgICAgIGlmKHRoaXMucmFkaXVzID4gZGlzdGFuY2UpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuam95RG90LnNldFBvc2l0aW9uKGNjLnYyKHBvc1gsIHBvc1kpKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGxldCB4ID0gdGhpcy5zdGlja1Bvcy54ICsgcC54ICogdGhpcy5yYWRpdXM7XHJcbiAgICAgICAgICAgICAgICBsZXQgeSA9IHRoaXMuc3RpY2tQb3MueSArIHAueSAqIHRoaXMucmFkaXVzO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5qb3lEb3Quc2V0UG9zaXRpb24oY2MudjIoeCwgeSkpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci5nZXRDb21wb25lbnQoQ2hhcmFjdGVyQ29udHJvbGxlcikuQXJyb3dEaXJlY3Rpb24uYWN0aXZlID0gdHJ1ZTtcclxuICAgIH1cclxuXHJcbiAgICB0b3VjaENhbmNlbChldmVudCkge1xyXG4gICAgICAgIGlmICghR2xvYmFsLmJvb2xlbmRHICYmIEdsb2JhbC5ib29sU3RhcnRQbGF5ICYmICFHbG9iYWwuYm9vbFN0YXJ0QXR0YWNraW5nKSB7XHJcbiAgICAgICAgICAgIEdsb2JhbC5ib29sRW5hYmxlVG91Y2ggPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5qb3lEb3Quc2V0UG9zaXRpb24odGhpcy5qb3lSaW5nLmdldFBvc2l0aW9uKCkpO1xyXG4gICAgICAgICAgICB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIuZ2V0Q29tcG9uZW50KENoYXJhY3RlckNvbnRyb2xsZXIpLkF0dGFja2luZygpO1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUub3BhY2l0eSA9IDA7XHJcbiAgICAgICAgICAgIEdsb2JhbC5ib29sRmlyc3RUb3VjaEpveVN0aWNrID0gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8vIHVwZGF0ZSAoZHQpIHt9XHJcbn1cclxuIl19