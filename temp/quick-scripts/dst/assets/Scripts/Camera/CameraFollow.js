
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Camera/CameraFollow.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '9a3c4BNdsNGOKf91Tc0+3pS', 'CameraFollow');
// Scripts/Camera/CameraFollow.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var CharacterController_1 = require("../Character/CharacterController");
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var CameraFollow = /** @class */ (function (_super) {
    __extends(CameraFollow, _super);
    function CameraFollow() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.gameplayInstance = null;
        _this.cameraOffsetX = 0;
        _this.cameraOffsetY = 0;
        _this.cameraOffsetZ = 0;
        _this.Target = null;
        //9,12
        _this.plusY = 0;
        _this.plusZ = 0;
        return _this;
    }
    CameraFollow.prototype.start = function () {
        this.gameplayInstance = GamePlayInstance_1.default.Instance(GamePlayInstance_1.default);
        this.cameraOffsetX = this.node.x - 0;
        this.cameraOffsetY = this.node.y + 45;
        this.cameraOffsetZ = this.node.z + 5;
    };
    CameraFollow.prototype.update = function () {
        if (Global_1.default.boolStartPlay && !Global_1.default.boolendG) {
            //let newPosX = this.gameplayInstance.gameplay.MyCharacter.x + this.cameraOffsetX;
            //let newPosZ = this.gameplayInstance.gameplay.MyCharacter.z + this.cameraOffsetZ;
            //if (!Global.boolendG) {
            if (this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController_1.default).level == 0) {
                this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.MyCharacter.x + this.cameraOffsetX, 0.2);
                this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.MyCharacter.y + this.cameraOffsetY, 0.2);
                this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.MyCharacter.z + this.cameraOffsetZ, 0.2);
            }
            // else if (this.gameplayInstance.gameplay.myCharacter.getComponent(CharacterController).level == 1) {
            //     this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.myCharacter.x + this.cameraOffsetX, 0.2);
            //     this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.myCharacter.y + this.cameraOffsetY - this.plusY, 0.2);
            //     this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.myCharacter.z + this.cameraOffsetZ + this.plusZ, 0.2);
            // }
            // else if (this.gameplayInstance.gameplay.myCharacter.getComponent(CharacterController).level == 2) {
            //     this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.myCharacter.x + this.cameraOffsetX, 0.2);
            //     this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.myCharacter.y + this.cameraOffsetY - this.plusY, 0.2);
            //     this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.myCharacter.z + this.cameraOffsetZ + this.plusZ, 0.2);
            // }
            // else if (this.gameplayInstance.gameplay.myCharacter.getComponent(CharacterController).level == 3) {
            //     this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.myCharacter.x + this.cameraOffsetX, 0.2);
            //     this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.myCharacter.y + this.cameraOffsetY - this.plusY, 0.2);
            //     this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.myCharacter.z + this.cameraOffsetZ + this.plusZ, 0.2);
            // }
            // else if (this.gameplayInstance.gameplay.myCharacter.getComponent(CharacterController).level == 4) {
            //     this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.myCharacter.x + this.cameraOffsetX, 0.2);
            //     this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.myCharacter.y + this.cameraOffsetY - this.plusY, 0.2);
            //     this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.myCharacter.z + this.cameraOffsetZ + this.plusZ, 0.2);
            // }
            //}
            else if (this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController_1.default).level == 1) {
                //this.resetOffset(0, 12, 9);
                this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.MyCharacter.x + this.cameraOffsetX, 0.2);
                this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.MyCharacter.y + this.cameraOffsetY - 12, 0.2);
                this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.MyCharacter.z + this.cameraOffsetZ + 9, 0.2);
            }
            else if (this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController_1.default).level == 2) {
                this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.MyCharacter.x + this.cameraOffsetX, 0.2);
                this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.MyCharacter.y + this.cameraOffsetY - 24, 0.2);
                this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.MyCharacter.z + this.cameraOffsetZ + 18, 0.2);
            }
            else if (this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController_1.default).level == 3) {
                this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.MyCharacter.x + this.cameraOffsetX, 0.2);
                this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.MyCharacter.y + this.cameraOffsetY - 36, 0.2);
                this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.MyCharacter.z + this.cameraOffsetZ + 27, 0.2);
            }
            else if (this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController_1.default).level == 4) {
                this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.MyCharacter.x + this.cameraOffsetX, 0.2);
                this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.MyCharacter.y + this.cameraOffsetY - 48, 0.2);
                this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.MyCharacter.z + this.cameraOffsetZ + 36, 0.2);
            }
        }
        else if (Global_1.default.boolendG) {
            this.node.x = 0;
            this.node.y = -120;
            this.node.z = 120;
            this.node.eulerAngles = new cc.Vec3(38, 0, 0);
        }
        // else {
        //     for (let i = 0; i < this.gameplayInstance.gameplay.enemyParent.childrenCount; i++) {
        //         this.Target = this.gameplayInstance.gameplay.enemyParent.children[i];
        //     }
        //     this.resetOffset();
        //     this.node.x = cc.misc.lerp(this.node.x, this.Target.x / 2, 0.2);
        //     this.node.y = cc.misc.lerp(this.node.y, this.Target.y + this.cameraOffsetY, 0.2);
        //     this.node.z = cc.misc.lerp(this.node.z, this.Target.z + this.cameraOffsetZ, 0.2);
        //}
        //}
    };
    // resetOffset(x: number, y: number, z: number) {
    //     this.cameraOffsetX = this.cameraOffsetX - x;
    //     this.cameraOffsetY = this.cameraOffsetY - y;
    //     this.cameraOffsetZ = this.cameraOffsetZ + z;
    // }
    CameraFollow.prototype.PlusYZ = function () {
        this.plusY += 12;
        this.plusZ += 9;
    };
    CameraFollow = __decorate([
        ccclass
    ], CameraFollow);
    return CameraFollow;
}(cc.Component));
exports.default = CameraFollow;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ2FtZXJhXFxDYW1lcmFGb2xsb3cudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRUEsd0VBQW1FO0FBQ25FLCtEQUEwRDtBQUMxRCwyQ0FBc0M7QUFFaEMsSUFBQSxrQkFBcUMsRUFBbkMsb0JBQU8sRUFBRSxzQkFBMEIsQ0FBQztBQUc1QztJQUEwQyxnQ0FBWTtJQUR0RDtRQUFBLHFFQStGQztRQTdGRyxzQkFBZ0IsR0FBcUIsSUFBSSxDQUFDO1FBQzFDLG1CQUFhLEdBQVcsQ0FBQyxDQUFDO1FBQzFCLG1CQUFhLEdBQVcsQ0FBQyxDQUFDO1FBQzFCLG1CQUFhLEdBQVcsQ0FBQyxDQUFDO1FBQzFCLFlBQU0sR0FBWSxJQUFJLENBQUM7UUFDdkIsTUFBTTtRQUNOLFdBQUssR0FBVyxDQUFDLENBQUM7UUFDbEIsV0FBSyxHQUFXLENBQUMsQ0FBQzs7SUFzRnRCLENBQUM7SUFyRkcsNEJBQUssR0FBTDtRQUNJLElBQUksQ0FBQyxnQkFBZ0IsR0FBRywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQztRQUNwRSxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNyQyxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztRQUN0QyxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUN6QyxDQUFDO0lBQ0QsNkJBQU0sR0FBTjtRQUNJLElBQUksZ0JBQU0sQ0FBQyxhQUFhLElBQUksQ0FBQyxnQkFBTSxDQUFDLFFBQVEsRUFBRTtZQUMxQyxrRkFBa0Y7WUFDbEYsa0ZBQWtGO1lBQ2xGLHlCQUF5QjtZQUN6QixJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyw2QkFBbUIsQ0FBQyxDQUFDLEtBQUssSUFBSSxDQUFDLEVBQUU7Z0JBQ3pGLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxFQUFFLEdBQUcsQ0FBQyxDQUFDO2dCQUNoSCxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGFBQWEsRUFBRSxHQUFHLENBQUMsQ0FBQztnQkFDaEgsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLEVBQUUsR0FBRyxDQUFDLENBQUM7YUFDbkg7WUFDRCxzR0FBc0c7WUFDdEcsdUhBQXVIO1lBQ3ZILG9JQUFvSTtZQUNwSSxvSUFBb0k7WUFDcEksSUFBSTtZQUNKLHNHQUFzRztZQUN0Ryx1SEFBdUg7WUFDdkgsb0lBQW9JO1lBQ3BJLG9JQUFvSTtZQUNwSSxJQUFJO1lBQ0osc0dBQXNHO1lBQ3RHLHVIQUF1SDtZQUN2SCxvSUFBb0k7WUFDcEksb0lBQW9JO1lBQ3BJLElBQUk7WUFDSixzR0FBc0c7WUFDdEcsdUhBQXVIO1lBQ3ZILG9JQUFvSTtZQUNwSSxvSUFBb0k7WUFDcEksSUFBSTtZQUNKLEdBQUc7aUJBQ0UsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsNkJBQW1CLENBQUMsQ0FBQyxLQUFLLElBQUksQ0FBQyxFQUFFO2dCQUM5Riw2QkFBNkI7Z0JBQzdCLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxFQUFFLEdBQUcsQ0FBQyxDQUFDO2dCQUNoSCxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBQ3JILElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxHQUFHLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQzthQUN2SDtpQkFDSSxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyw2QkFBbUIsQ0FBQyxDQUFDLEtBQUssSUFBSSxDQUFDLEVBQUU7Z0JBQzlGLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxFQUFFLEdBQUcsQ0FBQyxDQUFDO2dCQUNoSCxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBQ3JILElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsRUFBRSxHQUFHLENBQUMsQ0FBQzthQUN4SDtpQkFDSSxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyw2QkFBbUIsQ0FBQyxDQUFDLEtBQUssSUFBSSxDQUFDLEVBQUU7Z0JBQzlGLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxFQUFFLEdBQUcsQ0FBQyxDQUFDO2dCQUNoSCxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBQ3JILElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsRUFBRSxHQUFHLENBQUMsQ0FBQzthQUN4SDtpQkFDSSxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyw2QkFBbUIsQ0FBQyxDQUFDLEtBQUssSUFBSSxDQUFDLEVBQUU7Z0JBQzlGLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxFQUFFLEdBQUcsQ0FBQyxDQUFDO2dCQUNoSCxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBQ3JILElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsRUFBRSxHQUFHLENBQUMsQ0FBQzthQUN4SDtTQUNKO2FBQ0ksSUFBRyxnQkFBTSxDQUFDLFFBQVEsRUFBQztZQUNwQixJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDaEIsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUM7WUFDbkIsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDO1lBQ2xCLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxDQUFDO1NBQy9DO1FBQ0QsU0FBUztRQUNULDJGQUEyRjtRQUMzRixnRkFBZ0Y7UUFDaEYsUUFBUTtRQUNSLDBCQUEwQjtRQUMxQix1RUFBdUU7UUFDdkUsd0ZBQXdGO1FBQ3hGLHdGQUF3RjtRQUN4RixHQUFHO1FBQ0gsR0FBRztJQUNQLENBQUM7SUFDRCxpREFBaUQ7SUFDakQsbURBQW1EO0lBQ25ELG1EQUFtRDtJQUNuRCxtREFBbUQ7SUFDbkQsSUFBSTtJQUNKLDZCQUFNLEdBQU47UUFDSSxJQUFJLENBQUMsS0FBSyxJQUFJLEVBQUUsQ0FBQztRQUNqQixJQUFJLENBQUMsS0FBSyxJQUFJLENBQUMsQ0FBQztJQUNwQixDQUFDO0lBN0ZnQixZQUFZO1FBRGhDLE9BQU87T0FDYSxZQUFZLENBOEZoQztJQUFELG1CQUFDO0NBOUZELEFBOEZDLENBOUZ5QyxFQUFFLENBQUMsU0FBUyxHQThGckQ7a0JBOUZvQixZQUFZIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiXHJcblxyXG5pbXBvcnQgQ2hhcmFjdGVyQ29udHJvbGxlciBmcm9tIFwiLi4vQ2hhcmFjdGVyL0NoYXJhY3RlckNvbnRyb2xsZXJcIjtcclxuaW1wb3J0IEdhbWVQbGF5SW5zdGFuY2UgZnJvbSBcIi4uL0NvbW1vbi9HYW1lUGxheUluc3RhbmNlXCI7XHJcbmltcG9ydCBHbG9iYWwgZnJvbSBcIi4uL0NvbW1vbi9HbG9iYWxcIjtcclxuXHJcbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XHJcblxyXG5AY2NjbGFzc1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBDYW1lcmFGb2xsb3cgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xyXG4gICAgZ2FtZXBsYXlJbnN0YW5jZTogR2FtZVBsYXlJbnN0YW5jZSA9IG51bGw7XHJcbiAgICBjYW1lcmFPZmZzZXRYOiBudW1iZXIgPSAwO1xyXG4gICAgY2FtZXJhT2Zmc2V0WTogbnVtYmVyID0gMDtcclxuICAgIGNhbWVyYU9mZnNldFo6IG51bWJlciA9IDA7XHJcbiAgICBUYXJnZXQ6IGNjLk5vZGUgPSBudWxsO1xyXG4gICAgLy85LDEyXHJcbiAgICBwbHVzWTogbnVtYmVyID0gMDtcclxuICAgIHBsdXNaOiBudW1iZXIgPSAwO1xyXG4gICAgc3RhcnQoKSB7XHJcbiAgICAgICAgdGhpcy5nYW1lcGxheUluc3RhbmNlID0gR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKTtcclxuICAgICAgICB0aGlzLmNhbWVyYU9mZnNldFggPSB0aGlzLm5vZGUueCAtIDA7XHJcbiAgICAgICAgdGhpcy5jYW1lcmFPZmZzZXRZID0gdGhpcy5ub2RlLnkgKyA0NTtcclxuICAgICAgICB0aGlzLmNhbWVyYU9mZnNldFogPSB0aGlzLm5vZGUueiArIDU7XHJcbiAgICB9XHJcbiAgICB1cGRhdGUoKSB7XHJcbiAgICAgICAgaWYgKEdsb2JhbC5ib29sU3RhcnRQbGF5ICYmICFHbG9iYWwuYm9vbGVuZEcpIHtcclxuICAgICAgICAgICAgLy9sZXQgbmV3UG9zWCA9IHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci54ICsgdGhpcy5jYW1lcmFPZmZzZXRYO1xyXG4gICAgICAgICAgICAvL2xldCBuZXdQb3NaID0gdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLnogKyB0aGlzLmNhbWVyYU9mZnNldFo7XHJcbiAgICAgICAgICAgIC8vaWYgKCFHbG9iYWwuYm9vbGVuZEcpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci5nZXRDb21wb25lbnQoQ2hhcmFjdGVyQ29udHJvbGxlcikubGV2ZWwgPT0gMCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLnggPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLngsIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci54ICsgdGhpcy5jYW1lcmFPZmZzZXRYLCAwLjIpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLnkgPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLnksIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci55ICsgdGhpcy5jYW1lcmFPZmZzZXRZLCAwLjIpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLnogPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLnosIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci56ICsgdGhpcy5jYW1lcmFPZmZzZXRaLCAwLjIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC8vIGVsc2UgaWYgKHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5teUNoYXJhY3Rlci5nZXRDb21wb25lbnQoQ2hhcmFjdGVyQ29udHJvbGxlcikubGV2ZWwgPT0gMSkge1xyXG4gICAgICAgICAgICAvLyAgICAgdGhpcy5ub2RlLnggPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLngsIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5teUNoYXJhY3Rlci54ICsgdGhpcy5jYW1lcmFPZmZzZXRYLCAwLjIpO1xyXG4gICAgICAgICAgICAvLyAgICAgdGhpcy5ub2RlLnkgPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLnksIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5teUNoYXJhY3Rlci55ICsgdGhpcy5jYW1lcmFPZmZzZXRZIC0gdGhpcy5wbHVzWSwgMC4yKTtcclxuICAgICAgICAgICAgLy8gICAgIHRoaXMubm9kZS56ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS56LCB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkubXlDaGFyYWN0ZXIueiArIHRoaXMuY2FtZXJhT2Zmc2V0WiArIHRoaXMucGx1c1osIDAuMik7XHJcbiAgICAgICAgICAgIC8vIH1cclxuICAgICAgICAgICAgLy8gZWxzZSBpZiAodGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLmdldENvbXBvbmVudChDaGFyYWN0ZXJDb250cm9sbGVyKS5sZXZlbCA9PSAyKSB7XHJcbiAgICAgICAgICAgIC8vICAgICB0aGlzLm5vZGUueCA9IGNjLm1pc2MubGVycCh0aGlzLm5vZGUueCwgdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLnggKyB0aGlzLmNhbWVyYU9mZnNldFgsIDAuMik7XHJcbiAgICAgICAgICAgIC8vICAgICB0aGlzLm5vZGUueSA9IGNjLm1pc2MubGVycCh0aGlzLm5vZGUueSwgdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLnkgKyB0aGlzLmNhbWVyYU9mZnNldFkgLSB0aGlzLnBsdXNZLCAwLjIpO1xyXG4gICAgICAgICAgICAvLyAgICAgdGhpcy5ub2RlLnogPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLnosIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5teUNoYXJhY3Rlci56ICsgdGhpcy5jYW1lcmFPZmZzZXRaICsgdGhpcy5wbHVzWiwgMC4yKTtcclxuICAgICAgICAgICAgLy8gfVxyXG4gICAgICAgICAgICAvLyBlbHNlIGlmICh0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkubXlDaGFyYWN0ZXIuZ2V0Q29tcG9uZW50KENoYXJhY3RlckNvbnRyb2xsZXIpLmxldmVsID09IDMpIHtcclxuICAgICAgICAgICAgLy8gICAgIHRoaXMubm9kZS54ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS54LCB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkubXlDaGFyYWN0ZXIueCArIHRoaXMuY2FtZXJhT2Zmc2V0WCwgMC4yKTtcclxuICAgICAgICAgICAgLy8gICAgIHRoaXMubm9kZS55ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS55LCB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkubXlDaGFyYWN0ZXIueSArIHRoaXMuY2FtZXJhT2Zmc2V0WSAtIHRoaXMucGx1c1ksIDAuMik7XHJcbiAgICAgICAgICAgIC8vICAgICB0aGlzLm5vZGUueiA9IGNjLm1pc2MubGVycCh0aGlzLm5vZGUueiwgdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLnogKyB0aGlzLmNhbWVyYU9mZnNldFogKyB0aGlzLnBsdXNaLCAwLjIpO1xyXG4gICAgICAgICAgICAvLyB9XHJcbiAgICAgICAgICAgIC8vIGVsc2UgaWYgKHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5teUNoYXJhY3Rlci5nZXRDb21wb25lbnQoQ2hhcmFjdGVyQ29udHJvbGxlcikubGV2ZWwgPT0gNCkge1xyXG4gICAgICAgICAgICAvLyAgICAgdGhpcy5ub2RlLnggPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLngsIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5teUNoYXJhY3Rlci54ICsgdGhpcy5jYW1lcmFPZmZzZXRYLCAwLjIpO1xyXG4gICAgICAgICAgICAvLyAgICAgdGhpcy5ub2RlLnkgPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLnksIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5teUNoYXJhY3Rlci55ICsgdGhpcy5jYW1lcmFPZmZzZXRZIC0gdGhpcy5wbHVzWSwgMC4yKTtcclxuICAgICAgICAgICAgLy8gICAgIHRoaXMubm9kZS56ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS56LCB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkubXlDaGFyYWN0ZXIueiArIHRoaXMuY2FtZXJhT2Zmc2V0WiArIHRoaXMucGx1c1osIDAuMik7XHJcbiAgICAgICAgICAgIC8vIH1cclxuICAgICAgICAgICAgLy99XHJcbiAgICAgICAgICAgIGVsc2UgaWYgKHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci5nZXRDb21wb25lbnQoQ2hhcmFjdGVyQ29udHJvbGxlcikubGV2ZWwgPT0gMSkge1xyXG4gICAgICAgICAgICAgICAgLy90aGlzLnJlc2V0T2Zmc2V0KDAsIDEyLCA5KTtcclxuICAgICAgICAgICAgICAgIHRoaXMubm9kZS54ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS54LCB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueCArIHRoaXMuY2FtZXJhT2Zmc2V0WCwgMC4yKTtcclxuICAgICAgICAgICAgICAgIHRoaXMubm9kZS55ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS55LCB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueSArIHRoaXMuY2FtZXJhT2Zmc2V0WSAtIDEyLCAwLjIpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLnogPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLnosIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci56ICsgdGhpcy5jYW1lcmFPZmZzZXRaICsgOSwgMC4yKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBlbHNlIGlmICh0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIuZ2V0Q29tcG9uZW50KENoYXJhY3RlckNvbnRyb2xsZXIpLmxldmVsID09IDIpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubm9kZS54ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS54LCB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueCArIHRoaXMuY2FtZXJhT2Zmc2V0WCwgMC4yKTtcclxuICAgICAgICAgICAgICAgIHRoaXMubm9kZS55ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS55LCB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueSArIHRoaXMuY2FtZXJhT2Zmc2V0WSAtIDI0LCAwLjIpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLnogPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLnosIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci56ICsgdGhpcy5jYW1lcmFPZmZzZXRaICsgMTgsIDAuMik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZWxzZSBpZiAodGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLmdldENvbXBvbmVudChDaGFyYWN0ZXJDb250cm9sbGVyKS5sZXZlbCA9PSAzKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vZGUueCA9IGNjLm1pc2MubGVycCh0aGlzLm5vZGUueCwgdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLnggKyB0aGlzLmNhbWVyYU9mZnNldFgsIDAuMik7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vZGUueSA9IGNjLm1pc2MubGVycCh0aGlzLm5vZGUueSwgdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLnkgKyB0aGlzLmNhbWVyYU9mZnNldFkgLSAzNiwgMC4yKTtcclxuICAgICAgICAgICAgICAgIHRoaXMubm9kZS56ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS56LCB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueiArIHRoaXMuY2FtZXJhT2Zmc2V0WiArIDI3LCAwLjIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGVsc2UgaWYgKHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci5nZXRDb21wb25lbnQoQ2hhcmFjdGVyQ29udHJvbGxlcikubGV2ZWwgPT0gNCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLnggPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLngsIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci54ICsgdGhpcy5jYW1lcmFPZmZzZXRYLCAwLjIpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLnkgPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLnksIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci55ICsgdGhpcy5jYW1lcmFPZmZzZXRZIC0gNDgsIDAuMik7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vZGUueiA9IGNjLm1pc2MubGVycCh0aGlzLm5vZGUueiwgdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLnogKyB0aGlzLmNhbWVyYU9mZnNldFogKyAzNiwgMC4yKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNlIGlmKEdsb2JhbC5ib29sZW5kRyl7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS54ID0gMDtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLnkgPSAtMTIwO1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUueiA9IDEyMDtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLmV1bGVyQW5nbGVzID0gbmV3IGNjLlZlYzMoMzgsMCwwKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLy8gZWxzZSB7XHJcbiAgICAgICAgLy8gICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5LmVuZW15UGFyZW50LmNoaWxkcmVuQ291bnQ7IGkrKykge1xyXG4gICAgICAgIC8vICAgICAgICAgdGhpcy5UYXJnZXQgPSB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuZW5lbXlQYXJlbnQuY2hpbGRyZW5baV07XHJcbiAgICAgICAgLy8gICAgIH1cclxuICAgICAgICAvLyAgICAgdGhpcy5yZXNldE9mZnNldCgpO1xyXG4gICAgICAgIC8vICAgICB0aGlzLm5vZGUueCA9IGNjLm1pc2MubGVycCh0aGlzLm5vZGUueCwgdGhpcy5UYXJnZXQueCAvIDIsIDAuMik7XHJcbiAgICAgICAgLy8gICAgIHRoaXMubm9kZS55ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS55LCB0aGlzLlRhcmdldC55ICsgdGhpcy5jYW1lcmFPZmZzZXRZLCAwLjIpO1xyXG4gICAgICAgIC8vICAgICB0aGlzLm5vZGUueiA9IGNjLm1pc2MubGVycCh0aGlzLm5vZGUueiwgdGhpcy5UYXJnZXQueiArIHRoaXMuY2FtZXJhT2Zmc2V0WiwgMC4yKTtcclxuICAgICAgICAvL31cclxuICAgICAgICAvL31cclxuICAgIH1cclxuICAgIC8vIHJlc2V0T2Zmc2V0KHg6IG51bWJlciwgeTogbnVtYmVyLCB6OiBudW1iZXIpIHtcclxuICAgIC8vICAgICB0aGlzLmNhbWVyYU9mZnNldFggPSB0aGlzLmNhbWVyYU9mZnNldFggLSB4O1xyXG4gICAgLy8gICAgIHRoaXMuY2FtZXJhT2Zmc2V0WSA9IHRoaXMuY2FtZXJhT2Zmc2V0WSAtIHk7XHJcbiAgICAvLyAgICAgdGhpcy5jYW1lcmFPZmZzZXRaID0gdGhpcy5jYW1lcmFPZmZzZXRaICsgejtcclxuICAgIC8vIH1cclxuICAgIFBsdXNZWigpIHtcclxuICAgICAgICB0aGlzLnBsdXNZICs9IDEyO1xyXG4gICAgICAgIHRoaXMucGx1c1ogKz0gOTtcclxuICAgIH1cclxufVxyXG4iXX0=