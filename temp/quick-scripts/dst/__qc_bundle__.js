
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/__qc_index__.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}
require('./assets/Scripts/Camera/CameraFollow');
require('./assets/Scripts/Character/CharacterController');
require('./assets/Scripts/ColliderObj/MapController');
require('./assets/Scripts/ColliderObj/SquareCollider');
require('./assets/Scripts/Common/AdManager');
require('./assets/Scripts/Common/EnumDefine');
require('./assets/Scripts/Common/GamePlayInstance');
require('./assets/Scripts/Common/Global');
require('./assets/Scripts/Common/KeyEvent');
require('./assets/Scripts/Common/PlatformBtn');
require('./assets/Scripts/Common/Random');
require('./assets/Scripts/Common/Singleton');
require('./assets/Scripts/Common/SoundManager');
require('./assets/Scripts/Common/Utility');
require('./assets/Scripts/Enemy/EnemyController');
require('./assets/Scripts/GamePlay/EndCard1');
require('./assets/Scripts/GamePlay/Roblox2');
require('./assets/Scripts/GamePlay/StickMan1');
require('./assets/Scripts/GamePlay/StickMan2');
require('./assets/Scripts/GamePlay/StickMan3');
require('./assets/Scripts/GamePlay/StickMan4');
require('./assets/Scripts/Joystick/JoystickFollow');
require('./assets/Scripts/VFX/BloodController');
require('./assets/Scripts/VFX/DestroyVFX');

                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Enemy/EnemyController.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '6fb44ar/vdHCqZo6WpsF2GK', 'EnemyController');
// Scripts/Enemy/EnemyController.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var CharacterController_1 = require("../Character/CharacterController");
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var KeyEvent_1 = require("../Common/KeyEvent");
var Random_1 = require("../Common/Random");
var Utility_1 = require("../Common/Utility");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var EnemyController = /** @class */ (function (_super) {
    __extends(EnemyController, _super);
    function EnemyController() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.clampLeft = 0;
        _this.clampRight = 0;
        _this.clampTop = 0;
        _this.clampBottom = 0;
        _this.PointShoot = null;
        _this.bodyDeath = null;
        _this.effectBlood = null;
        _this.posBlood = null;
        _this.weapon = null;
        _this.moveX = 0;
        _this.moveY = 0;
        _this.moveZ = 0;
        _this.degree = 0;
        _this.boolCheckAttacking = false;
        _this.boolCheckAttacked = false;
        _this.boolEnemyDeath = false;
        _this.checkFollowPlayer = false;
        _this.checkDeath = false;
        return _this;
    }
    EnemyController.prototype.onLoad = function () {
        // this.node.getComponent(cc.BoxCollider3D).enabled = true;
    };
    EnemyController.prototype.update = function () {
        if (!this.checkDeath) {
            if (Global_1.default.boolStartAttacking && Global_1.default.boolCheckAttacking) {
                var pos1 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.convertToWorldSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.children[0].getPosition()));
                var pos2 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.convertToWorldSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.children[1].getPosition()));
                var pos3 = cc.Canvas.instance.node.convertToNodeSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.convertToWorldSpaceAR(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.children[2].getPosition()));
                var direction1 = cc.v2(pos1.x - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.x, pos1.y - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.y);
                var direction2 = cc.v2(pos2.x - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.x, pos2.y - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.y);
                var degree = direction1.signAngle(direction2);
                degree = cc.misc.radiansToDegrees(degree);
                var posEnemy = cc.v2(this.node.x - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.x, this.node.y - GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.y);
                var degreeWithPos1 = posEnemy.signAngle(direction1);
                degreeWithPos1 = cc.misc.radiansToDegrees(degreeWithPos1);
                var degreeWithPos2 = posEnemy.signAngle(direction2);
                degreeWithPos2 = cc.misc.radiansToDegrees(degreeWithPos2);
                var realNeed = 0;
                degreeWithPos1 = Math.abs(degreeWithPos1);
                degreeWithPos2 = Math.abs(degreeWithPos2);
                if (degreeWithPos1 > degreeWithPos2) {
                    realNeed = degreeWithPos1;
                }
                else {
                    realNeed = degreeWithPos2;
                }
                var distance = Utility_1.default.Instance(Utility_1.default).Distance(cc.v2(this.node.x, this.node.y), cc.v2(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.x, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.y));
                var maxDistance = Utility_1.default.Instance(Utility_1.default).Distance(cc.v2(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.x, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.y), cc.v2(pos3.x, pos3.y));
                if (Math.abs(realNeed) < degree) {
                    if (distance < maxDistance) {
                        if (Global_1.default.boolCheckAttacked && !this.boolEnemyDeath) {
                            GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.getComponent(CharacterController_1.default).LevelUpPlayer();
                            // this.DestroyByKatana();
                            this.DestroyByHammer();
                            this.checkDeath = true;
                            Global_1.default.boolCheckAttacked = false;
                            Global_1.default.boolCheckAttacking = false;
                        }
                    }
                }
            }
        }
    };
    EnemyController.prototype.StartMove = function () {
        var _this = this;
        this.schedule(function () {
            _this.moveEnemy();
        }, 6.5, cc.macro.REPEAT_FOREVER, 0.1);
    };
    EnemyController.prototype.DestroyByHammer = function () {
        this.boolEnemyDeath = true;
        cc.audioEngine.playEffect(Global_1.default.soundScream, false);
        GamePlayInstance_1.instance.emit(KeyEvent_1.default.plusEnemy);
        GamePlayInstance_1.instance.emit(KeyEvent_1.default.scale);
        this.node.stopAllActions();
        this.unscheduleAllCallbacks();
        this.node.getComponent(cc.SkeletonAnimation).stop();
        this.node.eulerAngles = new cc.Vec3(0, 0, -this.node.eulerAngles.z);
        this.weapon.active = false;
        this.node.scaleZ = 12;
        this.node.z = this.moveZ + 1;
        this.checkDeath = true;
        this.SpawnerBlood();
        this.SpawnerEffectBlood(this.node.x, this.node.y, this.node.z + 2);
    };
    EnemyController.prototype.DestroyByKatana = function () {
        this.boolEnemyDeath = true;
        cc.audioEngine.playEffect(Global_1.default.soundScream, false);
        GamePlayInstance_1.instance.emit(KeyEvent_1.default.plusEnemy);
        GamePlayInstance_1.instance.emit(KeyEvent_1.default.scale);
        this.checkDeath = true;
        this.SpawnerBodyDeath(0.7);
        this.SpawnerEffectBlood(this.node.x, this.node.y, this.node.z);
    };
    EnemyController.prototype.SpawnerBlood = function () {
        this.bodyDeath.active = true;
        var pos = this.node.convertToWorldSpaceAR(this.posBlood.getPosition());
        pos = cc.Canvas.instance.node.convertToNodeSpaceAR(pos);
        this.bodyDeath.setPosition(pos.x, pos.y, 0);
        this.bodyDeath.eulerAngles = cc.v3(90, 0, this.node.eulerAngles.z);
    };
    EnemyController.prototype.SpawnerEffectBlood = function (x, y, z) {
        var EffectBlood = cc.instantiate(this.effectBlood);
        EffectBlood.parent = cc.Canvas.instance.node;
        EffectBlood.x = x;
        EffectBlood.y = y;
        EffectBlood.z = z + 1;
    };
    EnemyController.prototype.SpawnerBodyDeath = function (timing) {
        var _this = this;
        this.bodyDeath.active = true;
        this.bodyDeath.setPosition(this.node.x, this.node.y, this.node.z);
        this.bodyDeath.eulerAngles = cc.v3(-90, 180, this.node.eulerAngles.z);
        this.node.opacity = 0;
        this.scheduleOnce(function () {
            _this.bodyDeath.getComponent(cc.SkeletonAnimation).stop();
            _this.node.destroy();
        }, timing);
    };
    EnemyController.prototype.moveEnemy = function () {
        var _this = this;
        this.node.getComponent(cc.SkeletonAnimation).play("Armature|sword_run");
        if (this.checkFollowPlayer) {
            var Distance = Utility_1.default.Instance(Utility_1.default).Distance(cc.v2(this.node.x, this.node.y), cc.v2(GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.x, GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.y));
            var duration = Distance / 21;
            this.moveX = GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.x;
            this.moveY = GamePlayInstance_1.default.Instance(GamePlayInstance_1.default).gameplay.MyCharacter.y;
            this.degree = this.betweenDegree(cc.v2(this.node.x, this.node.y), cc.v2(this.moveX, this.moveY)) - 90;
            var tween = new cc.Tween().to(duration, { position: cc.v3(this.moveX, this.moveY, this.moveZ) }).call(function () {
                _this.EnemyAttack();
            });
            tween.target(this.node).start();
        }
        else {
            this.moveX = Random_1.default.Instance(Random_1.default).RandomRange(this.clampLeft, this.clampRight);
            this.moveY = Random_1.default.Instance(Random_1.default).RandomRange(this.clampBottom, this.clampTop);
            while (Utility_1.default.Instance(Utility_1.default).Distance(cc.v2(this.node.x, this.node.y), cc.v2(this.moveX, this.moveY)) < 100) {
                this.moveX = Random_1.default.Instance(Random_1.default).RandomRange(this.clampLeft, this.clampRight);
                this.moveY = Random_1.default.Instance(Random_1.default).RandomRange(this.clampBottom, this.clampTop);
            }
            this.degree = this.betweenDegree(cc.v2(this.node.x, this.node.y), cc.v2(this.moveX, this.moveY)) + 90;
            var tween = new cc.Tween().to(6, { position: cc.v3(this.moveX, this.moveY, this.moveZ) }).call(function () {
                _this.node.getComponent(cc.SkeletonAnimation).play("Armature|sword_idle");
            });
            tween.target(this.node).start();
        }
        this.node.runAction(cc.rotate3DTo(0.2, cc.v3(-90, -180, -this.degree)));
    };
    EnemyController.prototype.betweenDegree = function (dirVec, comVec) {
        var angleDeg = Math.atan2(dirVec.y - comVec.y, dirVec.x - comVec.x) * 180 / Math.PI;
        return angleDeg;
    };
    EnemyController.prototype.EnemyAttack = function () {
        var _this = this;
        this.boolCheckAttacking = false;
        if (this.node.name == "MyVenom") {
            this.node.getComponent(cc.SkeletonAnimation).play("AmongUs_Attack");
        }
        else {
            this.node.getComponent(cc.SkeletonAnimation).play("Hammer Attack");
        }
        this.scheduleOnce(function () {
            _this.boolCheckAttacking = true;
            _this.boolCheckAttacked = true;
            cc.audioEngine.playEffect(Global_1.default.soundAttack, false);
        }, 0.5);
        this.scheduleOnce(function () {
            Global_1.default.boolStartAttacking = false;
            _this.node.getComponent(cc.SkeletonAnimation).play("Among_US_idle");
        }, 1);
    };
    __decorate([
        property(cc.Integer)
    ], EnemyController.prototype, "clampLeft", void 0);
    __decorate([
        property(cc.Integer)
    ], EnemyController.prototype, "clampRight", void 0);
    __decorate([
        property(cc.Integer)
    ], EnemyController.prototype, "clampTop", void 0);
    __decorate([
        property(cc.Integer)
    ], EnemyController.prototype, "clampBottom", void 0);
    __decorate([
        property(cc.Node)
    ], EnemyController.prototype, "PointShoot", void 0);
    __decorate([
        property(cc.Node)
    ], EnemyController.prototype, "bodyDeath", void 0);
    __decorate([
        property(cc.Prefab)
    ], EnemyController.prototype, "effectBlood", void 0);
    __decorate([
        property(cc.Node)
    ], EnemyController.prototype, "posBlood", void 0);
    __decorate([
        property(cc.Node)
    ], EnemyController.prototype, "weapon", void 0);
    EnemyController = __decorate([
        ccclass
    ], EnemyController);
    return EnemyController;
}(cc.Component));
exports.default = EnemyController;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcRW5lbXlcXEVuZW15Q29udHJvbGxlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSx3RUFBbUU7QUFFbkUsK0RBQXdFO0FBQ3hFLDJDQUFzQztBQUN0QywrQ0FBMEM7QUFDMUMsMkNBQXNDO0FBQ3RDLDZDQUF3QztBQUVsQyxJQUFBLGtCQUFxQyxFQUFuQyxvQkFBTyxFQUFFLHNCQUEwQixDQUFDO0FBRTVDO0lBQTZDLG1DQUFZO0lBRHpEO1FBQUEscUVBdU1DO1FBbk1HLGVBQVMsR0FBVyxDQUFDLENBQUM7UUFHdEIsZ0JBQVUsR0FBVyxDQUFDLENBQUM7UUFHdkIsY0FBUSxHQUFXLENBQUMsQ0FBQztRQUdyQixpQkFBVyxHQUFXLENBQUMsQ0FBQztRQUd4QixnQkFBVSxHQUFZLElBQUksQ0FBQztRQUczQixlQUFTLEdBQVksSUFBSSxDQUFDO1FBRzFCLGlCQUFXLEdBQWMsSUFBSSxDQUFDO1FBRzlCLGNBQVEsR0FBWSxJQUFJLENBQUM7UUFHekIsWUFBTSxHQUFZLElBQUksQ0FBQztRQUV2QixXQUFLLEdBQVcsQ0FBQyxDQUFDO1FBQ2xCLFdBQUssR0FBVyxDQUFDLENBQUM7UUFDbEIsV0FBSyxHQUFXLENBQUMsQ0FBQztRQUNsQixZQUFNLEdBQVcsQ0FBQyxDQUFDO1FBQ25CLHdCQUFrQixHQUFZLEtBQUssQ0FBQztRQUNwQyx1QkFBaUIsR0FBWSxLQUFLLENBQUM7UUFDbkMsb0JBQWMsR0FBWSxLQUFLLENBQUM7UUFDaEMsdUJBQWlCLEdBQVksS0FBSyxDQUFDO1FBQ25DLGdCQUFVLEdBQVksS0FBSyxDQUFDOztJQWlLaEMsQ0FBQztJQS9KRyxnQ0FBTSxHQUFOO1FBQ0ksMkRBQTJEO0lBQy9ELENBQUM7SUFFRCxnQ0FBTSxHQUFOO1FBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDbEIsSUFBSSxnQkFBTSxDQUFDLGtCQUFrQixJQUFJLGdCQUFNLENBQUMsa0JBQWtCLEVBQUU7Z0JBQ3hELElBQUksSUFBSSxHQUFHLEVBQUUsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLHFCQUFxQixDQUFDLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDNU8sSUFBSSxJQUFJLEdBQUcsRUFBRSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUMsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUM1TyxJQUFJLElBQUksR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxxQkFBcUIsQ0FBQywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQzVPLElBQUksVUFBVSxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQyxHQUFHLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2pMLElBQUksVUFBVSxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQyxHQUFHLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2pMLElBQUksTUFBTSxHQUFHLFVBQVUsQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQzlDLE1BQU0sR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUMxQyxJQUFJLFFBQVEsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3pMLElBQUksY0FBYyxHQUFHLFFBQVEsQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQ3BELGNBQWMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGNBQWMsQ0FBQyxDQUFDO2dCQUMxRCxJQUFJLGNBQWMsR0FBRyxRQUFRLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUNwRCxjQUFjLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFDMUQsSUFBSSxRQUFRLEdBQUcsQ0FBQyxDQUFDO2dCQUNqQixjQUFjLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFDMUMsY0FBYyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUM7Z0JBQzFDLElBQUksY0FBYyxHQUFHLGNBQWMsRUFBRTtvQkFDakMsUUFBUSxHQUFHLGNBQWMsQ0FBQztpQkFDN0I7cUJBQ0k7b0JBQ0QsUUFBUSxHQUFHLGNBQWMsQ0FBQztpQkFDN0I7Z0JBQ0QsSUFBSSxRQUFRLEdBQUcsaUJBQU8sQ0FBQyxRQUFRLENBQUMsaUJBQU8sQ0FBQyxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsRUFBRSwwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2xPLElBQUksV0FBVyxHQUFHLGlCQUFPLENBQUMsUUFBUSxDQUFDLGlCQUFPLENBQUMsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsRUFBRSwwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDM04sSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxHQUFHLE1BQU0sRUFBRTtvQkFDN0IsSUFBSSxRQUFRLEdBQUcsV0FBVyxFQUFFO3dCQUN4QixJQUFJLGdCQUFNLENBQUMsaUJBQWlCLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFOzRCQUNsRCwwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyw2QkFBbUIsQ0FBQyxDQUFDLGFBQWEsRUFBRSxDQUFDOzRCQUNuSCwwQkFBMEI7NEJBQzFCLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQzs0QkFDdkIsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7NEJBQ3ZCLGdCQUFNLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDOzRCQUNqQyxnQkFBTSxDQUFDLGtCQUFrQixHQUFHLEtBQUssQ0FBQzt5QkFDckM7cUJBQ0o7aUJBQ0o7YUFDSjtTQUNKO0lBQ0wsQ0FBQztJQUVELG1DQUFTLEdBQVQ7UUFBQSxpQkFJQztRQUhHLElBQUksQ0FBQyxRQUFRLENBQUM7WUFDVixLQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDckIsQ0FBQyxFQUFFLEdBQUcsRUFBRSxFQUFFLENBQUMsS0FBSyxDQUFDLGNBQWMsRUFBRSxHQUFHLENBQUMsQ0FBQztJQUMxQyxDQUFDO0lBRUQseUNBQWUsR0FBZjtRQUNJLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO1FBQzNCLEVBQUUsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLGdCQUFNLENBQUMsV0FBVyxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQ3JELDJCQUFRLENBQUMsSUFBSSxDQUFDLGtCQUFRLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDbEMsMkJBQVEsQ0FBQyxJQUFJLENBQUMsa0JBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM5QixJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQzNCLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1FBQzlCLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ3BELElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDcEUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQzNCLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQztRQUN0QixJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztRQUM3QixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztRQUN2QixJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDcEIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO0lBQ3ZFLENBQUM7SUFFRCx5Q0FBZSxHQUFmO1FBQ0ksSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7UUFDM0IsRUFBRSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsZ0JBQU0sQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDckQsMkJBQVEsQ0FBQyxJQUFJLENBQUMsa0JBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNsQywyQkFBUSxDQUFDLElBQUksQ0FBQyxrQkFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzlCLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUMzQixJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNuRSxDQUFDO0lBRUQsc0NBQVksR0FBWjtRQUNJLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUM3QixJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztRQUN2RSxHQUFHLEdBQUcsRUFBRSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3hELElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUM1QyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDdkUsQ0FBQztJQUVELDRDQUFrQixHQUFsQixVQUFtQixDQUFTLEVBQUUsQ0FBUyxFQUFFLENBQVM7UUFDOUMsSUFBSSxXQUFXLEdBQUcsRUFBRSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDbkQsV0FBVyxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUM7UUFDN0MsV0FBVyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDbEIsV0FBVyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDbEIsV0FBVyxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQzFCLENBQUM7SUFFRCwwQ0FBZ0IsR0FBaEIsVUFBaUIsTUFBYztRQUEvQixpQkFTQztRQVJHLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUM3QixJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ2xFLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUUsR0FBRyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3RFLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQztRQUN0QixJQUFJLENBQUMsWUFBWSxDQUFDO1lBQ2QsS0FBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDekQsS0FBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUN4QixDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDZixDQUFDO0lBRUQsbUNBQVMsR0FBVDtRQUFBLGlCQTJCQztRQTFCRyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQztRQUN4RSxJQUFJLElBQUksQ0FBQyxpQkFBaUIsRUFBRTtZQUN4QixJQUFJLFFBQVEsR0FBRyxpQkFBTyxDQUFDLFFBQVEsQ0FBQyxpQkFBTyxDQUFDLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxFQUFFLDBCQUFnQixDQUFDLFFBQVEsQ0FBQywwQkFBZ0IsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNsTyxJQUFJLFFBQVEsR0FBRyxRQUFRLEdBQUcsRUFBRSxDQUFDO1lBQzdCLElBQUksQ0FBQyxLQUFLLEdBQUcsMEJBQWdCLENBQUMsUUFBUSxDQUFDLDBCQUFnQixDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7WUFDaEYsSUFBSSxDQUFDLEtBQUssR0FBRywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztZQUNoRixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDdEcsSUFBSSxLQUFLLEdBQUcsSUFBSSxFQUFFLENBQUMsS0FBSyxFQUFFLENBQUMsRUFBRSxDQUFDLFFBQVEsRUFBRSxFQUFFLFFBQVEsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQztnQkFDbEcsS0FBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ3ZCLENBQUMsQ0FBQyxDQUFDO1lBQ0gsS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7U0FDbkM7YUFDSTtZQUNELElBQUksQ0FBQyxLQUFLLEdBQUcsZ0JBQU0sQ0FBQyxRQUFRLENBQUMsZ0JBQU0sQ0FBQyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUNsRixJQUFJLENBQUMsS0FBSyxHQUFHLGdCQUFNLENBQUMsUUFBUSxDQUFDLGdCQUFNLENBQUMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDbEYsT0FBTyxpQkFBTyxDQUFDLFFBQVEsQ0FBQyxpQkFBTyxDQUFDLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsR0FBRyxFQUFFO2dCQUM3RyxJQUFJLENBQUMsS0FBSyxHQUFHLGdCQUFNLENBQUMsUUFBUSxDQUFDLGdCQUFNLENBQUMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQ2xGLElBQUksQ0FBQyxLQUFLLEdBQUcsZ0JBQU0sQ0FBQyxRQUFRLENBQUMsZ0JBQU0sQ0FBQyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQzthQUNyRjtZQUNELElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUN0RyxJQUFJLEtBQUssR0FBRyxJQUFJLEVBQUUsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUUsUUFBUSxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUMzRixLQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsQ0FBQztZQUM3RSxDQUFDLENBQUMsQ0FBQztZQUNILEtBQUssQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO1NBQ25DO1FBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDNUUsQ0FBQztJQUVELHVDQUFhLEdBQWIsVUFBYyxNQUFNLEVBQUUsTUFBTTtRQUN4QixJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQztRQUNwRixPQUFPLFFBQVEsQ0FBQztJQUNwQixDQUFDO0lBRUQscUNBQVcsR0FBWDtRQUFBLGlCQWlCQztRQWhCRyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO1FBQ2hDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksU0FBUyxFQUFFO1lBQzdCLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1NBQ3ZFO2FBQ0k7WUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7U0FDdEU7UUFDRCxJQUFJLENBQUMsWUFBWSxDQUFDO1lBQ2QsS0FBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQztZQUMvQixLQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO1lBQzlCLEVBQUUsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLGdCQUFNLENBQUMsV0FBVyxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQ3pELENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUNSLElBQUksQ0FBQyxZQUFZLENBQUM7WUFDZCxnQkFBTSxDQUFDLGtCQUFrQixHQUFHLEtBQUssQ0FBQztZQUNsQyxLQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7UUFDdkUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQ1YsQ0FBQztJQWpNRDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDO3NEQUNDO0lBR3RCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUM7dURBQ0U7SUFHdkI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQztxREFDQTtJQUdyQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDO3dEQUNHO0lBR3hCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7dURBQ1M7SUFHM0I7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztzREFDUTtJQUcxQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDO3dEQUNVO0lBRzlCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7cURBQ087SUFHekI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzttREFDSztJQTNCTixlQUFlO1FBRG5DLE9BQU87T0FDYSxlQUFlLENBc01uQztJQUFELHNCQUFDO0NBdE1ELEFBc01DLENBdE00QyxFQUFFLENBQUMsU0FBUyxHQXNNeEQ7a0JBdE1vQixlQUFlIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IENoYXJhY3RlckNvbnRyb2xsZXIgZnJvbSBcIi4uL0NoYXJhY3Rlci9DaGFyYWN0ZXJDb250cm9sbGVyXCI7XHJcbmltcG9ydCBNYXBDb250cm9sbGVyIGZyb20gXCIuLi9Db2xsaWRlck9iai9NYXBDb250cm9sbGVyXCI7XHJcbmltcG9ydCBHYW1lUGxheUluc3RhbmNlLCB7IGluc3RhbmNlIH0gZnJvbSBcIi4uL0NvbW1vbi9HYW1lUGxheUluc3RhbmNlXCI7XHJcbmltcG9ydCBHbG9iYWwgZnJvbSBcIi4uL0NvbW1vbi9HbG9iYWxcIjtcclxuaW1wb3J0IEtleUV2ZW50IGZyb20gXCIuLi9Db21tb24vS2V5RXZlbnRcIjtcclxuaW1wb3J0IFJhbmRvbSBmcm9tIFwiLi4vQ29tbW9uL1JhbmRvbVwiO1xyXG5pbXBvcnQgVXRpbGl0eSBmcm9tIFwiLi4vQ29tbW9uL1V0aWxpdHlcIjtcclxuXHJcbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XHJcbkBjY2NsYXNzXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEVuZW15Q29udHJvbGxlciBleHRlbmRzIGNjLkNvbXBvbmVudCB7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLkludGVnZXIpXHJcbiAgICBjbGFtcExlZnQ6IG51bWJlciA9IDA7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLkludGVnZXIpXHJcbiAgICBjbGFtcFJpZ2h0OiBudW1iZXIgPSAwO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5JbnRlZ2VyKVxyXG4gICAgY2xhbXBUb3A6IG51bWJlciA9IDA7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLkludGVnZXIpXHJcbiAgICBjbGFtcEJvdHRvbTogbnVtYmVyID0gMDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIFBvaW50U2hvb3Q6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgYm9keURlYXRoOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuUHJlZmFiKVxyXG4gICAgZWZmZWN0Qmxvb2Q6IGNjLlByZWZhYiA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBwb3NCbG9vZDogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICB3ZWFwb246IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIG1vdmVYOiBudW1iZXIgPSAwO1xyXG4gICAgbW92ZVk6IG51bWJlciA9IDA7XHJcbiAgICBtb3ZlWjogbnVtYmVyID0gMDtcclxuICAgIGRlZ3JlZTogbnVtYmVyID0gMDtcclxuICAgIGJvb2xDaGVja0F0dGFja2luZzogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgYm9vbENoZWNrQXR0YWNrZWQ6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIGJvb2xFbmVteURlYXRoOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBjaGVja0ZvbGxvd1BsYXllcjogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgY2hlY2tEZWF0aDogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuICAgIG9uTG9hZCgpIHtcclxuICAgICAgICAvLyB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLkJveENvbGxpZGVyM0QpLmVuYWJsZWQgPSB0cnVlO1xyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZSgpIHtcclxuICAgICAgICBpZiAoIXRoaXMuY2hlY2tEZWF0aCkge1xyXG4gICAgICAgICAgICBpZiAoR2xvYmFsLmJvb2xTdGFydEF0dGFja2luZyAmJiBHbG9iYWwuYm9vbENoZWNrQXR0YWNraW5nKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgcG9zMSA9IGNjLkNhbnZhcy5pbnN0YW5jZS5ub2RlLmNvbnZlcnRUb05vZGVTcGFjZUFSKEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIuY29udmVydFRvV29ybGRTcGFjZUFSKEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIuY2hpbGRyZW5bMF0uZ2V0UG9zaXRpb24oKSkpO1xyXG4gICAgICAgICAgICAgICAgbGV0IHBvczIgPSBjYy5DYW52YXMuaW5zdGFuY2Uubm9kZS5jb252ZXJ0VG9Ob2RlU3BhY2VBUihHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLmNvbnZlcnRUb1dvcmxkU3BhY2VBUihHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLmNoaWxkcmVuWzFdLmdldFBvc2l0aW9uKCkpKTtcclxuICAgICAgICAgICAgICAgIGxldCBwb3MzID0gY2MuQ2FudmFzLmluc3RhbmNlLm5vZGUuY29udmVydFRvTm9kZVNwYWNlQVIoR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5NeUNoYXJhY3Rlci5jb252ZXJ0VG9Xb3JsZFNwYWNlQVIoR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5NeUNoYXJhY3Rlci5jaGlsZHJlblsyXS5nZXRQb3NpdGlvbigpKSk7XHJcbiAgICAgICAgICAgICAgICBsZXQgZGlyZWN0aW9uMSA9IGNjLnYyKHBvczEueCAtIEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueCwgcG9zMS55IC0gR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5NeUNoYXJhY3Rlci55KTtcclxuICAgICAgICAgICAgICAgIGxldCBkaXJlY3Rpb24yID0gY2MudjIocG9zMi54IC0gR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5NeUNoYXJhY3Rlci54LCBwb3MyLnkgLSBHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLnkpO1xyXG4gICAgICAgICAgICAgICAgbGV0IGRlZ3JlZSA9IGRpcmVjdGlvbjEuc2lnbkFuZ2xlKGRpcmVjdGlvbjIpO1xyXG4gICAgICAgICAgICAgICAgZGVncmVlID0gY2MubWlzYy5yYWRpYW5zVG9EZWdyZWVzKGRlZ3JlZSk7XHJcbiAgICAgICAgICAgICAgICBsZXQgcG9zRW5lbXkgPSBjYy52Mih0aGlzLm5vZGUueCAtIEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueCwgdGhpcy5ub2RlLnkgLSBHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLnkpO1xyXG4gICAgICAgICAgICAgICAgbGV0IGRlZ3JlZVdpdGhQb3MxID0gcG9zRW5lbXkuc2lnbkFuZ2xlKGRpcmVjdGlvbjEpO1xyXG4gICAgICAgICAgICAgICAgZGVncmVlV2l0aFBvczEgPSBjYy5taXNjLnJhZGlhbnNUb0RlZ3JlZXMoZGVncmVlV2l0aFBvczEpO1xyXG4gICAgICAgICAgICAgICAgbGV0IGRlZ3JlZVdpdGhQb3MyID0gcG9zRW5lbXkuc2lnbkFuZ2xlKGRpcmVjdGlvbjIpO1xyXG4gICAgICAgICAgICAgICAgZGVncmVlV2l0aFBvczIgPSBjYy5taXNjLnJhZGlhbnNUb0RlZ3JlZXMoZGVncmVlV2l0aFBvczIpO1xyXG4gICAgICAgICAgICAgICAgbGV0IHJlYWxOZWVkID0gMDtcclxuICAgICAgICAgICAgICAgIGRlZ3JlZVdpdGhQb3MxID0gTWF0aC5hYnMoZGVncmVlV2l0aFBvczEpO1xyXG4gICAgICAgICAgICAgICAgZGVncmVlV2l0aFBvczIgPSBNYXRoLmFicyhkZWdyZWVXaXRoUG9zMik7XHJcbiAgICAgICAgICAgICAgICBpZiAoZGVncmVlV2l0aFBvczEgPiBkZWdyZWVXaXRoUG9zMikge1xyXG4gICAgICAgICAgICAgICAgICAgIHJlYWxOZWVkID0gZGVncmVlV2l0aFBvczE7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICByZWFsTmVlZCA9IGRlZ3JlZVdpdGhQb3MyO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgbGV0IGRpc3RhbmNlID0gVXRpbGl0eS5JbnN0YW5jZShVdGlsaXR5KS5EaXN0YW5jZShjYy52Mih0aGlzLm5vZGUueCwgdGhpcy5ub2RlLnkpLCBjYy52MihHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLngsIEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueSkpO1xyXG4gICAgICAgICAgICAgICAgbGV0IG1heERpc3RhbmNlID0gVXRpbGl0eS5JbnN0YW5jZShVdGlsaXR5KS5EaXN0YW5jZShjYy52MihHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLngsIEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueSksIGNjLnYyKHBvczMueCwgcG9zMy55KSk7XHJcbiAgICAgICAgICAgICAgICBpZiAoTWF0aC5hYnMocmVhbE5lZWQpIDwgZGVncmVlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGRpc3RhbmNlIDwgbWF4RGlzdGFuY2UpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKEdsb2JhbC5ib29sQ2hlY2tBdHRhY2tlZCAmJiAhdGhpcy5ib29sRW5lbXlEZWF0aCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5NeUNoYXJhY3Rlci5nZXRDb21wb25lbnQoQ2hhcmFjdGVyQ29udHJvbGxlcikuTGV2ZWxVcFBsYXllcigpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gdGhpcy5EZXN0cm95QnlLYXRhbmEoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuRGVzdHJveUJ5SGFtbWVyKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNoZWNrRGVhdGggPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgR2xvYmFsLmJvb2xDaGVja0F0dGFja2VkID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBHbG9iYWwuYm9vbENoZWNrQXR0YWNraW5nID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgU3RhcnRNb3ZlKCkge1xyXG4gICAgICAgIHRoaXMuc2NoZWR1bGUoKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLm1vdmVFbmVteSgpO1xyXG4gICAgICAgIH0sIDYuNSwgY2MubWFjcm8uUkVQRUFUX0ZPUkVWRVIsIDAuMSk7XHJcbiAgICB9XHJcblxyXG4gICAgRGVzdHJveUJ5SGFtbWVyKCkge1xyXG4gICAgICAgIHRoaXMuYm9vbEVuZW15RGVhdGggPSB0cnVlO1xyXG4gICAgICAgIGNjLmF1ZGlvRW5naW5lLnBsYXlFZmZlY3QoR2xvYmFsLnNvdW5kU2NyZWFtLCBmYWxzZSk7XHJcbiAgICAgICAgaW5zdGFuY2UuZW1pdChLZXlFdmVudC5wbHVzRW5lbXkpO1xyXG4gICAgICAgIGluc3RhbmNlLmVtaXQoS2V5RXZlbnQuc2NhbGUpO1xyXG4gICAgICAgIHRoaXMubm9kZS5zdG9wQWxsQWN0aW9ucygpO1xyXG4gICAgICAgIHRoaXMudW5zY2hlZHVsZUFsbENhbGxiYWNrcygpO1xyXG4gICAgICAgIHRoaXMubm9kZS5nZXRDb21wb25lbnQoY2MuU2tlbGV0b25BbmltYXRpb24pLnN0b3AoKTtcclxuICAgICAgICB0aGlzLm5vZGUuZXVsZXJBbmdsZXMgPSBuZXcgY2MuVmVjMygwLCAwLCAtdGhpcy5ub2RlLmV1bGVyQW5nbGVzLnopO1xyXG4gICAgICAgIHRoaXMud2VhcG9uLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMubm9kZS5zY2FsZVogPSAxMjtcclxuICAgICAgICB0aGlzLm5vZGUueiA9IHRoaXMubW92ZVogKyAxO1xyXG4gICAgICAgIHRoaXMuY2hlY2tEZWF0aCA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5TcGF3bmVyQmxvb2QoKTtcclxuICAgICAgICB0aGlzLlNwYXduZXJFZmZlY3RCbG9vZCh0aGlzLm5vZGUueCwgdGhpcy5ub2RlLnksIHRoaXMubm9kZS56ICsgMik7XHJcbiAgICB9XHJcblxyXG4gICAgRGVzdHJveUJ5S2F0YW5hKCkge1xyXG4gICAgICAgIHRoaXMuYm9vbEVuZW15RGVhdGggPSB0cnVlO1xyXG4gICAgICAgIGNjLmF1ZGlvRW5naW5lLnBsYXlFZmZlY3QoR2xvYmFsLnNvdW5kU2NyZWFtLCBmYWxzZSk7XHJcbiAgICAgICAgaW5zdGFuY2UuZW1pdChLZXlFdmVudC5wbHVzRW5lbXkpO1xyXG4gICAgICAgIGluc3RhbmNlLmVtaXQoS2V5RXZlbnQuc2NhbGUpO1xyXG4gICAgICAgIHRoaXMuY2hlY2tEZWF0aCA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5TcGF3bmVyQm9keURlYXRoKDAuNyk7XHJcbiAgICAgICAgdGhpcy5TcGF3bmVyRWZmZWN0Qmxvb2QodGhpcy5ub2RlLngsIHRoaXMubm9kZS55LCB0aGlzLm5vZGUueik7XHJcbiAgICB9XHJcblxyXG4gICAgU3Bhd25lckJsb29kKCkge1xyXG4gICAgICAgIHRoaXMuYm9keURlYXRoLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgbGV0IHBvcyA9IHRoaXMubm9kZS5jb252ZXJ0VG9Xb3JsZFNwYWNlQVIodGhpcy5wb3NCbG9vZC5nZXRQb3NpdGlvbigpKTtcclxuICAgICAgICBwb3MgPSBjYy5DYW52YXMuaW5zdGFuY2Uubm9kZS5jb252ZXJ0VG9Ob2RlU3BhY2VBUihwb3MpO1xyXG4gICAgICAgIHRoaXMuYm9keURlYXRoLnNldFBvc2l0aW9uKHBvcy54LCBwb3MueSwgMCk7XHJcbiAgICAgICAgdGhpcy5ib2R5RGVhdGguZXVsZXJBbmdsZXMgPSBjYy52Myg5MCwgMCwgdGhpcy5ub2RlLmV1bGVyQW5nbGVzLnopO1xyXG4gICAgfVxyXG5cclxuICAgIFNwYXduZXJFZmZlY3RCbG9vZCh4OiBudW1iZXIsIHk6IG51bWJlciwgejogbnVtYmVyKSB7XHJcbiAgICAgICAgbGV0IEVmZmVjdEJsb29kID0gY2MuaW5zdGFudGlhdGUodGhpcy5lZmZlY3RCbG9vZCk7XHJcbiAgICAgICAgRWZmZWN0Qmxvb2QucGFyZW50ID0gY2MuQ2FudmFzLmluc3RhbmNlLm5vZGU7XHJcbiAgICAgICAgRWZmZWN0Qmxvb2QueCA9IHg7XHJcbiAgICAgICAgRWZmZWN0Qmxvb2QueSA9IHk7XHJcbiAgICAgICAgRWZmZWN0Qmxvb2QueiA9IHogKyAxO1xyXG4gICAgfVxyXG5cclxuICAgIFNwYXduZXJCb2R5RGVhdGgodGltaW5nOiBudW1iZXIpIHtcclxuICAgICAgICB0aGlzLmJvZHlEZWF0aC5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuYm9keURlYXRoLnNldFBvc2l0aW9uKHRoaXMubm9kZS54LCB0aGlzLm5vZGUueSwgdGhpcy5ub2RlLnopO1xyXG4gICAgICAgIHRoaXMuYm9keURlYXRoLmV1bGVyQW5nbGVzID0gY2MudjMoLTkwLCAxODAsIHRoaXMubm9kZS5ldWxlckFuZ2xlcy56KTtcclxuICAgICAgICB0aGlzLm5vZGUub3BhY2l0eSA9IDA7XHJcbiAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmJvZHlEZWF0aC5nZXRDb21wb25lbnQoY2MuU2tlbGV0b25BbmltYXRpb24pLnN0b3AoKTtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLmRlc3Ryb3koKTtcclxuICAgICAgICB9LCB0aW1pbmcpO1xyXG4gICAgfVxyXG5cclxuICAgIG1vdmVFbmVteSgpIHtcclxuICAgICAgICB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlNrZWxldG9uQW5pbWF0aW9uKS5wbGF5KFwiQXJtYXR1cmV8c3dvcmRfcnVuXCIpO1xyXG4gICAgICAgIGlmICh0aGlzLmNoZWNrRm9sbG93UGxheWVyKSB7XHJcbiAgICAgICAgICAgIGxldCBEaXN0YW5jZSA9IFV0aWxpdHkuSW5zdGFuY2UoVXRpbGl0eSkuRGlzdGFuY2UoY2MudjIodGhpcy5ub2RlLngsIHRoaXMubm9kZS55KSwgY2MudjIoR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKS5nYW1lcGxheS5NeUNoYXJhY3Rlci54LCBHYW1lUGxheUluc3RhbmNlLkluc3RhbmNlKEdhbWVQbGF5SW5zdGFuY2UpLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLnkpKTtcclxuICAgICAgICAgICAgbGV0IGR1cmF0aW9uID0gRGlzdGFuY2UgLyAyMTtcclxuICAgICAgICAgICAgdGhpcy5tb3ZlWCA9IEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueDtcclxuICAgICAgICAgICAgdGhpcy5tb3ZlWSA9IEdhbWVQbGF5SW5zdGFuY2UuSW5zdGFuY2UoR2FtZVBsYXlJbnN0YW5jZSkuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueTtcclxuICAgICAgICAgICAgdGhpcy5kZWdyZWUgPSB0aGlzLmJldHdlZW5EZWdyZWUoY2MudjIodGhpcy5ub2RlLngsIHRoaXMubm9kZS55KSwgY2MudjIodGhpcy5tb3ZlWCwgdGhpcy5tb3ZlWSkpIC0gOTA7XHJcbiAgICAgICAgICAgIHZhciB0d2VlbiA9IG5ldyBjYy5Ud2VlbigpLnRvKGR1cmF0aW9uLCB7IHBvc2l0aW9uOiBjYy52Myh0aGlzLm1vdmVYLCB0aGlzLm1vdmVZLCB0aGlzLm1vdmVaKSB9KS5jYWxsKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuRW5lbXlBdHRhY2soKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIHR3ZWVuLnRhcmdldCh0aGlzLm5vZGUpLnN0YXJ0KCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLm1vdmVYID0gUmFuZG9tLkluc3RhbmNlKFJhbmRvbSkuUmFuZG9tUmFuZ2UodGhpcy5jbGFtcExlZnQsIHRoaXMuY2xhbXBSaWdodCk7XHJcbiAgICAgICAgICAgIHRoaXMubW92ZVkgPSBSYW5kb20uSW5zdGFuY2UoUmFuZG9tKS5SYW5kb21SYW5nZSh0aGlzLmNsYW1wQm90dG9tLCB0aGlzLmNsYW1wVG9wKTtcclxuICAgICAgICAgICAgd2hpbGUgKFV0aWxpdHkuSW5zdGFuY2UoVXRpbGl0eSkuRGlzdGFuY2UoY2MudjIodGhpcy5ub2RlLngsIHRoaXMubm9kZS55KSwgY2MudjIodGhpcy5tb3ZlWCwgdGhpcy5tb3ZlWSkpIDwgMTAwKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm1vdmVYID0gUmFuZG9tLkluc3RhbmNlKFJhbmRvbSkuUmFuZG9tUmFuZ2UodGhpcy5jbGFtcExlZnQsIHRoaXMuY2xhbXBSaWdodCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm1vdmVZID0gUmFuZG9tLkluc3RhbmNlKFJhbmRvbSkuUmFuZG9tUmFuZ2UodGhpcy5jbGFtcEJvdHRvbSwgdGhpcy5jbGFtcFRvcCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5kZWdyZWUgPSB0aGlzLmJldHdlZW5EZWdyZWUoY2MudjIodGhpcy5ub2RlLngsIHRoaXMubm9kZS55KSwgY2MudjIodGhpcy5tb3ZlWCwgdGhpcy5tb3ZlWSkpICsgOTA7XHJcbiAgICAgICAgICAgIHZhciB0d2VlbiA9IG5ldyBjYy5Ud2VlbigpLnRvKDYsIHsgcG9zaXRpb246IGNjLnYzKHRoaXMubW92ZVgsIHRoaXMubW92ZVksIHRoaXMubW92ZVopIH0pLmNhbGwoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLmdldENvbXBvbmVudChjYy5Ta2VsZXRvbkFuaW1hdGlvbikucGxheShcIkFybWF0dXJlfHN3b3JkX2lkbGVcIik7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB0d2Vlbi50YXJnZXQodGhpcy5ub2RlKS5zdGFydCgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLm5vZGUucnVuQWN0aW9uKGNjLnJvdGF0ZTNEVG8oMC4yLCBjYy52MygtOTAsIC0xODAsIC10aGlzLmRlZ3JlZSkpKTtcclxuICAgIH1cclxuXHJcbiAgICBiZXR3ZWVuRGVncmVlKGRpclZlYywgY29tVmVjKSB7XHJcbiAgICAgICAgbGV0IGFuZ2xlRGVnID0gTWF0aC5hdGFuMihkaXJWZWMueSAtIGNvbVZlYy55LCBkaXJWZWMueCAtIGNvbVZlYy54KSAqIDE4MCAvIE1hdGguUEk7XHJcbiAgICAgICAgcmV0dXJuIGFuZ2xlRGVnO1xyXG4gICAgfVxyXG5cclxuICAgIEVuZW15QXR0YWNrKCkge1xyXG4gICAgICAgIHRoaXMuYm9vbENoZWNrQXR0YWNraW5nID0gZmFsc2U7XHJcbiAgICAgICAgaWYgKHRoaXMubm9kZS5uYW1lID09IFwiTXlWZW5vbVwiKSB7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS5nZXRDb21wb25lbnQoY2MuU2tlbGV0b25BbmltYXRpb24pLnBsYXkoXCJBbW9uZ1VzX0F0dGFja1wiKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS5nZXRDb21wb25lbnQoY2MuU2tlbGV0b25BbmltYXRpb24pLnBsYXkoXCJIYW1tZXIgQXR0YWNrXCIpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuYm9vbENoZWNrQXR0YWNraW5nID0gdHJ1ZTtcclxuICAgICAgICAgICAgdGhpcy5ib29sQ2hlY2tBdHRhY2tlZCA9IHRydWU7XHJcbiAgICAgICAgICAgIGNjLmF1ZGlvRW5naW5lLnBsYXlFZmZlY3QoR2xvYmFsLnNvdW5kQXR0YWNrLCBmYWxzZSk7XHJcbiAgICAgICAgfSwgMC41KTtcclxuICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgIEdsb2JhbC5ib29sU3RhcnRBdHRhY2tpbmcgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLmdldENvbXBvbmVudChjYy5Ta2VsZXRvbkFuaW1hdGlvbikucGxheShcIkFtb25nX1VTX2lkbGVcIik7XHJcbiAgICAgICAgfSwgMSk7XHJcbiAgICB9XHJcblxyXG59Il19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/GamePlay/StickMan3.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '74f44KHFKxG/6SyuFNJl0Fm', 'StickMan3');
// Scripts/GamePlay/StickMan3.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var CharacterController_1 = require("../Character/CharacterController");
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var KeyEvent_1 = require("../Common/KeyEvent");
var Singleton_1 = require("../Common/Singleton");
var EnemyController_1 = require("../Enemy/EnemyController");
var StickMan1_1 = require("./StickMan1");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var StickMan3 = /** @class */ (function (_super) {
    __extends(StickMan3, _super);
    function StickMan3() {
        var _this = _super.call(this) || this;
        _this.joyStickFollow = null;
        _this.Map = null;
        _this.countEnemyEnd = 0;
        _this.MyCharacter = null;
        _this.Guide = null;
        _this.Effect = null;
        _this.txtStart = null;
        _this.enemyParent = null;
        _this.btnDownload = null;
        _this.endCard = null;
        _this.nightEnd = null;
        _this.circleEnd = null;
        _this.btnAll = null;
        _this.nEnemyEnd = null;
        _this.countEnemy = 0;
        _this.boolCheckEnd = false;
        _this.boolcheckInteraction = false;
        _this.ironsource = false;
        _this.mindworks = false;
        _this.vungle = false;
        StickMan1_1.default._instance = _this;
        return _this;
    }
    StickMan3.prototype.onEnable = function () {
        GamePlayInstance_1.instance.on(KeyEvent_1.default.scale, this.scale, this);
        GamePlayInstance_1.instance.on(KeyEvent_1.default.plusEnemy, this.plusEnemy, this);
        GamePlayInstance_1.instance.on(KeyEvent_1.default.activeGuide, this.activeGuide, this);
    };
    StickMan3.prototype.onDisable = function () {
        GamePlayInstance_1.instance.off(KeyEvent_1.default.scale, this.scale, this);
        GamePlayInstance_1.instance.off(KeyEvent_1.default.plusEnemy, this.plusEnemy, this);
        GamePlayInstance_1.instance.off(KeyEvent_1.default.activeGuide, this.activeGuide, this);
    };
    StickMan3.prototype.start = function () {
        this.playGame();
    };
    StickMan3.prototype.update = function (dt) {
        var _this = this;
        if (Global_1.default.boolEnableTouch && !this.boolcheckInteraction) {
            this.txtStart.active = false;
            if (this.ironsource) {
                window.NUC.trigger.interaction();
            }
        }
        if (this.countEnemy == this.countEnemyEnd && !this.boolCheckEnd) {
            Global_1.default.boolStartPlay = false;
            this.boolCheckEnd = true;
            this.scheduleOnce(function () {
                _this.SupportEnd();
            }, 1);
        }
    };
    StickMan3.prototype.playGame = function () {
        this.MyCharacter.active = true;
        Global_1.default.boolStartPlay = true;
        for (var i = 0; i < this.enemyParent.childrenCount; i++) {
            this.enemyParent.children[i].getComponent(EnemyController_1.default).StartMove();
        }
    };
    StickMan3.prototype.SupportEnd = function () {
        var _this = this;
        this.scheduleOnce(function () {
            _this.nightEnd.active = true;
            _this.scheduleOnce(function () {
                _this.circleEnd.active = true;
                _this.scheduleOnce(function () {
                    _this.nightEnd.active = false;
                    _this.circleEnd.active = false;
                    _this.MyCharacter.opacity = 0;
                    _this.EndGame();
                }, 0.6);
            }, 0.4);
            _this.circleEnd.active = true;
        }, 0.2);
    };
    StickMan3.prototype.EndGame = function () {
        Global_1.default.boolendG = true;
        this.joyStickFollow.active = false;
        this.endCard.active = true;
        this.playGame();
        this.Map.active = true;
        this.endCard.children[2].active = true;
        this.endCard.children[3].active = true;
        this.endCard.children[4].active = true;
        for (var i = 0; i < this.nEnemyEnd.childrenCount; i++) {
            this.nEnemyEnd.children[i].getComponent(EnemyController_1.default).StartMove();
        }
        this.MyCharacter.opacity = 255;
        this.MyCharacter.x = -3;
        this.MyCharacter.y = -60;
        this.MyCharacter.scale = 350;
        this.MyCharacter.eulerAngles = new cc.Vec3(90, 0, 180);
        this.MyCharacter.getComponent(CharacterController_1.default).ArrowDirection.active = false;
        this.Guide.active = true;
        for (var i = 0; i < this.enemyParent.childrenCount; i++) {
            this.enemyParent.children[i].getComponent(EnemyController_1.default).bodyDeath.active = false;
            this.enemyParent.children[i].active = false;
            ;
        }
        this.btnAll.active = true;
        this.btnDownload.getComponent(cc.Button).interactable = false;
        if (this.mindworks) {
            window.gameEnd && window.gameEnd();
        }
        if (this.ironsource) {
            window.NUC.trigger.endGame('win');
        }
        if (this.vungle) {
            parent.postMessage('complete', '*');
        }
    };
    StickMan3.prototype.scale = function () {
        var _this = this;
        this.Effect.scale = 0;
        this.Effect.opacity = 255;
        this.Effect.runAction(cc.sequence(cc.scaleTo(0.2, 1).easing(cc.easeBounceOut()), cc.callFunc(function () {
            _this.scheduleOnce(function () {
                _this.Effect.runAction(cc.fadeOut(0.1));
            }, 0.2);
        })));
    };
    StickMan3.prototype.plusEnemy = function () {
        if (this.countEnemy < this.countEnemyEnd)
            this.countEnemy++;
    };
    StickMan3.prototype.activeGuide = function () {
        if (Global_1.default.boolStartPlay)
            this.Guide.active = true;
    };
    __decorate([
        property(cc.Node)
    ], StickMan3.prototype, "joyStickFollow", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan3.prototype, "Map", void 0);
    __decorate([
        property(cc.Integer)
    ], StickMan3.prototype, "countEnemyEnd", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan3.prototype, "MyCharacter", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan3.prototype, "Guide", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan3.prototype, "Effect", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan3.prototype, "txtStart", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan3.prototype, "enemyParent", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan3.prototype, "btnDownload", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan3.prototype, "endCard", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan3.prototype, "nightEnd", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan3.prototype, "circleEnd", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan3.prototype, "btnAll", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan3.prototype, "nEnemyEnd", void 0);
    StickMan3 = __decorate([
        ccclass
    ], StickMan3);
    return StickMan3;
}(Singleton_1.default));
exports.default = StickMan3;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcR2FtZVBsYXlcXFN0aWNrTWFuMy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSx3RUFBbUU7QUFDbkUsK0RBQXNEO0FBQ3RELDJDQUFzQztBQUN0QywrQ0FBMEM7QUFDMUMsaURBQTRDO0FBQzVDLDREQUF1RDtBQUN2RCx5Q0FBb0M7QUFFOUIsSUFBQSxrQkFBbUMsRUFBbEMsb0JBQU8sRUFBRSxzQkFBeUIsQ0FBQztBQUsxQztJQUF1Qyw2QkFBb0I7SUFvRHZEO1FBQUEsWUFDSSxpQkFBTyxTQUVWO1FBcERELG9CQUFjLEdBQVksSUFBSSxDQUFDO1FBRy9CLFNBQUcsR0FBWSxJQUFJLENBQUM7UUFHcEIsbUJBQWEsR0FBVyxDQUFDLENBQUM7UUFHMUIsaUJBQVcsR0FBWSxJQUFJLENBQUM7UUFHNUIsV0FBSyxHQUFZLElBQUksQ0FBQztRQUd0QixZQUFNLEdBQVksSUFBSSxDQUFDO1FBR3ZCLGNBQVEsR0FBWSxJQUFJLENBQUM7UUFHekIsaUJBQVcsR0FBWSxJQUFJLENBQUM7UUFHNUIsaUJBQVcsR0FBWSxJQUFJLENBQUM7UUFHNUIsYUFBTyxHQUFZLElBQUksQ0FBQztRQUd4QixjQUFRLEdBQVksSUFBSSxDQUFDO1FBR3pCLGVBQVMsR0FBWSxJQUFJLENBQUM7UUFHMUIsWUFBTSxHQUFZLElBQUksQ0FBQztRQUd2QixlQUFTLEdBQVksSUFBSSxDQUFDO1FBRTFCLGdCQUFVLEdBQVcsQ0FBQyxDQUFDO1FBQ3ZCLGtCQUFZLEdBQVksS0FBSyxDQUFDO1FBRTlCLDBCQUFvQixHQUFZLEtBQUssQ0FBQztRQUN0QyxnQkFBVSxHQUFZLEtBQUssQ0FBQztRQUM1QixlQUFTLEdBQVksS0FBSyxDQUFDO1FBQzNCLFlBQU0sR0FBWSxLQUFLLENBQUM7UUFJcEIsbUJBQVMsQ0FBQyxTQUFTLEdBQUcsS0FBSSxDQUFDOztJQUMvQixDQUFDO0lBRUQsNEJBQVEsR0FBUjtRQUNJLDJCQUFRLENBQUMsRUFBRSxDQUFDLGtCQUFRLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDOUMsMkJBQVEsQ0FBQyxFQUFFLENBQUMsa0JBQVEsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUN0RCwyQkFBUSxDQUFDLEVBQUUsQ0FBQyxrQkFBUSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQzlELENBQUM7SUFFRCw2QkFBUyxHQUFUO1FBQ0ksMkJBQVEsQ0FBQyxHQUFHLENBQUMsa0JBQVEsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztRQUMvQywyQkFBUSxDQUFDLEdBQUcsQ0FBQyxrQkFBUSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3ZELDJCQUFRLENBQUMsR0FBRyxDQUFDLGtCQUFRLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDL0QsQ0FBQztJQUVELHlCQUFLLEdBQUw7UUFDSSxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDcEIsQ0FBQztJQUVELDBCQUFNLEdBQU4sVUFBUSxFQUFFO1FBQVYsaUJBZUM7UUFkRyxJQUFJLGdCQUFNLENBQUMsZUFBZSxJQUFJLENBQUMsSUFBSSxDQUFDLG9CQUFvQixFQUFFO1lBQ3RELElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztZQUM3QixJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7Z0JBQ2pCLE1BQU0sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxDQUFDO2FBQ3BDO1NBQ0o7UUFFRCxJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLGFBQWEsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDN0QsZ0JBQU0sQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO1lBQzdCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1lBQ3pCLElBQUksQ0FBQyxZQUFZLENBQUM7Z0JBQ2QsS0FBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO1lBQ3RCLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUNUO0lBQ0wsQ0FBQztJQUVELDRCQUFRLEdBQVI7UUFDSSxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDL0IsZ0JBQU0sQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1FBQzVCLEtBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUNwRCxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMseUJBQWUsQ0FBQyxDQUFDLFNBQVMsRUFBRSxDQUFDO1NBQzFFO0lBQ0wsQ0FBQztJQUVELDhCQUFVLEdBQVY7UUFBQSxpQkFjQztRQWJHLElBQUksQ0FBQyxZQUFZLENBQUM7WUFDZCxLQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7WUFDNUIsS0FBSSxDQUFDLFlBQVksQ0FBQztnQkFDZCxLQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7Z0JBQzdCLEtBQUksQ0FBQyxZQUFZLENBQUM7b0JBQ2QsS0FBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO29CQUM3QixLQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7b0JBQzlCLEtBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQztvQkFDN0IsS0FBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO2dCQUNuQixDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7WUFDWixDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7WUFDUixLQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDakMsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQ1osQ0FBQztJQUVELDJCQUFPLEdBQVA7UUFDSSxnQkFBTSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFDdkIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ25DLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUMzQixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDaEIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDdkMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUN2QyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ3ZDLEtBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUNsRCxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMseUJBQWUsQ0FBQyxDQUFDLFNBQVMsRUFBRSxDQUFDO1NBQ3hFO1FBQ0QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDO1FBQy9CLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQ3hCLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQztRQUM3QixJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsR0FBRyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUN2RCxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyw2QkFBbUIsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ2pGLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUN6QixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDckQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLHlCQUFlLENBQUMsQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztZQUNwRixJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1lBQUEsQ0FBQztTQUNoRDtRQUNELElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUMxQixJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztRQUM5RCxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDaEIsTUFBTSxDQUFDLE9BQU8sSUFBSSxNQUFNLENBQUMsT0FBTyxFQUFFLENBQUM7U0FDdEM7UUFDRCxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDakIsTUFBTSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFBO1NBQ3BDO1FBQ0QsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2IsTUFBTSxDQUFDLFdBQVcsQ0FBQyxVQUFVLEVBQUUsR0FBRyxDQUFDLENBQUM7U0FDdkM7SUFDTCxDQUFDO0lBRUQseUJBQUssR0FBTDtRQUFBLGlCQVFDO1FBUEcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQztRQUMxQixJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsYUFBYSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsUUFBUSxDQUFDO1lBQ3pGLEtBQUksQ0FBQyxZQUFZLENBQUM7Z0JBQ2QsS0FBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQzNDLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUNaLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNULENBQUM7SUFFRCw2QkFBUyxHQUFUO1FBQ0ksSUFBSSxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxhQUFhO1lBQ3BDLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztJQUMxQixDQUFDO0lBRUQsK0JBQVcsR0FBWDtRQUNJLElBQUcsZ0JBQU0sQ0FBQyxhQUFhO1lBQ25CLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztJQUNqQyxDQUFDO0lBcktEO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7cURBQ2E7SUFHL0I7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzswQ0FDRTtJQUdwQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDO29EQUNLO0lBRzFCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7a0RBQ1U7SUFHNUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzs0Q0FDSTtJQUd0QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOzZDQUNLO0lBR3ZCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7K0NBQ087SUFHekI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztrREFDVTtJQUc1QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO2tEQUNVO0lBRzVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7OENBQ007SUFHeEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzsrQ0FDTztJQUd6QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO2dEQUNRO0lBRzFCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7NkNBQ0s7SUFHdkI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztnREFDUTtJQTFDVCxTQUFTO1FBRDdCLE9BQU87T0FDYSxTQUFTLENBeUs3QjtJQUFELGdCQUFDO0NBektELEFBeUtDLENBektzQyxtQkFBUyxHQXlLL0M7a0JBektvQixTQUFTIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IENoYXJhY3RlckNvbnRyb2xsZXIgZnJvbSBcIi4uL0NoYXJhY3Rlci9DaGFyYWN0ZXJDb250cm9sbGVyXCI7XHJcbmltcG9ydCB7IGluc3RhbmNlIH0gZnJvbSBcIi4uL0NvbW1vbi9HYW1lUGxheUluc3RhbmNlXCI7XHJcbmltcG9ydCBHbG9iYWwgZnJvbSBcIi4uL0NvbW1vbi9HbG9iYWxcIjtcclxuaW1wb3J0IEtleUV2ZW50IGZyb20gXCIuLi9Db21tb24vS2V5RXZlbnRcIjtcclxuaW1wb3J0IFNpbmdsZXRvbiBmcm9tIFwiLi4vQ29tbW9uL1NpbmdsZXRvblwiO1xyXG5pbXBvcnQgRW5lbXlDb250cm9sbGVyIGZyb20gXCIuLi9FbmVteS9FbmVteUNvbnRyb2xsZXJcIjtcclxuaW1wb3J0IFN0aWNrTWFuMSBmcm9tIFwiLi9TdGlja01hbjFcIjtcclxuXHJcbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eX0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuZGVjbGFyZSBjb25zdCB3aW5kb3c6IGFueTtcclxuXHJcbkBjY2NsYXNzXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFN0aWNrTWFuMyBleHRlbmRzIFNpbmdsZXRvbjxTdGlja01hbjE+IHtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGpveVN0aWNrRm9sbG93OiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIE1hcDogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLkludGVnZXIpXHJcbiAgICBjb3VudEVuZW15RW5kOiBudW1iZXIgPSAwO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgTXlDaGFyYWN0ZXI6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgR3VpZGU6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgRWZmZWN0OiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIHR4dFN0YXJ0OiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGVuZW15UGFyZW50OiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGJ0bkRvd25sb2FkOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGVuZENhcmQ6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgbmlnaHRFbmQ6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgY2lyY2xlRW5kOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGJ0bkFsbDogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBuRW5lbXlFbmQ6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIGNvdW50RW5lbXk6IG51bWJlciA9IDA7XHJcbiAgICBib29sQ2hlY2tFbmQ6IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgICBib29sY2hlY2tJbnRlcmFjdGlvbjogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgaXJvbnNvdXJjZTogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgbWluZHdvcmtzOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICB2dW5nbGU6IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICBzdXBlcigpO1xyXG4gICAgICAgIFN0aWNrTWFuMS5faW5zdGFuY2UgPSB0aGlzO1xyXG4gICAgfVxyXG5cclxuICAgIG9uRW5hYmxlKCkge1xyXG4gICAgICAgIGluc3RhbmNlLm9uKEtleUV2ZW50LnNjYWxlLCB0aGlzLnNjYWxlLCB0aGlzKTtcclxuICAgICAgICBpbnN0YW5jZS5vbihLZXlFdmVudC5wbHVzRW5lbXksIHRoaXMucGx1c0VuZW15LCB0aGlzKTtcclxuICAgICAgICBpbnN0YW5jZS5vbihLZXlFdmVudC5hY3RpdmVHdWlkZSwgdGhpcy5hY3RpdmVHdWlkZSwgdGhpcyk7XHJcbiAgICB9XHJcblxyXG4gICAgb25EaXNhYmxlKCkge1xyXG4gICAgICAgIGluc3RhbmNlLm9mZihLZXlFdmVudC5zY2FsZSwgdGhpcy5zY2FsZSwgdGhpcyk7XHJcbiAgICAgICAgaW5zdGFuY2Uub2ZmKEtleUV2ZW50LnBsdXNFbmVteSwgdGhpcy5wbHVzRW5lbXksIHRoaXMpO1xyXG4gICAgICAgIGluc3RhbmNlLm9mZihLZXlFdmVudC5hY3RpdmVHdWlkZSwgdGhpcy5hY3RpdmVHdWlkZSwgdGhpcyk7XHJcbiAgICB9XHJcblxyXG4gICAgc3RhcnQoKSB7XHJcbiAgICAgICAgdGhpcy5wbGF5R2FtZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZSAoZHQpIHtcclxuICAgICAgICBpZiAoR2xvYmFsLmJvb2xFbmFibGVUb3VjaCAmJiAhdGhpcy5ib29sY2hlY2tJbnRlcmFjdGlvbikge1xyXG4gICAgICAgICAgICB0aGlzLnR4dFN0YXJ0LmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICBpZiAodGhpcy5pcm9uc291cmNlKSB7XHJcbiAgICAgICAgICAgICAgICB3aW5kb3cuTlVDLnRyaWdnZXIuaW50ZXJhY3Rpb24oKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRoaXMuY291bnRFbmVteSA9PSB0aGlzLmNvdW50RW5lbXlFbmQgJiYgIXRoaXMuYm9vbENoZWNrRW5kKSB7XHJcbiAgICAgICAgICAgIEdsb2JhbC5ib29sU3RhcnRQbGF5ID0gZmFsc2U7XHJcbiAgICAgICAgICAgIHRoaXMuYm9vbENoZWNrRW5kID0gdHJ1ZTtcclxuICAgICAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5TdXBwb3J0RW5kKCk7XHJcbiAgICAgICAgICAgIH0sIDEpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwbGF5R2FtZSgpIHtcclxuICAgICAgICB0aGlzLk15Q2hhcmFjdGVyLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgR2xvYmFsLmJvb2xTdGFydFBsYXkgPSB0cnVlO1xyXG4gICAgICAgIGZvcihsZXQgaSA9IDA7IGkgPCB0aGlzLmVuZW15UGFyZW50LmNoaWxkcmVuQ291bnQ7IGkrKykge1xyXG4gICAgICAgICAgICB0aGlzLmVuZW15UGFyZW50LmNoaWxkcmVuW2ldLmdldENvbXBvbmVudChFbmVteUNvbnRyb2xsZXIpLlN0YXJ0TW92ZSgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBTdXBwb3J0RW5kKCkge1xyXG4gICAgICAgIHRoaXMuc2NoZWR1bGVPbmNlKCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5uaWdodEVuZC5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNpcmNsZUVuZC5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubmlnaHRFbmQuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jaXJjbGVFbmQuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5NeUNoYXJhY3Rlci5vcGFjaXR5ID0gMDtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLkVuZEdhbWUoKTtcclxuICAgICAgICAgICAgICAgIH0sIDAuNik7XHJcbiAgICAgICAgICAgIH0sIDAuNCk7XHJcbiAgICAgICAgICAgIHRoaXMuY2lyY2xlRW5kLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgfSwgMC4yKTtcclxuICAgIH1cclxuXHJcbiAgICBFbmRHYW1lKCkge1xyXG4gICAgICAgIEdsb2JhbC5ib29sZW5kRyA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5qb3lTdGlja0ZvbGxvdy5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLmVuZENhcmQuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLnBsYXlHYW1lKCk7XHJcbiAgICAgICAgdGhpcy5NYXAuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLmVuZENhcmQuY2hpbGRyZW5bMl0uYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLmVuZENhcmQuY2hpbGRyZW5bM10uYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLmVuZENhcmQuY2hpbGRyZW5bNF0uYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICBmb3IobGV0IGkgPSAwOyBpIDwgdGhpcy5uRW5lbXlFbmQuY2hpbGRyZW5Db3VudDsgaSsrKSB7XHJcbiAgICAgICAgICAgIHRoaXMubkVuZW15RW5kLmNoaWxkcmVuW2ldLmdldENvbXBvbmVudChFbmVteUNvbnRyb2xsZXIpLlN0YXJ0TW92ZSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLk15Q2hhcmFjdGVyLm9wYWNpdHkgPSAyNTU7XHJcbiAgICAgICAgdGhpcy5NeUNoYXJhY3Rlci54ID0gLTM7XHJcbiAgICAgICAgdGhpcy5NeUNoYXJhY3Rlci55ID0gLTYwO1xyXG4gICAgICAgIHRoaXMuTXlDaGFyYWN0ZXIuc2NhbGUgPSAzNTA7XHJcbiAgICAgICAgdGhpcy5NeUNoYXJhY3Rlci5ldWxlckFuZ2xlcyA9IG5ldyBjYy5WZWMzKDkwLCAwLCAxODApO1xyXG4gICAgICAgIHRoaXMuTXlDaGFyYWN0ZXIuZ2V0Q29tcG9uZW50KENoYXJhY3RlckNvbnRyb2xsZXIpLkFycm93RGlyZWN0aW9uLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuR3VpZGUuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMuZW5lbXlQYXJlbnQuY2hpbGRyZW5Db3VudDsgaSsrKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZW5lbXlQYXJlbnQuY2hpbGRyZW5baV0uZ2V0Q29tcG9uZW50KEVuZW15Q29udHJvbGxlcikuYm9keURlYXRoLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLmVuZW15UGFyZW50LmNoaWxkcmVuW2ldLmFjdGl2ZSA9IGZhbHNlOztcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5idG5BbGwuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLmJ0bkRvd25sb2FkLmdldENvbXBvbmVudChjYy5CdXR0b24pLmludGVyYWN0YWJsZSA9IGZhbHNlO1xyXG4gICAgICAgIGlmICh0aGlzLm1pbmR3b3Jrcykge1xyXG4gICAgICAgICAgICB3aW5kb3cuZ2FtZUVuZCAmJiB3aW5kb3cuZ2FtZUVuZCgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5pcm9uc291cmNlKSB7XHJcbiAgICAgICAgICAgIHdpbmRvdy5OVUMudHJpZ2dlci5lbmRHYW1lKCd3aW4nKVxyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy52dW5nbGUpIHtcclxuICAgICAgICAgICAgcGFyZW50LnBvc3RNZXNzYWdlKCdjb21wbGV0ZScsICcqJyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHNjYWxlKCkge1xyXG4gICAgICAgIHRoaXMuRWZmZWN0LnNjYWxlID0gMDtcclxuICAgICAgICB0aGlzLkVmZmVjdC5vcGFjaXR5ID0gMjU1O1xyXG4gICAgICAgIHRoaXMuRWZmZWN0LnJ1bkFjdGlvbihjYy5zZXF1ZW5jZShjYy5zY2FsZVRvKDAuMiwgMSkuZWFzaW5nKGNjLmVhc2VCb3VuY2VPdXQoKSksIGNjLmNhbGxGdW5jKCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5FZmZlY3QucnVuQWN0aW9uKGNjLmZhZGVPdXQoMC4xKSk7XHJcbiAgICAgICAgICAgIH0sIDAuMik7XHJcbiAgICAgICAgfSkpKTtcclxuICAgIH1cclxuXHJcbiAgICBwbHVzRW5lbXkoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuY291bnRFbmVteSA8IHRoaXMuY291bnRFbmVteUVuZClcclxuICAgICAgICAgICAgdGhpcy5jb3VudEVuZW15Kys7XHJcbiAgICB9XHJcblxyXG4gICAgYWN0aXZlR3VpZGUoKSB7XHJcbiAgICAgICAgaWYoR2xvYmFsLmJvb2xTdGFydFBsYXkpXHJcbiAgICAgICAgICAgIHRoaXMuR3VpZGUuYWN0aXZlID0gdHJ1ZTtcclxuICAgIH1cclxufVxyXG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Camera/CameraFollow.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '9a3c4BNdsNGOKf91Tc0+3pS', 'CameraFollow');
// Scripts/Camera/CameraFollow.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var CharacterController_1 = require("../Character/CharacterController");
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var CameraFollow = /** @class */ (function (_super) {
    __extends(CameraFollow, _super);
    function CameraFollow() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.gameplayInstance = null;
        _this.cameraOffsetX = 0;
        _this.cameraOffsetY = 0;
        _this.cameraOffsetZ = 0;
        _this.Target = null;
        //9,12
        _this.plusY = 0;
        _this.plusZ = 0;
        return _this;
    }
    CameraFollow.prototype.start = function () {
        this.gameplayInstance = GamePlayInstance_1.default.Instance(GamePlayInstance_1.default);
        this.cameraOffsetX = this.node.x - 0;
        this.cameraOffsetY = this.node.y + 45;
        this.cameraOffsetZ = this.node.z + 5;
    };
    CameraFollow.prototype.update = function () {
        if (Global_1.default.boolStartPlay && !Global_1.default.boolendG) {
            //let newPosX = this.gameplayInstance.gameplay.MyCharacter.x + this.cameraOffsetX;
            //let newPosZ = this.gameplayInstance.gameplay.MyCharacter.z + this.cameraOffsetZ;
            //if (!Global.boolendG) {
            if (this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController_1.default).level == 0) {
                this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.MyCharacter.x + this.cameraOffsetX, 0.2);
                this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.MyCharacter.y + this.cameraOffsetY, 0.2);
                this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.MyCharacter.z + this.cameraOffsetZ, 0.2);
            }
            // else if (this.gameplayInstance.gameplay.myCharacter.getComponent(CharacterController).level == 1) {
            //     this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.myCharacter.x + this.cameraOffsetX, 0.2);
            //     this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.myCharacter.y + this.cameraOffsetY - this.plusY, 0.2);
            //     this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.myCharacter.z + this.cameraOffsetZ + this.plusZ, 0.2);
            // }
            // else if (this.gameplayInstance.gameplay.myCharacter.getComponent(CharacterController).level == 2) {
            //     this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.myCharacter.x + this.cameraOffsetX, 0.2);
            //     this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.myCharacter.y + this.cameraOffsetY - this.plusY, 0.2);
            //     this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.myCharacter.z + this.cameraOffsetZ + this.plusZ, 0.2);
            // }
            // else if (this.gameplayInstance.gameplay.myCharacter.getComponent(CharacterController).level == 3) {
            //     this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.myCharacter.x + this.cameraOffsetX, 0.2);
            //     this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.myCharacter.y + this.cameraOffsetY - this.plusY, 0.2);
            //     this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.myCharacter.z + this.cameraOffsetZ + this.plusZ, 0.2);
            // }
            // else if (this.gameplayInstance.gameplay.myCharacter.getComponent(CharacterController).level == 4) {
            //     this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.myCharacter.x + this.cameraOffsetX, 0.2);
            //     this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.myCharacter.y + this.cameraOffsetY - this.plusY, 0.2);
            //     this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.myCharacter.z + this.cameraOffsetZ + this.plusZ, 0.2);
            // }
            //}
            else if (this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController_1.default).level == 1) {
                //this.resetOffset(0, 12, 9);
                this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.MyCharacter.x + this.cameraOffsetX, 0.2);
                this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.MyCharacter.y + this.cameraOffsetY - 12, 0.2);
                this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.MyCharacter.z + this.cameraOffsetZ + 9, 0.2);
            }
            else if (this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController_1.default).level == 2) {
                this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.MyCharacter.x + this.cameraOffsetX, 0.2);
                this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.MyCharacter.y + this.cameraOffsetY - 24, 0.2);
                this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.MyCharacter.z + this.cameraOffsetZ + 18, 0.2);
            }
            else if (this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController_1.default).level == 3) {
                this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.MyCharacter.x + this.cameraOffsetX, 0.2);
                this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.MyCharacter.y + this.cameraOffsetY - 36, 0.2);
                this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.MyCharacter.z + this.cameraOffsetZ + 27, 0.2);
            }
            else if (this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController_1.default).level == 4) {
                this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.MyCharacter.x + this.cameraOffsetX, 0.2);
                this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.MyCharacter.y + this.cameraOffsetY - 48, 0.2);
                this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.MyCharacter.z + this.cameraOffsetZ + 36, 0.2);
            }
        }
        else if (Global_1.default.boolendG) {
            this.node.x = 0;
            this.node.y = -120;
            this.node.z = 120;
            this.node.eulerAngles = new cc.Vec3(38, 0, 0);
        }
        // else {
        //     for (let i = 0; i < this.gameplayInstance.gameplay.enemyParent.childrenCount; i++) {
        //         this.Target = this.gameplayInstance.gameplay.enemyParent.children[i];
        //     }
        //     this.resetOffset();
        //     this.node.x = cc.misc.lerp(this.node.x, this.Target.x / 2, 0.2);
        //     this.node.y = cc.misc.lerp(this.node.y, this.Target.y + this.cameraOffsetY, 0.2);
        //     this.node.z = cc.misc.lerp(this.node.z, this.Target.z + this.cameraOffsetZ, 0.2);
        //}
        //}
    };
    // resetOffset(x: number, y: number, z: number) {
    //     this.cameraOffsetX = this.cameraOffsetX - x;
    //     this.cameraOffsetY = this.cameraOffsetY - y;
    //     this.cameraOffsetZ = this.cameraOffsetZ + z;
    // }
    CameraFollow.prototype.PlusYZ = function () {
        this.plusY += 12;
        this.plusZ += 9;
    };
    CameraFollow = __decorate([
        ccclass
    ], CameraFollow);
    return CameraFollow;
}(cc.Component));
exports.default = CameraFollow;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ2FtZXJhXFxDYW1lcmFGb2xsb3cudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRUEsd0VBQW1FO0FBQ25FLCtEQUEwRDtBQUMxRCwyQ0FBc0M7QUFFaEMsSUFBQSxrQkFBcUMsRUFBbkMsb0JBQU8sRUFBRSxzQkFBMEIsQ0FBQztBQUc1QztJQUEwQyxnQ0FBWTtJQUR0RDtRQUFBLHFFQStGQztRQTdGRyxzQkFBZ0IsR0FBcUIsSUFBSSxDQUFDO1FBQzFDLG1CQUFhLEdBQVcsQ0FBQyxDQUFDO1FBQzFCLG1CQUFhLEdBQVcsQ0FBQyxDQUFDO1FBQzFCLG1CQUFhLEdBQVcsQ0FBQyxDQUFDO1FBQzFCLFlBQU0sR0FBWSxJQUFJLENBQUM7UUFDdkIsTUFBTTtRQUNOLFdBQUssR0FBVyxDQUFDLENBQUM7UUFDbEIsV0FBSyxHQUFXLENBQUMsQ0FBQzs7SUFzRnRCLENBQUM7SUFyRkcsNEJBQUssR0FBTDtRQUNJLElBQUksQ0FBQyxnQkFBZ0IsR0FBRywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQztRQUNwRSxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNyQyxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztRQUN0QyxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUN6QyxDQUFDO0lBQ0QsNkJBQU0sR0FBTjtRQUNJLElBQUksZ0JBQU0sQ0FBQyxhQUFhLElBQUksQ0FBQyxnQkFBTSxDQUFDLFFBQVEsRUFBRTtZQUMxQyxrRkFBa0Y7WUFDbEYsa0ZBQWtGO1lBQ2xGLHlCQUF5QjtZQUN6QixJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyw2QkFBbUIsQ0FBQyxDQUFDLEtBQUssSUFBSSxDQUFDLEVBQUU7Z0JBQ3pGLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxFQUFFLEdBQUcsQ0FBQyxDQUFDO2dCQUNoSCxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGFBQWEsRUFBRSxHQUFHLENBQUMsQ0FBQztnQkFDaEgsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLEVBQUUsR0FBRyxDQUFDLENBQUM7YUFDbkg7WUFDRCxzR0FBc0c7WUFDdEcsdUhBQXVIO1lBQ3ZILG9JQUFvSTtZQUNwSSxvSUFBb0k7WUFDcEksSUFBSTtZQUNKLHNHQUFzRztZQUN0Ryx1SEFBdUg7WUFDdkgsb0lBQW9JO1lBQ3BJLG9JQUFvSTtZQUNwSSxJQUFJO1lBQ0osc0dBQXNHO1lBQ3RHLHVIQUF1SDtZQUN2SCxvSUFBb0k7WUFDcEksb0lBQW9JO1lBQ3BJLElBQUk7WUFDSixzR0FBc0c7WUFDdEcsdUhBQXVIO1lBQ3ZILG9JQUFvSTtZQUNwSSxvSUFBb0k7WUFDcEksSUFBSTtZQUNKLEdBQUc7aUJBQ0UsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsNkJBQW1CLENBQUMsQ0FBQyxLQUFLLElBQUksQ0FBQyxFQUFFO2dCQUM5Riw2QkFBNkI7Z0JBQzdCLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxFQUFFLEdBQUcsQ0FBQyxDQUFDO2dCQUNoSCxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBQ3JILElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxHQUFHLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQzthQUN2SDtpQkFDSSxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyw2QkFBbUIsQ0FBQyxDQUFDLEtBQUssSUFBSSxDQUFDLEVBQUU7Z0JBQzlGLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxFQUFFLEdBQUcsQ0FBQyxDQUFDO2dCQUNoSCxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBQ3JILElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsRUFBRSxHQUFHLENBQUMsQ0FBQzthQUN4SDtpQkFDSSxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyw2QkFBbUIsQ0FBQyxDQUFDLEtBQUssSUFBSSxDQUFDLEVBQUU7Z0JBQzlGLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxFQUFFLEdBQUcsQ0FBQyxDQUFDO2dCQUNoSCxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBQ3JILElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsRUFBRSxHQUFHLENBQUMsQ0FBQzthQUN4SDtpQkFDSSxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyw2QkFBbUIsQ0FBQyxDQUFDLEtBQUssSUFBSSxDQUFDLEVBQUU7Z0JBQzlGLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxFQUFFLEdBQUcsQ0FBQyxDQUFDO2dCQUNoSCxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBQ3JILElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsRUFBRSxHQUFHLENBQUMsQ0FBQzthQUN4SDtTQUNKO2FBQ0ksSUFBRyxnQkFBTSxDQUFDLFFBQVEsRUFBQztZQUNwQixJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDaEIsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUM7WUFDbkIsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDO1lBQ2xCLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxDQUFDO1NBQy9DO1FBQ0QsU0FBUztRQUNULDJGQUEyRjtRQUMzRixnRkFBZ0Y7UUFDaEYsUUFBUTtRQUNSLDBCQUEwQjtRQUMxQix1RUFBdUU7UUFDdkUsd0ZBQXdGO1FBQ3hGLHdGQUF3RjtRQUN4RixHQUFHO1FBQ0gsR0FBRztJQUNQLENBQUM7SUFDRCxpREFBaUQ7SUFDakQsbURBQW1EO0lBQ25ELG1EQUFtRDtJQUNuRCxtREFBbUQ7SUFDbkQsSUFBSTtJQUNKLDZCQUFNLEdBQU47UUFDSSxJQUFJLENBQUMsS0FBSyxJQUFJLEVBQUUsQ0FBQztRQUNqQixJQUFJLENBQUMsS0FBSyxJQUFJLENBQUMsQ0FBQztJQUNwQixDQUFDO0lBN0ZnQixZQUFZO1FBRGhDLE9BQU87T0FDYSxZQUFZLENBOEZoQztJQUFELG1CQUFDO0NBOUZELEFBOEZDLENBOUZ5QyxFQUFFLENBQUMsU0FBUyxHQThGckQ7a0JBOUZvQixZQUFZIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiXHJcblxyXG5pbXBvcnQgQ2hhcmFjdGVyQ29udHJvbGxlciBmcm9tIFwiLi4vQ2hhcmFjdGVyL0NoYXJhY3RlckNvbnRyb2xsZXJcIjtcclxuaW1wb3J0IEdhbWVQbGF5SW5zdGFuY2UgZnJvbSBcIi4uL0NvbW1vbi9HYW1lUGxheUluc3RhbmNlXCI7XHJcbmltcG9ydCBHbG9iYWwgZnJvbSBcIi4uL0NvbW1vbi9HbG9iYWxcIjtcclxuXHJcbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XHJcblxyXG5AY2NjbGFzc1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBDYW1lcmFGb2xsb3cgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xyXG4gICAgZ2FtZXBsYXlJbnN0YW5jZTogR2FtZVBsYXlJbnN0YW5jZSA9IG51bGw7XHJcbiAgICBjYW1lcmFPZmZzZXRYOiBudW1iZXIgPSAwO1xyXG4gICAgY2FtZXJhT2Zmc2V0WTogbnVtYmVyID0gMDtcclxuICAgIGNhbWVyYU9mZnNldFo6IG51bWJlciA9IDA7XHJcbiAgICBUYXJnZXQ6IGNjLk5vZGUgPSBudWxsO1xyXG4gICAgLy85LDEyXHJcbiAgICBwbHVzWTogbnVtYmVyID0gMDtcclxuICAgIHBsdXNaOiBudW1iZXIgPSAwO1xyXG4gICAgc3RhcnQoKSB7XHJcbiAgICAgICAgdGhpcy5nYW1lcGxheUluc3RhbmNlID0gR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKTtcclxuICAgICAgICB0aGlzLmNhbWVyYU9mZnNldFggPSB0aGlzLm5vZGUueCAtIDA7XHJcbiAgICAgICAgdGhpcy5jYW1lcmFPZmZzZXRZID0gdGhpcy5ub2RlLnkgKyA0NTtcclxuICAgICAgICB0aGlzLmNhbWVyYU9mZnNldFogPSB0aGlzLm5vZGUueiArIDU7XHJcbiAgICB9XHJcbiAgICB1cGRhdGUoKSB7XHJcbiAgICAgICAgaWYgKEdsb2JhbC5ib29sU3RhcnRQbGF5ICYmICFHbG9iYWwuYm9vbGVuZEcpIHtcclxuICAgICAgICAgICAgLy9sZXQgbmV3UG9zWCA9IHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci54ICsgdGhpcy5jYW1lcmFPZmZzZXRYO1xyXG4gICAgICAgICAgICAvL2xldCBuZXdQb3NaID0gdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLnogKyB0aGlzLmNhbWVyYU9mZnNldFo7XHJcbiAgICAgICAgICAgIC8vaWYgKCFHbG9iYWwuYm9vbGVuZEcpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci5nZXRDb21wb25lbnQoQ2hhcmFjdGVyQ29udHJvbGxlcikubGV2ZWwgPT0gMCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLnggPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLngsIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci54ICsgdGhpcy5jYW1lcmFPZmZzZXRYLCAwLjIpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLnkgPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLnksIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci55ICsgdGhpcy5jYW1lcmFPZmZzZXRZLCAwLjIpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLnogPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLnosIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci56ICsgdGhpcy5jYW1lcmFPZmZzZXRaLCAwLjIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC8vIGVsc2UgaWYgKHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5teUNoYXJhY3Rlci5nZXRDb21wb25lbnQoQ2hhcmFjdGVyQ29udHJvbGxlcikubGV2ZWwgPT0gMSkge1xyXG4gICAgICAgICAgICAvLyAgICAgdGhpcy5ub2RlLnggPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLngsIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5teUNoYXJhY3Rlci54ICsgdGhpcy5jYW1lcmFPZmZzZXRYLCAwLjIpO1xyXG4gICAgICAgICAgICAvLyAgICAgdGhpcy5ub2RlLnkgPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLnksIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5teUNoYXJhY3Rlci55ICsgdGhpcy5jYW1lcmFPZmZzZXRZIC0gdGhpcy5wbHVzWSwgMC4yKTtcclxuICAgICAgICAgICAgLy8gICAgIHRoaXMubm9kZS56ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS56LCB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkubXlDaGFyYWN0ZXIueiArIHRoaXMuY2FtZXJhT2Zmc2V0WiArIHRoaXMucGx1c1osIDAuMik7XHJcbiAgICAgICAgICAgIC8vIH1cclxuICAgICAgICAgICAgLy8gZWxzZSBpZiAodGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLmdldENvbXBvbmVudChDaGFyYWN0ZXJDb250cm9sbGVyKS5sZXZlbCA9PSAyKSB7XHJcbiAgICAgICAgICAgIC8vICAgICB0aGlzLm5vZGUueCA9IGNjLm1pc2MubGVycCh0aGlzLm5vZGUueCwgdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLnggKyB0aGlzLmNhbWVyYU9mZnNldFgsIDAuMik7XHJcbiAgICAgICAgICAgIC8vICAgICB0aGlzLm5vZGUueSA9IGNjLm1pc2MubGVycCh0aGlzLm5vZGUueSwgdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLnkgKyB0aGlzLmNhbWVyYU9mZnNldFkgLSB0aGlzLnBsdXNZLCAwLjIpO1xyXG4gICAgICAgICAgICAvLyAgICAgdGhpcy5ub2RlLnogPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLnosIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5teUNoYXJhY3Rlci56ICsgdGhpcy5jYW1lcmFPZmZzZXRaICsgdGhpcy5wbHVzWiwgMC4yKTtcclxuICAgICAgICAgICAgLy8gfVxyXG4gICAgICAgICAgICAvLyBlbHNlIGlmICh0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkubXlDaGFyYWN0ZXIuZ2V0Q29tcG9uZW50KENoYXJhY3RlckNvbnRyb2xsZXIpLmxldmVsID09IDMpIHtcclxuICAgICAgICAgICAgLy8gICAgIHRoaXMubm9kZS54ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS54LCB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkubXlDaGFyYWN0ZXIueCArIHRoaXMuY2FtZXJhT2Zmc2V0WCwgMC4yKTtcclxuICAgICAgICAgICAgLy8gICAgIHRoaXMubm9kZS55ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS55LCB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkubXlDaGFyYWN0ZXIueSArIHRoaXMuY2FtZXJhT2Zmc2V0WSAtIHRoaXMucGx1c1ksIDAuMik7XHJcbiAgICAgICAgICAgIC8vICAgICB0aGlzLm5vZGUueiA9IGNjLm1pc2MubGVycCh0aGlzLm5vZGUueiwgdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lm15Q2hhcmFjdGVyLnogKyB0aGlzLmNhbWVyYU9mZnNldFogKyB0aGlzLnBsdXNaLCAwLjIpO1xyXG4gICAgICAgICAgICAvLyB9XHJcbiAgICAgICAgICAgIC8vIGVsc2UgaWYgKHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5teUNoYXJhY3Rlci5nZXRDb21wb25lbnQoQ2hhcmFjdGVyQ29udHJvbGxlcikubGV2ZWwgPT0gNCkge1xyXG4gICAgICAgICAgICAvLyAgICAgdGhpcy5ub2RlLnggPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLngsIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5teUNoYXJhY3Rlci54ICsgdGhpcy5jYW1lcmFPZmZzZXRYLCAwLjIpO1xyXG4gICAgICAgICAgICAvLyAgICAgdGhpcy5ub2RlLnkgPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLnksIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5teUNoYXJhY3Rlci55ICsgdGhpcy5jYW1lcmFPZmZzZXRZIC0gdGhpcy5wbHVzWSwgMC4yKTtcclxuICAgICAgICAgICAgLy8gICAgIHRoaXMubm9kZS56ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS56LCB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkubXlDaGFyYWN0ZXIueiArIHRoaXMuY2FtZXJhT2Zmc2V0WiArIHRoaXMucGx1c1osIDAuMik7XHJcbiAgICAgICAgICAgIC8vIH1cclxuICAgICAgICAgICAgLy99XHJcbiAgICAgICAgICAgIGVsc2UgaWYgKHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci5nZXRDb21wb25lbnQoQ2hhcmFjdGVyQ29udHJvbGxlcikubGV2ZWwgPT0gMSkge1xyXG4gICAgICAgICAgICAgICAgLy90aGlzLnJlc2V0T2Zmc2V0KDAsIDEyLCA5KTtcclxuICAgICAgICAgICAgICAgIHRoaXMubm9kZS54ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS54LCB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueCArIHRoaXMuY2FtZXJhT2Zmc2V0WCwgMC4yKTtcclxuICAgICAgICAgICAgICAgIHRoaXMubm9kZS55ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS55LCB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueSArIHRoaXMuY2FtZXJhT2Zmc2V0WSAtIDEyLCAwLjIpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLnogPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLnosIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci56ICsgdGhpcy5jYW1lcmFPZmZzZXRaICsgOSwgMC4yKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBlbHNlIGlmICh0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIuZ2V0Q29tcG9uZW50KENoYXJhY3RlckNvbnRyb2xsZXIpLmxldmVsID09IDIpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubm9kZS54ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS54LCB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueCArIHRoaXMuY2FtZXJhT2Zmc2V0WCwgMC4yKTtcclxuICAgICAgICAgICAgICAgIHRoaXMubm9kZS55ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS55LCB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueSArIHRoaXMuY2FtZXJhT2Zmc2V0WSAtIDI0LCAwLjIpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLnogPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLnosIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci56ICsgdGhpcy5jYW1lcmFPZmZzZXRaICsgMTgsIDAuMik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZWxzZSBpZiAodGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLmdldENvbXBvbmVudChDaGFyYWN0ZXJDb250cm9sbGVyKS5sZXZlbCA9PSAzKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vZGUueCA9IGNjLm1pc2MubGVycCh0aGlzLm5vZGUueCwgdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLnggKyB0aGlzLmNhbWVyYU9mZnNldFgsIDAuMik7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vZGUueSA9IGNjLm1pc2MubGVycCh0aGlzLm5vZGUueSwgdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLnkgKyB0aGlzLmNhbWVyYU9mZnNldFkgLSAzNiwgMC4yKTtcclxuICAgICAgICAgICAgICAgIHRoaXMubm9kZS56ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS56LCB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIueiArIHRoaXMuY2FtZXJhT2Zmc2V0WiArIDI3LCAwLjIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGVsc2UgaWYgKHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci5nZXRDb21wb25lbnQoQ2hhcmFjdGVyQ29udHJvbGxlcikubGV2ZWwgPT0gNCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLnggPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLngsIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci54ICsgdGhpcy5jYW1lcmFPZmZzZXRYLCAwLjIpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLnkgPSBjYy5taXNjLmxlcnAodGhpcy5ub2RlLnksIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci55ICsgdGhpcy5jYW1lcmFPZmZzZXRZIC0gNDgsIDAuMik7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm5vZGUueiA9IGNjLm1pc2MubGVycCh0aGlzLm5vZGUueiwgdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLnogKyB0aGlzLmNhbWVyYU9mZnNldFogKyAzNiwgMC4yKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNlIGlmKEdsb2JhbC5ib29sZW5kRyl7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS54ID0gMDtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLnkgPSAtMTIwO1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUueiA9IDEyMDtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLmV1bGVyQW5nbGVzID0gbmV3IGNjLlZlYzMoMzgsMCwwKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLy8gZWxzZSB7XHJcbiAgICAgICAgLy8gICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5LmVuZW15UGFyZW50LmNoaWxkcmVuQ291bnQ7IGkrKykge1xyXG4gICAgICAgIC8vICAgICAgICAgdGhpcy5UYXJnZXQgPSB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuZW5lbXlQYXJlbnQuY2hpbGRyZW5baV07XHJcbiAgICAgICAgLy8gICAgIH1cclxuICAgICAgICAvLyAgICAgdGhpcy5yZXNldE9mZnNldCgpO1xyXG4gICAgICAgIC8vICAgICB0aGlzLm5vZGUueCA9IGNjLm1pc2MubGVycCh0aGlzLm5vZGUueCwgdGhpcy5UYXJnZXQueCAvIDIsIDAuMik7XHJcbiAgICAgICAgLy8gICAgIHRoaXMubm9kZS55ID0gY2MubWlzYy5sZXJwKHRoaXMubm9kZS55LCB0aGlzLlRhcmdldC55ICsgdGhpcy5jYW1lcmFPZmZzZXRZLCAwLjIpO1xyXG4gICAgICAgIC8vICAgICB0aGlzLm5vZGUueiA9IGNjLm1pc2MubGVycCh0aGlzLm5vZGUueiwgdGhpcy5UYXJnZXQueiArIHRoaXMuY2FtZXJhT2Zmc2V0WiwgMC4yKTtcclxuICAgICAgICAvL31cclxuICAgICAgICAvL31cclxuICAgIH1cclxuICAgIC8vIHJlc2V0T2Zmc2V0KHg6IG51bWJlciwgeTogbnVtYmVyLCB6OiBudW1iZXIpIHtcclxuICAgIC8vICAgICB0aGlzLmNhbWVyYU9mZnNldFggPSB0aGlzLmNhbWVyYU9mZnNldFggLSB4O1xyXG4gICAgLy8gICAgIHRoaXMuY2FtZXJhT2Zmc2V0WSA9IHRoaXMuY2FtZXJhT2Zmc2V0WSAtIHk7XHJcbiAgICAvLyAgICAgdGhpcy5jYW1lcmFPZmZzZXRaID0gdGhpcy5jYW1lcmFPZmZzZXRaICsgejtcclxuICAgIC8vIH1cclxuICAgIFBsdXNZWigpIHtcclxuICAgICAgICB0aGlzLnBsdXNZICs9IDEyO1xyXG4gICAgICAgIHRoaXMucGx1c1ogKz0gOTtcclxuICAgIH1cclxufVxyXG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/ColliderObj/MapController.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'bc404dQ86xGZIbTFHeg8YaK', 'MapController');
// Scripts/ColliderObj/MapController.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var SquareCollider_1 = require("./SquareCollider");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var MapController = /** @class */ (function (_super) {
    __extends(MapController, _super);
    function MapController() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        // @property(ObjectCollider)
        // public ListCircleObjects: ObjectCollider[] = [];
        _this.ListSquareObjects = [];
        return _this;
    }
    __decorate([
        property(SquareCollider_1.default)
    ], MapController.prototype, "ListSquareObjects", void 0);
    MapController = __decorate([
        ccclass
    ], MapController);
    return MapController;
}(cc.Component));
exports.default = MapController;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29sbGlkZXJPYmpcXE1hcENvbnRyb2xsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0EsbURBQThDO0FBRXhDLElBQUEsa0JBQW1DLEVBQWxDLG9CQUFPLEVBQUUsc0JBQXlCLENBQUM7QUFHMUM7SUFBMkMsaUNBQVk7SUFEdkQ7UUFBQSxxRUFPQztRQUpHLDRCQUE0QjtRQUM1QixtREFBbUQ7UUFFNUMsdUJBQWlCLEdBQXFCLEVBQUUsQ0FBQzs7SUFDcEQsQ0FBQztJQURHO1FBREMsUUFBUSxDQUFDLHdCQUFjLENBQUM7NERBQ3VCO0lBTC9CLGFBQWE7UUFEakMsT0FBTztPQUNhLGFBQWEsQ0FNakM7SUFBRCxvQkFBQztDQU5ELEFBTUMsQ0FOMEMsRUFBRSxDQUFDLFNBQVMsR0FNdEQ7a0JBTm9CLGFBQWEiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJcclxuaW1wb3J0IFNxdWFyZUNvbGxpZGVyIGZyb20gXCIuL1NxdWFyZUNvbGxpZGVyXCI7XHJcblxyXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcclxuXHJcbkBjY2NsYXNzXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIE1hcENvbnRyb2xsZXIgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xyXG5cclxuICAgIC8vIEBwcm9wZXJ0eShPYmplY3RDb2xsaWRlcilcclxuICAgIC8vIHB1YmxpYyBMaXN0Q2lyY2xlT2JqZWN0czogT2JqZWN0Q29sbGlkZXJbXSA9IFtdO1xyXG4gICAgQHByb3BlcnR5KFNxdWFyZUNvbGxpZGVyKVxyXG4gICAgcHVibGljIExpc3RTcXVhcmVPYmplY3RzOiBTcXVhcmVDb2xsaWRlcltdID0gW107XHJcbn1cclxuIl19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/GamePlay/Roblox2.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '8a4f5AjrJxHWL4OQhM6YREH', 'Roblox2');
// Scripts/GamePlay/Roblox2.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var CharacterController_1 = require("../Character/CharacterController");
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var KeyEvent_1 = require("../Common/KeyEvent");
var Singleton_1 = require("../Common/Singleton");
var EnemyController_1 = require("../Enemy/EnemyController");
var StickMan1_1 = require("./StickMan1");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Roblox2 = /** @class */ (function (_super) {
    __extends(Roblox2, _super);
    function Roblox2() {
        var _this = _super.call(this) || this;
        _this.joyStickFollow = null;
        _this.Map = null;
        _this.countEnemyEnd = 0;
        _this.MyCharacter = null;
        _this.Guide = null;
        _this.Effect = null;
        _this.enemyParent = null;
        _this.btnDownload = null;
        _this.endCard = null;
        _this.nightEnd = null;
        _this.circleEnd = null;
        _this.btnAll = null;
        _this.txtSmasher = null;
        _this.countEnemy = 0;
        _this.boolCheckEnd = false;
        _this.boolcheckInteraction = false;
        _this.ironsource = false;
        _this.mindworks = false;
        _this.vungle = false;
        StickMan1_1.default._instance = _this;
        return _this;
    }
    Roblox2.prototype.onEnable = function () {
        GamePlayInstance_1.instance.on(KeyEvent_1.default.scale, this.scale, this);
        GamePlayInstance_1.instance.on(KeyEvent_1.default.plusEnemy, this.plusEnemy, this);
        GamePlayInstance_1.instance.on(KeyEvent_1.default.activeGuide, this.activeGuide, this);
    };
    Roblox2.prototype.onDisable = function () {
        GamePlayInstance_1.instance.off(KeyEvent_1.default.scale, this.scale, this);
        GamePlayInstance_1.instance.off(KeyEvent_1.default.plusEnemy, this.plusEnemy, this);
        GamePlayInstance_1.instance.off(KeyEvent_1.default.activeGuide, this.activeGuide, this);
    };
    Roblox2.prototype.start = function () {
        cc.audioEngine.play(Global_1.default.soundBG, true, 1);
        this.playGame();
    };
    Roblox2.prototype.update = function (dt) {
        var _this = this;
        if (Global_1.default.boolEnableTouch && !this.boolcheckInteraction) {
            this.txtSmasher.active = false;
            if (this.ironsource) {
                window.NUC.trigger.interaction();
            }
        }
        if (this.countEnemy == this.countEnemyEnd && !this.boolCheckEnd) {
            Global_1.default.boolStartPlay = false;
            this.boolCheckEnd = true;
            this.scheduleOnce(function () {
                _this.SupportEnd();
            }, 1);
        }
    };
    Roblox2.prototype.playGame = function () {
        this.MyCharacter.active = true;
        Global_1.default.boolStartPlay = true;
        for (var i = 0; i < this.enemyParent.childrenCount; i++) {
            this.enemyParent.children[i].getComponent(EnemyController_1.default).StartMove();
        }
    };
    Roblox2.prototype.SupportEnd = function () {
        var _this = this;
        this.scheduleOnce(function () {
            _this.nightEnd.active = true;
            _this.scheduleOnce(function () {
                _this.circleEnd.active = true;
                _this.scheduleOnce(function () {
                    _this.nightEnd.active = false;
                    _this.circleEnd.active = false;
                    _this.MyCharacter.opacity = 0;
                    _this.EndGame();
                }, 0.6);
            }, 0.4);
            _this.circleEnd.active = true;
        }, 0.2);
    };
    Roblox2.prototype.EndGame = function () {
        Global_1.default.boolendG = true;
        cc.audioEngine.stopAllEffects();
        this.joyStickFollow.active = false;
        this.endCard.active = true;
        this.Map.active = true;
        this.endCard.children[2].active = true;
        this.endCard.children[3].active = true;
        this.endCard.children[4].active = true;
        this.MyCharacter.opacity = 255;
        this.MyCharacter.x = 0;
        this.MyCharacter.y = -60;
        this.MyCharacter.scale = 15;
        this.MyCharacter.eulerAngles = new cc.Vec3(90, 0, 180);
        this.MyCharacter.getComponent(CharacterController_1.default).ArrowDirection.active = false;
        this.Guide.active = true;
        for (var i = 0; i < this.enemyParent.childrenCount; i++) {
            this.enemyParent.children[i].getComponent(EnemyController_1.default).bodyDeath.active = false;
            this.enemyParent.children[i].active = false;
            ;
        }
        this.btnAll.active = true;
        this.btnDownload.getComponent(cc.Button).interactable = false;
        if (this.mindworks) {
            window.gameEnd && window.gameEnd();
        }
        if (this.ironsource) {
            window.NUC.trigger.endGame('win');
        }
        if (this.vungle) {
            parent.postMessage('complete', '*');
        }
    };
    Roblox2.prototype.scale = function () {
        var _this = this;
        this.Effect.scale = 0;
        this.Effect.opacity = 255;
        this.Effect.runAction(cc.sequence(cc.scaleTo(0.2, 1).easing(cc.easeBounceOut()), cc.callFunc(function () {
            _this.scheduleOnce(function () {
                _this.Effect.runAction(cc.fadeOut(0.1));
            }, 0.2);
        })));
    };
    Roblox2.prototype.plusEnemy = function () {
        if (this.countEnemy < this.countEnemyEnd)
            this.countEnemy++;
    };
    Roblox2.prototype.activeGuide = function () {
        if (Global_1.default.boolStartPlay)
            this.Guide.active = true;
    };
    __decorate([
        property(cc.Node)
    ], Roblox2.prototype, "joyStickFollow", void 0);
    __decorate([
        property(cc.Node)
    ], Roblox2.prototype, "Map", void 0);
    __decorate([
        property(cc.Integer)
    ], Roblox2.prototype, "countEnemyEnd", void 0);
    __decorate([
        property(cc.Node)
    ], Roblox2.prototype, "MyCharacter", void 0);
    __decorate([
        property(cc.Node)
    ], Roblox2.prototype, "Guide", void 0);
    __decorate([
        property(cc.Node)
    ], Roblox2.prototype, "Effect", void 0);
    __decorate([
        property(cc.Node)
    ], Roblox2.prototype, "enemyParent", void 0);
    __decorate([
        property(cc.Node)
    ], Roblox2.prototype, "btnDownload", void 0);
    __decorate([
        property(cc.Node)
    ], Roblox2.prototype, "endCard", void 0);
    __decorate([
        property(cc.Node)
    ], Roblox2.prototype, "nightEnd", void 0);
    __decorate([
        property(cc.Node)
    ], Roblox2.prototype, "circleEnd", void 0);
    __decorate([
        property(cc.Node)
    ], Roblox2.prototype, "btnAll", void 0);
    __decorate([
        property(cc.Node)
    ], Roblox2.prototype, "txtSmasher", void 0);
    Roblox2 = __decorate([
        ccclass
    ], Roblox2);
    return Roblox2;
}(Singleton_1.default));
exports.default = Roblox2;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcR2FtZVBsYXlcXFJvYmxveDIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsd0VBQW1FO0FBQ25FLCtEQUFzRDtBQUN0RCwyQ0FBc0M7QUFDdEMsK0NBQTBDO0FBQzFDLGlEQUE0QztBQUM1Qyw0REFBdUQ7QUFDdkQseUNBQW9DO0FBRTlCLElBQUEsa0JBQW1DLEVBQWxDLG9CQUFPLEVBQUUsc0JBQXlCLENBQUM7QUFLMUM7SUFBcUMsMkJBQW9CO0lBaURyRDtRQUFBLFlBQ0ksaUJBQU8sU0FFVjtRQWpERCxvQkFBYyxHQUFZLElBQUksQ0FBQztRQUcvQixTQUFHLEdBQVksSUFBSSxDQUFDO1FBR3BCLG1CQUFhLEdBQVcsQ0FBQyxDQUFDO1FBRzFCLGlCQUFXLEdBQVksSUFBSSxDQUFDO1FBRzVCLFdBQUssR0FBWSxJQUFJLENBQUM7UUFHdEIsWUFBTSxHQUFZLElBQUksQ0FBQztRQUd2QixpQkFBVyxHQUFZLElBQUksQ0FBQztRQUc1QixpQkFBVyxHQUFZLElBQUksQ0FBQztRQUc1QixhQUFPLEdBQVksSUFBSSxDQUFDO1FBR3hCLGNBQVEsR0FBWSxJQUFJLENBQUM7UUFHekIsZUFBUyxHQUFZLElBQUksQ0FBQztRQUcxQixZQUFNLEdBQVksSUFBSSxDQUFDO1FBR3ZCLGdCQUFVLEdBQVksSUFBSSxDQUFDO1FBRTNCLGdCQUFVLEdBQVcsQ0FBQyxDQUFDO1FBQ3ZCLGtCQUFZLEdBQVksS0FBSyxDQUFDO1FBRTlCLDBCQUFvQixHQUFZLEtBQUssQ0FBQztRQUN0QyxnQkFBVSxHQUFZLEtBQUssQ0FBQztRQUM1QixlQUFTLEdBQVksS0FBSyxDQUFDO1FBQzNCLFlBQU0sR0FBWSxLQUFLLENBQUM7UUFJcEIsbUJBQVMsQ0FBQyxTQUFTLEdBQUcsS0FBSSxDQUFDOztJQUMvQixDQUFDO0lBRUQsMEJBQVEsR0FBUjtRQUNJLDJCQUFRLENBQUMsRUFBRSxDQUFDLGtCQUFRLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDOUMsMkJBQVEsQ0FBQyxFQUFFLENBQUMsa0JBQVEsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUN0RCwyQkFBUSxDQUFDLEVBQUUsQ0FBQyxrQkFBUSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQzlELENBQUM7SUFFRCwyQkFBUyxHQUFUO1FBQ0ksMkJBQVEsQ0FBQyxHQUFHLENBQUMsa0JBQVEsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztRQUMvQywyQkFBUSxDQUFDLEdBQUcsQ0FBQyxrQkFBUSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3ZELDJCQUFRLENBQUMsR0FBRyxDQUFDLGtCQUFRLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDL0QsQ0FBQztJQUVELHVCQUFLLEdBQUw7UUFDSSxFQUFFLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxnQkFBTSxDQUFDLE9BQU8sRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDN0MsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQ3BCLENBQUM7SUFFRCx3QkFBTSxHQUFOLFVBQVEsRUFBRTtRQUFWLGlCQWVDO1FBZEcsSUFBSSxnQkFBTSxDQUFDLGVBQWUsSUFBSSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsRUFBRTtZQUN0RCxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFDL0IsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO2dCQUNqQixNQUFNLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsQ0FBQzthQUNwQztTQUNKO1FBRUQsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxhQUFhLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFO1lBQzdELGdCQUFNLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztZQUM3QixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztZQUN6QixJQUFJLENBQUMsWUFBWSxDQUFDO2dCQUNkLEtBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztZQUN0QixDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDVDtJQUNMLENBQUM7SUFFRCwwQkFBUSxHQUFSO1FBQ0ksSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQy9CLGdCQUFNLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztRQUM1QixLQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDcEQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLHlCQUFlLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQztTQUMxRTtJQUNMLENBQUM7SUFFRCw0QkFBVSxHQUFWO1FBQUEsaUJBY0M7UUFiRyxJQUFJLENBQUMsWUFBWSxDQUFDO1lBQ2QsS0FBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1lBQzVCLEtBQUksQ0FBQyxZQUFZLENBQUM7Z0JBQ2QsS0FBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO2dCQUM3QixLQUFJLENBQUMsWUFBWSxDQUFDO29CQUNkLEtBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztvQkFDN0IsS0FBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO29CQUM5QixLQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUM7b0JBQzdCLEtBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztnQkFDbkIsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQ1osQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQ1IsS0FBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ2pDLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztJQUNaLENBQUM7SUFFRCx5QkFBTyxHQUFQO1FBQ0ksZ0JBQU0sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLEVBQUUsQ0FBQyxXQUFXLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDaEMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ25DLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUMzQixJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDdkIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUN2QyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDdkMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDO1FBQy9CLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN2QixJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQztRQUN6QixJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7UUFDNUIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEdBQUcsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDdkQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsNkJBQW1CLENBQUMsQ0FBQyxjQUFjLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUNqRixJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDekIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3JELElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyx5QkFBZSxDQUFDLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFDcEYsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztZQUFBLENBQUM7U0FDaEQ7UUFDRCxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDMUIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7UUFDOUQsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2hCLE1BQU0sQ0FBQyxPQUFPLElBQUksTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO1NBQ3RDO1FBQ0QsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ2pCLE1BQU0sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQTtTQUNwQztRQUNELElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNiLE1BQU0sQ0FBQyxXQUFXLENBQUMsVUFBVSxFQUFFLEdBQUcsQ0FBQyxDQUFDO1NBQ3ZDO0lBQ0wsQ0FBQztJQUVELHVCQUFLLEdBQUw7UUFBQSxpQkFRQztRQVBHLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztRQUN0QixJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUM7UUFDMUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLGFBQWEsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLFFBQVEsQ0FBQztZQUN6RixLQUFJLENBQUMsWUFBWSxDQUFDO2dCQUNkLEtBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUMzQyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDWixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDVCxDQUFDO0lBRUQsMkJBQVMsR0FBVDtRQUNJLElBQUksSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsYUFBYTtZQUNwQyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDMUIsQ0FBQztJQUVELDZCQUFXLEdBQVg7UUFDSSxJQUFHLGdCQUFNLENBQUMsYUFBYTtZQUNuQixJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7SUFDakMsQ0FBQztJQWhLRDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO21EQUNhO0lBRy9CO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7d0NBQ0U7SUFHcEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQztrREFDSztJQUcxQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO2dEQUNVO0lBRzVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7MENBQ0k7SUFHdEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzsyQ0FDSztJQUd2QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO2dEQUNVO0lBRzVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7Z0RBQ1U7SUFHNUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzs0Q0FDTTtJQUd4QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOzZDQUNPO0lBR3pCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7OENBQ1E7SUFHMUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzsyQ0FDSztJQUd2QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOytDQUNTO0lBdkNWLE9BQU87UUFEM0IsT0FBTztPQUNhLE9BQU8sQ0FvSzNCO0lBQUQsY0FBQztDQXBLRCxBQW9LQyxDQXBLb0MsbUJBQVMsR0FvSzdDO2tCQXBLb0IsT0FBTyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBDaGFyYWN0ZXJDb250cm9sbGVyIGZyb20gXCIuLi9DaGFyYWN0ZXIvQ2hhcmFjdGVyQ29udHJvbGxlclwiO1xyXG5pbXBvcnQgeyBpbnN0YW5jZSB9IGZyb20gXCIuLi9Db21tb24vR2FtZVBsYXlJbnN0YW5jZVwiO1xyXG5pbXBvcnQgR2xvYmFsIGZyb20gXCIuLi9Db21tb24vR2xvYmFsXCI7XHJcbmltcG9ydCBLZXlFdmVudCBmcm9tIFwiLi4vQ29tbW9uL0tleUV2ZW50XCI7XHJcbmltcG9ydCBTaW5nbGV0b24gZnJvbSBcIi4uL0NvbW1vbi9TaW5nbGV0b25cIjtcclxuaW1wb3J0IEVuZW15Q29udHJvbGxlciBmcm9tIFwiLi4vRW5lbXkvRW5lbXlDb250cm9sbGVyXCI7XHJcbmltcG9ydCBTdGlja01hbjEgZnJvbSBcIi4vU3RpY2tNYW4xXCI7XHJcblxyXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcclxuXHJcbmRlY2xhcmUgY29uc3Qgd2luZG93OiBhbnk7XHJcblxyXG5AY2NjbGFzc1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBSb2Jsb3gyIGV4dGVuZHMgU2luZ2xldG9uPFN0aWNrTWFuMT4ge1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgam95U3RpY2tGb2xsb3c6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgTWFwOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuSW50ZWdlcilcclxuICAgIGNvdW50RW5lbXlFbmQ6IG51bWJlciA9IDA7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBNeUNoYXJhY3RlcjogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBHdWlkZTogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBFZmZlY3Q6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgZW5lbXlQYXJlbnQ6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgYnRuRG93bmxvYWQ6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgZW5kQ2FyZDogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBuaWdodEVuZDogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBjaXJjbGVFbmQ6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgYnRuQWxsOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIHR4dFNtYXNoZXI6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIGNvdW50RW5lbXk6IG51bWJlciA9IDA7XHJcbiAgICBib29sQ2hlY2tFbmQ6IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgICBib29sY2hlY2tJbnRlcmFjdGlvbjogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgaXJvbnNvdXJjZTogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgbWluZHdvcmtzOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICB2dW5nbGU6IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICBzdXBlcigpO1xyXG4gICAgICAgIFN0aWNrTWFuMS5faW5zdGFuY2UgPSB0aGlzO1xyXG4gICAgfVxyXG5cclxuICAgIG9uRW5hYmxlKCkge1xyXG4gICAgICAgIGluc3RhbmNlLm9uKEtleUV2ZW50LnNjYWxlLCB0aGlzLnNjYWxlLCB0aGlzKTtcclxuICAgICAgICBpbnN0YW5jZS5vbihLZXlFdmVudC5wbHVzRW5lbXksIHRoaXMucGx1c0VuZW15LCB0aGlzKTtcclxuICAgICAgICBpbnN0YW5jZS5vbihLZXlFdmVudC5hY3RpdmVHdWlkZSwgdGhpcy5hY3RpdmVHdWlkZSwgdGhpcyk7XHJcbiAgICB9XHJcblxyXG4gICAgb25EaXNhYmxlKCkge1xyXG4gICAgICAgIGluc3RhbmNlLm9mZihLZXlFdmVudC5zY2FsZSwgdGhpcy5zY2FsZSwgdGhpcyk7XHJcbiAgICAgICAgaW5zdGFuY2Uub2ZmKEtleUV2ZW50LnBsdXNFbmVteSwgdGhpcy5wbHVzRW5lbXksIHRoaXMpO1xyXG4gICAgICAgIGluc3RhbmNlLm9mZihLZXlFdmVudC5hY3RpdmVHdWlkZSwgdGhpcy5hY3RpdmVHdWlkZSwgdGhpcyk7XHJcbiAgICB9XHJcblxyXG4gICAgc3RhcnQoKSB7XHJcbiAgICAgICAgY2MuYXVkaW9FbmdpbmUucGxheShHbG9iYWwuc291bmRCRywgdHJ1ZSwgMSk7XHJcbiAgICAgICAgdGhpcy5wbGF5R2FtZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZSAoZHQpIHtcclxuICAgICAgICBpZiAoR2xvYmFsLmJvb2xFbmFibGVUb3VjaCAmJiAhdGhpcy5ib29sY2hlY2tJbnRlcmFjdGlvbikge1xyXG4gICAgICAgICAgICB0aGlzLnR4dFNtYXNoZXIuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmlyb25zb3VyY2UpIHtcclxuICAgICAgICAgICAgICAgIHdpbmRvdy5OVUMudHJpZ2dlci5pbnRlcmFjdGlvbigpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAodGhpcy5jb3VudEVuZW15ID09IHRoaXMuY291bnRFbmVteUVuZCAmJiAhdGhpcy5ib29sQ2hlY2tFbmQpIHtcclxuICAgICAgICAgICAgR2xvYmFsLmJvb2xTdGFydFBsYXkgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5ib29sQ2hlY2tFbmQgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLlN1cHBvcnRFbmQoKTtcclxuICAgICAgICAgICAgfSwgMSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHBsYXlHYW1lKCkge1xyXG4gICAgICAgIHRoaXMuTXlDaGFyYWN0ZXIuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICBHbG9iYWwuYm9vbFN0YXJ0UGxheSA9IHRydWU7XHJcbiAgICAgICAgZm9yKGxldCBpID0gMDsgaSA8IHRoaXMuZW5lbXlQYXJlbnQuY2hpbGRyZW5Db3VudDsgaSsrKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZW5lbXlQYXJlbnQuY2hpbGRyZW5baV0uZ2V0Q29tcG9uZW50KEVuZW15Q29udHJvbGxlcikuU3RhcnRNb3ZlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIFN1cHBvcnRFbmQoKSB7XHJcbiAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLm5pZ2h0RW5kLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgICAgIHRoaXMuc2NoZWR1bGVPbmNlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuY2lyY2xlRW5kLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5uaWdodEVuZC5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNpcmNsZUVuZC5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLk15Q2hhcmFjdGVyLm9wYWNpdHkgPSAwO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuRW5kR2FtZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgMC42KTtcclxuICAgICAgICAgICAgfSwgMC40KTtcclxuICAgICAgICAgICAgdGhpcy5jaXJjbGVFbmQuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB9LCAwLjIpO1xyXG4gICAgfVxyXG5cclxuICAgIEVuZEdhbWUoKSB7XHJcbiAgICAgICAgR2xvYmFsLmJvb2xlbmRHID0gdHJ1ZTtcclxuICAgICAgICBjYy5hdWRpb0VuZ2luZS5zdG9wQWxsRWZmZWN0cygpO1xyXG4gICAgICAgIHRoaXMuam95U3RpY2tGb2xsb3cuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5lbmRDYXJkLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5NYXAuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLmVuZENhcmQuY2hpbGRyZW5bMl0uYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLmVuZENhcmQuY2hpbGRyZW5bM10uYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLmVuZENhcmQuY2hpbGRyZW5bNF0uYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLk15Q2hhcmFjdGVyLm9wYWNpdHkgPSAyNTU7XHJcbiAgICAgICAgdGhpcy5NeUNoYXJhY3Rlci54ID0gMDtcclxuICAgICAgICB0aGlzLk15Q2hhcmFjdGVyLnkgPSAtNjA7XHJcbiAgICAgICAgdGhpcy5NeUNoYXJhY3Rlci5zY2FsZSA9IDE1O1xyXG4gICAgICAgIHRoaXMuTXlDaGFyYWN0ZXIuZXVsZXJBbmdsZXMgPSBuZXcgY2MuVmVjMyg5MCwgMCwgMTgwKTtcclxuICAgICAgICB0aGlzLk15Q2hhcmFjdGVyLmdldENvbXBvbmVudChDaGFyYWN0ZXJDb250cm9sbGVyKS5BcnJvd0RpcmVjdGlvbi5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLkd1aWRlLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLmVuZW15UGFyZW50LmNoaWxkcmVuQ291bnQ7IGkrKykge1xyXG4gICAgICAgICAgICB0aGlzLmVuZW15UGFyZW50LmNoaWxkcmVuW2ldLmdldENvbXBvbmVudChFbmVteUNvbnRyb2xsZXIpLmJvZHlEZWF0aC5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5lbmVteVBhcmVudC5jaGlsZHJlbltpXS5hY3RpdmUgPSBmYWxzZTs7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuYnRuQWxsLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5idG5Eb3dubG9hZC5nZXRDb21wb25lbnQoY2MuQnV0dG9uKS5pbnRlcmFjdGFibGUgPSBmYWxzZTtcclxuICAgICAgICBpZiAodGhpcy5taW5kd29ya3MpIHtcclxuICAgICAgICAgICAgd2luZG93LmdhbWVFbmQgJiYgd2luZG93LmdhbWVFbmQoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMuaXJvbnNvdXJjZSkge1xyXG4gICAgICAgICAgICB3aW5kb3cuTlVDLnRyaWdnZXIuZW5kR2FtZSgnd2luJylcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMudnVuZ2xlKSB7XHJcbiAgICAgICAgICAgIHBhcmVudC5wb3N0TWVzc2FnZSgnY29tcGxldGUnLCAnKicpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBzY2FsZSgpIHtcclxuICAgICAgICB0aGlzLkVmZmVjdC5zY2FsZSA9IDA7XHJcbiAgICAgICAgdGhpcy5FZmZlY3Qub3BhY2l0eSA9IDI1NTtcclxuICAgICAgICB0aGlzLkVmZmVjdC5ydW5BY3Rpb24oY2Muc2VxdWVuY2UoY2Muc2NhbGVUbygwLjIsIDEpLmVhc2luZyhjYy5lYXNlQm91bmNlT3V0KCkpLCBjYy5jYWxsRnVuYygoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuc2NoZWR1bGVPbmNlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuRWZmZWN0LnJ1bkFjdGlvbihjYy5mYWRlT3V0KDAuMSkpO1xyXG4gICAgICAgICAgICB9LCAwLjIpO1xyXG4gICAgICAgIH0pKSk7XHJcbiAgICB9XHJcblxyXG4gICAgcGx1c0VuZW15KCkge1xyXG4gICAgICAgIGlmICh0aGlzLmNvdW50RW5lbXkgPCB0aGlzLmNvdW50RW5lbXlFbmQpXHJcbiAgICAgICAgICAgIHRoaXMuY291bnRFbmVteSsrO1xyXG4gICAgfVxyXG5cclxuICAgIGFjdGl2ZUd1aWRlKCkge1xyXG4gICAgICAgIGlmKEdsb2JhbC5ib29sU3RhcnRQbGF5KVxyXG4gICAgICAgICAgICB0aGlzLkd1aWRlLmFjdGl2ZSA9IHRydWU7XHJcbiAgICB9XHJcbn1cclxuIl19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/GamePlay/StickMan1.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '743adJDcwFAjoO1TT8zhVhi', 'StickMan1');
// Scripts/GamePlay/StickMan1.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var CharacterController_1 = require("../Character/CharacterController");
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var KeyEvent_1 = require("../Common/KeyEvent");
var Singleton_1 = require("../Common/Singleton");
var EnemyController_1 = require("../Enemy/EnemyController");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var StickMan1 = /** @class */ (function (_super) {
    __extends(StickMan1, _super);
    function StickMan1() {
        var _this = _super.call(this) || this;
        _this.joyStickFollow = null;
        _this.Map = null;
        _this.countEnemyEnd = 0;
        _this.MyCharacter = null;
        _this.Guide = null;
        _this.Effect = null;
        _this.enemyParent = null;
        _this.btnDownload = null;
        _this.nStart = null;
        _this.endCard = null;
        _this.nightEnd = null;
        _this.circleEnd = null;
        _this.btnAll = null;
        _this.countEnemy = 0;
        _this.boolCheckEnd = false;
        _this.boolcheckInteraction = false;
        _this.ironsource = false;
        _this.mindworks = false;
        _this.vungle = false;
        StickMan1_1._instance = _this;
        return _this;
    }
    StickMan1_1 = StickMan1;
    StickMan1.prototype.onEnable = function () {
        GamePlayInstance_1.instance.on(KeyEvent_1.default.scale, this.scale, this);
        GamePlayInstance_1.instance.on(KeyEvent_1.default.plusEnemy, this.plusEnemy, this);
        GamePlayInstance_1.instance.on(KeyEvent_1.default.activeGuide, this.activeGuide, this);
    };
    StickMan1.prototype.onDisable = function () {
        GamePlayInstance_1.instance.off(KeyEvent_1.default.scale, this.scale, this);
        GamePlayInstance_1.instance.off(KeyEvent_1.default.plusEnemy, this.plusEnemy, this);
        GamePlayInstance_1.instance.off(KeyEvent_1.default.activeGuide, this.activeGuide, this);
    };
    StickMan1.prototype.start = function () {
        this.Guide.active = false;
        this.MyCharacter.active = false;
        this.enemyParent.active = false;
    };
    StickMan1.prototype.onClickBtnPlay = function () {
        if (this.ironsource) {
            window.NUC.trigger.interaction();
        }
        this.nStart.active = false;
        this.Guide.active = true;
        this.MyCharacter.active = true;
        this.enemyParent.active = true;
        this.playGame();
    };
    StickMan1.prototype.update = function (dt) {
        var _this = this;
        if (this.countEnemy == this.countEnemyEnd && !this.boolCheckEnd) {
            Global_1.default.boolStartPlay = false;
            this.boolCheckEnd = true;
            this.scheduleOnce(function () {
                _this.SupportEnd();
            }, 1);
        }
    };
    StickMan1.prototype.playGame = function () {
        Global_1.default.boolStartPlay = true;
        for (var i = 0; i < this.enemyParent.childrenCount; i++) {
            this.enemyParent.children[i].getComponent(EnemyController_1.default).StartMove();
        }
    };
    StickMan1.prototype.SupportEnd = function () {
        var _this = this;
        this.scheduleOnce(function () {
            _this.nightEnd.active = true;
            _this.scheduleOnce(function () {
                _this.circleEnd.active = true;
                _this.scheduleOnce(function () {
                    _this.nightEnd.active = false;
                    _this.circleEnd.active = false;
                    _this.MyCharacter.opacity = 0;
                    _this.EndGame();
                }, 0.6);
            }, 0.4);
            _this.circleEnd.active = true;
        }, 0.2);
    };
    StickMan1.prototype.EndGame = function () {
        Global_1.default.boolendG = true;
        this.joyStickFollow.active = false;
        this.endCard.active = true;
        this.Map.active = true;
        this.endCard.children[2].active = true;
        this.endCard.children[3].active = true;
        this.endCard.children[4].active = true;
        this.MyCharacter.opacity = 255;
        this.MyCharacter.x = -3;
        this.MyCharacter.y = -60;
        this.MyCharacter.scale = 350;
        this.MyCharacter.eulerAngles = new cc.Vec3(90, 0, 180);
        this.MyCharacter.getComponent(CharacterController_1.default).ArrowDirection.active = false;
        this.Guide.active = true;
        for (var i = 0; i < this.enemyParent.childrenCount; i++) {
            this.enemyParent.children[i].getComponent(EnemyController_1.default).bodyDeath.active = false;
            this.enemyParent.children[i].active = false;
            ;
        }
        this.btnAll.active = true;
        this.btnDownload.getComponent(cc.Button).interactable = false;
        if (this.mindworks) {
            window.gameEnd && window.gameEnd();
        }
        if (this.ironsource) {
            window.NUC.trigger.endGame('win');
        }
        if (this.vungle) {
            parent.postMessage('complete', '*');
        }
    };
    StickMan1.prototype.scale = function () {
        var _this = this;
        this.Effect.scale = 0;
        this.Effect.opacity = 255;
        this.Effect.runAction(cc.sequence(cc.scaleTo(0.2, 1).easing(cc.easeBounceOut()), cc.callFunc(function () {
            _this.scheduleOnce(function () {
                _this.Effect.runAction(cc.fadeOut(0.1));
            }, 0.2);
        })));
    };
    StickMan1.prototype.plusEnemy = function () {
        if (this.countEnemy < this.countEnemyEnd)
            this.countEnemy++;
    };
    StickMan1.prototype.activeGuide = function () {
        if (Global_1.default.boolStartPlay)
            this.Guide.active = true;
    };
    var StickMan1_1;
    __decorate([
        property(cc.Node)
    ], StickMan1.prototype, "joyStickFollow", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan1.prototype, "Map", void 0);
    __decorate([
        property(cc.Integer)
    ], StickMan1.prototype, "countEnemyEnd", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan1.prototype, "MyCharacter", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan1.prototype, "Guide", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan1.prototype, "Effect", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan1.prototype, "enemyParent", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan1.prototype, "btnDownload", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan1.prototype, "nStart", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan1.prototype, "endCard", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan1.prototype, "nightEnd", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan1.prototype, "circleEnd", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan1.prototype, "btnAll", void 0);
    StickMan1 = StickMan1_1 = __decorate([
        ccclass
    ], StickMan1);
    return StickMan1;
}(Singleton_1.default));
exports.default = StickMan1;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcR2FtZVBsYXlcXFN0aWNrTWFuMS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSx3RUFBbUU7QUFDbkUsK0RBQXNEO0FBQ3RELDJDQUFzQztBQUN0QywrQ0FBMEM7QUFDMUMsaURBQTRDO0FBQzVDLDREQUF1RDtBQUdqRCxJQUFBLGtCQUFtQyxFQUFsQyxvQkFBTyxFQUFFLHNCQUF5QixDQUFDO0FBSzFDO0lBQXVDLDZCQUFvQjtJQWlEdkQ7UUFBQSxZQUNJLGlCQUFPLFNBRVY7UUFqREQsb0JBQWMsR0FBWSxJQUFJLENBQUM7UUFHL0IsU0FBRyxHQUFZLElBQUksQ0FBQztRQUdwQixtQkFBYSxHQUFXLENBQUMsQ0FBQztRQUcxQixpQkFBVyxHQUFZLElBQUksQ0FBQztRQUc1QixXQUFLLEdBQVksSUFBSSxDQUFDO1FBR3RCLFlBQU0sR0FBWSxJQUFJLENBQUM7UUFHdkIsaUJBQVcsR0FBWSxJQUFJLENBQUM7UUFHNUIsaUJBQVcsR0FBWSxJQUFJLENBQUM7UUFHNUIsWUFBTSxHQUFZLElBQUksQ0FBQztRQUd2QixhQUFPLEdBQVksSUFBSSxDQUFDO1FBR3hCLGNBQVEsR0FBWSxJQUFJLENBQUM7UUFHekIsZUFBUyxHQUFZLElBQUksQ0FBQztRQUcxQixZQUFNLEdBQVksSUFBSSxDQUFDO1FBRXZCLGdCQUFVLEdBQVcsQ0FBQyxDQUFDO1FBQ3ZCLGtCQUFZLEdBQVksS0FBSyxDQUFDO1FBRTlCLDBCQUFvQixHQUFZLEtBQUssQ0FBQztRQUN0QyxnQkFBVSxHQUFZLEtBQUssQ0FBQztRQUM1QixlQUFTLEdBQVksS0FBSyxDQUFDO1FBQzNCLFlBQU0sR0FBWSxLQUFLLENBQUM7UUFJcEIsV0FBUyxDQUFDLFNBQVMsR0FBRyxLQUFJLENBQUM7O0lBQy9CLENBQUM7a0JBcERnQixTQUFTO0lBc0QxQiw0QkFBUSxHQUFSO1FBQ0ksMkJBQVEsQ0FBQyxFQUFFLENBQUMsa0JBQVEsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztRQUM5QywyQkFBUSxDQUFDLEVBQUUsQ0FBQyxrQkFBUSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3RELDJCQUFRLENBQUMsRUFBRSxDQUFDLGtCQUFRLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDOUQsQ0FBQztJQUVELDZCQUFTLEdBQVQ7UUFDSSwyQkFBUSxDQUFDLEdBQUcsQ0FBQyxrQkFBUSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQy9DLDJCQUFRLENBQUMsR0FBRyxDQUFDLGtCQUFRLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDdkQsMkJBQVEsQ0FBQyxHQUFHLENBQUMsa0JBQVEsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUMvRCxDQUFDO0lBRUQseUJBQUssR0FBTDtRQUNJLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUMxQixJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDaEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO0lBQ3BDLENBQUM7SUFFRCxrQ0FBYyxHQUFkO1FBQ0ksSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ2pCLE1BQU0sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQ3BDO1FBQ0QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQzNCLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUN6QixJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDL0IsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQy9CLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNwQixDQUFDO0lBRUQsMEJBQU0sR0FBTixVQUFRLEVBQUU7UUFBVixpQkFRQztRQVBHLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsYUFBYSxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRTtZQUM3RCxnQkFBTSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7WUFDN0IsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7WUFDekIsSUFBSSxDQUFDLFlBQVksQ0FBQztnQkFDZCxLQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7WUFDdEIsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ1Q7SUFDTCxDQUFDO0lBRUQsNEJBQVEsR0FBUjtRQUNJLGdCQUFNLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztRQUM1QixLQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDcEQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLHlCQUFlLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQztTQUMxRTtJQUNMLENBQUM7SUFFRCw4QkFBVSxHQUFWO1FBQUEsaUJBY0M7UUFiRyxJQUFJLENBQUMsWUFBWSxDQUFDO1lBQ2QsS0FBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1lBQzVCLEtBQUksQ0FBQyxZQUFZLENBQUM7Z0JBQ2QsS0FBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO2dCQUM3QixLQUFJLENBQUMsWUFBWSxDQUFDO29CQUNkLEtBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztvQkFDN0IsS0FBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO29CQUM5QixLQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUM7b0JBQzdCLEtBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztnQkFDbkIsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQ1osQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQ1IsS0FBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ2pDLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztJQUNaLENBQUM7SUFFRCwyQkFBTyxHQUFQO1FBQ0ksZ0JBQU0sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUNuQyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDM0IsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDdkMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUN2QyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQztRQUMvQixJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUN4QixJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQztRQUN6QixJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUM7UUFDN0IsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEdBQUcsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDdkQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsNkJBQW1CLENBQUMsQ0FBQyxjQUFjLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUNqRixJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDekIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3JELElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyx5QkFBZSxDQUFDLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFDcEYsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztZQUFBLENBQUM7U0FDaEQ7UUFDRCxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDMUIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7UUFDOUQsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2hCLE1BQU0sQ0FBQyxPQUFPLElBQUksTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO1NBQ3RDO1FBQ0QsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ2pCLE1BQU0sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQTtTQUNwQztRQUNELElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNiLE1BQU0sQ0FBQyxXQUFXLENBQUMsVUFBVSxFQUFFLEdBQUcsQ0FBQyxDQUFDO1NBQ3ZDO0lBQ0wsQ0FBQztJQUVELHlCQUFLLEdBQUw7UUFBQSxpQkFRQztRQVBHLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztRQUN0QixJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUM7UUFDMUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLGFBQWEsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLFFBQVEsQ0FBQztZQUN6RixLQUFJLENBQUMsWUFBWSxDQUFDO2dCQUNkLEtBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUMzQyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDWixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDVCxDQUFDO0lBRUQsNkJBQVMsR0FBVDtRQUNJLElBQUksSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsYUFBYTtZQUNwQyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDMUIsQ0FBQztJQUVELCtCQUFXLEdBQVg7UUFDSSxJQUFHLGdCQUFNLENBQUMsYUFBYTtZQUNuQixJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7SUFDakMsQ0FBQzs7SUFuS0Q7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztxREFDYTtJQUcvQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOzBDQUNFO0lBR3BCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUM7b0RBQ0s7SUFHMUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztrREFDVTtJQUc1QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOzRDQUNJO0lBR3RCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7NkNBQ0s7SUFHdkI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztrREFDVTtJQUc1QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO2tEQUNVO0lBRzVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7NkNBQ0s7SUFHdkI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzs4Q0FDTTtJQUd4QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOytDQUNPO0lBR3pCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7Z0RBQ1E7SUFHMUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzs2Q0FDSztJQXZDTixTQUFTO1FBRDdCLE9BQU87T0FDYSxTQUFTLENBdUs3QjtJQUFELGdCQUFDO0NBdktELEFBdUtDLENBdktzQyxtQkFBUyxHQXVLL0M7a0JBdktvQixTQUFTIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IENoYXJhY3RlckNvbnRyb2xsZXIgZnJvbSBcIi4uL0NoYXJhY3Rlci9DaGFyYWN0ZXJDb250cm9sbGVyXCI7XHJcbmltcG9ydCB7IGluc3RhbmNlIH0gZnJvbSBcIi4uL0NvbW1vbi9HYW1lUGxheUluc3RhbmNlXCI7XHJcbmltcG9ydCBHbG9iYWwgZnJvbSBcIi4uL0NvbW1vbi9HbG9iYWxcIjtcclxuaW1wb3J0IEtleUV2ZW50IGZyb20gXCIuLi9Db21tb24vS2V5RXZlbnRcIjtcclxuaW1wb3J0IFNpbmdsZXRvbiBmcm9tIFwiLi4vQ29tbW9uL1NpbmdsZXRvblwiO1xyXG5pbXBvcnQgRW5lbXlDb250cm9sbGVyIGZyb20gXCIuLi9FbmVteS9FbmVteUNvbnRyb2xsZXJcIjtcclxuXHJcblxyXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcclxuXHJcbmRlY2xhcmUgY29uc3Qgd2luZG93OiBhbnk7XHJcblxyXG5AY2NjbGFzc1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTdGlja01hbjEgZXh0ZW5kcyBTaW5nbGV0b248U3RpY2tNYW4xPiB7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBqb3lTdGlja0ZvbGxvdzogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBNYXA6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5JbnRlZ2VyKVxyXG4gICAgY291bnRFbmVteUVuZDogbnVtYmVyID0gMDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIE15Q2hhcmFjdGVyOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIEd1aWRlOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIEVmZmVjdDogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBlbmVteVBhcmVudDogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBidG5Eb3dubG9hZDogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBuU3RhcnQ6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgZW5kQ2FyZDogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBuaWdodEVuZDogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBjaXJjbGVFbmQ6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgYnRuQWxsOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBjb3VudEVuZW15OiBudW1iZXIgPSAwO1xyXG4gICAgYm9vbENoZWNrRW5kOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgYm9vbGNoZWNrSW50ZXJhY3Rpb246IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIGlyb25zb3VyY2U6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIG1pbmR3b3JrczogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgdnVuZ2xlOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuICAgICAgICBTdGlja01hbjEuX2luc3RhbmNlID0gdGhpcztcclxuICAgIH1cclxuXHJcbiAgICBvbkVuYWJsZSgpIHtcclxuICAgICAgICBpbnN0YW5jZS5vbihLZXlFdmVudC5zY2FsZSwgdGhpcy5zY2FsZSwgdGhpcyk7XHJcbiAgICAgICAgaW5zdGFuY2Uub24oS2V5RXZlbnQucGx1c0VuZW15LCB0aGlzLnBsdXNFbmVteSwgdGhpcyk7XHJcbiAgICAgICAgaW5zdGFuY2Uub24oS2V5RXZlbnQuYWN0aXZlR3VpZGUsIHRoaXMuYWN0aXZlR3VpZGUsIHRoaXMpO1xyXG4gICAgfVxyXG5cclxuICAgIG9uRGlzYWJsZSgpIHtcclxuICAgICAgICBpbnN0YW5jZS5vZmYoS2V5RXZlbnQuc2NhbGUsIHRoaXMuc2NhbGUsIHRoaXMpO1xyXG4gICAgICAgIGluc3RhbmNlLm9mZihLZXlFdmVudC5wbHVzRW5lbXksIHRoaXMucGx1c0VuZW15LCB0aGlzKTtcclxuICAgICAgICBpbnN0YW5jZS5vZmYoS2V5RXZlbnQuYWN0aXZlR3VpZGUsIHRoaXMuYWN0aXZlR3VpZGUsIHRoaXMpO1xyXG4gICAgfVxyXG5cclxuICAgIHN0YXJ0KCkge1xyXG4gICAgICAgIHRoaXMuR3VpZGUuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5NeUNoYXJhY3Rlci5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLmVuZW15UGFyZW50LmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIG9uQ2xpY2tCdG5QbGF5KCkge1xyXG4gICAgICAgIGlmICh0aGlzLmlyb25zb3VyY2UpIHtcclxuICAgICAgICAgICAgd2luZG93Lk5VQy50cmlnZ2VyLmludGVyYWN0aW9uKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMublN0YXJ0LmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuR3VpZGUuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLk15Q2hhcmFjdGVyLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5lbmVteVBhcmVudC5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMucGxheUdhbWUoKTtcclxuICAgIH1cclxuXHJcbiAgICB1cGRhdGUgKGR0KSB7XHJcbiAgICAgICAgaWYgKHRoaXMuY291bnRFbmVteSA9PSB0aGlzLmNvdW50RW5lbXlFbmQgJiYgIXRoaXMuYm9vbENoZWNrRW5kKSB7XHJcbiAgICAgICAgICAgIEdsb2JhbC5ib29sU3RhcnRQbGF5ID0gZmFsc2U7XHJcbiAgICAgICAgICAgIHRoaXMuYm9vbENoZWNrRW5kID0gdHJ1ZTtcclxuICAgICAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5TdXBwb3J0RW5kKCk7XHJcbiAgICAgICAgICAgIH0sIDEpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwbGF5R2FtZSgpIHtcclxuICAgICAgICBHbG9iYWwuYm9vbFN0YXJ0UGxheSA9IHRydWU7XHJcbiAgICAgICAgZm9yKGxldCBpID0gMDsgaSA8IHRoaXMuZW5lbXlQYXJlbnQuY2hpbGRyZW5Db3VudDsgaSsrKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZW5lbXlQYXJlbnQuY2hpbGRyZW5baV0uZ2V0Q29tcG9uZW50KEVuZW15Q29udHJvbGxlcikuU3RhcnRNb3ZlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIFN1cHBvcnRFbmQoKSB7XHJcbiAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLm5pZ2h0RW5kLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgICAgIHRoaXMuc2NoZWR1bGVPbmNlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuY2lyY2xlRW5kLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5uaWdodEVuZC5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNpcmNsZUVuZC5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLk15Q2hhcmFjdGVyLm9wYWNpdHkgPSAwO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuRW5kR2FtZSgpO1xyXG4gICAgICAgICAgICAgICAgfSwgMC42KTtcclxuICAgICAgICAgICAgfSwgMC40KTtcclxuICAgICAgICAgICAgdGhpcy5jaXJjbGVFbmQuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB9LCAwLjIpO1xyXG4gICAgfVxyXG5cclxuICAgIEVuZEdhbWUoKSB7XHJcbiAgICAgICAgR2xvYmFsLmJvb2xlbmRHID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLmpveVN0aWNrRm9sbG93LmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuZW5kQ2FyZC5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuTWFwLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5lbmRDYXJkLmNoaWxkcmVuWzJdLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5lbmRDYXJkLmNoaWxkcmVuWzNdLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5lbmRDYXJkLmNoaWxkcmVuWzRdLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5NeUNoYXJhY3Rlci5vcGFjaXR5ID0gMjU1O1xyXG4gICAgICAgIHRoaXMuTXlDaGFyYWN0ZXIueCA9IC0zO1xyXG4gICAgICAgIHRoaXMuTXlDaGFyYWN0ZXIueSA9IC02MDtcclxuICAgICAgICB0aGlzLk15Q2hhcmFjdGVyLnNjYWxlID0gMzUwO1xyXG4gICAgICAgIHRoaXMuTXlDaGFyYWN0ZXIuZXVsZXJBbmdsZXMgPSBuZXcgY2MuVmVjMyg5MCwgMCwgMTgwKTtcclxuICAgICAgICB0aGlzLk15Q2hhcmFjdGVyLmdldENvbXBvbmVudChDaGFyYWN0ZXJDb250cm9sbGVyKS5BcnJvd0RpcmVjdGlvbi5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLkd1aWRlLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLmVuZW15UGFyZW50LmNoaWxkcmVuQ291bnQ7IGkrKykge1xyXG4gICAgICAgICAgICB0aGlzLmVuZW15UGFyZW50LmNoaWxkcmVuW2ldLmdldENvbXBvbmVudChFbmVteUNvbnRyb2xsZXIpLmJvZHlEZWF0aC5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5lbmVteVBhcmVudC5jaGlsZHJlbltpXS5hY3RpdmUgPSBmYWxzZTs7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuYnRuQWxsLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5idG5Eb3dubG9hZC5nZXRDb21wb25lbnQoY2MuQnV0dG9uKS5pbnRlcmFjdGFibGUgPSBmYWxzZTtcclxuICAgICAgICBpZiAodGhpcy5taW5kd29ya3MpIHtcclxuICAgICAgICAgICAgd2luZG93LmdhbWVFbmQgJiYgd2luZG93LmdhbWVFbmQoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMuaXJvbnNvdXJjZSkge1xyXG4gICAgICAgICAgICB3aW5kb3cuTlVDLnRyaWdnZXIuZW5kR2FtZSgnd2luJylcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMudnVuZ2xlKSB7XHJcbiAgICAgICAgICAgIHBhcmVudC5wb3N0TWVzc2FnZSgnY29tcGxldGUnLCAnKicpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBzY2FsZSgpIHtcclxuICAgICAgICB0aGlzLkVmZmVjdC5zY2FsZSA9IDA7XHJcbiAgICAgICAgdGhpcy5FZmZlY3Qub3BhY2l0eSA9IDI1NTtcclxuICAgICAgICB0aGlzLkVmZmVjdC5ydW5BY3Rpb24oY2Muc2VxdWVuY2UoY2Muc2NhbGVUbygwLjIsIDEpLmVhc2luZyhjYy5lYXNlQm91bmNlT3V0KCkpLCBjYy5jYWxsRnVuYygoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuc2NoZWR1bGVPbmNlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuRWZmZWN0LnJ1bkFjdGlvbihjYy5mYWRlT3V0KDAuMSkpO1xyXG4gICAgICAgICAgICB9LCAwLjIpO1xyXG4gICAgICAgIH0pKSk7XHJcbiAgICB9XHJcblxyXG4gICAgcGx1c0VuZW15KCkge1xyXG4gICAgICAgIGlmICh0aGlzLmNvdW50RW5lbXkgPCB0aGlzLmNvdW50RW5lbXlFbmQpXHJcbiAgICAgICAgICAgIHRoaXMuY291bnRFbmVteSsrO1xyXG4gICAgfVxyXG5cclxuICAgIGFjdGl2ZUd1aWRlKCkge1xyXG4gICAgICAgIGlmKEdsb2JhbC5ib29sU3RhcnRQbGF5KVxyXG4gICAgICAgICAgICB0aGlzLkd1aWRlLmFjdGl2ZSA9IHRydWU7XHJcbiAgICB9XHJcbn1cclxuIl19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/EnumDefine.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'cf993Azr2BF7p40pHHz6qKS', 'EnumDefine');
// Scripts/Common/EnumDefine.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ActionType;
(function (ActionType) {
    ActionType[ActionType["IDLE"] = 0] = "IDLE";
    ActionType[ActionType["RUN"] = 1] = "RUN";
    ActionType[ActionType["ATTACK"] = 2] = "ATTACK";
    ActionType[ActionType["VICTORY"] = 3] = "VICTORY";
    ActionType[ActionType["SCARE"] = 4] = "SCARE";
    ActionType[ActionType["NORMAL"] = 5] = "NORMAL";
    ActionType[ActionType["FAST"] = 6] = "FAST";
})(ActionType = exports.ActionType || (exports.ActionType = {}));

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxFbnVtRGVmaW5lLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsSUFBWSxVQVNYO0FBVEQsV0FBWSxVQUFVO0lBRWxCLDJDQUFJLENBQUE7SUFDSix5Q0FBRyxDQUFBO0lBQ0gsK0NBQU0sQ0FBQTtJQUNOLGlEQUFPLENBQUE7SUFDUCw2Q0FBSyxDQUFBO0lBQ0wsK0NBQU0sQ0FBQTtJQUNOLDJDQUFJLENBQUE7QUFDUixDQUFDLEVBVFcsVUFBVSxHQUFWLGtCQUFVLEtBQVYsa0JBQVUsUUFTckIiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZW51bSBBY3Rpb25UeXBlXHJcbntcclxuICAgIElETEUsXHJcbiAgICBSVU4sXHJcbiAgICBBVFRBQ0ssXHJcbiAgICBWSUNUT1JZLFxyXG4gICAgU0NBUkUsXHJcbiAgICBOT1JNQUwsXHJcbiAgICBGQVNULFxyXG59Il19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Joystick/JoystickFollow.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '73034Tph19N/64CSzvxwsiQ', 'JoystickFollow');
// Scripts/Joystick/JoystickFollow.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var CharacterController_1 = require("../Character/CharacterController");
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var JoystickFollow = /** @class */ (function (_super) {
    __extends(JoystickFollow, _super);
    function JoystickFollow() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.joyRing = null;
        _this.joyDot = null;
        _this.stickPos = null;
        _this.touchLocation = null;
        _this.radius = 0;
        return _this;
        // update (dt) {}
    }
    JoystickFollow.prototype.onLoad = function () {
        this.radius = this.joyRing.width / 2;
    };
    JoystickFollow.prototype.start = function () {
        this.gameplayInstance = GamePlayInstance_1.default.Instance(GamePlayInstance_1.default);
        this.node.on(cc.Node.EventType.TOUCH_START, this.touchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.touchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.touchCancel, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.touchCancel, this);
    };
    JoystickFollow.prototype.touchStart = function (event) {
        if (!Global_1.default.boolendG && Global_1.default.boolStartPlay && !Global_1.default.boolStartAttacking) {
            var mousePos = event.getLocation();
            var localMousePos = this.node.convertToNodeSpaceAR(mousePos);
            this.node.opacity = 255;
            this.stickPos = localMousePos;
            this.touchLocation = event.getLocation();
            this.joyRing.setPosition(localMousePos);
            this.joyDot.setPosition(localMousePos);
            this.gameplayInstance.gameplay.Guide.active = false;
            // this.gameplayInstance.gameplay.txtSmasher.active = false;
            this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController_1.default).ArrowDirection.active = true;
        }
    };
    JoystickFollow.prototype.touchMove = function (event) {
        if (!Global_1.default.boolendG && Global_1.default.boolStartPlay && !Global_1.default.boolStartAttacking) {
            this.node.opacity = 255;
            Global_1.default.boolEnableTouch = true;
            if (!Global_1.default.boolFirstTouchJoyStick) {
                Global_1.default.boolFirstTouchJoyStick = true;
                if (this.gameplayInstance.gameplay.MyCharacter.name == 'MyDeadpool') {
                    this.gameplayInstance.gameplay.MyCharacter.getComponent(cc.SkeletonAnimation).play("Armature|sword_run");
                }
                else {
                    this.gameplayInstance.gameplay.MyCharacter.getComponent(cc.SkeletonAnimation).play("Armature|hammer_run");
                }
            }
            if (this.touchLocation === event.getLocation()) {
                return false;
            }
            this.gameplayInstance.gameplay.Guide.active = false;
            var touchPos = this.joyRing.convertToNodeSpaceAR(event.getLocation());
            var distance = touchPos.mag();
            var posX = this.stickPos.x + touchPos.x;
            var posY = this.stickPos.y + touchPos.y;
            var p = cc.v2(posX, posY).sub(this.joyRing.getPosition()).normalize();
            Global_1.default.touchPos = p;
            if (this.radius > distance) {
                this.joyDot.setPosition(cc.v2(posX, posY));
            }
            else {
                var x = this.stickPos.x + p.x * this.radius;
                var y = this.stickPos.y + p.y * this.radius;
                this.joyDot.setPosition(cc.v2(x, y));
            }
        }
        this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController_1.default).ArrowDirection.active = true;
    };
    JoystickFollow.prototype.touchCancel = function (event) {
        if (!Global_1.default.boolendG && Global_1.default.boolStartPlay && !Global_1.default.boolStartAttacking) {
            Global_1.default.boolEnableTouch = false;
            this.joyDot.setPosition(this.joyRing.getPosition());
            this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController_1.default).Attacking();
            this.node.opacity = 0;
            Global_1.default.boolFirstTouchJoyStick = false;
        }
    };
    __decorate([
        property(cc.Node)
    ], JoystickFollow.prototype, "joyRing", void 0);
    __decorate([
        property(cc.Node)
    ], JoystickFollow.prototype, "joyDot", void 0);
    JoystickFollow = __decorate([
        ccclass
    ], JoystickFollow);
    return JoystickFollow;
}(cc.Component));
exports.default = JoystickFollow;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcSm95c3RpY2tcXEpveXN0aWNrRm9sbG93LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLHdFQUFtRTtBQUNuRSwrREFBMEQ7QUFDMUQsMkNBQXNDO0FBRWhDLElBQUEsa0JBQW1DLEVBQWxDLG9CQUFPLEVBQUUsc0JBQXlCLENBQUM7QUFHMUM7SUFBNEMsa0NBQVk7SUFEeEQ7UUFBQSxxRUFzRkM7UUFsRkcsYUFBTyxHQUFZLElBQUksQ0FBQztRQUd4QixZQUFNLEdBQVksSUFBSSxDQUFDO1FBRXZCLGNBQVEsR0FBWSxJQUFJLENBQUM7UUFDekIsbUJBQWEsR0FBWSxJQUFJLENBQUM7UUFDOUIsWUFBTSxHQUFXLENBQUMsQ0FBQzs7UUEwRW5CLGlCQUFpQjtJQUNyQixDQUFDO0lBeEVHLCtCQUFNLEdBQU47UUFDSSxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztJQUN6QyxDQUFDO0lBRUQsOEJBQUssR0FBTDtRQUNJLElBQUksQ0FBQyxnQkFBZ0IsR0FBRywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQztRQUNwRSxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNuRSxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNqRSxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNsRSxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUN6RSxDQUFDO0lBRUQsbUNBQVUsR0FBVixVQUFXLEtBQUs7UUFDWixJQUFHLENBQUMsZ0JBQU0sQ0FBQyxRQUFRLElBQUksZ0JBQU0sQ0FBQyxhQUFhLElBQUksQ0FBQyxnQkFBTSxDQUFDLGtCQUFrQixFQUFFO1lBQ3ZFLElBQUksUUFBUSxHQUFHLEtBQUssQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUNuQyxJQUFJLGFBQWEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzdELElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQztZQUN4QixJQUFJLENBQUMsUUFBUSxHQUFHLGFBQWEsQ0FBQztZQUM5QixJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUN6QyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUN4QyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUN2QyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1lBQ3BELDREQUE0RDtZQUM1RCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsNkJBQW1CLENBQUMsQ0FBQyxjQUFjLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztTQUM3RztJQUNMLENBQUM7SUFFRCxrQ0FBUyxHQUFULFVBQVUsS0FBSztRQUNYLElBQUcsQ0FBQyxnQkFBTSxDQUFDLFFBQVEsSUFBSSxnQkFBTSxDQUFDLGFBQWEsSUFBSSxDQUFDLGdCQUFNLENBQUMsa0JBQWtCLEVBQUU7WUFDdkUsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDO1lBQ3hCLGdCQUFNLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztZQUM5QixJQUFHLENBQUMsZ0JBQU0sQ0FBQyxzQkFBc0IsRUFBRTtnQkFDL0IsZ0JBQU0sQ0FBQyxzQkFBc0IsR0FBRyxJQUFJLENBQUM7Z0JBQ3JDLElBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxJQUFJLFlBQVksRUFBRTtvQkFDaEUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO2lCQUM1RztxQkFBTTtvQkFDSCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUM7aUJBQzdHO2FBQ0o7WUFDRCxJQUFHLElBQUksQ0FBQyxhQUFhLEtBQUssS0FBSyxDQUFDLFdBQVcsRUFBRSxFQUFDO2dCQUMxQyxPQUFPLEtBQUssQ0FBQzthQUNoQjtZQUVELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFDcEQsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztZQUN0RSxJQUFJLFFBQVEsR0FBRyxRQUFRLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDOUIsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUN4QyxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQ3hDLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUMsU0FBUyxFQUFFLENBQUM7WUFDdEUsZ0JBQU0sQ0FBQyxRQUFRLEdBQUcsQ0FBQyxDQUFDO1lBQ3BCLElBQUcsSUFBSSxDQUFDLE1BQU0sR0FBRyxRQUFRLEVBQUU7Z0JBQ3ZCLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUM7YUFDOUM7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO2dCQUM1QyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7Z0JBQzVDLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDeEM7U0FDSjtRQUNELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyw2QkFBbUIsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO0lBQzlHLENBQUM7SUFFRCxvQ0FBVyxHQUFYLFVBQVksS0FBSztRQUNiLElBQUksQ0FBQyxnQkFBTSxDQUFDLFFBQVEsSUFBSSxnQkFBTSxDQUFDLGFBQWEsSUFBSSxDQUFDLGdCQUFNLENBQUMsa0JBQWtCLEVBQUU7WUFDeEUsZ0JBQU0sQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDO1lBQy9CLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztZQUNwRCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsNkJBQW1CLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQztZQUN6RixJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUM7WUFDdEIsZ0JBQU0sQ0FBQyxzQkFBc0IsR0FBRyxLQUFLLENBQUM7U0FDekM7SUFDTCxDQUFDO0lBL0VEO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7bURBQ007SUFHeEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztrREFDSztJQU5OLGNBQWM7UUFEbEMsT0FBTztPQUNhLGNBQWMsQ0FxRmxDO0lBQUQscUJBQUM7Q0FyRkQsQUFxRkMsQ0FyRjJDLEVBQUUsQ0FBQyxTQUFTLEdBcUZ2RDtrQkFyRm9CLGNBQWMiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgQ2hhcmFjdGVyQ29udHJvbGxlciBmcm9tIFwiLi4vQ2hhcmFjdGVyL0NoYXJhY3RlckNvbnRyb2xsZXJcIjtcclxuaW1wb3J0IEdhbWVQbGF5SW5zdGFuY2UgZnJvbSBcIi4uL0NvbW1vbi9HYW1lUGxheUluc3RhbmNlXCI7XHJcbmltcG9ydCBHbG9iYWwgZnJvbSBcIi4uL0NvbW1vbi9HbG9iYWxcIjtcclxuXHJcbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eX0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgSm95c3RpY2tGb2xsb3cgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgam95UmluZzogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBqb3lEb3Q6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIHN0aWNrUG9zOiBjYy5WZWMyID0gbnVsbDtcclxuICAgIHRvdWNoTG9jYXRpb246IGNjLlZlYzIgPSBudWxsO1xyXG4gICAgcmFkaXVzOiBudW1iZXIgPSAwO1xyXG4gICAgZ2FtZXBsYXlJbnN0YW5jZTogR2FtZVBsYXlJbnN0YW5jZTtcclxuXHJcbiAgICBvbkxvYWQgKCkge1xyXG4gICAgICAgIHRoaXMucmFkaXVzID0gdGhpcy5qb3lSaW5nLndpZHRoIC8gMjtcclxuICAgIH1cclxuXHJcbiAgICBzdGFydCAoKSB7XHJcbiAgICAgICAgdGhpcy5nYW1lcGxheUluc3RhbmNlID0gR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKTtcclxuICAgICAgICB0aGlzLm5vZGUub24oY2MuTm9kZS5FdmVudFR5cGUuVE9VQ0hfU1RBUlQsIHRoaXMudG91Y2hTdGFydCwgdGhpcyk7XHJcbiAgICAgICAgdGhpcy5ub2RlLm9uKGNjLk5vZGUuRXZlbnRUeXBlLlRPVUNIX01PVkUsIHRoaXMudG91Y2hNb3ZlLCB0aGlzKTtcclxuICAgICAgICB0aGlzLm5vZGUub24oY2MuTm9kZS5FdmVudFR5cGUuVE9VQ0hfRU5ELCB0aGlzLnRvdWNoQ2FuY2VsLCB0aGlzKTtcclxuICAgICAgICB0aGlzLm5vZGUub24oY2MuTm9kZS5FdmVudFR5cGUuVE9VQ0hfQ0FOQ0VMLCB0aGlzLnRvdWNoQ2FuY2VsLCB0aGlzKTtcclxuICAgIH1cclxuXHJcbiAgICB0b3VjaFN0YXJ0KGV2ZW50KSB7XHJcbiAgICAgICAgaWYoIUdsb2JhbC5ib29sZW5kRyAmJiBHbG9iYWwuYm9vbFN0YXJ0UGxheSAmJiAhR2xvYmFsLmJvb2xTdGFydEF0dGFja2luZykge1xyXG4gICAgICAgICAgICB2YXIgbW91c2VQb3MgPSBldmVudC5nZXRMb2NhdGlvbigpO1xyXG4gICAgICAgICAgICBsZXQgbG9jYWxNb3VzZVBvcyA9IHRoaXMubm9kZS5jb252ZXJ0VG9Ob2RlU3BhY2VBUihtb3VzZVBvcyk7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS5vcGFjaXR5ID0gMjU1O1xyXG4gICAgICAgICAgICB0aGlzLnN0aWNrUG9zID0gbG9jYWxNb3VzZVBvcztcclxuICAgICAgICAgICAgdGhpcy50b3VjaExvY2F0aW9uID0gZXZlbnQuZ2V0TG9jYXRpb24oKTtcclxuICAgICAgICAgICAgdGhpcy5qb3lSaW5nLnNldFBvc2l0aW9uKGxvY2FsTW91c2VQb3MpO1xyXG4gICAgICAgICAgICB0aGlzLmpveURvdC5zZXRQb3NpdGlvbihsb2NhbE1vdXNlUG9zKTtcclxuICAgICAgICAgICAgdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lkd1aWRlLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICAvLyB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkudHh0U21hc2hlci5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5nYW1lcGxheUluc3RhbmNlLmdhbWVwbGF5Lk15Q2hhcmFjdGVyLmdldENvbXBvbmVudChDaGFyYWN0ZXJDb250cm9sbGVyKS5BcnJvd0RpcmVjdGlvbi5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICB0b3VjaE1vdmUoZXZlbnQpIHtcclxuICAgICAgICBpZighR2xvYmFsLmJvb2xlbmRHICYmIEdsb2JhbC5ib29sU3RhcnRQbGF5ICYmICFHbG9iYWwuYm9vbFN0YXJ0QXR0YWNraW5nKSB7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS5vcGFjaXR5ID0gMjU1O1xyXG4gICAgICAgICAgICBHbG9iYWwuYm9vbEVuYWJsZVRvdWNoID0gdHJ1ZTtcclxuICAgICAgICAgICAgaWYoIUdsb2JhbC5ib29sRmlyc3RUb3VjaEpveVN0aWNrKSB7XHJcbiAgICAgICAgICAgICAgICBHbG9iYWwuYm9vbEZpcnN0VG91Y2hKb3lTdGljayA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICBpZih0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIubmFtZSA9PSAnTXlEZWFkcG9vbCcpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIuZ2V0Q29tcG9uZW50KGNjLlNrZWxldG9uQW5pbWF0aW9uKS5wbGF5KFwiQXJtYXR1cmV8c3dvcmRfcnVuXCIpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIuZ2V0Q29tcG9uZW50KGNjLlNrZWxldG9uQW5pbWF0aW9uKS5wbGF5KFwiQXJtYXR1cmV8aGFtbWVyX3J1blwiKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZih0aGlzLnRvdWNoTG9jYXRpb24gPT09IGV2ZW50LmdldExvY2F0aW9uKCkpe1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuR3VpZGUuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgICAgIGxldCB0b3VjaFBvcyA9IHRoaXMuam95UmluZy5jb252ZXJ0VG9Ob2RlU3BhY2VBUihldmVudC5nZXRMb2NhdGlvbigpKTtcclxuICAgICAgICAgICAgbGV0IGRpc3RhbmNlID0gdG91Y2hQb3MubWFnKCk7XHJcbiAgICAgICAgICAgIGxldCBwb3NYID0gdGhpcy5zdGlja1Bvcy54ICsgdG91Y2hQb3MueDtcclxuICAgICAgICAgICAgbGV0IHBvc1kgPSB0aGlzLnN0aWNrUG9zLnkgKyB0b3VjaFBvcy55O1xyXG4gICAgICAgICAgICBsZXQgcCA9IGNjLnYyKHBvc1gsIHBvc1kpLnN1Yih0aGlzLmpveVJpbmcuZ2V0UG9zaXRpb24oKSkubm9ybWFsaXplKCk7XHJcbiAgICAgICAgICAgIEdsb2JhbC50b3VjaFBvcyA9IHA7XHJcbiAgICAgICAgICAgIGlmKHRoaXMucmFkaXVzID4gZGlzdGFuY2UpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuam95RG90LnNldFBvc2l0aW9uKGNjLnYyKHBvc1gsIHBvc1kpKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGxldCB4ID0gdGhpcy5zdGlja1Bvcy54ICsgcC54ICogdGhpcy5yYWRpdXM7XHJcbiAgICAgICAgICAgICAgICBsZXQgeSA9IHRoaXMuc3RpY2tQb3MueSArIHAueSAqIHRoaXMucmFkaXVzO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5qb3lEb3Quc2V0UG9zaXRpb24oY2MudjIoeCwgeSkpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuZ2FtZXBsYXlJbnN0YW5jZS5nYW1lcGxheS5NeUNoYXJhY3Rlci5nZXRDb21wb25lbnQoQ2hhcmFjdGVyQ29udHJvbGxlcikuQXJyb3dEaXJlY3Rpb24uYWN0aXZlID0gdHJ1ZTtcclxuICAgIH1cclxuXHJcbiAgICB0b3VjaENhbmNlbChldmVudCkge1xyXG4gICAgICAgIGlmICghR2xvYmFsLmJvb2xlbmRHICYmIEdsb2JhbC5ib29sU3RhcnRQbGF5ICYmICFHbG9iYWwuYm9vbFN0YXJ0QXR0YWNraW5nKSB7XHJcbiAgICAgICAgICAgIEdsb2JhbC5ib29sRW5hYmxlVG91Y2ggPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5qb3lEb3Quc2V0UG9zaXRpb24odGhpcy5qb3lSaW5nLmdldFBvc2l0aW9uKCkpO1xyXG4gICAgICAgICAgICB0aGlzLmdhbWVwbGF5SW5zdGFuY2UuZ2FtZXBsYXkuTXlDaGFyYWN0ZXIuZ2V0Q29tcG9uZW50KENoYXJhY3RlckNvbnRyb2xsZXIpLkF0dGFja2luZygpO1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUub3BhY2l0eSA9IDA7XHJcbiAgICAgICAgICAgIEdsb2JhbC5ib29sRmlyc3RUb3VjaEpveVN0aWNrID0gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8vIHVwZGF0ZSAoZHQpIHt9XHJcbn1cclxuIl19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/VFX/DestroyVFX.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'd0b6c4IwSBMe46SP3WrqOM5', 'DestroyVFX');
// Scripts/VFX/DestroyVFX.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var DestroyVFX = /** @class */ (function (_super) {
    __extends(DestroyVFX, _super);
    function DestroyVFX() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.timming = 0;
        return _this;
    }
    DestroyVFX.prototype.start = function () {
        var _this = this;
        this.scheduleOnce(function () {
            _this.node.destroy();
        }, this.timming);
    };
    __decorate([
        property(cc.Integer)
    ], DestroyVFX.prototype, "timming", void 0);
    DestroyVFX = __decorate([
        ccclass
    ], DestroyVFX);
    return DestroyVFX;
}(cc.Component));
exports.default = DestroyVFX;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcVkZYXFxEZXN0cm95VkZYLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNNLElBQUEsa0JBQW1DLEVBQWxDLG9CQUFPLEVBQUUsc0JBQXlCLENBQUM7QUFHMUM7SUFBd0MsOEJBQVk7SUFEcEQ7UUFBQSxxRUFVQztRQU5HLGFBQU8sR0FBVyxDQUFDLENBQUM7O0lBTXhCLENBQUM7SUFMRywwQkFBSyxHQUFMO1FBQUEsaUJBSUM7UUFIRyxJQUFJLENBQUMsWUFBWSxDQUFDO1lBQ2QsS0FBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUN4QixDQUFDLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ3JCLENBQUM7SUFMRDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDOytDQUNEO0lBSEgsVUFBVTtRQUQ5QixPQUFPO09BQ2EsVUFBVSxDQVM5QjtJQUFELGlCQUFDO0NBVEQsQUFTQyxDQVR1QyxFQUFFLENBQUMsU0FBUyxHQVNuRDtrQkFUb0IsVUFBVSIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG5jb25zdCB7Y2NjbGFzcywgcHJvcGVydHl9ID0gY2MuX2RlY29yYXRvcjtcclxuXHJcbkBjY2NsYXNzXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIERlc3Ryb3lWRlggZXh0ZW5kcyBjYy5Db21wb25lbnQge1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5JbnRlZ2VyKVxyXG4gICAgdGltbWluZzogbnVtYmVyID0gMDtcclxuICAgIHN0YXJ0KCkge1xyXG4gICAgICAgIHRoaXMuc2NoZWR1bGVPbmNlKCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLmRlc3Ryb3koKTtcclxuICAgICAgICB9LCB0aGlzLnRpbW1pbmcpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/Global.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '3c677+i/i1KO7mX8mMrc5uJ', 'Global');
// Scripts/Common/Global.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Global = {
    touchPos: null,
    boolEnableTouch: false,
    boolFirstTouchJoyStick: false,
    boolStartPlay: false,
    boolStartAttacking: false,
    boolCheckAttacking: false,
    boolCheckAttacked: false,
    boolendG: false,
    soundBG: null,
    soundIntro: null,
    soundAttack: null,
    soundFootStep: null,
    soundKatana: null,
    soundScream: null,
    soundSpin: null,
    soundReward: null,
    soundClickBtn: null
};
exports.default = Global;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxHbG9iYWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsSUFBSSxNQUFNLEdBQVc7SUFDakIsUUFBUSxFQUFFLElBQUk7SUFDZCxlQUFlLEVBQUUsS0FBSztJQUN0QixzQkFBc0IsRUFBRSxLQUFLO0lBQzdCLGFBQWEsRUFBRSxLQUFLO0lBQ3BCLGtCQUFrQixFQUFFLEtBQUs7SUFDekIsa0JBQWtCLEVBQUUsS0FBSztJQUN6QixpQkFBaUIsRUFBRSxLQUFLO0lBQ3hCLFFBQVEsRUFBRSxLQUFLO0lBQ2YsT0FBTyxFQUFFLElBQUk7SUFDYixVQUFVLEVBQUUsSUFBSTtJQUNoQixXQUFXLEVBQUUsSUFBSTtJQUNqQixhQUFhLEVBQUUsSUFBSTtJQUNuQixXQUFXLEVBQUUsSUFBSTtJQUNqQixXQUFXLEVBQUUsSUFBSTtJQUNqQixTQUFTLEVBQUUsSUFBSTtJQUNmLFdBQVcsRUFBRSxJQUFJO0lBQ2pCLGFBQWEsRUFBRSxJQUFJO0NBQ3RCLENBQUM7QUFDRixrQkFBZSxNQUFNLENBQUMiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbnRlcmZhY2UgR2xvYmFsIHtcclxuICAgIHRvdWNoUG9zOiBjYy5WZWMyLFxyXG4gICAgYm9vbEVuYWJsZVRvdWNoOiBib29sZWFuLFxyXG4gICAgYm9vbEZpcnN0VG91Y2hKb3lTdGljazogYm9vbGVhbixcclxuICAgIGJvb2xTdGFydFBsYXk6IGJvb2xlYW4sXHJcbiAgICBib29sU3RhcnRBdHRhY2tpbmc6IGJvb2xlYW4sXHJcbiAgICBib29sQ2hlY2tBdHRhY2tpbmc6IGJvb2xlYW4sXHJcbiAgICBib29sQ2hlY2tBdHRhY2tlZDogYm9vbGVhbixcclxuICAgIGJvb2xlbmRHOiBib29sZWFuLFxyXG4gICAgc291bmRCRzogY2MuQXVkaW9DbGlwLFxyXG4gICAgc291bmRJbnRybzogY2MuQXVkaW9DbGlwLFxyXG4gICAgc291bmRBdHRhY2s6IGNjLkF1ZGlvQ2xpcCxcclxuICAgIHNvdW5kRm9vdFN0ZXA6IGNjLkF1ZGlvQ2xpcCxcclxuICAgIHNvdW5kS2F0YW5hOiBjYy5BdWRpb0NsaXAsXHJcbiAgICBzb3VuZFNjcmVhbTogY2MuQXVkaW9DbGlwLFxyXG4gICAgc291bmRTcGluOiBjYy5BdWRpb0NsaXAsXHJcbiAgICBzb3VuZFJld2FyZDogY2MuQXVkaW9DbGlwLFxyXG4gICAgc291bmRDbGlja0J0bjogY2MuQXVkaW9DbGlwXHJcbn1cclxubGV0IEdsb2JhbDogR2xvYmFsID0ge1xyXG4gICAgdG91Y2hQb3M6IG51bGwsXHJcbiAgICBib29sRW5hYmxlVG91Y2g6IGZhbHNlLFxyXG4gICAgYm9vbEZpcnN0VG91Y2hKb3lTdGljazogZmFsc2UsXHJcbiAgICBib29sU3RhcnRQbGF5OiBmYWxzZSxcclxuICAgIGJvb2xTdGFydEF0dGFja2luZzogZmFsc2UsXHJcbiAgICBib29sQ2hlY2tBdHRhY2tpbmc6IGZhbHNlLFxyXG4gICAgYm9vbENoZWNrQXR0YWNrZWQ6IGZhbHNlLFxyXG4gICAgYm9vbGVuZEc6IGZhbHNlLFxyXG4gICAgc291bmRCRzogbnVsbCxcclxuICAgIHNvdW5kSW50cm86IG51bGwsXHJcbiAgICBzb3VuZEF0dGFjazogbnVsbCxcclxuICAgIHNvdW5kRm9vdFN0ZXA6IG51bGwsXHJcbiAgICBzb3VuZEthdGFuYTogbnVsbCxcclxuICAgIHNvdW5kU2NyZWFtOiBudWxsLFxyXG4gICAgc291bmRTcGluOiBudWxsLFxyXG4gICAgc291bmRSZXdhcmQ6IG51bGwsXHJcbiAgICBzb3VuZENsaWNrQnRuOiBudWxsXHJcbn07XHJcbmV4cG9ydCBkZWZhdWx0IEdsb2JhbDsiXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/GamePlayInstance.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '3079c9yA9NNG7yWTNHi7OqT', 'GamePlayInstance');
// Scripts/Common/GamePlayInstance.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var StickMan1_1 = require("../GamePlay/StickMan1");
var Singleton_1 = require("./Singleton");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
exports.instance = new cc.EventTarget();
var GamePlayInstance = /** @class */ (function (_super) {
    __extends(GamePlayInstance, _super);
    function GamePlayInstance() {
        var _this = _super.call(this) || this;
        _this.gameplay = StickMan1_1.default.Instance(StickMan1_1.default);
        GamePlayInstance_1._instance = _this;
        return _this;
    }
    GamePlayInstance_1 = GamePlayInstance;
    var GamePlayInstance_1;
    GamePlayInstance = GamePlayInstance_1 = __decorate([
        ccclass
    ], GamePlayInstance);
    return GamePlayInstance;
}(Singleton_1.default));
exports.default = GamePlayInstance;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxHYW1lUGxheUluc3RhbmNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNBLG1EQUE4QztBQUM5Qyx5Q0FBb0M7QUFFOUIsSUFBQSxrQkFBcUMsRUFBbkMsb0JBQU8sRUFBRSxzQkFBMEIsQ0FBQztBQUMvQixRQUFBLFFBQVEsR0FBRyxJQUFJLEVBQUUsQ0FBQyxXQUFXLEVBQUUsQ0FBQztBQUU3QztJQUE4QyxvQ0FBMkI7SUFFckU7UUFBQSxZQUNJLGlCQUFPLFNBRVY7UUFKRCxjQUFRLEdBQWMsbUJBQVMsQ0FBQyxRQUFRLENBQUMsbUJBQVMsQ0FBQyxDQUFDO1FBR2hELGtCQUFnQixDQUFDLFNBQVMsR0FBRyxLQUFJLENBQUM7O0lBQ3RDLENBQUM7eUJBTGdCLGdCQUFnQjs7SUFBaEIsZ0JBQWdCO1FBRHBDLE9BQU87T0FDYSxnQkFBZ0IsQ0FNcEM7SUFBRCx1QkFBQztDQU5ELEFBTUMsQ0FONkMsbUJBQVMsR0FNdEQ7a0JBTm9CLGdCQUFnQiIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG5pbXBvcnQgU3RpY2tNYW4xIGZyb20gXCIuLi9HYW1lUGxheS9TdGlja01hbjFcIjtcclxuaW1wb3J0IFNpbmdsZXRvbiBmcm9tIFwiLi9TaW5nbGV0b25cIjtcclxuXHJcbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XHJcbmV4cG9ydCBjb25zdCBpbnN0YW5jZSA9IG5ldyBjYy5FdmVudFRhcmdldCgpO1xyXG5AY2NjbGFzc1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBHYW1lUGxheUluc3RhbmNlIGV4dGVuZHMgU2luZ2xldG9uPEdhbWVQbGF5SW5zdGFuY2U+IHtcclxuICAgIGdhbWVwbGF5OiBTdGlja01hbjEgPSBTdGlja01hbjEuSW5zdGFuY2UoU3RpY2tNYW4xKTtcclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIHN1cGVyKCk7XHJcbiAgICAgICAgR2FtZVBsYXlJbnN0YW5jZS5faW5zdGFuY2UgPSB0aGlzO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/Singleton.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'b8b25AOUfxKOZOdWSle/5qU', 'Singleton');
// Scripts/Common/Singleton.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Singleton = /** @class */ (function (_super) {
    __extends(Singleton, _super);
    function Singleton() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Singleton.Instance = function (c) {
        if (this._instance == null) {
            this._instance = new c();
        }
        return this._instance;
    };
    Singleton._instance = null;
    return Singleton;
}(cc.Component));
exports.default = Singleton;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxTaW5nbGV0b24udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ0E7SUFBMEMsNkJBQVk7SUFBdEQ7O0lBUUEsQ0FBQztJQVBpQixrQkFBUSxHQUF0QixVQUEwQixDQUFlO1FBQ3JDLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLEVBQUM7WUFDdkIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDO1NBQzVCO1FBQ0QsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO0lBQzFCLENBQUM7SUFDYSxtQkFBUyxHQUFHLElBQUksQ0FBQztJQUNuQyxnQkFBQztDQVJELEFBUUMsQ0FSeUMsRUFBRSxDQUFDLFNBQVMsR0FRckQ7a0JBUm9CLFNBQVMiLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgU2luZ2xldG9uPFQ+IGV4dGVuZHMgY2MuQ29tcG9uZW50e1xyXG4gICAgcHVibGljIHN0YXRpYyBJbnN0YW5jZTxUPihjOiB7bmV3KCk6IFQ7IH0pIDogVHtcclxuICAgICAgICBpZiAodGhpcy5faW5zdGFuY2UgPT0gbnVsbCl7XHJcbiAgICAgICAgICAgIHRoaXMuX2luc3RhbmNlID0gbmV3IGMoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2luc3RhbmNlO1xyXG4gICAgfVxyXG4gICAgcHVibGljIHN0YXRpYyBfaW5zdGFuY2UgPSBudWxsO1xyXG59XHJcbiJdfQ==
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Character/CharacterController.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '8c229TU8nVEg7patjhCarLi', 'CharacterController');
// Scripts/Character/CharacterController.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var EnumDefine_1 = require("../Common/EnumDefine");
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var KeyEvent_1 = require("../Common/KeyEvent");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var CharacterController = /** @class */ (function (_super) {
    __extends(CharacterController, _super);
    function CharacterController() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.ArrowDirection = null;
        _this.JoystickFollow = null;
        _this.actionType = EnumDefine_1.ActionType.IDLE;
        _this.speed = 100;
        _this.Weapon = null;
        _this.timeAnim = 0;
        _this.placeWeapon = null;
        _this.effectSmoke = null;
        _this.clampLeft = 0;
        _this.clampRight = 0;
        _this.clampBottom = 0;
        _this.clampTop = 0;
        _this.level = 0;
        _this.scale = 0;
        // @property(MapController)
        // mapController: MapController = null;
        _this.originalSpeed = 0;
        _this.gameplayInstance = null;
        _this.boolPlaySoundFoot = false;
        return _this;
    }
    // onLoad () {}
    CharacterController.prototype.start = function () {
        this.gameplayInstance = GamePlayInstance_1.default.Instance(GamePlayInstance_1.default);
        Global_1.default.touchPos = cc.v2(0, 0);
        this.originalSpeed = this.speed;
    };
    CharacterController.prototype.update = function () {
        var _this = this;
        if (Global_1.default.boolEnableTouch && !Global_1.default.boolStartAttacking) {
            this.node.x = cc.misc.clampf(this.node.x, this.clampLeft, this.clampRight);
            this.node.y = cc.misc.clampf(this.node.y, this.clampBottom, this.clampTop);
            // let radius = Math.sqrt(this.clampRight * this.clampRight - this.node.x * this.node.x);
            // this.node.x = cc.misc.clampf(this.node.x, -radius, radius);
            // this.node.y = cc.misc.clampf(this.node.y, -radius, radius);  
            // this.mapController.ListSquareObjects.forEach(element => {
            //     if(element.OnTheLeft(this.node))
            //         {            
            //             this.node.x = cc.misc.clampf(this.node.x, this.clampLeft, element.positionLeft.x - 1);
            //             this.node.y = cc.misc.clampf(this.node.y, this.clampBottom, this.clampTop);
            //         }
            //     else if(element.OnTheRight(this.node))
            //         {
            //             this.node.x = cc.misc.clampf(this.node.x, element.positionRight.x + 1, this.clampRight);
            //             this.node.y = cc.misc.clampf(this.node.y, this.clampBottom, this.clampTop);
            //         }
            //     else if(element.OnTheTop(this.node))
            //         {
            //             this.node.x = cc.misc.clampf(this.node.x, this.clampLeft, this.clampRight);
            //             this.node.y = cc.misc.clampf(this.node.y, element.positionTop.y + 1, this.clampTop);
            //         }
            //     else if(element.OnTheBottom(this.node))
            //         {
            //             this.node.x = cc.misc.clampf(this.node.x, this.clampLeft, this.clampRight);
            //             this.node.y = cc.misc.clampf(this.node.y, this.clampBottom,  element.positionBottom.y - 1);
            //         }
            //         else
            //         {
            //             this.node.x = cc.misc.clampf(this.node.x, this.clampLeft, this.clampRight);
            //             this.node.y = cc.misc.clampf(this.node.y, this.clampBottom, this.clampTop);
            //         }
            // });
            var PosForX = this.node.getPosition();
            var PosForY = this.node.getPosition();
            PosForX.addSelf(Global_1.default.touchPos.mul(this.speed / 100));
            PosForY.addSelf(Global_1.default.touchPos.mul(this.speed / 100));
            this.node.x = PosForX.x;
            this.node.y = PosForY.y;
            if (!this.boolPlaySoundFoot) {
                this.boolPlaySoundFoot = true;
                cc.audioEngine.playEffect(Global_1.default.soundFootStep, false);
                this.scheduleOnce(function () {
                    _this.boolPlaySoundFoot = false;
                }, 0.3);
            }
            var r = Math.atan2(Global_1.default.touchPos.y, Global_1.default.touchPos.x);
            var degree = r * 180 / (Math.PI);
            degree = 360 - degree + 90;
            this.node.is3DNode = true;
            this.node.eulerAngles = new cc.Vec3(-90, 180, degree);
        }
    };
    CharacterController.prototype.Attacking = function () {
        var _this = this;
        Global_1.default.boolStartAttacking = true;
        Global_1.default.boolCheckAttacking = false;
        if (this.node.name == "MyDeadpool") {
            this.node.getComponent(cc.SkeletonAnimation).play("Armature|sword_attack");
        }
        else {
            this.node.getComponent(cc.SkeletonAnimation).play("Armature|hammer_attack");
        }
        this.scheduleOnce(function () {
            Global_1.default.boolCheckAttacking = true;
            Global_1.default.boolCheckAttacked = true;
            _this.spawnEffectSmoke(_this.effectSmoke);
            // cc.audioEngine.playEffect(Global.soundAttack, false);
            cc.audioEngine.playEffect(Global_1.default.soundKatana, false);
        }, 0.5);
        this.scheduleOnce(function () {
            GamePlayInstance_1.instance.emit(KeyEvent_1.default.activeGuide);
            Global_1.default.boolStartAttacking = false;
            if (_this.node.name == "MyDeadpool") {
                _this.node.getComponent(cc.SkeletonAnimation).play("Armature|sword_idle");
            }
            else {
                _this.node.getComponent(cc.SkeletonAnimation).play("Armature|hammer_idle");
            }
        }, this.timeAnim);
    };
    CharacterController.prototype.LevelUpPlayer = function () {
        var tween = new cc.Tween().to(0.4, { scale: this.node.scale + this.scale });
        tween.target(this.node).start();
        if (this.level < 4)
            this.level++;
    };
    CharacterController.prototype.spawnEffectSmoke = function (smoke) {
        var smk = cc.instantiate(smoke);
        smk.parent = cc.Canvas.instance.node;
        var pos = this.node.convertToWorldSpaceAR(this.placeWeapon.getPosition());
        pos = cc.Canvas.instance.node.convertToNodeSpaceAR(pos);
        smk.x = pos.x;
        smk.y = pos.y;
        smk.z = 0;
    };
    __decorate([
        property(cc.Node)
    ], CharacterController.prototype, "ArrowDirection", void 0);
    __decorate([
        property(cc.Node)
    ], CharacterController.prototype, "JoystickFollow", void 0);
    __decorate([
        property({ type: cc.Enum(EnumDefine_1.ActionType) })
    ], CharacterController.prototype, "actionType", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "speed", void 0);
    __decorate([
        property(cc.Node)
    ], CharacterController.prototype, "Weapon", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "timeAnim", void 0);
    __decorate([
        property(cc.Node)
    ], CharacterController.prototype, "placeWeapon", void 0);
    __decorate([
        property(cc.Prefab)
    ], CharacterController.prototype, "effectSmoke", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "clampLeft", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "clampRight", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "clampBottom", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "clampTop", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "level", void 0);
    __decorate([
        property(cc.Integer)
    ], CharacterController.prototype, "scale", void 0);
    CharacterController = __decorate([
        ccclass
    ], CharacterController);
    return CharacterController;
}(cc.Component));
exports.default = CharacterController;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ2hhcmFjdGVyXFxDaGFyYWN0ZXJDb250cm9sbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNBLG1EQUFrRDtBQUNsRCwrREFBd0U7QUFDeEUsMkNBQXNDO0FBQ3RDLCtDQUEwQztBQUVwQyxJQUFBLGtCQUFxQyxFQUFuQyxvQkFBTyxFQUFFLHNCQUEwQixDQUFDO0FBRzVDO0lBQWlELHVDQUFZO0lBRDdEO1FBQUEscUVBcUtDO1FBaktHLG9CQUFjLEdBQVksSUFBSSxDQUFDO1FBRy9CLG9CQUFjLEdBQVksSUFBSSxDQUFDO1FBRy9CLGdCQUFVLEdBQWUsdUJBQVUsQ0FBQyxJQUFJLENBQUM7UUFHekMsV0FBSyxHQUFXLEdBQUcsQ0FBQztRQUdwQixZQUFNLEdBQVksSUFBSSxDQUFDO1FBR3ZCLGNBQVEsR0FBVyxDQUFDLENBQUM7UUFHckIsaUJBQVcsR0FBWSxJQUFJLENBQUM7UUFHNUIsaUJBQVcsR0FBYyxJQUFJLENBQUM7UUFHOUIsZUFBUyxHQUFXLENBQUMsQ0FBQztRQUd0QixnQkFBVSxHQUFXLENBQUMsQ0FBQztRQUd2QixpQkFBVyxHQUFXLENBQUMsQ0FBQztRQUd4QixjQUFRLEdBQVcsQ0FBQyxDQUFDO1FBR3JCLFdBQUssR0FBVyxDQUFDLENBQUM7UUFHbEIsV0FBSyxHQUFXLENBQUMsQ0FBQztRQUVsQiwyQkFBMkI7UUFDM0IsdUNBQXVDO1FBRXZDLG1CQUFhLEdBQVcsQ0FBQyxDQUFDO1FBRTFCLHNCQUFnQixHQUFxQixJQUFJLENBQUM7UUFDMUMsdUJBQWlCLEdBQVksS0FBSyxDQUFDOztJQWtIdkMsQ0FBQztJQWhIRyxlQUFlO0lBRWYsbUNBQUssR0FBTDtRQUNJLElBQUksQ0FBQyxnQkFBZ0IsR0FBRywwQkFBZ0IsQ0FBQyxRQUFRLENBQUMsMEJBQWdCLENBQUMsQ0FBQztRQUNwRSxnQkFBTSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUM5QixJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7SUFDcEMsQ0FBQztJQUVELG9DQUFNLEdBQU47UUFBQSxpQkF1REM7UUF0REcsSUFBSSxnQkFBTSxDQUFDLGVBQWUsSUFBSSxDQUFDLGdCQUFNLENBQUMsa0JBQWtCLEVBQUU7WUFDdEQsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDM0UsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDM0UseUZBQXlGO1lBQ3pGLDhEQUE4RDtZQUM5RCxnRUFBZ0U7WUFFaEUsNERBQTREO1lBQzVELHVDQUF1QztZQUN2Qyx3QkFBd0I7WUFDeEIscUdBQXFHO1lBQ3JHLDBGQUEwRjtZQUMxRixZQUFZO1lBQ1osNkNBQTZDO1lBQzdDLFlBQVk7WUFDWix1R0FBdUc7WUFDdkcsMEZBQTBGO1lBQzFGLFlBQVk7WUFDWiwyQ0FBMkM7WUFDM0MsWUFBWTtZQUNaLDBGQUEwRjtZQUMxRixtR0FBbUc7WUFDbkcsWUFBWTtZQUNaLDhDQUE4QztZQUM5QyxZQUFZO1lBQ1osMEZBQTBGO1lBQzFGLDBHQUEwRztZQUMxRyxZQUFZO1lBQ1osZUFBZTtZQUNmLFlBQVk7WUFDWiwwRkFBMEY7WUFDMUYsMEZBQTBGO1lBQzFGLFlBQVk7WUFDWixNQUFNO1lBRU4sSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUN0QyxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ3RDLE9BQU8sQ0FBQyxPQUFPLENBQUMsZ0JBQU0sQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUN2RCxPQUFPLENBQUMsT0FBTyxDQUFDLGdCQUFNLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDdkQsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUN4QixJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQ3hCLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUU7Z0JBQ3pCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUM7Z0JBQzlCLEVBQUUsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLGdCQUFNLENBQUMsYUFBYSxFQUFFLEtBQUssQ0FBQyxDQUFDO2dCQUN2RCxJQUFJLENBQUMsWUFBWSxDQUFDO29CQUNkLEtBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUM7Z0JBQ25DLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQzthQUNYO1lBQ0QsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxnQkFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUUsZ0JBQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDekQsSUFBSSxNQUFNLEdBQUcsQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUNqQyxNQUFNLEdBQUcsR0FBRyxHQUFHLE1BQU0sR0FBRyxFQUFFLENBQUM7WUFDM0IsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1lBQzFCLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBTSxDQUFDLENBQUM7U0FDekQ7SUFDTCxDQUFDO0lBRUQsdUNBQVMsR0FBVDtRQUFBLGlCQTZCQztRQTVCRyxnQkFBTSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQztRQUNqQyxnQkFBTSxDQUFDLGtCQUFrQixHQUFHLEtBQUssQ0FBQztRQUVsQyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLFlBQVksRUFBRTtZQUNoQyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsQ0FBQztTQUM5RTthQUNJO1lBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLENBQUM7U0FDL0U7UUFFRCxJQUFJLENBQUMsWUFBWSxDQUFDO1lBQ2QsZ0JBQU0sQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUM7WUFDakMsZ0JBQU0sQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUM7WUFDaEMsS0FBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUN4Qyx3REFBd0Q7WUFDeEQsRUFBRSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsZ0JBQU0sQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDekQsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBRVIsSUFBSSxDQUFDLFlBQVksQ0FBQztZQUNkLDJCQUFRLENBQUMsSUFBSSxDQUFDLGtCQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDcEMsZ0JBQU0sQ0FBQyxrQkFBa0IsR0FBRyxLQUFLLENBQUM7WUFDbEMsSUFBSSxLQUFJLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxZQUFZLEVBQUU7Z0JBQ2hDLEtBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO2FBQzVFO2lCQUNJO2dCQUNELEtBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO2FBQzdFO1FBQ0wsQ0FBQyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUN0QixDQUFDO0lBRUQsMkNBQWEsR0FBYjtRQUNJLElBQUksS0FBSyxHQUFHLElBQUksRUFBRSxDQUFDLEtBQUssRUFBRSxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUM7UUFDNUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDaEMsSUFBSSxJQUFJLENBQUMsS0FBSyxHQUFHLENBQUM7WUFDZCxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDckIsQ0FBQztJQUVELDhDQUFnQixHQUFoQixVQUFpQixLQUFnQjtRQUM3QixJQUFJLEdBQUcsR0FBRyxFQUFFLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ2hDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDO1FBQ3JDLElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO1FBQzFFLEdBQUcsR0FBRyxFQUFFLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDeEQsR0FBRyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQ2QsR0FBRyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQ2QsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDZCxDQUFDO0lBaEtEO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7K0RBQ2E7SUFHL0I7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzsrREFDYTtJQUcvQjtRQURDLFFBQVEsQ0FBQyxFQUFFLElBQUksRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLHVCQUFVLENBQUMsRUFBRSxDQUFDOzJEQUNDO0lBR3pDO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUM7c0RBQ0Q7SUFHcEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzt1REFDSztJQUd2QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDO3lEQUNBO0lBR3JCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7NERBQ1U7SUFHNUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQzs0REFDVTtJQUc5QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDOzBEQUNDO0lBR3RCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUM7MkRBQ0U7SUFHdkI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQzs0REFDRztJQUd4QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDO3lEQUNBO0lBR3JCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUM7c0RBQ0g7SUFHbEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQztzREFDSDtJQTFDRCxtQkFBbUI7UUFEdkMsT0FBTztPQUNhLG1CQUFtQixDQW9LdkM7SUFBRCwwQkFBQztDQXBLRCxBQW9LQyxDQXBLZ0QsRUFBRSxDQUFDLFNBQVMsR0FvSzVEO2tCQXBLb0IsbUJBQW1CIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IE1hcENvbnRyb2xsZXIgZnJvbSBcIi4uL0NvbGxpZGVyT2JqL01hcENvbnRyb2xsZXJcIjtcclxuaW1wb3J0IHsgQWN0aW9uVHlwZSB9IGZyb20gXCIuLi9Db21tb24vRW51bURlZmluZVwiO1xyXG5pbXBvcnQgR2FtZVBsYXlJbnN0YW5jZSwgeyBpbnN0YW5jZSB9IGZyb20gXCIuLi9Db21tb24vR2FtZVBsYXlJbnN0YW5jZVwiO1xyXG5pbXBvcnQgR2xvYmFsIGZyb20gXCIuLi9Db21tb24vR2xvYmFsXCI7XHJcbmltcG9ydCBLZXlFdmVudCBmcm9tIFwiLi4vQ29tbW9uL0tleUV2ZW50XCI7XHJcblxyXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQ2hhcmFjdGVyQ29udHJvbGxlciBleHRlbmRzIGNjLkNvbXBvbmVudCB7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBBcnJvd0RpcmVjdGlvbjogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBKb3lzdGlja0ZvbGxvdzogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KHsgdHlwZTogY2MuRW51bShBY3Rpb25UeXBlKSB9KVxyXG4gICAgYWN0aW9uVHlwZTogQWN0aW9uVHlwZSA9IEFjdGlvblR5cGUuSURMRTtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuSW50ZWdlcilcclxuICAgIHNwZWVkOiBudW1iZXIgPSAxMDA7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBXZWFwb246IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5JbnRlZ2VyKVxyXG4gICAgdGltZUFuaW06IG51bWJlciA9IDA7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICBwbGFjZVdlYXBvbjogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLlByZWZhYilcclxuICAgIGVmZmVjdFNtb2tlOiBjYy5QcmVmYWIgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5JbnRlZ2VyKVxyXG4gICAgY2xhbXBMZWZ0OiBudW1iZXIgPSAwO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5JbnRlZ2VyKVxyXG4gICAgY2xhbXBSaWdodDogbnVtYmVyID0gMDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuSW50ZWdlcilcclxuICAgIGNsYW1wQm90dG9tOiBudW1iZXIgPSAwO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5JbnRlZ2VyKVxyXG4gICAgY2xhbXBUb3A6IG51bWJlciA9IDA7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLkludGVnZXIpXHJcbiAgICBsZXZlbDogbnVtYmVyID0gMDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuSW50ZWdlcilcclxuICAgIHNjYWxlOiBudW1iZXIgPSAwO1xyXG5cclxuICAgIC8vIEBwcm9wZXJ0eShNYXBDb250cm9sbGVyKVxyXG4gICAgLy8gbWFwQ29udHJvbGxlcjogTWFwQ29udHJvbGxlciA9IG51bGw7XHJcblxyXG4gICAgb3JpZ2luYWxTcGVlZDogbnVtYmVyID0gMDtcclxuXHJcbiAgICBnYW1lcGxheUluc3RhbmNlOiBHYW1lUGxheUluc3RhbmNlID0gbnVsbDtcclxuICAgIGJvb2xQbGF5U291bmRGb290OiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgLy8gb25Mb2FkICgpIHt9XHJcblxyXG4gICAgc3RhcnQoKSB7XHJcbiAgICAgICAgdGhpcy5nYW1lcGxheUluc3RhbmNlID0gR2FtZVBsYXlJbnN0YW5jZS5JbnN0YW5jZShHYW1lUGxheUluc3RhbmNlKTtcclxuICAgICAgICBHbG9iYWwudG91Y2hQb3MgPSBjYy52MigwLCAwKTtcclxuICAgICAgICB0aGlzLm9yaWdpbmFsU3BlZWQgPSB0aGlzLnNwZWVkO1xyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZSgpIHtcclxuICAgICAgICBpZiAoR2xvYmFsLmJvb2xFbmFibGVUb3VjaCAmJiAhR2xvYmFsLmJvb2xTdGFydEF0dGFja2luZykge1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUueCA9IGNjLm1pc2MuY2xhbXBmKHRoaXMubm9kZS54LCB0aGlzLmNsYW1wTGVmdCwgdGhpcy5jbGFtcFJpZ2h0KTtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLnkgPSBjYy5taXNjLmNsYW1wZih0aGlzLm5vZGUueSwgdGhpcy5jbGFtcEJvdHRvbSwgdGhpcy5jbGFtcFRvcCk7XHJcbiAgICAgICAgICAgIC8vIGxldCByYWRpdXMgPSBNYXRoLnNxcnQodGhpcy5jbGFtcFJpZ2h0ICogdGhpcy5jbGFtcFJpZ2h0IC0gdGhpcy5ub2RlLnggKiB0aGlzLm5vZGUueCk7XHJcbiAgICAgICAgICAgIC8vIHRoaXMubm9kZS54ID0gY2MubWlzYy5jbGFtcGYodGhpcy5ub2RlLngsIC1yYWRpdXMsIHJhZGl1cyk7XHJcbiAgICAgICAgICAgIC8vIHRoaXMubm9kZS55ID0gY2MubWlzYy5jbGFtcGYodGhpcy5ub2RlLnksIC1yYWRpdXMsIHJhZGl1cyk7ICBcclxuXHJcbiAgICAgICAgICAgIC8vIHRoaXMubWFwQ29udHJvbGxlci5MaXN0U3F1YXJlT2JqZWN0cy5mb3JFYWNoKGVsZW1lbnQgPT4ge1xyXG4gICAgICAgICAgICAvLyAgICAgaWYoZWxlbWVudC5PblRoZUxlZnQodGhpcy5ub2RlKSlcclxuICAgICAgICAgICAgLy8gICAgICAgICB7ICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIC8vICAgICAgICAgICAgIHRoaXMubm9kZS54ID0gY2MubWlzYy5jbGFtcGYodGhpcy5ub2RlLngsIHRoaXMuY2xhbXBMZWZ0LCBlbGVtZW50LnBvc2l0aW9uTGVmdC54IC0gMSk7XHJcbiAgICAgICAgICAgIC8vICAgICAgICAgICAgIHRoaXMubm9kZS55ID0gY2MubWlzYy5jbGFtcGYodGhpcy5ub2RlLnksIHRoaXMuY2xhbXBCb3R0b20sIHRoaXMuY2xhbXBUb3ApO1xyXG4gICAgICAgICAgICAvLyAgICAgICAgIH1cclxuICAgICAgICAgICAgLy8gICAgIGVsc2UgaWYoZWxlbWVudC5PblRoZVJpZ2h0KHRoaXMubm9kZSkpXHJcbiAgICAgICAgICAgIC8vICAgICAgICAge1xyXG4gICAgICAgICAgICAvLyAgICAgICAgICAgICB0aGlzLm5vZGUueCA9IGNjLm1pc2MuY2xhbXBmKHRoaXMubm9kZS54LCBlbGVtZW50LnBvc2l0aW9uUmlnaHQueCArIDEsIHRoaXMuY2xhbXBSaWdodCk7XHJcbiAgICAgICAgICAgIC8vICAgICAgICAgICAgIHRoaXMubm9kZS55ID0gY2MubWlzYy5jbGFtcGYodGhpcy5ub2RlLnksIHRoaXMuY2xhbXBCb3R0b20sIHRoaXMuY2xhbXBUb3ApO1xyXG4gICAgICAgICAgICAvLyAgICAgICAgIH1cclxuICAgICAgICAgICAgLy8gICAgIGVsc2UgaWYoZWxlbWVudC5PblRoZVRvcCh0aGlzLm5vZGUpKVxyXG4gICAgICAgICAgICAvLyAgICAgICAgIHtcclxuICAgICAgICAgICAgLy8gICAgICAgICAgICAgdGhpcy5ub2RlLnggPSBjYy5taXNjLmNsYW1wZih0aGlzLm5vZGUueCwgdGhpcy5jbGFtcExlZnQsIHRoaXMuY2xhbXBSaWdodCk7XHJcbiAgICAgICAgICAgIC8vICAgICAgICAgICAgIHRoaXMubm9kZS55ID0gY2MubWlzYy5jbGFtcGYodGhpcy5ub2RlLnksIGVsZW1lbnQucG9zaXRpb25Ub3AueSArIDEsIHRoaXMuY2xhbXBUb3ApO1xyXG4gICAgICAgICAgICAvLyAgICAgICAgIH1cclxuICAgICAgICAgICAgLy8gICAgIGVsc2UgaWYoZWxlbWVudC5PblRoZUJvdHRvbSh0aGlzLm5vZGUpKVxyXG4gICAgICAgICAgICAvLyAgICAgICAgIHtcclxuICAgICAgICAgICAgLy8gICAgICAgICAgICAgdGhpcy5ub2RlLnggPSBjYy5taXNjLmNsYW1wZih0aGlzLm5vZGUueCwgdGhpcy5jbGFtcExlZnQsIHRoaXMuY2xhbXBSaWdodCk7XHJcbiAgICAgICAgICAgIC8vICAgICAgICAgICAgIHRoaXMubm9kZS55ID0gY2MubWlzYy5jbGFtcGYodGhpcy5ub2RlLnksIHRoaXMuY2xhbXBCb3R0b20sICBlbGVtZW50LnBvc2l0aW9uQm90dG9tLnkgLSAxKTtcclxuICAgICAgICAgICAgLy8gICAgICAgICB9XHJcbiAgICAgICAgICAgIC8vICAgICAgICAgZWxzZVxyXG4gICAgICAgICAgICAvLyAgICAgICAgIHtcclxuICAgICAgICAgICAgLy8gICAgICAgICAgICAgdGhpcy5ub2RlLnggPSBjYy5taXNjLmNsYW1wZih0aGlzLm5vZGUueCwgdGhpcy5jbGFtcExlZnQsIHRoaXMuY2xhbXBSaWdodCk7XHJcbiAgICAgICAgICAgIC8vICAgICAgICAgICAgIHRoaXMubm9kZS55ID0gY2MubWlzYy5jbGFtcGYodGhpcy5ub2RlLnksIHRoaXMuY2xhbXBCb3R0b20sIHRoaXMuY2xhbXBUb3ApO1xyXG4gICAgICAgICAgICAvLyAgICAgICAgIH1cclxuICAgICAgICAgICAgLy8gfSk7XHJcblxyXG4gICAgICAgICAgICBsZXQgUG9zRm9yWCA9IHRoaXMubm9kZS5nZXRQb3NpdGlvbigpO1xyXG4gICAgICAgICAgICBsZXQgUG9zRm9yWSA9IHRoaXMubm9kZS5nZXRQb3NpdGlvbigpO1xyXG4gICAgICAgICAgICBQb3NGb3JYLmFkZFNlbGYoR2xvYmFsLnRvdWNoUG9zLm11bCh0aGlzLnNwZWVkIC8gMTAwKSk7XHJcbiAgICAgICAgICAgIFBvc0ZvclkuYWRkU2VsZihHbG9iYWwudG91Y2hQb3MubXVsKHRoaXMuc3BlZWQgLyAxMDApKTtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLnggPSBQb3NGb3JYLng7XHJcbiAgICAgICAgICAgIHRoaXMubm9kZS55ID0gUG9zRm9yWS55O1xyXG4gICAgICAgICAgICBpZiAoIXRoaXMuYm9vbFBsYXlTb3VuZEZvb3QpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuYm9vbFBsYXlTb3VuZEZvb3QgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgY2MuYXVkaW9FbmdpbmUucGxheUVmZmVjdChHbG9iYWwuc291bmRGb290U3RlcCwgZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuYm9vbFBsYXlTb3VuZEZvb3QgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIH0sIDAuMyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdmFyIHIgPSBNYXRoLmF0YW4yKEdsb2JhbC50b3VjaFBvcy55LCBHbG9iYWwudG91Y2hQb3MueCk7XHJcbiAgICAgICAgICAgIHZhciBkZWdyZWUgPSByICogMTgwIC8gKE1hdGguUEkpO1xyXG4gICAgICAgICAgICBkZWdyZWUgPSAzNjAgLSBkZWdyZWUgKyA5MDtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLmlzM0ROb2RlID0gdHJ1ZTtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLmV1bGVyQW5nbGVzID0gbmV3IGNjLlZlYzMoLTkwLCAxODAsIGRlZ3JlZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIEF0dGFja2luZygpIHtcclxuICAgICAgICBHbG9iYWwuYm9vbFN0YXJ0QXR0YWNraW5nID0gdHJ1ZTtcclxuICAgICAgICBHbG9iYWwuYm9vbENoZWNrQXR0YWNraW5nID0gZmFsc2U7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLm5vZGUubmFtZSA9PSBcIk15RGVhZHBvb2xcIikge1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlNrZWxldG9uQW5pbWF0aW9uKS5wbGF5KFwiQXJtYXR1cmV8c3dvcmRfYXR0YWNrXCIpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLmdldENvbXBvbmVudChjYy5Ta2VsZXRvbkFuaW1hdGlvbikucGxheShcIkFybWF0dXJlfGhhbW1lcl9hdHRhY2tcIik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLnNjaGVkdWxlT25jZSgoKSA9PiB7XHJcbiAgICAgICAgICAgIEdsb2JhbC5ib29sQ2hlY2tBdHRhY2tpbmcgPSB0cnVlO1xyXG4gICAgICAgICAgICBHbG9iYWwuYm9vbENoZWNrQXR0YWNrZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLnNwYXduRWZmZWN0U21va2UodGhpcy5lZmZlY3RTbW9rZSk7XHJcbiAgICAgICAgICAgIC8vIGNjLmF1ZGlvRW5naW5lLnBsYXlFZmZlY3QoR2xvYmFsLnNvdW5kQXR0YWNrLCBmYWxzZSk7XHJcbiAgICAgICAgICAgIGNjLmF1ZGlvRW5naW5lLnBsYXlFZmZlY3QoR2xvYmFsLnNvdW5kS2F0YW5hLCBmYWxzZSk7XHJcbiAgICAgICAgfSwgMC41KTtcclxuXHJcbiAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICBpbnN0YW5jZS5lbWl0KEtleUV2ZW50LmFjdGl2ZUd1aWRlKTtcclxuICAgICAgICAgICAgR2xvYmFsLmJvb2xTdGFydEF0dGFja2luZyA9IGZhbHNlO1xyXG4gICAgICAgICAgICBpZiAodGhpcy5ub2RlLm5hbWUgPT0gXCJNeURlYWRwb29sXCIpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubm9kZS5nZXRDb21wb25lbnQoY2MuU2tlbGV0b25BbmltYXRpb24pLnBsYXkoXCJBcm1hdHVyZXxzd29yZF9pZGxlXCIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlLmdldENvbXBvbmVudChjYy5Ta2VsZXRvbkFuaW1hdGlvbikucGxheShcIkFybWF0dXJlfGhhbW1lcl9pZGxlXCIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSwgdGhpcy50aW1lQW5pbSk7XHJcbiAgICB9XHJcblxyXG4gICAgTGV2ZWxVcFBsYXllcigpIHtcclxuICAgICAgICB2YXIgdHdlZW4gPSBuZXcgY2MuVHdlZW4oKS50bygwLjQsIHsgc2NhbGU6IHRoaXMubm9kZS5zY2FsZSArIHRoaXMuc2NhbGUgfSk7XHJcbiAgICAgICAgdHdlZW4udGFyZ2V0KHRoaXMubm9kZSkuc3RhcnQoKTtcclxuICAgICAgICBpZiAodGhpcy5sZXZlbCA8IDQpXHJcbiAgICAgICAgICAgIHRoaXMubGV2ZWwrKztcclxuICAgIH1cclxuXHJcbiAgICBzcGF3bkVmZmVjdFNtb2tlKHNtb2tlOiBjYy5QcmVmYWIpIHtcclxuICAgICAgICBsZXQgc21rID0gY2MuaW5zdGFudGlhdGUoc21va2UpO1xyXG4gICAgICAgIHNtay5wYXJlbnQgPSBjYy5DYW52YXMuaW5zdGFuY2Uubm9kZTtcclxuICAgICAgICBsZXQgcG9zID0gdGhpcy5ub2RlLmNvbnZlcnRUb1dvcmxkU3BhY2VBUih0aGlzLnBsYWNlV2VhcG9uLmdldFBvc2l0aW9uKCkpO1xyXG4gICAgICAgIHBvcyA9IGNjLkNhbnZhcy5pbnN0YW5jZS5ub2RlLmNvbnZlcnRUb05vZGVTcGFjZUFSKHBvcyk7XHJcbiAgICAgICAgc21rLnggPSBwb3MueDtcclxuICAgICAgICBzbWsueSA9IHBvcy55O1xyXG4gICAgICAgIHNtay56ID0gMDtcclxuICAgIH1cclxufVxyXG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/PlatformBtn.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '1dd30jCURNKwqdKI3VVurRk', 'PlatformBtn');
// Scripts/Common/PlatformBtn.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var PlatformBtn = /** @class */ (function (_super) {
    __extends(PlatformBtn, _super);
    function PlatformBtn() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.androidSprites = null;
        _this.iosSprites = null;
        return _this;
    }
    PlatformBtn.prototype.start = function () {
        if (cc.sys.os == cc.sys.OS_ANDROID)
            this.getComponent(cc.Sprite).spriteFrame = this.androidSprites;
        else if (cc.sys.os == cc.sys.OS_IOS)
            this.getComponent(cc.Sprite).spriteFrame = this.iosSprites;
    };
    __decorate([
        property(cc.SpriteFrame)
    ], PlatformBtn.prototype, "androidSprites", void 0);
    __decorate([
        property(cc.SpriteFrame)
    ], PlatformBtn.prototype, "iosSprites", void 0);
    PlatformBtn = __decorate([
        ccclass
    ], PlatformBtn);
    return PlatformBtn;
}(cc.Component));
exports.default = PlatformBtn;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxQbGF0Zm9ybUJ0bi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBTSxJQUFBLGtCQUFtQyxFQUFsQyxvQkFBTyxFQUFFLHNCQUF5QixDQUFDO0FBRzFDO0lBQXlDLCtCQUFZO0lBRHJEO1FBQUEscUVBY0M7UUFYRyxvQkFBYyxHQUFtQixJQUFJLENBQUM7UUFFdEMsZ0JBQVUsR0FBbUIsSUFBSSxDQUFDOztJQVN0QyxDQUFDO0lBUkcsMkJBQUssR0FBTDtRQUVJLElBQUcsRUFBRSxDQUFDLEdBQUcsQ0FBQyxFQUFFLElBQUksRUFBRSxDQUFDLEdBQUcsQ0FBQyxVQUFVO1lBQzdCLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDO2FBQzlELElBQUcsRUFBRSxDQUFDLEdBQUcsQ0FBQyxFQUFFLElBQUksRUFBRSxDQUFDLEdBQUcsQ0FBQyxNQUFNO1lBQzlCLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO0lBQ25FLENBQUM7SUFURDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDO3VEQUNhO0lBRXRDO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUM7bURBQ1M7SUFKakIsV0FBVztRQUQvQixPQUFPO09BQ2EsV0FBVyxDQWEvQjtJQUFELGtCQUFDO0NBYkQsQUFhQyxDQWJ3QyxFQUFFLENBQUMsU0FBUyxHQWFwRDtrQkFib0IsV0FBVyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eX0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgUGxhdGZvcm1CdG4gZXh0ZW5kcyBjYy5Db21wb25lbnQge1xyXG4gICAgQHByb3BlcnR5KGNjLlNwcml0ZUZyYW1lKVxyXG4gICAgYW5kcm9pZFNwcml0ZXM6IGNjLlNwcml0ZUZyYW1lID0gbnVsbDtcclxuICAgIEBwcm9wZXJ0eShjYy5TcHJpdGVGcmFtZSlcclxuICAgIGlvc1Nwcml0ZXM6IGNjLlNwcml0ZUZyYW1lID0gbnVsbDtcclxuICAgIHN0YXJ0KClcclxuICAgIHtcclxuICAgICAgICBpZihjYy5zeXMub3MgPT0gY2Muc3lzLk9TX0FORFJPSUQpXHJcbiAgICAgICAgICAgIHRoaXMuZ2V0Q29tcG9uZW50KGNjLlNwcml0ZSkuc3ByaXRlRnJhbWUgPSB0aGlzLmFuZHJvaWRTcHJpdGVzO1xyXG4gICAgICAgIGVsc2UgaWYoY2Muc3lzLm9zID09IGNjLnN5cy5PU19JT1MpXHJcbiAgICAgICAgICAgIHRoaXMuZ2V0Q29tcG9uZW50KGNjLlNwcml0ZSkuc3ByaXRlRnJhbWUgPSB0aGlzLmlvc1Nwcml0ZXM7XHJcbiAgICB9XHJcbiAgICBcclxufVxyXG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/KeyEvent.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '9d7b6zZ1X1KDK3CFBTDTqHF', 'KeyEvent');
// Scripts/Common/KeyEvent.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var KeyEvent = {
    scale: "congra",
    checkAttacked: "checkAttacked",
    plusEnemy: "plusEnemy",
    plusCamera: "plusCamera",
    activeGuide: "activeGuide"
};
exports.default = KeyEvent;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxLZXlFdmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQU9BLElBQUksUUFBUSxHQUNaO0lBQ0ksS0FBSyxFQUFFLFFBQVE7SUFDZixhQUFhLEVBQUUsZUFBZTtJQUM5QixTQUFTLEVBQUUsV0FBVztJQUN0QixVQUFVLEVBQUUsWUFBWTtJQUN4QixXQUFXLEVBQUUsYUFBYTtDQUM3QixDQUFBO0FBQ0Qsa0JBQWUsUUFBUSxDQUFBIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW50ZXJmYWNlIEtleUV2ZW50IHtcclxuICAgIHNjYWxlOiBzdHJpbmcsXHJcbiAgICBjaGVja0F0dGFja2VkOiBzdHJpbmcsXHJcbiAgICBwbHVzRW5lbXk6IHN0cmluZyxcclxuICAgIHBsdXNDYW1lcmE6IHN0cmluZyxcclxuICAgIGFjdGl2ZUd1aWRlOiBzdHJpbmdcclxufVxyXG5sZXQgS2V5RXZlbnQ6IEtleUV2ZW50ID1cclxue1xyXG4gICAgc2NhbGU6IFwiY29uZ3JhXCIsXHJcbiAgICBjaGVja0F0dGFja2VkOiBcImNoZWNrQXR0YWNrZWRcIixcclxuICAgIHBsdXNFbmVteTogXCJwbHVzRW5lbXlcIixcclxuICAgIHBsdXNDYW1lcmE6IFwicGx1c0NhbWVyYVwiLFxyXG4gICAgYWN0aXZlR3VpZGU6IFwiYWN0aXZlR3VpZGVcIlxyXG59XHJcbmV4cG9ydCBkZWZhdWx0IEtleUV2ZW50Il19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/Random.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '698cbLh+RhKEI3RbCxjaWwp', 'Random');
// Scripts/Common/Random.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Singleton_1 = require("./Singleton");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Random = /** @class */ (function (_super) {
    __extends(Random, _super);
    function Random() {
        var _this = _super.call(this) || this;
        Random_1._instance = _this;
        return _this;
    }
    Random_1 = Random;
    Random.prototype.RandomRange = function (lower, upper) {
        return Math.random() * (upper - lower) + lower;
        //return Math.floor(Math.random() * (lower - lower)) + lower;
    };
    var Random_1;
    Random = Random_1 = __decorate([
        ccclass
    ], Random);
    return Random;
}(Singleton_1.default));
exports.default = Random;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxSYW5kb20udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEseUNBQW9DO0FBQzlCLElBQUEsa0JBQXFDLEVBQW5DLG9CQUFPLEVBQUUsc0JBQTBCLENBQUM7QUFHNUM7SUFBb0MsMEJBQWlCO0lBQ2pEO1FBQUEsWUFDSSxpQkFBTyxTQUVWO1FBREcsUUFBTSxDQUFDLFNBQVMsR0FBRyxLQUFJLENBQUM7O0lBQzVCLENBQUM7ZUFKZ0IsTUFBTTtJQUt2Qiw0QkFBVyxHQUFYLFVBQVksS0FBYSxFQUFFLEtBQWE7UUFDcEMsT0FBTyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDLEdBQUcsS0FBSyxDQUFDO1FBQy9DLDZEQUE2RDtJQUNqRSxDQUFDOztJQVJnQixNQUFNO1FBRDFCLE9BQU87T0FDYSxNQUFNLENBUzFCO0lBQUQsYUFBQztDQVRELEFBU0MsQ0FUbUMsbUJBQVMsR0FTNUM7a0JBVG9CLE1BQU0iLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgU2luZ2xldG9uIGZyb20gXCIuL1NpbmdsZXRvblwiO1xyXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgUmFuZG9tIGV4dGVuZHMgU2luZ2xldG9uPFJhbmRvbT4ge1xyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuICAgICAgICBSYW5kb20uX2luc3RhbmNlID0gdGhpcztcclxuICAgIH1cclxuICAgIFJhbmRvbVJhbmdlKGxvd2VyOiBudW1iZXIsIHVwcGVyOiBudW1iZXIpIHtcclxuICAgICAgICByZXR1cm4gTWF0aC5yYW5kb20oKSAqICh1cHBlciAtIGxvd2VyKSArIGxvd2VyO1xyXG4gICAgICAgIC8vcmV0dXJuIE1hdGguZmxvb3IoTWF0aC5yYW5kb20oKSAqIChsb3dlciAtIGxvd2VyKSkgKyBsb3dlcjtcclxuICAgIH1cclxufVxyXG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/VFX/BloodController.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '49cb46jVzNB6KWPjpyr+e3g', 'BloodController');
// Scripts/VFX/BloodController.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var BloodController = /** @class */ (function (_super) {
    __extends(BloodController, _super);
    function BloodController() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    BloodController.prototype.onLoad = function () {
        var tween = new cc.Tween().to(0.5, { scale: 600 });
        //tween.target(this.node.children[2]).start();
        tween.target(this.node).start();
    };
    BloodController = __decorate([
        ccclass
    ], BloodController);
    return BloodController;
}(cc.Component));
exports.default = BloodController;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcVkZYXFxCbG9vZENvbnRyb2xsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQU0sSUFBQSxrQkFBcUMsRUFBbkMsb0JBQU8sRUFBRSxzQkFBMEIsQ0FBQztBQUc1QztJQUE2QyxtQ0FBWTtJQUF6RDs7SUFNQSxDQUFDO0lBTEcsZ0NBQU0sR0FBTjtRQUNJLElBQUksS0FBSyxHQUFHLElBQUksRUFBRSxDQUFDLEtBQUssRUFBRSxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQztRQUNuRCw4Q0FBOEM7UUFDOUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDcEMsQ0FBQztJQUxnQixlQUFlO1FBRG5DLE9BQU87T0FDYSxlQUFlLENBTW5DO0lBQUQsc0JBQUM7Q0FORCxBQU1DLENBTjRDLEVBQUUsQ0FBQyxTQUFTLEdBTXhEO2tCQU5vQixlQUFlIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcclxuXHJcbkBjY2NsYXNzXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEJsb29kQ29udHJvbGxlciBleHRlbmRzIGNjLkNvbXBvbmVudCB7XHJcbiAgICBvbkxvYWQoKSB7XHJcbiAgICAgICAgdmFyIHR3ZWVuID0gbmV3IGNjLlR3ZWVuKCkudG8oMC41LCB7IHNjYWxlOiA2MDAgfSk7XHJcbiAgICAgICAgLy90d2Vlbi50YXJnZXQodGhpcy5ub2RlLmNoaWxkcmVuWzJdKS5zdGFydCgpO1xyXG4gICAgICAgIHR3ZWVuLnRhcmdldCh0aGlzLm5vZGUpLnN0YXJ0KCk7XHJcbiAgICB9XHJcbn0iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/GamePlay/StickMan2.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '0d8b754C3JKK6LSITAlYBhq', 'StickMan2');
// Scripts/GamePlay/StickMan2.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var KeyEvent_1 = require("../Common/KeyEvent");
var Singleton_1 = require("../Common/Singleton");
var EnemyController_1 = require("../Enemy/EnemyController");
var StickMan1_1 = require("./StickMan1");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var StickMan2 = /** @class */ (function (_super) {
    __extends(StickMan2, _super);
    function StickMan2() {
        var _this = _super.call(this) || this;
        _this.joyStickFollow = null;
        _this.nEndGame = null;
        _this.Map = null;
        _this.countEnemyEnd = 0;
        _this.MyCharacter = null;
        _this.characterMaterial = [];
        _this.katana = [];
        _this.Guide = null;
        _this.Effect = null;
        _this.txtSmasher = null;
        _this.enemyParent = null;
        _this.btnDownload = null;
        _this.nStart = null;
        _this.countEnemy = 0;
        _this.chooseDoneDeadpool = false;
        _this.chooseDoneIronMan = false;
        _this.boolcheckInteraction = false;
        _this.ironsource = false;
        _this.mindworks = false;
        _this.vungle = false;
        StickMan1_1.default._instance = _this;
        return _this;
    }
    StickMan2.prototype.onEnable = function () {
        GamePlayInstance_1.instance.on(KeyEvent_1.default.scale, this.scale, this);
        GamePlayInstance_1.instance.on(KeyEvent_1.default.plusEnemy, this.plusEnemy, this);
        GamePlayInstance_1.instance.on(KeyEvent_1.default.activeGuide, this.activeGuide, this);
    };
    StickMan2.prototype.onDisable = function () {
        GamePlayInstance_1.instance.off(KeyEvent_1.default.scale, this.scale, this);
        GamePlayInstance_1.instance.off(KeyEvent_1.default.plusEnemy, this.plusEnemy, this);
        GamePlayInstance_1.instance.off(KeyEvent_1.default.activeGuide, this.activeGuide, this);
    };
    StickMan2.prototype.start = function () {
        this.Guide.active = false;
        this.enemyParent.active = false;
    };
    StickMan2.prototype.chooseDeadpool = function () {
        this.MyCharacter.getChildByName('stickman_new').getComponent(cc.MeshRenderer).setMaterial(0, this.characterMaterial[0]);
        this.katana[0].active = true;
        this.chooseDone();
    };
    StickMan2.prototype.chooseIronMan = function () {
        this.MyCharacter.getChildByName('stickman_new').getComponent(cc.MeshRenderer).setMaterial(0, this.characterMaterial[1]);
        this.katana[1].active = true;
        this.chooseDone();
    };
    StickMan2.prototype.chooseDone = function () {
        if (this.ironsource) {
            window.NUC.trigger.interaction();
        }
        this.MyCharacter.active = true;
        this.nStart.active = false;
        this.Guide.active = true;
        this.enemyParent.active = true;
        this.playGame();
    };
    StickMan2.prototype.update = function (dt) {
        if (this.countEnemy == this.countEnemyEnd) {
            this.endGame();
        }
    };
    StickMan2.prototype.playGame = function () {
        Global_1.default.boolStartPlay = true;
        for (var i = 0; i < this.enemyParent.childrenCount; i++) {
            this.enemyParent.children[i].getComponent(EnemyController_1.default).StartMove();
        }
    };
    StickMan2.prototype.endGame = function () {
        var _this = this;
        if (this.mindworks) {
            window.gameEnd && window.gameEnd();
        }
        if (this.ironsource) {
            window.NUC.trigger.endGame('win');
        }
        if (this.vungle) {
            parent.postMessage('complete', '*');
        }
        this.scheduleOnce(function () {
            _this.MyCharacter.active = false;
            _this.nEndGame.active = true;
        }, 1.3);
        this.joyStickFollow.active = false;
        this.Guide.active = false;
    };
    StickMan2.prototype.scale = function () {
        var _this = this;
        this.Effect.scale = 0;
        this.Effect.opacity = 255;
        this.Effect.runAction(cc.sequence(cc.scaleTo(0.2, 1).easing(cc.easeBounceOut()), cc.callFunc(function () {
            _this.scheduleOnce(function () {
                _this.Effect.runAction(cc.fadeOut(0.1));
            }, 0.2);
        })));
    };
    StickMan2.prototype.plusEnemy = function () {
        if (this.countEnemy < this.countEnemyEnd)
            this.countEnemy++;
    };
    StickMan2.prototype.activeGuide = function () {
        if (Global_1.default.boolStartPlay)
            this.Guide.active = true;
    };
    __decorate([
        property(cc.Node)
    ], StickMan2.prototype, "joyStickFollow", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan2.prototype, "nEndGame", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan2.prototype, "Map", void 0);
    __decorate([
        property(cc.Integer)
    ], StickMan2.prototype, "countEnemyEnd", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan2.prototype, "MyCharacter", void 0);
    __decorate([
        property([cc.Material])
    ], StickMan2.prototype, "characterMaterial", void 0);
    __decorate([
        property([cc.Node])
    ], StickMan2.prototype, "katana", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan2.prototype, "Guide", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan2.prototype, "Effect", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan2.prototype, "txtSmasher", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan2.prototype, "enemyParent", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan2.prototype, "btnDownload", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan2.prototype, "nStart", void 0);
    StickMan2 = __decorate([
        ccclass
    ], StickMan2);
    return StickMan2;
}(Singleton_1.default));
exports.default = StickMan2;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcR2FtZVBsYXlcXFN0aWNrTWFuMi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQSwrREFBc0Q7QUFDdEQsMkNBQXNDO0FBQ3RDLCtDQUEwQztBQUMxQyxpREFBNEM7QUFDNUMsNERBQXVEO0FBQ3ZELHlDQUFvQztBQUU5QixJQUFBLGtCQUFxQyxFQUFuQyxvQkFBTyxFQUFFLHNCQUEwQixDQUFDO0FBSzVDO0lBQXVDLDZCQUFvQjtJQWtEdkQ7UUFBQSxZQUNJLGlCQUFPLFNBRVY7UUFsREQsb0JBQWMsR0FBWSxJQUFJLENBQUM7UUFHL0IsY0FBUSxHQUFZLElBQUksQ0FBQztRQUd6QixTQUFHLEdBQVksSUFBSSxDQUFDO1FBR3BCLG1CQUFhLEdBQVcsQ0FBQyxDQUFDO1FBRzFCLGlCQUFXLEdBQVksSUFBSSxDQUFDO1FBRzVCLHVCQUFpQixHQUFrQixFQUFFLENBQUM7UUFHdEMsWUFBTSxHQUFjLEVBQUUsQ0FBQztRQUd2QixXQUFLLEdBQVksSUFBSSxDQUFDO1FBR3RCLFlBQU0sR0FBWSxJQUFJLENBQUM7UUFHdkIsZ0JBQVUsR0FBWSxJQUFJLENBQUM7UUFHM0IsaUJBQVcsR0FBWSxJQUFJLENBQUM7UUFHNUIsaUJBQVcsR0FBWSxJQUFJLENBQUM7UUFHNUIsWUFBTSxHQUFZLElBQUksQ0FBQztRQUV2QixnQkFBVSxHQUFXLENBQUMsQ0FBQztRQUN2Qix3QkFBa0IsR0FBWSxLQUFLLENBQUM7UUFDcEMsdUJBQWlCLEdBQVksS0FBSyxDQUFDO1FBRW5DLDBCQUFvQixHQUFZLEtBQUssQ0FBQztRQUN0QyxnQkFBVSxHQUFZLEtBQUssQ0FBQztRQUM1QixlQUFTLEdBQVksS0FBSyxDQUFDO1FBQzNCLFlBQU0sR0FBWSxLQUFLLENBQUM7UUFJcEIsbUJBQVMsQ0FBQyxTQUFTLEdBQUcsS0FBSSxDQUFDOztJQUMvQixDQUFDO0lBRUQsNEJBQVEsR0FBUjtRQUNJLDJCQUFRLENBQUMsRUFBRSxDQUFDLGtCQUFRLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDOUMsMkJBQVEsQ0FBQyxFQUFFLENBQUMsa0JBQVEsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUN0RCwyQkFBUSxDQUFDLEVBQUUsQ0FBQyxrQkFBUSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQzlELENBQUM7SUFFRCw2QkFBUyxHQUFUO1FBQ0ksMkJBQVEsQ0FBQyxHQUFHLENBQUMsa0JBQVEsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztRQUMvQywyQkFBUSxDQUFDLEdBQUcsQ0FBQyxrQkFBUSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3ZELDJCQUFRLENBQUMsR0FBRyxDQUFDLGtCQUFRLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDL0QsQ0FBQztJQUVELHlCQUFLLEdBQUw7UUFDSSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDMUIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO0lBQ3BDLENBQUM7SUFFRCxrQ0FBYyxHQUFkO1FBQ0ksSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsY0FBYyxDQUFDLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxZQUFZLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3hILElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUM3QixJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDdEIsQ0FBQztJQUVELGlDQUFhLEdBQWI7UUFDSSxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLFlBQVksQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDeEgsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQzdCLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztJQUN0QixDQUFDO0lBRUQsOEJBQVUsR0FBVjtRQUNJLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNqQixNQUFNLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUNwQztRQUNELElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUMvQixJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDM0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUMvQixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDcEIsQ0FBQztJQUVELDBCQUFNLEdBQU4sVUFBTyxFQUFFO1FBQ0wsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7WUFDdkMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO1NBQ2xCO0lBQ0wsQ0FBQztJQUVELDRCQUFRLEdBQVI7UUFDSSxnQkFBTSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7UUFDNUIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3JELElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyx5QkFBZSxDQUFDLENBQUMsU0FBUyxFQUFFLENBQUM7U0FDMUU7SUFDTCxDQUFDO0lBRUQsMkJBQU8sR0FBUDtRQUFBLGlCQWdCQztRQWZHLElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNoQixNQUFNLENBQUMsT0FBTyxJQUFJLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztTQUN0QztRQUNELElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNqQixNQUFNLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUE7U0FDcEM7UUFDRCxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDYixNQUFNLENBQUMsV0FBVyxDQUFDLFVBQVUsRUFBRSxHQUFHLENBQUMsQ0FBQztTQUN2QztRQUNELElBQUksQ0FBQyxZQUFZLENBQUM7WUFDZCxLQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7WUFDaEMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ2hDLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUNSLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUNuQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7SUFDOUIsQ0FBQztJQUVELHlCQUFLLEdBQUw7UUFBQSxpQkFRQztRQVBHLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztRQUN0QixJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUM7UUFDMUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLGFBQWEsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLFFBQVEsQ0FBQztZQUN6RixLQUFJLENBQUMsWUFBWSxDQUFDO2dCQUNkLEtBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUMzQyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDWixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDVCxDQUFDO0lBRUQsNkJBQVMsR0FBVDtRQUNJLElBQUksSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsYUFBYTtZQUNwQyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDMUIsQ0FBQztJQUVELCtCQUFXLEdBQVg7UUFDSSxJQUFJLGdCQUFNLENBQUMsYUFBYTtZQUNwQixJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7SUFDakMsQ0FBQztJQTdJRDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO3FEQUNhO0lBRy9CO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7K0NBQ087SUFHekI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzswQ0FDRTtJQUdwQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDO29EQUNLO0lBRzFCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7a0RBQ1U7SUFHNUI7UUFEQyxRQUFRLENBQUMsQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUM7d0RBQ2M7SUFHdEM7UUFEQyxRQUFRLENBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUM7NkNBQ0c7SUFHdkI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQzs0Q0FDSTtJQUd0QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOzZDQUNLO0lBR3ZCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7aURBQ1M7SUFHM0I7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztrREFDVTtJQUc1QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO2tEQUNVO0lBRzVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7NkNBQ0s7SUF2Q04sU0FBUztRQUQ3QixPQUFPO09BQ2EsU0FBUyxDQWlKN0I7SUFBRCxnQkFBQztDQWpKRCxBQWlKQyxDQWpKc0MsbUJBQVMsR0FpSi9DO2tCQWpKb0IsU0FBUyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG5pbXBvcnQgeyBpbnN0YW5jZSB9IGZyb20gXCIuLi9Db21tb24vR2FtZVBsYXlJbnN0YW5jZVwiO1xyXG5pbXBvcnQgR2xvYmFsIGZyb20gXCIuLi9Db21tb24vR2xvYmFsXCI7XHJcbmltcG9ydCBLZXlFdmVudCBmcm9tIFwiLi4vQ29tbW9uL0tleUV2ZW50XCI7XHJcbmltcG9ydCBTaW5nbGV0b24gZnJvbSBcIi4uL0NvbW1vbi9TaW5nbGV0b25cIjtcclxuaW1wb3J0IEVuZW15Q29udHJvbGxlciBmcm9tIFwiLi4vRW5lbXkvRW5lbXlDb250cm9sbGVyXCI7XHJcbmltcG9ydCBTdGlja01hbjEgZnJvbSBcIi4vU3RpY2tNYW4xXCI7XHJcblxyXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuZGVjbGFyZSBjb25zdCB3aW5kb3c6IGFueTtcclxuXHJcbkBjY2NsYXNzXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFN0aWNrTWFuMiBleHRlbmRzIFNpbmdsZXRvbjxTdGlja01hbjE+IHtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGpveVN0aWNrRm9sbG93OiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIG5FbmRHYW1lOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIE1hcDogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLkludGVnZXIpXHJcbiAgICBjb3VudEVuZW15RW5kOiBudW1iZXIgPSAwO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgTXlDaGFyYWN0ZXI6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShbY2MuTWF0ZXJpYWxdKVxyXG4gICAgY2hhcmFjdGVyTWF0ZXJpYWw6IGNjLk1hdGVyaWFsW10gPSBbXTtcclxuXHJcbiAgICBAcHJvcGVydHkoW2NjLk5vZGVdKVxyXG4gICAga2F0YW5hOiBjYy5Ob2RlW10gPSBbXTtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIEd1aWRlOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIEVmZmVjdDogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLk5vZGUpXHJcbiAgICB0eHRTbWFzaGVyOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGVuZW15UGFyZW50OiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGJ0bkRvd25sb2FkOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIG5TdGFydDogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgY291bnRFbmVteTogbnVtYmVyID0gMDtcclxuICAgIGNob29zZURvbmVEZWFkcG9vbDogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgY2hvb3NlRG9uZUlyb25NYW46IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgICBib29sY2hlY2tJbnRlcmFjdGlvbjogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgaXJvbnNvdXJjZTogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgbWluZHdvcmtzOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICB2dW5nbGU6IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICBzdXBlcigpO1xyXG4gICAgICAgIFN0aWNrTWFuMS5faW5zdGFuY2UgPSB0aGlzO1xyXG4gICAgfVxyXG5cclxuICAgIG9uRW5hYmxlKCkge1xyXG4gICAgICAgIGluc3RhbmNlLm9uKEtleUV2ZW50LnNjYWxlLCB0aGlzLnNjYWxlLCB0aGlzKTtcclxuICAgICAgICBpbnN0YW5jZS5vbihLZXlFdmVudC5wbHVzRW5lbXksIHRoaXMucGx1c0VuZW15LCB0aGlzKTtcclxuICAgICAgICBpbnN0YW5jZS5vbihLZXlFdmVudC5hY3RpdmVHdWlkZSwgdGhpcy5hY3RpdmVHdWlkZSwgdGhpcyk7XHJcbiAgICB9XHJcblxyXG4gICAgb25EaXNhYmxlKCkge1xyXG4gICAgICAgIGluc3RhbmNlLm9mZihLZXlFdmVudC5zY2FsZSwgdGhpcy5zY2FsZSwgdGhpcyk7XHJcbiAgICAgICAgaW5zdGFuY2Uub2ZmKEtleUV2ZW50LnBsdXNFbmVteSwgdGhpcy5wbHVzRW5lbXksIHRoaXMpO1xyXG4gICAgICAgIGluc3RhbmNlLm9mZihLZXlFdmVudC5hY3RpdmVHdWlkZSwgdGhpcy5hY3RpdmVHdWlkZSwgdGhpcyk7XHJcbiAgICB9XHJcblxyXG4gICAgc3RhcnQoKSB7XHJcbiAgICAgICAgdGhpcy5HdWlkZS5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLmVuZW15UGFyZW50LmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIGNob29zZURlYWRwb29sKCkge1xyXG4gICAgICAgIHRoaXMuTXlDaGFyYWN0ZXIuZ2V0Q2hpbGRCeU5hbWUoJ3N0aWNrbWFuX25ldycpLmdldENvbXBvbmVudChjYy5NZXNoUmVuZGVyZXIpLnNldE1hdGVyaWFsKDAsIHRoaXMuY2hhcmFjdGVyTWF0ZXJpYWxbMF0pO1xyXG4gICAgICAgIHRoaXMua2F0YW5hWzBdLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5jaG9vc2VEb25lKCk7XHJcbiAgICB9XHJcblxyXG4gICAgY2hvb3NlSXJvbk1hbigpIHtcclxuICAgICAgICB0aGlzLk15Q2hhcmFjdGVyLmdldENoaWxkQnlOYW1lKCdzdGlja21hbl9uZXcnKS5nZXRDb21wb25lbnQoY2MuTWVzaFJlbmRlcmVyKS5zZXRNYXRlcmlhbCgwLCB0aGlzLmNoYXJhY3Rlck1hdGVyaWFsWzFdKTtcclxuICAgICAgICB0aGlzLmthdGFuYVsxXS5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuY2hvb3NlRG9uZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIGNob29zZURvbmUoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuaXJvbnNvdXJjZSkge1xyXG4gICAgICAgICAgICB3aW5kb3cuTlVDLnRyaWdnZXIuaW50ZXJhY3Rpb24oKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5NeUNoYXJhY3Rlci5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMublN0YXJ0LmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuR3VpZGUuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLmVuZW15UGFyZW50LmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5wbGF5R2FtZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZShkdCkge1xyXG4gICAgICAgIGlmICh0aGlzLmNvdW50RW5lbXkgPT0gdGhpcy5jb3VudEVuZW15RW5kKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZW5kR2FtZSgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwbGF5R2FtZSgpIHtcclxuICAgICAgICBHbG9iYWwuYm9vbFN0YXJ0UGxheSA9IHRydWU7XHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLmVuZW15UGFyZW50LmNoaWxkcmVuQ291bnQ7IGkrKykge1xyXG4gICAgICAgICAgICB0aGlzLmVuZW15UGFyZW50LmNoaWxkcmVuW2ldLmdldENvbXBvbmVudChFbmVteUNvbnRyb2xsZXIpLlN0YXJ0TW92ZSgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBlbmRHYW1lKCkge1xyXG4gICAgICAgIGlmICh0aGlzLm1pbmR3b3Jrcykge1xyXG4gICAgICAgICAgICB3aW5kb3cuZ2FtZUVuZCAmJiB3aW5kb3cuZ2FtZUVuZCgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5pcm9uc291cmNlKSB7XHJcbiAgICAgICAgICAgIHdpbmRvdy5OVUMudHJpZ2dlci5lbmRHYW1lKCd3aW4nKVxyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy52dW5nbGUpIHtcclxuICAgICAgICAgICAgcGFyZW50LnBvc3RNZXNzYWdlKCdjb21wbGV0ZScsICcqJyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuc2NoZWR1bGVPbmNlKCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5NeUNoYXJhY3Rlci5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5uRW5kR2FtZS5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgIH0sIDEuMyk7XHJcbiAgICAgICAgdGhpcy5qb3lTdGlja0ZvbGxvdy5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLkd1aWRlLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIHNjYWxlKCkge1xyXG4gICAgICAgIHRoaXMuRWZmZWN0LnNjYWxlID0gMDtcclxuICAgICAgICB0aGlzLkVmZmVjdC5vcGFjaXR5ID0gMjU1O1xyXG4gICAgICAgIHRoaXMuRWZmZWN0LnJ1bkFjdGlvbihjYy5zZXF1ZW5jZShjYy5zY2FsZVRvKDAuMiwgMSkuZWFzaW5nKGNjLmVhc2VCb3VuY2VPdXQoKSksIGNjLmNhbGxGdW5jKCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5FZmZlY3QucnVuQWN0aW9uKGNjLmZhZGVPdXQoMC4xKSk7XHJcbiAgICAgICAgICAgIH0sIDAuMik7XHJcbiAgICAgICAgfSkpKTtcclxuICAgIH1cclxuXHJcbiAgICBwbHVzRW5lbXkoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuY291bnRFbmVteSA8IHRoaXMuY291bnRFbmVteUVuZClcclxuICAgICAgICAgICAgdGhpcy5jb3VudEVuZW15Kys7XHJcbiAgICB9XHJcblxyXG4gICAgYWN0aXZlR3VpZGUoKSB7XHJcbiAgICAgICAgaWYgKEdsb2JhbC5ib29sU3RhcnRQbGF5KVxyXG4gICAgICAgICAgICB0aGlzLkd1aWRlLmFjdGl2ZSA9IHRydWU7XHJcbiAgICB9XHJcbn1cclxuIl19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/GamePlay/EndCard1.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '78c86RAMdJC/7MmMPOz0ZCZ', 'EndCard1');
// Scripts/GamePlay/EndCard1.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var EndCard1 = /** @class */ (function (_super) {
    __extends(EndCard1, _super);
    function EndCard1() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.ironsource = false;
        _this.mindworks = false;
        _this.vungle = false;
        return _this;
        // update (dt) {}
    }
    // LIFE-CYCLE CALLBACKS:
    // onLoad () {}
    EndCard1.prototype.start = function () {
        if (this.mindworks) {
            window.gameEnd && window.gameEnd();
        }
        if (this.ironsource) {
            window.NUC.trigger.endGame('win');
        }
        if (this.vungle) {
            parent.postMessage('complete', '*');
        }
    };
    EndCard1 = __decorate([
        ccclass
    ], EndCard1);
    return EndCard1;
}(cc.Component));
exports.default = EndCard1;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcR2FtZVBsYXlcXEVuZENhcmQxLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNNLElBQUEsa0JBQW1DLEVBQWxDLG9CQUFPLEVBQUUsc0JBQXlCLENBQUM7QUFLMUM7SUFBc0MsNEJBQVk7SUFEbEQ7UUFBQSxxRUF3QkM7UUFyQkcsZ0JBQVUsR0FBWSxLQUFLLENBQUM7UUFDNUIsZUFBUyxHQUFZLEtBQUssQ0FBQztRQUMzQixZQUFNLEdBQVksS0FBSyxDQUFDOztRQWtCeEIsaUJBQWlCO0lBQ3JCLENBQUM7SUFqQkcsd0JBQXdCO0lBRXhCLGVBQWU7SUFFZix3QkFBSyxHQUFMO1FBQ0ksSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2hCLE1BQU0sQ0FBQyxPQUFPLElBQUksTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO1NBQ3RDO1FBQ0QsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ2pCLE1BQU0sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQTtTQUNwQztRQUNELElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNiLE1BQU0sQ0FBQyxXQUFXLENBQUMsVUFBVSxFQUFFLEdBQUcsQ0FBQyxDQUFDO1NBQ3ZDO0lBQ0wsQ0FBQztJQXBCZ0IsUUFBUTtRQUQ1QixPQUFPO09BQ2EsUUFBUSxDQXVCNUI7SUFBRCxlQUFDO0NBdkJELEFBdUJDLENBdkJxQyxFQUFFLENBQUMsU0FBUyxHQXVCakQ7a0JBdkJvQixRQUFRIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eX0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuZGVjbGFyZSBjb25zdCB3aW5kb3c6IGFueTtcclxuXHJcbkBjY2NsYXNzXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEVuZENhcmQxIGV4dGVuZHMgY2MuQ29tcG9uZW50IHtcclxuXHJcbiAgICBpcm9uc291cmNlOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBtaW5kd29ya3M6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIHZ1bmdsZTogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuICAgIC8vIExJRkUtQ1lDTEUgQ0FMTEJBQ0tTOlxyXG5cclxuICAgIC8vIG9uTG9hZCAoKSB7fVxyXG5cclxuICAgIHN0YXJ0ICgpIHtcclxuICAgICAgICBpZiAodGhpcy5taW5kd29ya3MpIHtcclxuICAgICAgICAgICAgd2luZG93LmdhbWVFbmQgJiYgd2luZG93LmdhbWVFbmQoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMuaXJvbnNvdXJjZSkge1xyXG4gICAgICAgICAgICB3aW5kb3cuTlVDLnRyaWdnZXIuZW5kR2FtZSgnd2luJylcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMudnVuZ2xlKSB7XHJcbiAgICAgICAgICAgIHBhcmVudC5wb3N0TWVzc2FnZSgnY29tcGxldGUnLCAnKicpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvLyB1cGRhdGUgKGR0KSB7fVxyXG59XHJcbiJdfQ==
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/ColliderObj/SquareCollider.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '2b4c7MyE2JG67FTDw4URo8E', 'SquareCollider');
// Scripts/ColliderObj/SquareCollider.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SquareCollider = /** @class */ (function (_super) {
    __extends(SquareCollider, _super);
    function SquareCollider() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.high = 0;
        _this.positionLeft = null;
        _this.positionRight = null;
        _this.positionTop = null;
        _this.positionBottom = null;
        return _this;
    }
    SquareCollider.prototype.start = function () {
        this.positionLeft = new cc.Vec2(this.node.x - this.high, this.node.y);
        this.positionRight = new cc.Vec2(this.node.x + this.high, this.node.y);
        this.positionTop = new cc.Vec2(this.node.x, this.node.y + this.high);
        this.positionBottom = new cc.Vec2(this.node.x, this.node.y - this.high);
    };
    SquareCollider.prototype.OnTheLeft = function (player) {
        if (player.x < this.positionLeft.x && player.y < this.positionTop.y && player.y > this.positionBottom.y)
            return true;
        else
            return false;
    };
    SquareCollider.prototype.OnTheRight = function (player) {
        if (player.x > this.positionRight.x && player.y < this.positionTop.y && player.y > this.positionBottom.y)
            return true;
        else
            return false;
    };
    SquareCollider.prototype.OnTheTop = function (player) {
        if (player.y > this.positionTop.y && player.x < this.positionRight.x && player.x > this.positionLeft.x)
            return true;
        else
            return false;
    };
    SquareCollider.prototype.OnTheBottom = function (player) {
        if (player.y < this.positionBottom.y && player.x < this.positionRight.x && player.x > this.positionLeft.x)
            return true;
        else
            return false;
    };
    __decorate([
        property(cc.Float)
    ], SquareCollider.prototype, "high", void 0);
    SquareCollider = __decorate([
        ccclass
    ], SquareCollider);
    return SquareCollider;
}(cc.Component));
exports.default = SquareCollider;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29sbGlkZXJPYmpcXFNxdWFyZUNvbGxpZGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNNLElBQUEsa0JBQXFDLEVBQW5DLG9CQUFPLEVBQUUsc0JBQTBCLENBQUM7QUFHNUM7SUFBNEMsa0NBQVk7SUFEeEQ7UUFBQSxxRUE2Q0M7UUF6Q0csVUFBSSxHQUFXLENBQUMsQ0FBQztRQUVWLGtCQUFZLEdBQVksSUFBSSxDQUFDO1FBQzdCLG1CQUFhLEdBQVksSUFBSSxDQUFDO1FBQzlCLGlCQUFXLEdBQVksSUFBSSxDQUFDO1FBQzVCLG9CQUFjLEdBQVksSUFBSSxDQUFDOztJQW9DMUMsQ0FBQztJQWxDRyw4QkFBSyxHQUFMO1FBQ0ksSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3RFLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN2RSxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDckUsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzVFLENBQUM7SUFFTSxrQ0FBUyxHQUFoQixVQUFpQixNQUFlO1FBQzVCLElBQUksTUFBTSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsSUFBSSxNQUFNLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBQ25HLE9BQU8sSUFBSSxDQUFDOztZQUVaLE9BQU8sS0FBSyxDQUFDO0lBQ3JCLENBQUM7SUFFTSxtQ0FBVSxHQUFqQixVQUFrQixNQUFlO1FBQzdCLElBQUksTUFBTSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsSUFBSSxNQUFNLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBQ3BHLE9BQU8sSUFBSSxDQUFDOztZQUVaLE9BQU8sS0FBSyxDQUFDO0lBQ3JCLENBQUM7SUFFTSxpQ0FBUSxHQUFmLFVBQWdCLE1BQWU7UUFDM0IsSUFBSSxNQUFNLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLElBQUksTUFBTSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDbEcsT0FBTyxJQUFJLENBQUM7O1lBRVosT0FBTyxLQUFLLENBQUM7SUFDckIsQ0FBQztJQUVNLG9DQUFXLEdBQWxCLFVBQW1CLE1BQWU7UUFDOUIsSUFBSSxNQUFNLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLElBQUksTUFBTSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDckcsT0FBTyxJQUFJLENBQUM7O1lBRVosT0FBTyxLQUFLLENBQUM7SUFDckIsQ0FBQztJQXhDRDtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO2dEQUNGO0lBSEEsY0FBYztRQURsQyxPQUFPO09BQ2EsY0FBYyxDQTRDbEM7SUFBRCxxQkFBQztDQTVDRCxBQTRDQyxDQTVDMkMsRUFBRSxDQUFDLFNBQVMsR0E0Q3ZEO2tCQTVDb0IsY0FBYyIsImZpbGUiOiIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG5jb25zdCB7IGNjY2xhc3MsIHByb3BlcnR5IH0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuQGNjY2xhc3NcclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgU3F1YXJlQ29sbGlkZXIgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5GbG9hdClcclxuICAgIGhpZ2g6IG51bWJlciA9IDA7XHJcblxyXG4gICAgcHVibGljIHBvc2l0aW9uTGVmdDogY2MuVmVjMiA9IG51bGw7XHJcbiAgICBwdWJsaWMgcG9zaXRpb25SaWdodDogY2MuVmVjMiA9IG51bGw7XHJcbiAgICBwdWJsaWMgcG9zaXRpb25Ub3A6IGNjLlZlYzIgPSBudWxsO1xyXG4gICAgcHVibGljIHBvc2l0aW9uQm90dG9tOiBjYy5WZWMyID0gbnVsbDtcclxuXHJcbiAgICBzdGFydCgpIHtcclxuICAgICAgICB0aGlzLnBvc2l0aW9uTGVmdCA9IG5ldyBjYy5WZWMyKHRoaXMubm9kZS54IC0gdGhpcy5oaWdoLCB0aGlzLm5vZGUueSk7XHJcbiAgICAgICAgdGhpcy5wb3NpdGlvblJpZ2h0ID0gbmV3IGNjLlZlYzIodGhpcy5ub2RlLnggKyB0aGlzLmhpZ2gsIHRoaXMubm9kZS55KTtcclxuICAgICAgICB0aGlzLnBvc2l0aW9uVG9wID0gbmV3IGNjLlZlYzIodGhpcy5ub2RlLngsIHRoaXMubm9kZS55ICsgdGhpcy5oaWdoKTtcclxuICAgICAgICB0aGlzLnBvc2l0aW9uQm90dG9tID0gbmV3IGNjLlZlYzIodGhpcy5ub2RlLngsIHRoaXMubm9kZS55IC0gdGhpcy5oaWdoKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgT25UaGVMZWZ0KHBsYXllcjogY2MuTm9kZSk6IEJvb2xlYW4ge1xyXG4gICAgICAgIGlmIChwbGF5ZXIueCA8IHRoaXMucG9zaXRpb25MZWZ0LnggJiYgcGxheWVyLnkgPCB0aGlzLnBvc2l0aW9uVG9wLnkgJiYgcGxheWVyLnkgPiB0aGlzLnBvc2l0aW9uQm90dG9tLnkpXHJcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIGVsc2VcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBPblRoZVJpZ2h0KHBsYXllcjogY2MuTm9kZSk6IEJvb2xlYW4ge1xyXG4gICAgICAgIGlmIChwbGF5ZXIueCA+IHRoaXMucG9zaXRpb25SaWdodC54ICYmIHBsYXllci55IDwgdGhpcy5wb3NpdGlvblRvcC55ICYmIHBsYXllci55ID4gdGhpcy5wb3NpdGlvbkJvdHRvbS55KVxyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICBlbHNlXHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgT25UaGVUb3AocGxheWVyOiBjYy5Ob2RlKTogQm9vbGVhbiB7XHJcbiAgICAgICAgaWYgKHBsYXllci55ID4gdGhpcy5wb3NpdGlvblRvcC55ICYmIHBsYXllci54IDwgdGhpcy5wb3NpdGlvblJpZ2h0LnggJiYgcGxheWVyLnggPiB0aGlzLnBvc2l0aW9uTGVmdC54KVxyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICBlbHNlXHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgT25UaGVCb3R0b20ocGxheWVyOiBjYy5Ob2RlKTogQm9vbGVhbiB7XHJcbiAgICAgICAgaWYgKHBsYXllci55IDwgdGhpcy5wb3NpdGlvbkJvdHRvbS55ICYmIHBsYXllci54IDwgdGhpcy5wb3NpdGlvblJpZ2h0LnggJiYgcGxheWVyLnggPiB0aGlzLnBvc2l0aW9uTGVmdC54KVxyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICBlbHNlXHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxufVxyXG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/GamePlay/StickMan4.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'fa7375rkj5NwoiatN7H+fwx', 'StickMan4');
// Scripts/GamePlay/StickMan4.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var KeyEvent_1 = require("../Common/KeyEvent");
var Singleton_1 = require("../Common/Singleton");
var EnemyController_1 = require("../Enemy/EnemyController");
var StickMan1_1 = require("./StickMan1");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var StickMan4 = /** @class */ (function (_super) {
    __extends(StickMan4, _super);
    function StickMan4() {
        var _this = _super.call(this) || this;
        _this.joyStickFollow = null;
        _this.Map = null;
        _this.countEnemyEnd = 0;
        _this.MyCharacter = null;
        _this.Guide = null;
        _this.Effect = null;
        _this.enemyParent = null;
        _this.btnDownload = null;
        _this.endCard = null;
        _this.txtSmasher = null;
        _this.countEnemy = 0;
        _this.boolCheckEnd = false;
        _this.boolcheckInteraction = false;
        _this.ironsource = false;
        _this.mindworks = false;
        _this.vungle = false;
        StickMan1_1.default._instance = _this;
        return _this;
    }
    StickMan4.prototype.onEnable = function () {
        GamePlayInstance_1.instance.on(KeyEvent_1.default.scale, this.scale, this);
        GamePlayInstance_1.instance.on(KeyEvent_1.default.plusEnemy, this.plusEnemy, this);
        GamePlayInstance_1.instance.on(KeyEvent_1.default.activeGuide, this.activeGuide, this);
    };
    StickMan4.prototype.onDisable = function () {
        GamePlayInstance_1.instance.off(KeyEvent_1.default.scale, this.scale, this);
        GamePlayInstance_1.instance.off(KeyEvent_1.default.plusEnemy, this.plusEnemy, this);
        GamePlayInstance_1.instance.off(KeyEvent_1.default.activeGuide, this.activeGuide, this);
    };
    StickMan4.prototype.start = function () {
        cc.audioEngine.play(Global_1.default.soundBG, true, 1);
        this.playGame();
    };
    StickMan4.prototype.update = function (dt) {
        var _this = this;
        if (Global_1.default.boolEnableTouch && !this.boolcheckInteraction) {
            this.txtSmasher.active = false;
            if (this.ironsource) {
                window.NUC.trigger.interaction();
            }
        }
        if (this.countEnemy == this.countEnemyEnd && !this.boolCheckEnd) {
            Global_1.default.boolStartPlay = false;
            this.boolCheckEnd = true;
            this.scheduleOnce(function () {
                _this.EndGame();
            }, 1);
        }
    };
    StickMan4.prototype.playGame = function () {
        this.MyCharacter.active = true;
        Global_1.default.boolStartPlay = true;
        for (var i = 0; i < this.enemyParent.childrenCount; i++) {
            this.enemyParent.children[i].getComponent(EnemyController_1.default).StartMove();
        }
    };
    StickMan4.prototype.EndGame = function () {
        Global_1.default.boolendG = true;
        cc.audioEngine.stopAllEffects();
        this.joyStickFollow.active = false;
        this.Guide.active = false;
        this.endCard.active = true;
        if (this.mindworks) {
            window.gameEnd && window.gameEnd();
        }
        if (this.ironsource) {
            window.NUC.trigger.endGame('win');
        }
        if (this.vungle) {
            parent.postMessage('complete', '*');
        }
    };
    StickMan4.prototype.scale = function () {
        var _this = this;
        this.Effect.scale = 0;
        this.Effect.opacity = 255;
        this.Effect.runAction(cc.sequence(cc.scaleTo(0.2, 1).easing(cc.easeBounceOut()), cc.callFunc(function () {
            _this.scheduleOnce(function () {
                _this.Effect.runAction(cc.fadeOut(0.1));
            }, 0.2);
        })));
    };
    StickMan4.prototype.plusEnemy = function () {
        if (this.countEnemy < this.countEnemyEnd)
            this.countEnemy++;
    };
    StickMan4.prototype.activeGuide = function () {
        if (Global_1.default.boolStartPlay)
            this.Guide.active = true;
    };
    __decorate([
        property(cc.Node)
    ], StickMan4.prototype, "joyStickFollow", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan4.prototype, "Map", void 0);
    __decorate([
        property(cc.Integer)
    ], StickMan4.prototype, "countEnemyEnd", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan4.prototype, "MyCharacter", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan4.prototype, "Guide", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan4.prototype, "Effect", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan4.prototype, "enemyParent", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan4.prototype, "btnDownload", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan4.prototype, "endCard", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan4.prototype, "txtSmasher", void 0);
    StickMan4 = __decorate([
        ccclass
    ], StickMan4);
    return StickMan4;
}(Singleton_1.default));
exports.default = StickMan4;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcR2FtZVBsYXlcXFN0aWNrTWFuNC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQSwrREFBc0Q7QUFDdEQsMkNBQXNDO0FBQ3RDLCtDQUEwQztBQUMxQyxpREFBNEM7QUFDNUMsNERBQXVEO0FBQ3ZELHlDQUFvQztBQUU5QixJQUFBLGtCQUFtQyxFQUFsQyxvQkFBTyxFQUFFLHNCQUF5QixDQUFDO0FBSzFDO0lBQXVDLDZCQUFvQjtJQXdDdkQ7UUFBQSxZQUNJLGlCQUFPLFNBRVY7UUF4Q0Qsb0JBQWMsR0FBWSxJQUFJLENBQUM7UUFHL0IsU0FBRyxHQUFZLElBQUksQ0FBQztRQUdwQixtQkFBYSxHQUFXLENBQUMsQ0FBQztRQUcxQixpQkFBVyxHQUFZLElBQUksQ0FBQztRQUc1QixXQUFLLEdBQVksSUFBSSxDQUFDO1FBR3RCLFlBQU0sR0FBWSxJQUFJLENBQUM7UUFHdkIsaUJBQVcsR0FBWSxJQUFJLENBQUM7UUFHNUIsaUJBQVcsR0FBWSxJQUFJLENBQUM7UUFHNUIsYUFBTyxHQUFZLElBQUksQ0FBQztRQUd4QixnQkFBVSxHQUFZLElBQUksQ0FBQztRQUUzQixnQkFBVSxHQUFXLENBQUMsQ0FBQztRQUN2QixrQkFBWSxHQUFZLEtBQUssQ0FBQztRQUU5QiwwQkFBb0IsR0FBWSxLQUFLLENBQUM7UUFDdEMsZ0JBQVUsR0FBWSxLQUFLLENBQUM7UUFDNUIsZUFBUyxHQUFZLEtBQUssQ0FBQztRQUMzQixZQUFNLEdBQVksS0FBSyxDQUFDO1FBSXBCLG1CQUFTLENBQUMsU0FBUyxHQUFHLEtBQUksQ0FBQzs7SUFDL0IsQ0FBQztJQUVELDRCQUFRLEdBQVI7UUFDSSwyQkFBUSxDQUFDLEVBQUUsQ0FBQyxrQkFBUSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzlDLDJCQUFRLENBQUMsRUFBRSxDQUFDLGtCQUFRLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDdEQsMkJBQVEsQ0FBQyxFQUFFLENBQUMsa0JBQVEsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUM5RCxDQUFDO0lBRUQsNkJBQVMsR0FBVDtRQUNJLDJCQUFRLENBQUMsR0FBRyxDQUFDLGtCQUFRLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDL0MsMkJBQVEsQ0FBQyxHQUFHLENBQUMsa0JBQVEsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUN2RCwyQkFBUSxDQUFDLEdBQUcsQ0FBQyxrQkFBUSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQy9ELENBQUM7SUFFRCx5QkFBSyxHQUFMO1FBQ0ksRUFBRSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsZ0JBQU0sQ0FBQyxPQUFPLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQzdDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNwQixDQUFDO0lBRUQsMEJBQU0sR0FBTixVQUFRLEVBQUU7UUFBVixpQkFlQztRQWRHLElBQUksZ0JBQU0sQ0FBQyxlQUFlLElBQUksQ0FBQyxJQUFJLENBQUMsb0JBQW9CLEVBQUU7WUFDdEQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1lBQy9CLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtnQkFDakIsTUFBTSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLENBQUM7YUFDcEM7U0FDSjtRQUVELElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsYUFBYSxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRTtZQUM3RCxnQkFBTSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7WUFDN0IsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7WUFDekIsSUFBSSxDQUFDLFlBQVksQ0FBQztnQkFDZCxLQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDbkIsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ1Q7SUFDTCxDQUFDO0lBRUQsNEJBQVEsR0FBUjtRQUNJLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUMvQixnQkFBTSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7UUFDNUIsS0FBSSxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3BELElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyx5QkFBZSxDQUFDLENBQUMsU0FBUyxFQUFFLENBQUM7U0FDMUU7SUFDTCxDQUFDO0lBRUQsMkJBQU8sR0FBUDtRQUNJLGdCQUFNLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztRQUN2QixFQUFFLENBQUMsV0FBVyxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ2hDLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUNuQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDMUIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQzNCLElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNoQixNQUFNLENBQUMsT0FBTyxJQUFJLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztTQUN0QztRQUNELElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNqQixNQUFNLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUE7U0FDcEM7UUFDRCxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDYixNQUFNLENBQUMsV0FBVyxDQUFDLFVBQVUsRUFBRSxHQUFHLENBQUMsQ0FBQztTQUN2QztJQUNMLENBQUM7SUFFRCx5QkFBSyxHQUFMO1FBQUEsaUJBUUM7UUFQRyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7UUFDdEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDO1FBQzFCLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxhQUFhLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxRQUFRLENBQUM7WUFDekYsS0FBSSxDQUFDLFlBQVksQ0FBQztnQkFDZCxLQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDM0MsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ1osQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ1QsQ0FBQztJQUVELDZCQUFTLEdBQVQ7UUFDSSxJQUFJLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLGFBQWE7WUFDcEMsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO0lBQzFCLENBQUM7SUFFRCwrQkFBVyxHQUFYO1FBQ0ksSUFBRyxnQkFBTSxDQUFDLGFBQWE7WUFDbkIsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO0lBQ2pDLENBQUM7SUF2SEQ7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztxREFDYTtJQUcvQjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOzBDQUNFO0lBR3BCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUM7b0RBQ0s7SUFHMUI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztrREFDVTtJQUc1QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDOzRDQUNJO0lBR3RCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7NkNBQ0s7SUFHdkI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztrREFDVTtJQUc1QjtRQURDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDO2tEQUNVO0lBRzVCO1FBREMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7OENBQ007SUFHeEI7UUFEQyxRQUFRLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztpREFDUztJQTlCVixTQUFTO1FBRDdCLE9BQU87T0FDYSxTQUFTLENBMkg3QjtJQUFELGdCQUFDO0NBM0hELEFBMkhDLENBM0hzQyxtQkFBUyxHQTJIL0M7a0JBM0hvQixTQUFTIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IENoYXJhY3RlckNvbnRyb2xsZXIgZnJvbSBcIi4uL0NoYXJhY3Rlci9DaGFyYWN0ZXJDb250cm9sbGVyXCI7XHJcbmltcG9ydCB7IGluc3RhbmNlIH0gZnJvbSBcIi4uL0NvbW1vbi9HYW1lUGxheUluc3RhbmNlXCI7XHJcbmltcG9ydCBHbG9iYWwgZnJvbSBcIi4uL0NvbW1vbi9HbG9iYWxcIjtcclxuaW1wb3J0IEtleUV2ZW50IGZyb20gXCIuLi9Db21tb24vS2V5RXZlbnRcIjtcclxuaW1wb3J0IFNpbmdsZXRvbiBmcm9tIFwiLi4vQ29tbW9uL1NpbmdsZXRvblwiO1xyXG5pbXBvcnQgRW5lbXlDb250cm9sbGVyIGZyb20gXCIuLi9FbmVteS9FbmVteUNvbnRyb2xsZXJcIjtcclxuaW1wb3J0IFN0aWNrTWFuMSBmcm9tIFwiLi9TdGlja01hbjFcIjtcclxuXHJcbmNvbnN0IHtjY2NsYXNzLCBwcm9wZXJ0eX0gPSBjYy5fZGVjb3JhdG9yO1xyXG5cclxuZGVjbGFyZSBjb25zdCB3aW5kb3c6IGFueTtcclxuXHJcbkBjY2NsYXNzXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFN0aWNrTWFuNCBleHRlbmRzIFNpbmdsZXRvbjxTdGlja01hbjE+IHtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGpveVN0aWNrRm9sbG93OiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIE1hcDogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgQHByb3BlcnR5KGNjLkludGVnZXIpXHJcbiAgICBjb3VudEVuZW15RW5kOiBudW1iZXIgPSAwO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgTXlDaGFyYWN0ZXI6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgR3VpZGU6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgRWZmZWN0OiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGVuZW15UGFyZW50OiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGJ0bkRvd25sb2FkOiBjYy5Ob2RlID0gbnVsbDtcclxuXHJcbiAgICBAcHJvcGVydHkoY2MuTm9kZSlcclxuICAgIGVuZENhcmQ6IGNjLk5vZGUgPSBudWxsO1xyXG5cclxuICAgIEBwcm9wZXJ0eShjYy5Ob2RlKVxyXG4gICAgdHh0U21hc2hlcjogY2MuTm9kZSA9IG51bGw7XHJcblxyXG4gICAgY291bnRFbmVteTogbnVtYmVyID0gMDtcclxuICAgIGJvb2xDaGVja0VuZDogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuICAgIGJvb2xjaGVja0ludGVyYWN0aW9uOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBpcm9uc291cmNlOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBtaW5kd29ya3M6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIHZ1bmdsZTogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIHN1cGVyKCk7XHJcbiAgICAgICAgU3RpY2tNYW4xLl9pbnN0YW5jZSA9IHRoaXM7XHJcbiAgICB9XHJcblxyXG4gICAgb25FbmFibGUoKSB7XHJcbiAgICAgICAgaW5zdGFuY2Uub24oS2V5RXZlbnQuc2NhbGUsIHRoaXMuc2NhbGUsIHRoaXMpO1xyXG4gICAgICAgIGluc3RhbmNlLm9uKEtleUV2ZW50LnBsdXNFbmVteSwgdGhpcy5wbHVzRW5lbXksIHRoaXMpO1xyXG4gICAgICAgIGluc3RhbmNlLm9uKEtleUV2ZW50LmFjdGl2ZUd1aWRlLCB0aGlzLmFjdGl2ZUd1aWRlLCB0aGlzKTtcclxuICAgIH1cclxuXHJcbiAgICBvbkRpc2FibGUoKSB7XHJcbiAgICAgICAgaW5zdGFuY2Uub2ZmKEtleUV2ZW50LnNjYWxlLCB0aGlzLnNjYWxlLCB0aGlzKTtcclxuICAgICAgICBpbnN0YW5jZS5vZmYoS2V5RXZlbnQucGx1c0VuZW15LCB0aGlzLnBsdXNFbmVteSwgdGhpcyk7XHJcbiAgICAgICAgaW5zdGFuY2Uub2ZmKEtleUV2ZW50LmFjdGl2ZUd1aWRlLCB0aGlzLmFjdGl2ZUd1aWRlLCB0aGlzKTtcclxuICAgIH1cclxuXHJcbiAgICBzdGFydCgpIHtcclxuICAgICAgICBjYy5hdWRpb0VuZ2luZS5wbGF5KEdsb2JhbC5zb3VuZEJHLCB0cnVlLCAxKTtcclxuICAgICAgICB0aGlzLnBsYXlHYW1lKCk7XHJcbiAgICB9XHJcblxyXG4gICAgdXBkYXRlIChkdCkge1xyXG4gICAgICAgIGlmIChHbG9iYWwuYm9vbEVuYWJsZVRvdWNoICYmICF0aGlzLmJvb2xjaGVja0ludGVyYWN0aW9uKSB7XHJcbiAgICAgICAgICAgIHRoaXMudHh0U21hc2hlci5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgaWYgKHRoaXMuaXJvbnNvdXJjZSkge1xyXG4gICAgICAgICAgICAgICAgd2luZG93Lk5VQy50cmlnZ2VyLmludGVyYWN0aW9uKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICh0aGlzLmNvdW50RW5lbXkgPT0gdGhpcy5jb3VudEVuZW15RW5kICYmICF0aGlzLmJvb2xDaGVja0VuZCkge1xyXG4gICAgICAgICAgICBHbG9iYWwuYm9vbFN0YXJ0UGxheSA9IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLmJvb2xDaGVja0VuZCA9IHRydWU7XHJcbiAgICAgICAgICAgIHRoaXMuc2NoZWR1bGVPbmNlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuRW5kR2FtZSgpO1xyXG4gICAgICAgICAgICB9LCAxKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcGxheUdhbWUoKSB7XHJcbiAgICAgICAgdGhpcy5NeUNoYXJhY3Rlci5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgIEdsb2JhbC5ib29sU3RhcnRQbGF5ID0gdHJ1ZTtcclxuICAgICAgICBmb3IobGV0IGkgPSAwOyBpIDwgdGhpcy5lbmVteVBhcmVudC5jaGlsZHJlbkNvdW50OyBpKyspIHtcclxuICAgICAgICAgICAgdGhpcy5lbmVteVBhcmVudC5jaGlsZHJlbltpXS5nZXRDb21wb25lbnQoRW5lbXlDb250cm9sbGVyKS5TdGFydE1vdmUoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgRW5kR2FtZSgpIHtcclxuICAgICAgICBHbG9iYWwuYm9vbGVuZEcgPSB0cnVlO1xyXG4gICAgICAgIGNjLmF1ZGlvRW5naW5lLnN0b3BBbGxFZmZlY3RzKCk7XHJcbiAgICAgICAgdGhpcy5qb3lTdGlja0ZvbGxvdy5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLkd1aWRlLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuZW5kQ2FyZC5hY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgIGlmICh0aGlzLm1pbmR3b3Jrcykge1xyXG4gICAgICAgICAgICB3aW5kb3cuZ2FtZUVuZCAmJiB3aW5kb3cuZ2FtZUVuZCgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5pcm9uc291cmNlKSB7XHJcbiAgICAgICAgICAgIHdpbmRvdy5OVUMudHJpZ2dlci5lbmRHYW1lKCd3aW4nKVxyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy52dW5nbGUpIHtcclxuICAgICAgICAgICAgcGFyZW50LnBvc3RNZXNzYWdlKCdjb21wbGV0ZScsICcqJyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHNjYWxlKCkge1xyXG4gICAgICAgIHRoaXMuRWZmZWN0LnNjYWxlID0gMDtcclxuICAgICAgICB0aGlzLkVmZmVjdC5vcGFjaXR5ID0gMjU1O1xyXG4gICAgICAgIHRoaXMuRWZmZWN0LnJ1bkFjdGlvbihjYy5zZXF1ZW5jZShjYy5zY2FsZVRvKDAuMiwgMSkuZWFzaW5nKGNjLmVhc2VCb3VuY2VPdXQoKSksIGNjLmNhbGxGdW5jKCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5FZmZlY3QucnVuQWN0aW9uKGNjLmZhZGVPdXQoMC4xKSk7XHJcbiAgICAgICAgICAgIH0sIDAuMik7XHJcbiAgICAgICAgfSkpKTtcclxuICAgIH1cclxuXHJcbiAgICBwbHVzRW5lbXkoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuY291bnRFbmVteSA8IHRoaXMuY291bnRFbmVteUVuZClcclxuICAgICAgICAgICAgdGhpcy5jb3VudEVuZW15Kys7XHJcbiAgICB9XHJcblxyXG4gICAgYWN0aXZlR3VpZGUoKSB7XHJcbiAgICAgICAgaWYoR2xvYmFsLmJvb2xTdGFydFBsYXkpXHJcbiAgICAgICAgICAgIHRoaXMuR3VpZGUuYWN0aXZlID0gdHJ1ZTtcclxuICAgIH1cclxufVxyXG4iXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/Utility.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '379e6qJyltJY6bKTs9IwXUo', 'Utility');
// Scripts/Common/Utility.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Singleton_1 = require("./Singleton");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Utility = /** @class */ (function (_super) {
    __extends(Utility, _super);
    function Utility() {
        var _this = _super.call(this) || this;
        Utility_1._instance = _this;
        return _this;
    }
    Utility_1 = Utility;
    Utility.prototype.RandomRange = function (lower, upper) {
        return Math.random() * (upper - lower) + lower;
        //return Math.floor(Math.random() * (lower - lower)) + lower;
    };
    Utility.prototype.Distance = function (vec1, vec2) {
        var Distance = Math.sqrt(Math.pow(vec1.x - vec2.x, 2) +
            Math.pow(vec1.y - vec2.y, 2));
        return Distance;
    };
    var Utility_1;
    Utility = Utility_1 = __decorate([
        ccclass
    ], Utility);
    return Utility;
}(Singleton_1.default));
exports.default = Utility;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxVdGlsaXR5LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLHlDQUFvQztBQUU5QixJQUFBLGtCQUFxQyxFQUFuQyxvQkFBTyxFQUFFLHNCQUEwQixDQUFDO0FBRzVDO0lBQXFDLDJCQUFrQjtJQUNuRDtRQUFBLFlBQ0ksaUJBQU8sU0FFVjtRQURHLFNBQU8sQ0FBQyxTQUFTLEdBQUcsS0FBSSxDQUFDOztJQUM3QixDQUFDO2dCQUpnQixPQUFPO0lBS3hCLDZCQUFXLEdBQVgsVUFBWSxLQUFhLEVBQUUsS0FBYTtRQUNwQyxPQUFPLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsR0FBRyxLQUFLLENBQUM7UUFDL0MsNkRBQTZEO0lBQ2pFLENBQUM7SUFDRCwwQkFBUSxHQUFSLFVBQVMsSUFBYSxFQUFFLElBQWE7UUFDakMsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDakQsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNsQyxPQUFPLFFBQVEsQ0FBQztJQUNwQixDQUFDOztJQWJnQixPQUFPO1FBRDNCLE9BQU87T0FDYSxPQUFPLENBYzNCO0lBQUQsY0FBQztDQWRELEFBY0MsQ0Fkb0MsbUJBQVMsR0FjN0M7a0JBZG9CLE9BQU8iLCJmaWxlIjoiIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgU2luZ2xldG9uIGZyb20gXCIuL1NpbmdsZXRvblwiO1xyXG5cclxuY29uc3QgeyBjY2NsYXNzLCBwcm9wZXJ0eSB9ID0gY2MuX2RlY29yYXRvcjtcclxuXHJcbkBjY2NsYXNzXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFV0aWxpdHkgZXh0ZW5kcyBTaW5nbGV0b248VXRpbGl0eT4ge1xyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuICAgICAgICBVdGlsaXR5Ll9pbnN0YW5jZSA9IHRoaXM7XHJcbiAgICB9XHJcbiAgICBSYW5kb21SYW5nZShsb3dlcjogbnVtYmVyLCB1cHBlcjogbnVtYmVyKSB7XHJcbiAgICAgICAgcmV0dXJuIE1hdGgucmFuZG9tKCkgKiAodXBwZXIgLSBsb3dlcikgKyBsb3dlcjtcclxuICAgICAgICAvL3JldHVybiBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiAobG93ZXIgLSBsb3dlcikpICsgbG93ZXI7XHJcbiAgICB9XHJcbiAgICBEaXN0YW5jZSh2ZWMxOiBjYy5WZWMyLCB2ZWMyOiBjYy5WZWMyKSB7XHJcbiAgICAgICAgbGV0IERpc3RhbmNlID0gTWF0aC5zcXJ0KE1hdGgucG93KHZlYzEueCAtIHZlYzIueCwgMikgK1xyXG4gICAgICAgICAgICBNYXRoLnBvdyh2ZWMxLnkgLSB2ZWMyLnksIDIpKTtcclxuICAgICAgICByZXR1cm4gRGlzdGFuY2U7XHJcbiAgICB9XHJcbn1cclxuIl19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/SoundManager.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'a465ftqP7xJ+4lT0kSrGOjc', 'SoundManager');
// Scripts/Common/SoundManager.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var Global_1 = require("./Global");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SoundManager = /** @class */ (function (_super) {
    __extends(SoundManager, _super);
    function SoundManager() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.Bg = null;
        _this.footStep = null;
        _this.Intro = null;
        _this.Attack = null;
        _this.katana = null;
        _this.Scream = null;
        _this.Spin = null;
        _this.Reward = null;
        _this.clickBtn = null;
        return _this;
    }
    SoundManager.prototype.onLoad = function () {
        Global_1.default.soundBG = this.Bg;
        Global_1.default.soundIntro = this.Intro;
        Global_1.default.soundFootStep = this.footStep;
        Global_1.default.soundAttack = this.Attack;
        Global_1.default.soundKatana = this.katana;
        Global_1.default.soundScream = this.Scream;
        Global_1.default.soundSpin = this.Spin;
        Global_1.default.soundReward = this.Reward;
        Global_1.default.soundClickBtn = this.clickBtn;
    };
    __decorate([
        property({
            type: cc.AudioClip
        })
    ], SoundManager.prototype, "Bg", void 0);
    __decorate([
        property({
            type: cc.AudioClip
        })
    ], SoundManager.prototype, "footStep", void 0);
    __decorate([
        property({
            type: cc.AudioClip
        })
    ], SoundManager.prototype, "Intro", void 0);
    __decorate([
        property({
            type: cc.AudioClip
        })
    ], SoundManager.prototype, "Attack", void 0);
    __decorate([
        property({
            type: cc.AudioClip
        })
    ], SoundManager.prototype, "katana", void 0);
    __decorate([
        property({
            type: cc.AudioClip
        })
    ], SoundManager.prototype, "Scream", void 0);
    __decorate([
        property({
            type: cc.AudioClip
        })
    ], SoundManager.prototype, "Spin", void 0);
    __decorate([
        property({
            type: cc.AudioClip
        })
    ], SoundManager.prototype, "Reward", void 0);
    __decorate([
        property({
            type: cc.AudioClip
        })
    ], SoundManager.prototype, "clickBtn", void 0);
    SoundManager = __decorate([
        ccclass
    ], SoundManager);
    return SoundManager;
}(cc.Component));
exports.default = SoundManager;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxTb3VuZE1hbmFnZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsbUNBQThCO0FBRXhCLElBQUEsa0JBQXFDLEVBQW5DLG9CQUFPLEVBQUUsc0JBQTBCLENBQUM7QUFHNUM7SUFBMEMsZ0NBQVk7SUFEdEQ7UUFBQSxxRUFpREM7UUE1Q0csUUFBRSxHQUFpQixJQUFJLENBQUM7UUFJeEIsY0FBUSxHQUFpQixJQUFJLENBQUM7UUFJOUIsV0FBSyxHQUFpQixJQUFJLENBQUM7UUFJM0IsWUFBTSxHQUFpQixJQUFJLENBQUM7UUFJNUIsWUFBTSxHQUFpQixJQUFJLENBQUM7UUFJNUIsWUFBTSxHQUFpQixJQUFJLENBQUM7UUFJNUIsVUFBSSxHQUFpQixJQUFJLENBQUM7UUFJMUIsWUFBTSxHQUFpQixJQUFJLENBQUM7UUFJNUIsY0FBUSxHQUFpQixJQUFJLENBQUM7O0lBWWxDLENBQUM7SUFYRyw2QkFBTSxHQUFOO1FBQ0ksZ0JBQU0sQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQztRQUN6QixnQkFBTSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQy9CLGdCQUFNLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDckMsZ0JBQU0sQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUNqQyxnQkFBTSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ2pDLGdCQUFNLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDakMsZ0JBQU0sQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztRQUM3QixnQkFBTSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ2pDLGdCQUFNLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDekMsQ0FBQztJQTNDRDtRQUhDLFFBQVEsQ0FBQztZQUNOLElBQUksRUFBRSxFQUFFLENBQUMsU0FBUztTQUNyQixDQUFDOzRDQUNzQjtJQUl4QjtRQUhDLFFBQVEsQ0FBQztZQUNOLElBQUksRUFBRSxFQUFFLENBQUMsU0FBUztTQUNyQixDQUFDO2tEQUM0QjtJQUk5QjtRQUhDLFFBQVEsQ0FBQztZQUNOLElBQUksRUFBRSxFQUFFLENBQUMsU0FBUztTQUNyQixDQUFDOytDQUN5QjtJQUkzQjtRQUhDLFFBQVEsQ0FBQztZQUNOLElBQUksRUFBRSxFQUFFLENBQUMsU0FBUztTQUNyQixDQUFDO2dEQUMwQjtJQUk1QjtRQUhDLFFBQVEsQ0FBQztZQUNOLElBQUksRUFBRSxFQUFFLENBQUMsU0FBUztTQUNyQixDQUFDO2dEQUMwQjtJQUk1QjtRQUhDLFFBQVEsQ0FBQztZQUNOLElBQUksRUFBRSxFQUFFLENBQUMsU0FBUztTQUNyQixDQUFDO2dEQUMwQjtJQUk1QjtRQUhDLFFBQVEsQ0FBQztZQUNOLElBQUksRUFBRSxFQUFFLENBQUMsU0FBUztTQUNyQixDQUFDOzhDQUN3QjtJQUkxQjtRQUhDLFFBQVEsQ0FBQztZQUNOLElBQUksRUFBRSxFQUFFLENBQUMsU0FBUztTQUNyQixDQUFDO2dEQUMwQjtJQUk1QjtRQUhDLFFBQVEsQ0FBQztZQUNOLElBQUksRUFBRSxFQUFFLENBQUMsU0FBUztTQUNyQixDQUFDO2tEQUM0QjtJQXBDYixZQUFZO1FBRGhDLE9BQU87T0FDYSxZQUFZLENBZ0RoQztJQUFELG1CQUFDO0NBaERELEFBZ0RDLENBaER5QyxFQUFFLENBQUMsU0FBUyxHQWdEckQ7a0JBaERvQixZQUFZIiwiZmlsZSI6IiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IEdsb2JhbCBmcm9tIFwiLi9HbG9iYWxcIjtcclxuXHJcbmNvbnN0IHsgY2NjbGFzcywgcHJvcGVydHkgfSA9IGNjLl9kZWNvcmF0b3I7XHJcblxyXG5AY2NjbGFzc1xyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBTb3VuZE1hbmFnZXIgZXh0ZW5kcyBjYy5Db21wb25lbnQge1xyXG4gICAgQHByb3BlcnR5KHtcclxuICAgICAgICB0eXBlOiBjYy5BdWRpb0NsaXBcclxuICAgIH0pXHJcbiAgICBCZzogY2MuQXVkaW9DbGlwID0gbnVsbDtcclxuICAgIEBwcm9wZXJ0eSh7XHJcbiAgICAgICAgdHlwZTogY2MuQXVkaW9DbGlwXHJcbiAgICB9KVxyXG4gICAgZm9vdFN0ZXA6IGNjLkF1ZGlvQ2xpcCA9IG51bGw7XHJcbiAgICBAcHJvcGVydHkoe1xyXG4gICAgICAgIHR5cGU6IGNjLkF1ZGlvQ2xpcFxyXG4gICAgfSlcclxuICAgIEludHJvOiBjYy5BdWRpb0NsaXAgPSBudWxsO1xyXG4gICAgQHByb3BlcnR5KHtcclxuICAgICAgICB0eXBlOiBjYy5BdWRpb0NsaXBcclxuICAgIH0pXHJcbiAgICBBdHRhY2s6IGNjLkF1ZGlvQ2xpcCA9IG51bGw7XHJcbiAgICBAcHJvcGVydHkoe1xyXG4gICAgICAgIHR5cGU6IGNjLkF1ZGlvQ2xpcFxyXG4gICAgfSlcclxuICAgIGthdGFuYTogY2MuQXVkaW9DbGlwID0gbnVsbDtcclxuICAgIEBwcm9wZXJ0eSh7XHJcbiAgICAgICAgdHlwZTogY2MuQXVkaW9DbGlwXHJcbiAgICB9KVxyXG4gICAgU2NyZWFtOiBjYy5BdWRpb0NsaXAgPSBudWxsO1xyXG4gICAgQHByb3BlcnR5KHtcclxuICAgICAgICB0eXBlOiBjYy5BdWRpb0NsaXBcclxuICAgIH0pXHJcbiAgICBTcGluOiBjYy5BdWRpb0NsaXAgPSBudWxsO1xyXG4gICAgQHByb3BlcnR5KHtcclxuICAgICAgICB0eXBlOiBjYy5BdWRpb0NsaXBcclxuICAgIH0pXHJcbiAgICBSZXdhcmQ6IGNjLkF1ZGlvQ2xpcCA9IG51bGw7XHJcbiAgICBAcHJvcGVydHkoe1xyXG4gICAgICAgIHR5cGU6IGNjLkF1ZGlvQ2xpcFxyXG4gICAgfSlcclxuICAgIGNsaWNrQnRuOiBjYy5BdWRpb0NsaXAgPSBudWxsO1xyXG4gICAgb25Mb2FkKCkge1xyXG4gICAgICAgIEdsb2JhbC5zb3VuZEJHID0gdGhpcy5CZztcclxuICAgICAgICBHbG9iYWwuc291bmRJbnRybyA9IHRoaXMuSW50cm87XHJcbiAgICAgICAgR2xvYmFsLnNvdW5kRm9vdFN0ZXAgPSB0aGlzLmZvb3RTdGVwO1xyXG4gICAgICAgIEdsb2JhbC5zb3VuZEF0dGFjayA9IHRoaXMuQXR0YWNrO1xyXG4gICAgICAgIEdsb2JhbC5zb3VuZEthdGFuYSA9IHRoaXMua2F0YW5hO1xyXG4gICAgICAgIEdsb2JhbC5zb3VuZFNjcmVhbSA9IHRoaXMuU2NyZWFtO1xyXG4gICAgICAgIEdsb2JhbC5zb3VuZFNwaW4gPSB0aGlzLlNwaW47XHJcbiAgICAgICAgR2xvYmFsLnNvdW5kUmV3YXJkID0gdGhpcy5SZXdhcmQ7XHJcbiAgICAgICAgR2xvYmFsLnNvdW5kQ2xpY2tCdG4gPSB0aGlzLmNsaWNrQnRuO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/Scripts/Common/AdManager.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'e64a5NCKE1JMag/DvwqBr33', 'AdManager');
// Scripts/Common/AdManager.js

"use strict";

cc.Class({
  "extends": cc.Component,
  properties: {
    androidLink: {
      "default": ''
    },
    iosLink: {
      "default": ''
    },
    defaultLink: {
      "default": ''
    }
  },
  openAdUrl: function openAdUrl() {
    var clickTag = '';
    window.androidLink = this.androidLink;
    window.iosLink = this.iosLink;
    window.defaultLink = this.defaultLink;

    if (window.openAdUrl) {
      window.openAdUrl();
    } else {
      window.open();
    }
  }
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcU2NyaXB0c1xcQ29tbW9uXFxBZE1hbmFnZXIuanMiXSwibmFtZXMiOlsiY2MiLCJDbGFzcyIsIkNvbXBvbmVudCIsInByb3BlcnRpZXMiLCJhbmRyb2lkTGluayIsImlvc0xpbmsiLCJkZWZhdWx0TGluayIsIm9wZW5BZFVybCIsImNsaWNrVGFnIiwid2luZG93Iiwib3BlbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQUEsRUFBRSxDQUFDQyxLQUFILENBQVM7QUFDTCxhQUFTRCxFQUFFLENBQUNFLFNBRFA7QUFHTEMsRUFBQUEsVUFBVSxFQUFFO0FBQ1JDLElBQUFBLFdBQVcsRUFBRTtBQUNULGlCQUFTO0FBREEsS0FETDtBQUlSQyxJQUFBQSxPQUFPLEVBQUU7QUFDTCxpQkFBUztBQURKLEtBSkQ7QUFPUkMsSUFBQUEsV0FBVyxFQUFFO0FBQ1QsaUJBQVM7QUFEQTtBQVBMLEdBSFA7QUFlTEMsRUFBQUEsU0FBUyxFQUFFLHFCQUFZO0FBQ25CLFFBQUlDLFFBQVEsR0FBRyxFQUFmO0FBQ0FDLElBQUFBLE1BQU0sQ0FBQ0wsV0FBUCxHQUFxQixLQUFLQSxXQUExQjtBQUNBSyxJQUFBQSxNQUFNLENBQUNKLE9BQVAsR0FBaUIsS0FBS0EsT0FBdEI7QUFDQUksSUFBQUEsTUFBTSxDQUFDSCxXQUFQLEdBQXFCLEtBQUtBLFdBQTFCOztBQUNBLFFBQUlHLE1BQU0sQ0FBQ0YsU0FBWCxFQUFzQjtBQUNsQkUsTUFBQUEsTUFBTSxDQUFDRixTQUFQO0FBQ0gsS0FGRCxNQUVPO0FBQ0hFLE1BQUFBLE1BQU0sQ0FBQ0MsSUFBUDtBQUNIO0FBQ0o7QUF6QkksQ0FBVCIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiY2MuQ2xhc3Moe1xyXG4gICAgZXh0ZW5kczogY2MuQ29tcG9uZW50LFxyXG5cclxuICAgIHByb3BlcnRpZXM6IHtcclxuICAgICAgICBhbmRyb2lkTGluazoge1xyXG4gICAgICAgICAgICBkZWZhdWx0OiAnJ1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgaW9zTGluazoge1xyXG4gICAgICAgICAgICBkZWZhdWx0OiAnJ1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZGVmYXVsdExpbms6IHtcclxuICAgICAgICAgICAgZGVmYXVsdDogJydcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIG9wZW5BZFVybDogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHZhciBjbGlja1RhZyA9ICcnO1xyXG4gICAgICAgIHdpbmRvdy5hbmRyb2lkTGluayA9IHRoaXMuYW5kcm9pZExpbms7XHJcbiAgICAgICAgd2luZG93Lmlvc0xpbmsgPSB0aGlzLmlvc0xpbms7XHJcbiAgICAgICAgd2luZG93LmRlZmF1bHRMaW5rID0gdGhpcy5kZWZhdWx0TGluaztcclxuICAgICAgICBpZiAod2luZG93Lm9wZW5BZFVybCkge1xyXG4gICAgICAgICAgICB3aW5kb3cub3BlbkFkVXJsKCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgd2luZG93Lm9wZW4oKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0pOyJdfQ==
//------QC-SOURCE-SPLIT------
