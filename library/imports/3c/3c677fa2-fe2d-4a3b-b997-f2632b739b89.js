"use strict";
cc._RF.push(module, '3c677+i/i1KO7mX8mMrc5uJ', 'Global');
// Scripts/Common/Global.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Global = {
    touchPos: null,
    boolEnableTouch: false,
    boolFirstTouchJoyStick: false,
    boolStartPlay: false,
    boolStartAttacking: false,
    boolCheckAttacking: false,
    boolCheckAttacked: false,
    boolendG: false,
    soundBG: null,
    soundIntro: null,
    soundAttack: null,
    soundFootStep: null,
    soundKatana: null,
    soundScream: null,
    soundSpin: null,
    soundReward: null,
    soundClickBtn: null
};
exports.default = Global;

cc._RF.pop();