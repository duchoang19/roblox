"use strict";
cc._RF.push(module, '9a3c4BNdsNGOKf91Tc0+3pS', 'CameraFollow');
// Scripts/Camera/CameraFollow.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var CharacterController_1 = require("../Character/CharacterController");
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var CameraFollow = /** @class */ (function (_super) {
    __extends(CameraFollow, _super);
    function CameraFollow() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.gameplayInstance = null;
        _this.cameraOffsetX = 0;
        _this.cameraOffsetY = 0;
        _this.cameraOffsetZ = 0;
        _this.Target = null;
        //9,12
        _this.plusY = 0;
        _this.plusZ = 0;
        return _this;
    }
    CameraFollow.prototype.start = function () {
        this.gameplayInstance = GamePlayInstance_1.default.Instance(GamePlayInstance_1.default);
        this.cameraOffsetX = this.node.x - 0;
        this.cameraOffsetY = this.node.y + 45;
        this.cameraOffsetZ = this.node.z + 5;
    };
    CameraFollow.prototype.update = function () {
        if (Global_1.default.boolStartPlay && !Global_1.default.boolendG) {
            //let newPosX = this.gameplayInstance.gameplay.MyCharacter.x + this.cameraOffsetX;
            //let newPosZ = this.gameplayInstance.gameplay.MyCharacter.z + this.cameraOffsetZ;
            //if (!Global.boolendG) {
            if (this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController_1.default).level == 0) {
                this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.MyCharacter.x + this.cameraOffsetX, 0.2);
                this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.MyCharacter.y + this.cameraOffsetY, 0.2);
                this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.MyCharacter.z + this.cameraOffsetZ, 0.2);
            }
            // else if (this.gameplayInstance.gameplay.myCharacter.getComponent(CharacterController).level == 1) {
            //     this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.myCharacter.x + this.cameraOffsetX, 0.2);
            //     this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.myCharacter.y + this.cameraOffsetY - this.plusY, 0.2);
            //     this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.myCharacter.z + this.cameraOffsetZ + this.plusZ, 0.2);
            // }
            // else if (this.gameplayInstance.gameplay.myCharacter.getComponent(CharacterController).level == 2) {
            //     this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.myCharacter.x + this.cameraOffsetX, 0.2);
            //     this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.myCharacter.y + this.cameraOffsetY - this.plusY, 0.2);
            //     this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.myCharacter.z + this.cameraOffsetZ + this.plusZ, 0.2);
            // }
            // else if (this.gameplayInstance.gameplay.myCharacter.getComponent(CharacterController).level == 3) {
            //     this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.myCharacter.x + this.cameraOffsetX, 0.2);
            //     this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.myCharacter.y + this.cameraOffsetY - this.plusY, 0.2);
            //     this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.myCharacter.z + this.cameraOffsetZ + this.plusZ, 0.2);
            // }
            // else if (this.gameplayInstance.gameplay.myCharacter.getComponent(CharacterController).level == 4) {
            //     this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.myCharacter.x + this.cameraOffsetX, 0.2);
            //     this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.myCharacter.y + this.cameraOffsetY - this.plusY, 0.2);
            //     this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.myCharacter.z + this.cameraOffsetZ + this.plusZ, 0.2);
            // }
            //}
            else if (this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController_1.default).level == 1) {
                //this.resetOffset(0, 12, 9);
                this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.MyCharacter.x + this.cameraOffsetX, 0.2);
                this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.MyCharacter.y + this.cameraOffsetY - 12, 0.2);
                this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.MyCharacter.z + this.cameraOffsetZ + 9, 0.2);
            }
            else if (this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController_1.default).level == 2) {
                this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.MyCharacter.x + this.cameraOffsetX, 0.2);
                this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.MyCharacter.y + this.cameraOffsetY - 24, 0.2);
                this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.MyCharacter.z + this.cameraOffsetZ + 18, 0.2);
            }
            else if (this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController_1.default).level == 3) {
                this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.MyCharacter.x + this.cameraOffsetX, 0.2);
                this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.MyCharacter.y + this.cameraOffsetY - 36, 0.2);
                this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.MyCharacter.z + this.cameraOffsetZ + 27, 0.2);
            }
            else if (this.gameplayInstance.gameplay.MyCharacter.getComponent(CharacterController_1.default).level == 4) {
                this.node.x = cc.misc.lerp(this.node.x, this.gameplayInstance.gameplay.MyCharacter.x + this.cameraOffsetX, 0.2);
                this.node.y = cc.misc.lerp(this.node.y, this.gameplayInstance.gameplay.MyCharacter.y + this.cameraOffsetY - 48, 0.2);
                this.node.z = cc.misc.lerp(this.node.z, this.gameplayInstance.gameplay.MyCharacter.z + this.cameraOffsetZ + 36, 0.2);
            }
        }
        else if (Global_1.default.boolendG) {
            this.node.x = 0;
            this.node.y = -120;
            this.node.z = 120;
            this.node.eulerAngles = new cc.Vec3(38, 0, 0);
        }
        // else {
        //     for (let i = 0; i < this.gameplayInstance.gameplay.enemyParent.childrenCount; i++) {
        //         this.Target = this.gameplayInstance.gameplay.enemyParent.children[i];
        //     }
        //     this.resetOffset();
        //     this.node.x = cc.misc.lerp(this.node.x, this.Target.x / 2, 0.2);
        //     this.node.y = cc.misc.lerp(this.node.y, this.Target.y + this.cameraOffsetY, 0.2);
        //     this.node.z = cc.misc.lerp(this.node.z, this.Target.z + this.cameraOffsetZ, 0.2);
        //}
        //}
    };
    // resetOffset(x: number, y: number, z: number) {
    //     this.cameraOffsetX = this.cameraOffsetX - x;
    //     this.cameraOffsetY = this.cameraOffsetY - y;
    //     this.cameraOffsetZ = this.cameraOffsetZ + z;
    // }
    CameraFollow.prototype.PlusYZ = function () {
        this.plusY += 12;
        this.plusZ += 9;
    };
    CameraFollow = __decorate([
        ccclass
    ], CameraFollow);
    return CameraFollow;
}(cc.Component));
exports.default = CameraFollow;

cc._RF.pop();