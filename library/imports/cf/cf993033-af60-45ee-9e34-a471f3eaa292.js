"use strict";
cc._RF.push(module, 'cf993Azr2BF7p40pHHz6qKS', 'EnumDefine');
// Scripts/Common/EnumDefine.ts

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ActionType;
(function (ActionType) {
    ActionType[ActionType["IDLE"] = 0] = "IDLE";
    ActionType[ActionType["RUN"] = 1] = "RUN";
    ActionType[ActionType["ATTACK"] = 2] = "ATTACK";
    ActionType[ActionType["VICTORY"] = 3] = "VICTORY";
    ActionType[ActionType["SCARE"] = 4] = "SCARE";
    ActionType[ActionType["NORMAL"] = 5] = "NORMAL";
    ActionType[ActionType["FAST"] = 6] = "FAST";
})(ActionType = exports.ActionType || (exports.ActionType = {}));

cc._RF.pop();