"use strict";
cc._RF.push(module, '2b4c7MyE2JG67FTDw4URo8E', 'SquareCollider');
// Scripts/ColliderObj/SquareCollider.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var SquareCollider = /** @class */ (function (_super) {
    __extends(SquareCollider, _super);
    function SquareCollider() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.high = 0;
        _this.positionLeft = null;
        _this.positionRight = null;
        _this.positionTop = null;
        _this.positionBottom = null;
        return _this;
    }
    SquareCollider.prototype.start = function () {
        this.positionLeft = new cc.Vec2(this.node.x - this.high, this.node.y);
        this.positionRight = new cc.Vec2(this.node.x + this.high, this.node.y);
        this.positionTop = new cc.Vec2(this.node.x, this.node.y + this.high);
        this.positionBottom = new cc.Vec2(this.node.x, this.node.y - this.high);
    };
    SquareCollider.prototype.OnTheLeft = function (player) {
        if (player.x < this.positionLeft.x && player.y < this.positionTop.y && player.y > this.positionBottom.y)
            return true;
        else
            return false;
    };
    SquareCollider.prototype.OnTheRight = function (player) {
        if (player.x > this.positionRight.x && player.y < this.positionTop.y && player.y > this.positionBottom.y)
            return true;
        else
            return false;
    };
    SquareCollider.prototype.OnTheTop = function (player) {
        if (player.y > this.positionTop.y && player.x < this.positionRight.x && player.x > this.positionLeft.x)
            return true;
        else
            return false;
    };
    SquareCollider.prototype.OnTheBottom = function (player) {
        if (player.y < this.positionBottom.y && player.x < this.positionRight.x && player.x > this.positionLeft.x)
            return true;
        else
            return false;
    };
    __decorate([
        property(cc.Float)
    ], SquareCollider.prototype, "high", void 0);
    SquareCollider = __decorate([
        ccclass
    ], SquareCollider);
    return SquareCollider;
}(cc.Component));
exports.default = SquareCollider;

cc._RF.pop();