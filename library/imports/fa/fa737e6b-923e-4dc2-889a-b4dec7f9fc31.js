"use strict";
cc._RF.push(module, 'fa7375rkj5NwoiatN7H+fwx', 'StickMan4');
// Scripts/GamePlay/StickMan4.ts

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var GamePlayInstance_1 = require("../Common/GamePlayInstance");
var Global_1 = require("../Common/Global");
var KeyEvent_1 = require("../Common/KeyEvent");
var Singleton_1 = require("../Common/Singleton");
var EnemyController_1 = require("../Enemy/EnemyController");
var StickMan1_1 = require("./StickMan1");
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var StickMan4 = /** @class */ (function (_super) {
    __extends(StickMan4, _super);
    function StickMan4() {
        var _this = _super.call(this) || this;
        _this.joyStickFollow = null;
        _this.Map = null;
        _this.countEnemyEnd = 0;
        _this.MyCharacter = null;
        _this.Guide = null;
        _this.Effect = null;
        _this.enemyParent = null;
        _this.btnDownload = null;
        _this.endCard = null;
        _this.txtSmasher = null;
        _this.countEnemy = 0;
        _this.boolCheckEnd = false;
        _this.boolcheckInteraction = false;
        _this.ironsource = false;
        _this.mindworks = false;
        _this.vungle = false;
        StickMan1_1.default._instance = _this;
        return _this;
    }
    StickMan4.prototype.onEnable = function () {
        GamePlayInstance_1.instance.on(KeyEvent_1.default.scale, this.scale, this);
        GamePlayInstance_1.instance.on(KeyEvent_1.default.plusEnemy, this.plusEnemy, this);
        GamePlayInstance_1.instance.on(KeyEvent_1.default.activeGuide, this.activeGuide, this);
    };
    StickMan4.prototype.onDisable = function () {
        GamePlayInstance_1.instance.off(KeyEvent_1.default.scale, this.scale, this);
        GamePlayInstance_1.instance.off(KeyEvent_1.default.plusEnemy, this.plusEnemy, this);
        GamePlayInstance_1.instance.off(KeyEvent_1.default.activeGuide, this.activeGuide, this);
    };
    StickMan4.prototype.start = function () {
        cc.audioEngine.play(Global_1.default.soundBG, true, 1);
        this.playGame();
    };
    StickMan4.prototype.update = function (dt) {
        var _this = this;
        if (Global_1.default.boolEnableTouch && !this.boolcheckInteraction) {
            this.txtSmasher.active = false;
            if (this.ironsource) {
                window.NUC.trigger.interaction();
            }
        }
        if (this.countEnemy == this.countEnemyEnd && !this.boolCheckEnd) {
            Global_1.default.boolStartPlay = false;
            this.boolCheckEnd = true;
            this.scheduleOnce(function () {
                _this.EndGame();
            }, 1);
        }
    };
    StickMan4.prototype.playGame = function () {
        this.MyCharacter.active = true;
        Global_1.default.boolStartPlay = true;
        for (var i = 0; i < this.enemyParent.childrenCount; i++) {
            this.enemyParent.children[i].getComponent(EnemyController_1.default).StartMove();
        }
    };
    StickMan4.prototype.EndGame = function () {
        Global_1.default.boolendG = true;
        cc.audioEngine.stopAllEffects();
        this.joyStickFollow.active = false;
        this.Guide.active = false;
        this.endCard.active = true;
        if (this.mindworks) {
            window.gameEnd && window.gameEnd();
        }
        if (this.ironsource) {
            window.NUC.trigger.endGame('win');
        }
        if (this.vungle) {
            parent.postMessage('complete', '*');
        }
    };
    StickMan4.prototype.scale = function () {
        var _this = this;
        this.Effect.scale = 0;
        this.Effect.opacity = 255;
        this.Effect.runAction(cc.sequence(cc.scaleTo(0.2, 1).easing(cc.easeBounceOut()), cc.callFunc(function () {
            _this.scheduleOnce(function () {
                _this.Effect.runAction(cc.fadeOut(0.1));
            }, 0.2);
        })));
    };
    StickMan4.prototype.plusEnemy = function () {
        if (this.countEnemy < this.countEnemyEnd)
            this.countEnemy++;
    };
    StickMan4.prototype.activeGuide = function () {
        if (Global_1.default.boolStartPlay)
            this.Guide.active = true;
    };
    __decorate([
        property(cc.Node)
    ], StickMan4.prototype, "joyStickFollow", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan4.prototype, "Map", void 0);
    __decorate([
        property(cc.Integer)
    ], StickMan4.prototype, "countEnemyEnd", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan4.prototype, "MyCharacter", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan4.prototype, "Guide", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan4.prototype, "Effect", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan4.prototype, "enemyParent", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan4.prototype, "btnDownload", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan4.prototype, "endCard", void 0);
    __decorate([
        property(cc.Node)
    ], StickMan4.prototype, "txtSmasher", void 0);
    StickMan4 = __decorate([
        ccclass
    ], StickMan4);
    return StickMan4;
}(Singleton_1.default));
exports.default = StickMan4;

cc._RF.pop();